#include "HelloWorldScene.h"
#ifdef XMC_TEST
  #include "Xmc.h"
  USING_NS_XMC;
  static XmcSymbol::Ptr stage;
  static XmcLoader::Ptr loader;
#else
//#ifdef XMC_BMC_COMPATIBLE
//  //#include "BmcCompatible.h"
//  //#include "../libs/libxmc/include/cocone/bmc.h"
//  #include <cocone/bmc.h>
//#else
  #include <cocone/bmc.h>
//#endif
  #include "cocone/bmc/Shaders.h"
  using namespace cocone;
  using namespace bmc;
  static cocone::BmcSymbol::Ptr stage;
  static cocone::BmcLoader::Ptr loader;
#endif

#include "shaders/Shaders.h"
#include "ui/UIScale9Sprite.h"

USING_NS_CC;

static const std::string& _test_bmc_path =
// primitives
//"bmc/primitives/_bitmap1.bmc";                    // 메모리 해제 테스트
//"bmc/primitives/_mc_bitmap1.bmc";
//"bmc/primitives/_mc_move_bitmap1.bmc";
//"bmc/primitives/_mc_rect_move_dup2.bmc";          // 동일 인스턴스명 테스트(5 프레임까지 겹침)(XmcShapeImpl::clone() 호출됨)
//"bmc/primitives/mc2.bmc";                         // 2개 이미지가 동시에 우로 이동해야 함(XmcShapeImpl::clone() 호출됨)
//"bmc/primitives/_mc_move_bitmap2.bmc";            // 다른 비트맵 2개 이동
//"bmc/primitives/shape_rect1.bmc";
//"bmc/primitives/_mc_move_bitmap2_parent.bmc";     // ng
//"bmc/primitives/_mc_move_bitmap2_test.bmc";
//
// translation
//"bmc/translations/mc_jump_right.bmc";
//"bmc/translations/mc_move_right.bmc";
//"bmc/translations/mc_move_right_bottom.bmc";
//"bmc/translations/mc_move_right_clear_frames.bmc";
//"bmc/translations/mc_move_right_bottom_rot.bmc";
//"bmc/translations/mc_move_right_bottom2.bmc";     // clone 테스트
//"bmc/translations/mc_move_right_bottom_test.bmc";
//
// color effects
//"bmc/color_effects/mc_alpha.bmc";
//"bmc/color_effects/mc_rect_alpha.bmc";
//"bmc/color_effects/mc_brightness.bmc";
//"bmc/color_effects/mc_brightness2.bmc";
//"bmc/color_effects/mc_tint.bmc";
//"bmc/color_effects/mc_advanced.bmc";
//"bmc/color_effects/color_tint_bitmap.bmc";
//"bmc/color_effects/color_move_rb_2_tint.bmc";
//
// buttons
//"bmc/buttons/button.bmc";
//"bmc/buttons/button_basic.bmc";
//"bmc/buttons/button_basic2.bmc";
//
// samples
//"bmc/samples/dUI_inventory.bmc";
//"bmc/samples/dUI_PremiumCampaign.bmc";
"bmc/samples/book_eff.bmc";

//static
template<class T, class T_EventType> void test_btn_callback(const std::string& btn_name) {
  auto btn = stage->getInstanceImpl(btn_name);
  if (btn != nullptr) {
    auto mc = btn->query<T>();
    mc->setEventListener([mc](T_EventType eventType, T* clip) {
      xmc_dlog("eventListener eventType=%d, clip=%p", eventType, clip);
      mc->stopAll();
    });
    mc->setLabelCallback([](const std::string& label, T* clip) {
      xmc_dlog("labelCallback label=%s, clip=%p", label.c_str(), clip);
    });
  }
}

template<class T> void test_PremiumCampaign() {
  auto popup_step = stage->getInstance("pg_1136", "popup_step");
  if (popup_step) {
    popup_step->asNode()->setVisible(false);
  }
  // 시작시 애니메이션 중지
  auto popup_main = stage->getInstance("pg_1136", "popup_main");
  auto mc = popup_main->query<T>();
  //mc->setLoop(false);
  mc->gotoAndStop(0);
  
  // 선물박스 애니메이션 중지
  popup_main->iterateInstancesForName([](const std::string& name, XmcSymbol* symbol) {
    if (name.compare(0, 4, "box_") == 0) {
      auto mc = symbol->query<T>();
      mc->gotoAndStop(0);
    }
  });
}

Scene* HelloWorld::createScene()
{
  // 'scene' is an autorelease object
  auto scene = Scene::create();
  
  // 'layer' is an autorelease object
  auto layer = HelloWorld::create();
  
  // add layer as a child to scene
  scene->addChild(layer);
  
  // return the scene
  return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
  ////////////////////`//////////
  // 1. super init first
  if ( !Layer::init() )
  {
    return false;
  }
  
  Size visibleSize = Director::getInstance()->getVisibleSize();
  Vec2 origin = Director::getInstance()->getVisibleOrigin();
  
  /////////////////////////////
  // 2. add a menu item with "X" image, which is clicked to quit the program
  //    you may modify it.
  
  // add a "close" icon to exit the progress. it's an autorelease object
  auto closeItem = MenuItemImage::create(
                                         "CloseNormal.png",
                                         "CloseSelected.png",
                                         CC_CALLBACK_1(HelloWorld::menuCloseCallback, this));
  
  closeItem->setPosition(Vec2(origin.x + visibleSize.width - closeItem->getContentSize().width/2 ,
                              origin.y + closeItem->getContentSize().height/2));
  
  // create menu, it's an autorelease object
  auto menu = Menu::create(closeItem, NULL);
  menu->setPosition(Vec2::ZERO);
  this->addChild(menu, 1);
  
  /////////////////////////////
  // 3. add your codes below...
  
#if 0
  // add a label shows "Hello World"
  // create and initialize a label
  
  auto label = Label::createWithTTF("Hello World", "fonts/Marker Felt.ttf", 24);
  
  // position the label on the center of the screen
  label->setPosition(Vec2(origin.x + visibleSize.width/2,
                          origin.y + visibleSize.height - label->getContentSize().height));
  
  // add the label as a child to this layer
  this->addChild(label, 1);
  
  // add "HelloWorld" splash screen"
  auto sprite = Sprite::create("HelloWorld.png");
  
  // position the sprite on the center of the screen
  sprite->setPosition(Vec2(visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y));
  
  // add the sprite as a child to this layer
  this->addChild(sprite, 0);
#elif 0
  const std::string filename = "CloseSelected.png"; // HelloWorld.png -> 195x270, CloseSelected.png -> 40x40
//  {
//    auto s = Sprite::create(filename);
//    s->setScale(10.f);
//    Size sz = s->getContentSize();
//    Rect rc = s->getTextureRect();
//    s->setPosition(Vec2(visibleSize.width/2, visibleSize.height - sz.height/2 * s->getScale())); // top middle
//    this->addChild(s, 0);
//  }
  {
    auto s = Sprite::create(filename);
    Size sz = s->getContentSize();
    sz.height *= 2;
    s->setContentSize(sz);
    sz = s->getContentSize();
    Rect rc = s->getTextureRect();
    auto pos = Vec2(visibleSize.width/2, visibleSize.height - sz.height/2 * s->getScale());
    s->setPosition(pos); // bottom middle
    this->addChild(s, 0);
  }
  {
    Rect capInsets(19, 19, 2, 2); // center 2x2 box 하늘색 부분이 늘어남
    auto s = ui::Scale9Sprite::create(capInsets, filename);
    Size sz = s->getContentSize();
    //sz.width *= 10;
    sz.height *= 10;
    s->setContentSize(sz);
    sz = s->getContentSize();
    Rect rc = s->getTextureRect();
    auto pos = Vec2(visibleSize.width/2, sz.height/2 * s->getScale());
    s->setPosition(pos); // bottom middle
    this->addChild(s, 0);
  }
  
  if (true) return true;
#endif
  
  NS_XMC::addShaders(GLProgramCache::getInstance());

#ifdef XMC_TEST
  if (true) {
    //loader = cocone::BmcFactory::createBmcLoader();
    loader = XmcFactory::createXmcLoader();
    loader->init(_test_bmc_path);
    stage = loader->getStage();
    stage->clearAdditionalTransform();
    addChild(stage->asNode());
    stage->asNode()->setPosition(0, visibleSize.height / CC_CONTENT_SCALE_FACTOR());

    auto mc = stage->query<XmcMovieClip>(); // dynamic_cast<XmcMovieClip*>(stage.get());
    //mc->setSpeedScale(mc->getOneFrameDelta()); //skc add XmcMovieClip::getOneFrameDelta() test force 1 fps
    //mc->setSpeedScale(1.f / 30); //skc test force 1 fps
    //mc->setLoop(false);
    //test_btn_callback<XmcMovieClip, XmcMovieClip::EventType>("btn_yes"); // 버튼 테스트일 경우에는 꺼줘야 함
    //test_PremiumCampaign<XmcMovieClip>();
    mc->gotoAndPlay(0);
  }
#else // XMC_TEST
  //cocone::addShaders(GLProgramCache::getInstance());
  if (true) {
    loader = cocone::BmcFactory::createBmcLoader();
    loader->init(_test_bmc_path);
    stage = loader->getStage();
    stage->clearAdditionalTransform();
    addChild(stage->asNode());
    stage->asNode()->setPosition(0, visibleSize.height / CC_CONTENT_SCALE_FACTOR());

    auto mc = stage->query<cocone::BmcMovieClip>();
    //mc->setSpeedScale(1.f / 30); //skc test force 1 fps
    //mc->setLoop(false);
    //test_btn_callback<BmcMovieClip, BmcMovieClip::EventType>("btn_yes"); // 버튼 테스트일 경우에는 꺼줘야 함
    //test_PremiumCampaign<BmcMovieClip>();
    mc->gotoAndPlay(0);
  }
#endif // XMC_TEST
  return true;
}

void HelloWorld::menuCloseCallback(Ref* pSender)
{
  Director::getInstance()->end();
  
#ifdef XMC_TEST
  removeChild(stage->asNode());
  auto mc = stage->query<XmcMovieClip>();
  mc->stop(); // stage == mc = 4 XmcClockGenerator 에서 참조
  XmcShader::Ptr shader = loader->getShader();
  shader.get()->release(); // 3 -> 2
  shader = nullptr; // 2 -> 1
  loader = nullptr; // 1 -> 0, stage: 2 -> 1
  stage = nullptr; // 1 -> 0
  XmcRef::printLeaks();
#else
  removeChild(stage->asNode());
  auto mc = stage->query<cocone::BmcMovieClip>();
  mc->stop(); // stage == mc = 4 XmcClockGenerator 에서 참조
  BmcShader::Ptr shader = loader->getShader();
  //shader->asRef()->release(); // 3 -> 2, 이렇게 2까지만 줄여야 아래 exit(0)에서 소멸자 호출중
  // BmcShapeImpl 등에서 헤제중임
  //shader = nullptr; // 2 -> 1, 이렇게 1까지 줄이면 소멸되어 Ref::printLeaks()에서 검출되지 않지만, 아래 exit(0) -> __cxa_finalize_ranges() crash 발생중.
  loader = nullptr; // 1 -> 0, 이렇게 해주면 Ref::printLeaks()에서 검출되지 않는다.
  stage = nullptr;
#if CC_REF_LEAK_DETECTION
  Ref::printLeaks(); // todo: cocos2d 객체들 다수에서 leak 발생중
#endif
#endif
  
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
  exit(0);
#endif
}
