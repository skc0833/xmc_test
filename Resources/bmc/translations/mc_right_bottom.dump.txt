[SWF]
  Header:
    Version: 37
    Compression: None
    FileLength: 13580
    FileLengthCompressed: 13580
    FrameSize: (550,400)
    FrameRate: 1
    FrameCount: 4
  Tags:
    [69:FileAttributes] AS3: true, HasMetadata: false, UseDirectBlit: false, UseGPU: false, UseNetwork: false
    [09:SetBackgroundColor] Color: #424242
    [86:DefineSceneAndFrameLabelData] 
      Scenes:
        [0] Frame: 0, Name: Scene 1
    [35:DefineBitsJPEG3] ID: 1, Type: JPEG, HasAlphaData: true, BitmapAlphaLength: 690, BitmapLength: 12615
    [02:DefineShape] ID: 2, Bounds: (-1000,1000,-1384,1385)
      FillStyles:
        [1] [SWFFillStyle] Type: 41 (clipped bitmap), BitmapID: 65535
        [2] [SWFFillStyle] Type: 41 (clipped bitmap), BitmapID: 1
      ShapeRecords:
        [0] [SWFShapeRecordStyleChange] MoveTo: -1000,-1384, FillStyle1: 2
        [1] [SWFShapeRecordStraightEdge] Horizontal: 2000
        [2] [SWFShapeRecordStraightEdge] Vertical: 2769
        [3] [SWFShapeRecordStraightEdge] Horizontal: -2000
        [4] [SWFShapeRecordStraightEdge] Vertical: -2769
        [5] [SWFShapeRecordEnd]
    [39:DefineSprite] ID: 3, FrameCount: 1
      Tags:
        [26:PlaceObject2] Depth: 1, CharacterID: 2, Matrix: (1,1,0,0,0,0)
        [01:ShowFrame] 
        [00:End] 
      Frames:
        [0] Start: 0, Length: 2
          Depth: 1, CharacterId: 2, PlacedAt: 0, IsKeyframe
    [39:DefineSprite] ID: 4, FrameCount: 4
      Tags:
        [26:PlaceObject2] Depth: 1, CharacterID: 3, Matrix: (1,1,0,0,0,0), Name: Symbol 2
        [01:ShowFrame] 
        [26:PlaceObject2] Depth: 1, Matrix: (1,1,0,0,0,1267)
        [01:ShowFrame] 
        [26:PlaceObject2] Depth: 1, Matrix: (1,1,0,0,0,2533)
        [01:ShowFrame] 
        [26:PlaceObject2] Depth: 1, Matrix: (1,1,0,0,0,3800)
        [01:ShowFrame] 
        [00:End] 
      Frames:
        [0] Start: 0, Length: 2
          Depth: 1, CharacterId: 3, PlacedAt: 0, IsKeyframe
        [1] Start: 2, Length: 2
          Depth: 1, CharacterId: 3, PlacedAt: 0, LastModifiedAt: 2
        [2] Start: 4, Length: 2
          Depth: 1, CharacterId: 3, PlacedAt: 0, LastModifiedAt: 4
        [3] Start: 6, Length: 2
          Depth: 1, CharacterId: 3, PlacedAt: 0, LastModifiedAt: 6
    [26:PlaceObject2] Depth: 1, CharacterID: 4, Matrix: (1,1,0,0,4100,2184), Name: Symbol 1
    [01:ShowFrame] 
    [26:PlaceObject2] Depth: 1, Matrix: (1,1,0,0,5233,2184)
    [01:ShowFrame] 
    [26:PlaceObject2] Depth: 1, Matrix: (1,1,0,0,6367,2184)
    [01:ShowFrame] 
    [26:PlaceObject2] Depth: 1, Matrix: (1,1,0,0,7500,2184)
    [01:ShowFrame] 
    [00:End] 
  Scenes:
    Name: Scene 1, Frame: 0
  Frames:
    [0] Start: 0, Length: 9
      Defined CharacterIDs: 1, 2, 3, 4
      Depth: 1, CharacterId: 4, PlacedAt: 7, IsKeyframe
    [1] Start: 9, Length: 2
      Depth: 1, CharacterId: 4, PlacedAt: 7, LastModifiedAt: 9
    [2] Start: 11, Length: 2
      Depth: 1, CharacterId: 4, PlacedAt: 7, LastModifiedAt: 11
    [3] Start: 13, Length: 2
      Depth: 1, CharacterId: 4, PlacedAt: 7, LastModifiedAt: 13