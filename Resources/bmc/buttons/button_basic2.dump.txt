[SWF]
  Header:
    Version: 37
    Compression: None
    FileLength: 345
    FileLengthCompressed: 345
    FrameSize: (550,400)
    FrameRate: 30
    FrameCount: 1
  Tags:
    [69:FileAttributes] AS3: true, HasMetadata: false, UseDirectBlit: false, UseGPU: false, UseNetwork: false
    [09:SetBackgroundColor] Color: #ffffff
    [86:DefineSceneAndFrameLabelData] 
      Scenes:
        [0] Frame: 0, Name: Scene 1
    [02:DefineShape] ID: 1, Bounds: (-949,950,-500,500)
      FillStyles:
        [1] [SWFFillStyle] Type: 00 (solid), Color: #0000ff
      ShapeRecords:
        [0] [SWFShapeRecordStyleChange] MoveTo: -949,-500, FillStyle1: 1
        [1] [SWFShapeRecordStraightEdge] Horizontal: 1899
        [2] [SWFShapeRecordStraightEdge] Vertical: 1000
        [3] [SWFShapeRecordStraightEdge] Horizontal: -1899
        [4] [SWFShapeRecordStraightEdge] Vertical: -1000
        [5] [SWFShapeRecordEnd]
    [39:DefineSprite] ID: 2, FrameCount: 1
      Tags:
        [26:PlaceObject2] Depth: 1, CharacterID: 1, Matrix: (1,1,0,0,0,0)
        [01:ShowFrame] 
        [00:End] 
      Frames:
        [0] Start: 0, Length: 2
          Depth: 1, CharacterId: 1, PlacedAt: 0, IsKeyframe
    [39:DefineSprite] ID: 3, FrameCount: 9
      Tags:
        [43:FrameLabel] Name: upnormal
        [26:PlaceObject2] Depth: 1, CharacterID: 2, Matrix: (1.05316162109375,1.052978515625,0,0,1000,0), Name: btn1
        [01:ShowFrame] 
        [43:FrameLabel] Name: down
        [26:PlaceObject2] Depth: 1, Matrix: (1.5797576904296875,1.5789642333984375,0,0,1000,-1), ColorTransform: 0,0,0,256,255,255,0,0
        [01:ShowFrame] 
        [01:ShowFrame] 
        [01:ShowFrame] 
        [43:FrameLabel] Name: downnormal
        [26:PlaceObject2] Depth: 1, Matrix: (1.05316162109375,1.052978515625,0,0,1000,0), ColorTransform: 0,0,0,256,255,0,0,0
        [01:ShowFrame] 
        [43:FrameLabel] Name: up
        [26:PlaceObject2] Depth: 1, Matrix: (1.5797576904296875,1.5789642333984375,0,0,1000,-1), ColorTransform: 0,0,0,256,0,255,255,0
        [01:ShowFrame] 
        [01:ShowFrame] 
        [01:ShowFrame] 
        [43:FrameLabel] Name: disable
        [26:PlaceObject2] Depth: 1, Matrix: (1.05316162109375,1.052978515625,0,0,1000,0), ColorTransform: 0,0,0,256,127,127,127,0
        [01:ShowFrame] 
        [00:End] 
      Frames:
        [0] Start: 0, Length: 3, Label: upnormal
          Depth: 1, CharacterId: 2, PlacedAt: 1, IsKeyframe
        [1] Start: 3, Length: 3, Label: down
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 4
        [2] Start: 6, Length: 1
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 4
        [3] Start: 7, Length: 1
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 4
        [4] Start: 8, Length: 3, Label: downnormal
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 9
        [5] Start: 11, Length: 3, Label: up
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 12
        [6] Start: 14, Length: 1
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 12
        [7] Start: 15, Length: 1
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 12
        [8] Start: 16, Length: 3, Label: disable
          Depth: 1, CharacterId: 2, PlacedAt: 1, LastModifiedAt: 17
    [26:PlaceObject2] Depth: 1, CharacterID: 3, Matrix: (1,1,0,0,2000,4000)
    [01:ShowFrame] 
    [00:End] 
  Scenes:
    Name: Scene 1, Frame: 0
  Frames:
    [0] Start: 0, Length: 8
      Defined CharacterIDs: 1, 2, 3
      Depth: 1, CharacterId: 3, PlacedAt: 6, IsKeyframe