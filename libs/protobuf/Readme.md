# protobuf-2.5.0 patch

after download protobuf-2.5.0 from
https://github.com/google/protobuf/releases/download/v2.5.0/protobuf-2.5.0.zip
(https://github.com/google/protobuf/tree/v2.5.0 build fails)

* add build script build_protobuf_dist.sh file for iOS
* -> this rename google namespace name to google_public
* -> built result saved at /tmp/protobuf_xxx/x86_64, ...
* add __aarch64__ feature at src/google/protobuf/stubs/platform_macros.h for build arm64

====================================================================
#elif defined(__ARMEL__)
#define GOOGLE_PROTOBUF_ARCH_ARM 1
#define GOOGLE_PROTOBUF_ARCH_32_BIT 1
#elif defined(__aarch64__)  //skc add for build_for arm64 arm-apple-darwin
#define GOOGLE_PROTOBUF_ARCH_AARCH64 1
#define GOOGLE_PROTOBUF_ARCH_64_BIT 1
#elif defined(__MIPSEL__)

====================================================================
