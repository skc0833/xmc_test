#include "XmcMovieClipAdapter.h"
#include "XmcNullMovieClip.h"
#include "XmcFactory.h"
#include "XmcSymbol.h"
#include "XmcClockGenerator.h"
#include "foundation/Log.h"
#include <vector>
#include <string>
#include <set>
#include "2d/CCLabel.h"

//#define DEBUG_LOG_ENABLED
#ifdef DEBUG_LOG_ENABLED
#define DEBUG_LOG(...) { NS_XMC::logOutput(false, NS_XMC::LogPrefix_Debug,__func__,__FILE__,__LINE__,__VA_ARGS__); }
#else
#define DEBUG_LOG(...) ((void)0)
#endif

USING_NS_CC;

NS_XMC_BEGIN

/*
 -- Color Effect --
 [Alpha]
 // Alpha: 10% ~ 100%
 m = 256, 256, 256, 26 -> 256, 256, 256, 84 -> 141 -> 199 -> x(has_colortrans_index)
 a = 0, 0, 0, 0        -> same
 white red -> red
 
 [Bright]
 // Bright: -50% ~ 50%
 m = 128, 128, 128, 256 -> same
 a = 0, 0, 0, 0         -> 32, 32, 32, 0 -> 64 -> 96 -> 128
 dark red -> white red
 
 // Bright: -100% ~ 100%
 m = 0, 0, 0, 256 -> same
 a = 0, 0, 0, 0   -> 64, 64, 64, 0 -> 64 -> 128 -> 191 -> 256
 black -> white
 
 [Tint]
 // tint: 100%, gb: 0, b: 255 ==> tint: 0%, gb: 0, b: 255
 m = 0, 0, 0, 256   -> 64, 64, 64, 256  -> 128 -> 192 -> x(has_colortrans_index)
 a = 0, 0, 255, 0   -> 0, 0, 191, 0     -> 128 -> 64  -> x(has_colortrans_index)
 blue -> purple -> red
 
 [Advanced]
 // r: 10% + 128, agb: 100% + 0 ==> r: 100% + 255, agb: 100% + 0
 m = 26, 256, 256, 256  -> 84, 256, 256, 256  -> 141 -> 199 -> 256
 a = 128, 0, 0, 0       -> 160, 0, 0, 0       -> 191 -> 223 -> 255
 dark red -> red
 */

static const int INSTANCE_TAG_OFFSET = 10000;
static const std::string EMPTY_STRING = "";

XmcMovieClipAdapter::XmcMovieClipAdapter()
: _blendMode(bmc::BmcBlendMode_Normal)
, _listener(nullptr)
, _node(nullptr)
{
}

XmcMovieClipAdapter::~XmcMovieClipAdapter() {
  xmc_assert(asNode()->getReferenceCount() == 0);
  if (asNode()->getReferenceCount() != 0) {
    //xmc_assert(false);
  }
  // 이미 stop 된 경우, _clockGenerator 가 삭제된 상태라 unregisterListener()를 호출하면 crash 된다.
  if (_clockGenerator) {
    // 현재 객체가 삭제되고, processFrame()이 호출되게 될수 있으므로 여기서 해제???
    _clockGenerator->unregisterListener(this);
  } else {
    xmc_assert(false);
  }
}

XmcSymbol::Ptr XmcMovieClipAdapter::clone() const {
  XmcMovieClipAdapter* newP = static_cast<XmcMovieClipAdapter*>(callNewAndNodeInit());
  newP->asNode()->setContentSize(Size::ZERO);
  newP->_timeline.CopyFrom(_timeline); // should be declared XMC_SHARABLEPTR_DEFINE(XmcMovieClipAdapter) or compile error.
  //newP->_speedScale = _speedScale;
  
  //newP->_oneFrameDelta = _oneFrameDelta;
  newP->_currentFrame = _currentFrame;
  //newP->_stockTime = _stockTime;
  newP->_isLoop = _isLoop;
  newP->_playStartFrame = _playStartFrame;
  newP->_playFrameCount = _playFrameCount;
  newP->_prevConstructFrame = -1;
  newP->setAdditionalTransform(getAdditionalTransform());
  newP->_additionalColorTransform = _additionalColorTransform;
  newP->_blendMode = _blendMode;
  //newP->_soundPlayer = _soundPlayer;
  newP->_clockGenerator = _clockGenerator;
  newP->_renderOnFBO = _renderOnFBO;

  newP->_instances.resize(_timeline.instance_table().size());
  for (int i = 0; i < _timeline.instance_table().size(); ++i) {
    if (_instances[i]) {
      newP->_instances[i] = _instances[i]->clone();
    }
  }
  newP->_indexhash.resize(_indexhash.size());
  for (int i = 0; i < _indexhash.size(); ++i) {
    newP->_indexhash[i] = _indexhash[i];
  }
  newP->setupContentSize();
  newP->clearLabelCallback();
  //newP->_enableLog = _enableLog;
  
#if 1
  XmcSymbol::Ptr ptr = newP;
  //XmcSymbol::Ptr ptr = newP->query<XmcSymbol>(); // same above
  ptr->asRef()->release();
  return ptr; // caller 에서 리턴받게 되는 XmcSymbol::Ptr 객체의 refcnt는 1 이됨
#else
  return newP; // 그냥 newP을 리턴하면, refcnt = 1 지만 XmcSymbol::Ptr 가 생성되면서 2로 증가되므로 caller 에서 release 한번 해줘야 함
#endif
}

void XmcMovieClipAdapter::init(XmcLoader* source, const bmc::BmcTimeline& bmcTimeline) {
  callNodeInit();
  asNode()->setContentSize(Size::ZERO);
  //_oneFrameDelta = 1.f / float(source->getFrameRate()); //skc needs when XmcMovieClipAdapter::nodeFuncUpdate() required and for debugging
  _timeline.CopyFrom(bmcTimeline);
  _currentFrame = 0;
  //_stockTime = 0.f;
  _isLoop = true;
  _prevConstructFrame = -1;
  _playStartFrame = 0;
  _playFrameCount = _timeline.frames().size();
  //_soundPlayer = source->getSoundPlayer();
  _clockGenerator = source->getClockGenerator();
  _renderOnFBO = false;
  
  // Extract symbols to use
  setupSymbols(source);
  
  // make hash table for overrapped named instances
  std::map<std::string, int> nameTable;
  _indexhash.clear();
  for (int i = 0; i < _timeline.instance_table_size(); ++i) {
    const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
    if (/*false && */nameTable.find(instanceInfo.instance_name()) != nameTable.end()) {
      // 만약 이 조건절을 없애고(false &&) 무조건 추가하면, 중복된 인스턴스가 그려지게 된다.
      // 이 경우에는 중복된 인스턴스는 인스턴스명으로 접근이 안될 것으로 예상됨(mc_move_right_bottom2.bmc)
      _indexhash.push_back(nameTable[instanceInfo.instance_name()]);
    } else {
      _indexhash.push_back(i);
      nameTable[instanceInfo.instance_name()] = i;
    }
    xmc_dlog("%d: '%s'", i, instanceInfo.instance_name().c_str());
  }
  setupContentSize();
  clearLabelCallback();
}

void XmcMovieClipAdapter::processFrame() {
  if (asNode()->getParent() == nullptr) {
    //xmc_assert(false); // ok
    return;
  }
  //xmc_assert(_prevConstructFrame != _currentFrame); // it's ok!
  if (_prevConstructFrame != _currentFrame) {
    updateFrameForView();
  }
  {
    const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
    // Sound play
    if (frame.has_play_sound_index() && asNode()->isVisible()) {
      xmc_assert(false);
      //_soundPlayer->playEffect(frame.play_sound_index());
    }
  }
  
  asRef()->retain(); //skc magical adventure 에서 타네 여행보낼때.. 책장 넘기는 연출완료 후 아래 nextFrame(false) 위치에서 현재 객체 delete 상태임
  processActionScriptByLabels();
  if (_currentFrame == _playStartFrame + _playFrameCount - 1) {
    if (!_isLoop) {
      stop();
    }
    if (_listener != nullptr) {
      _listener(EventType_PlayEndFrame, this);
    }
  }
  nextFrame(false);
  asRef()->release();
}

void XmcMovieClipAdapter::processActionScriptByLabels() {
  const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
  if (!frame.has_frame_label_index()) {
    return;
  }
  const std::string& label = _timeline.frame_label_table(frame.frame_label_index());
  if (label.empty()) {
    xmc_assert(false);
    return;
  }
  
  static const std::string& atStop = "@stop";
  static const std::string& atGoto = "@goto_";
  if (label == atStop) {
    xmc_assert(false); //skc todo test
    setPlayFrameRange(_currentFrame, 1);
    setLoop(false);
    return;
  }
  const std::string& str = label.substr(0, atGoto.size());
  if (str == atGoto) {
    xmc_assert(false); //skc todo test
    if (label.find(atGoto) != 0) {
      xmc_assert(false); // @goto_ is in middle of the label
    }
    const std::string& gotoLabel = label.substr(atGoto.size());
    gotoAndPlayToNextLabel(gotoLabel.c_str());
    return;
  }
  
  if (_labelCallback.find(label) != _labelCallback.end()) {
    auto callback = _labelCallback[label];
    callback(this);
  }
  if (_anyLabelCallback) {
    auto callback = _anyLabelCallback;
    callback(label, this);
  }
}

void XmcMovieClipAdapter::gotoAndPlay(int frameNumber) {
  setPlayFrameRange(frameNumber, _timeline.frames().size() - frameNumber);
  moveCurrentFrame(frameNumber);
  play();
}

void XmcMovieClipAdapter::gotoAndPlay(int frameNumber, int count) {
  if (xmc_econd(frameNumber + count > _timeline.frames().size(), "frameNumber=%d, count=%d, total=%d", frameNumber, count, _timeline.frames().size())) {
    count = _timeline.frames().size() - frameNumber;
  }
  if (count <= 0) {
    count = 1;
  }
  setPlayFrameRange(frameNumber, count);
  moveCurrentFrame(frameNumber);
  updateFrameForView();
  stop();
}

void XmcMovieClipAdapter::gotoAndStop(int frameNumber) {
  setPlayFrameRange(frameNumber, 1);
  moveCurrentFrame(frameNumber);
  updateFrameForView();
  stop();
}

// 전달받은 레이블 이후의 다음 번 레이블까지 재생시킨다.
void XmcMovieClipAdapter::gotoAndPlayToNextLabel(const char* label) {
#if 0
  gotoAndPlayLabel(label, nullptr); //skc nooooooooooo!!!
#else
  int frameLabelIndex = searchFrameLabelIndexByLabel(label);
  if (xmc_wcond(frameLabelIndex == -1, "label = %s", label)) {
    return;
  }
  int startFrameNumber = searchFrameNumberByFrameLabelIndex(0, frameLabelIndex); // Start frame with specified label
  if (xmc_wcond(startFrameNumber == -1, "label = %s", label)) {
    return;
  }
  int frameCount = -1;
  int nextFrameLabelIndex = frameLabelIndex + 1;
  if (nextFrameLabelIndex == _timeline.frame_label_table_size()) { // label is the last label
    // If there is no next label, the number of frames up to the last frame
    frameCount = getTotalFrames() - startFrameNumber;
  } else {
    int nextFrameNumber = searchFrameNumberByFrameLabelIndex(startFrameNumber + 1, nextFrameLabelIndex);
    if (xmc_econd(nextFrameNumber == -1, "label = %s, frameLabelIndex = %d, startFrameNumber = %d"
                  , label, frameLabelIndex, startFrameNumber)) {
      return;
    }
    // Number of frames to the next labeled position (from the start frame)
    frameCount = nextFrameNumber - startFrameNumber;
  }
  
  // Specify start position and number of playback and move to specified position
  setPlayFrameRange(startFrameNumber, frameCount);
  moveCurrentFrame(startFrameNumber);
  updateFrameForView();
  //xmc_assert(!_clockGenerator->isRegistered(this)); //skc ok! XmcButtonImpl::init() 에서 호출될 경우 등록되기 전인 상태임
  play(); //skc 아직 등록되지 않은 경우도 존재하나???
#endif
}

void XmcMovieClipAdapter::gotoAndPlayLabel(const char* labelFrom, const char* labelTo) {
  int labelIndexFrom = searchFrameLabelIndexByLabel(labelFrom);
  if (xmc_wcond(labelIndexFrom == -1, "labelFrom = %s", labelFrom)) {
    return;
  }
  int startFrameNumber = searchFrameNumberByFrameLabelIndex(0, labelIndexFrom); // Start frame with specified label
  if (xmc_wcond(startFrameNumber == -1, "labelFrom = %s", labelFrom)) {
    return;
  }
  int frameCount = -1;
  if (labelTo == nullptr) {
    frameCount = getTotalFrames() - startFrameNumber;
  } else {
    int labelIndexTo = searchFrameLabelIndexByLabel(labelTo);
    if (xmc_wcond(labelIndexTo == -1, "labelTo = %s", labelTo)) {
      return;
    }
    int endFrameNumber = searchFrameNumberByFrameLabelIndex(0, labelIndexTo); // Start frame with specified label
    if (xmc_wcond(endFrameNumber == -1, "labelTo = %s", labelTo)) {
      return;
    }
    if (xmc_wcond(startFrameNumber > endFrameNumber, "start frame(%d) > end frame(%d)", startFrameNumber, endFrameNumber)) {
      return;
    }
    frameCount = endFrameNumber - startFrameNumber;
  }
  // Specify start position and number of playback and move to specified position
  setPlayFrameRange(startFrameNumber, frameCount);
  DEBUG_LOG("start=%d, count=%d, current=%d", startFrameNumber, frameCount, _currentFrame);
  moveCurrentFrame(startFrameNumber);
  updateFrameForView();
  //xmc_assert(!_clockGenerator->isRegistered(this)); //skc ok! XmcButtonImpl::init() 에서 호출될 경우 등록되기 전인 상태임
  play(); //skc 아직 등록되지 않은 경우도 존재하나???
}

const std::string& XmcMovieClipAdapter::getCurrentLabel() const {
  const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
  if (frame.has_frame_label_index() == false) {
    //xmc_assert(false); //ok???
    return EMPTY_STRING;
  }
  const std::string& label = _timeline.frame_label_table(frame.frame_label_index());
  if (label.empty()) {
    //xmc_assert(false); //ok???
    return EMPTY_STRING;
  }
  return label;
}

int XmcMovieClipAdapter::getFrameByLabel(const char* label) const {
  int labelIndex = searchFrameLabelIndexByLabel(label);
  if (xmc_wcond(labelIndex == -1, "label=%s", label)) { //ok
    return -1;
  }
  int labelFrameNumber = searchFrameNumberByFrameLabelIndex(0, labelIndex);
  if (xmc_wcond(labelFrameNumber == -1, "label=%s", label)) { //skc ok???
    return -1;
  }
  return labelFrameNumber;
}

void XmcMovieClipAdapter::play() {
  if (getTotalFrames() > 1) { //skc: animation needs at least 2 frames
    _clockGenerator->registerListener(this); //skc 유일하게 등록되는 위치임
  }
}

void XmcMovieClipAdapter::stop() {
  setPlayFrameRange(_currentFrame, 1);
  xmc_assert(_clockGenerator);
  if (getTotalFrames() > 1 && _clockGenerator) {
    _clockGenerator->unregisterListener(this);
  }
}

void XmcMovieClipAdapter::stopAll() {
  //stop(); //skc needed?
  xmc_assert(_clockGenerator);
  if (_clockGenerator) {
    _clockGenerator->unregisterAllListener();
  }
}

void XmcMovieClipAdapter::nextFrame(bool child) {
  xmc_assert(_playFrameCount > 0 && _playStartFrame >= 0 && _playStartFrame < _timeline.frames().size());
#ifdef DEBUG_LOG_ENABLED
  auto prevFrame = _currentFrame;
#endif
  if (++_currentFrame == (_playStartFrame + _playFrameCount)) {
    DEBUG_LOG("%p: on last frame, isLoop=%d", static_cast<XmcMovieClip*>(this), _isLoop);
    if (_isLoop) {
      DEBUG_LOG("%p: go to start looped", static_cast<XmcMovieClip*>(this));
      _currentFrame = _playStartFrame;
    } else {
      DEBUG_LOG("%p: stop loop", static_cast<XmcMovieClip*>(this));
      _currentFrame = _playStartFrame + _playFrameCount - 1;
    }
  }
  DEBUG_LOG("%p: frame=%d->%d\n", static_cast<XmcMovieClip*>(this), prevFrame, _currentFrame);
  if (child) {
    // Also inform children
    //xmc_assert(false);
    const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
    int instanceSize = frame.placed_instances().size();
    for (int i = 0; i < instanceSize; ++i) {
      const bmc::BmcInstanceAtPlacedFrame& placedInst = frame.placed_instances(i);
      int inst_index = _indexhash[placedInst.instance_index()];
      XmcMovieClip* instAsMc = dynamic_cast<XmcMovieClip*>(_instances[inst_index].get());
      //XmcMovieClip* instAsMc = _instances[inst_index]->query<XmcMovieClip>(); // _instances[inst_index] == nullptr 가능
      if (instAsMc) {
        instAsMc->nextFrame(true);
      }
    }
  }
}

void XmcMovieClipAdapter::prevFrame(bool child) {
  xmc_assert(_playFrameCount > 0);
  xmc_assert(_playStartFrame >= 0 && _playStartFrame < _timeline.frames().size());
  if (--_currentFrame == (_playStartFrame - 1)) {
    if (_isLoop) {
      _currentFrame = _playStartFrame + _playFrameCount - 1;
    } else {
      _currentFrame = _playStartFrame;
    }
  }
  if (child) {
    // Also inform children
    //xmc_assert(false);
    const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
    int instanceSize = frame.placed_instances().size();
    for (int i = 0; i < instanceSize; ++i) {
      const bmc::BmcInstanceAtPlacedFrame& placedInst = frame.placed_instances(i);
      int inst_index = _indexhash[placedInst.instance_index()];
      //XmcMovieClipAdapter::Ptr instAsMc = std::dynamic_pointer_cast<XmcMovieClipAdapter>(_instances[inst_index]);
      XmcMovieClip* instAsMc = dynamic_cast<XmcMovieClip*>(_instances[inst_index].get());
      if (instAsMc) {
        instAsMc->prevFrame(true);
      }
    }
  }
}

void XmcMovieClipAdapter::updateFrameForView() {
  if (_prevConstructFrame == _currentFrame) {
    //xmc_assert(false); // ok! it occurs when called from updateFrameForView()
    DEBUG_LOG("%p: frame=%d/%d skipped", static_cast<XmcMovieClip*>(this), _currentFrame, _playStartFrame + _playFrameCount - 1);
    return;
  }
  DEBUG_LOG("%p: frame=%d/%d ========================================"
            , static_cast<XmcMovieClip*>(this), _currentFrame, _playStartFrame + _playFrameCount - 1);
  
  _prevConstructFrame = _currentFrame;
  const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
  
  std::set<XmcSymbol::Ptr> liveObject;
  
  int instanceSize = frame.placed_instances().size();
  for (int i = 0; i < instanceSize; ++i) {
    const bmc::BmcInstanceAtPlacedFrame& placedInstance = frame.placed_instances(i);
    int instance_index = _indexhash[placedInstance.instance_index()];
    XmcSymbol::Ptr inst = _instances[instance_index];
    
#ifdef DEBUG_LOG_ENABLED
    //const bmc::BmcInstance& instance = _timeline.instance_table(i);
#endif
    if (inst == nullptr) { // is it deleted???
      //xmc_assert(false); //skc ok when destroyInstance() called
      continue;
    }
    //    inst->setRenderOnFBO(getRenderOnFBO());
    //    DEBUG_LOG("renderOnFBO:%d", getRenderOnFBO());
    
    if (placedInstance.has_matrix_index()) {
      const bmc::BmcMatrix& bmcMatrix = _timeline.matrix_instance_table(placedInstance.matrix_index());
      inst->setAdditionalTransform(toMat4(bmcMatrix)); // bmcMatrix 는 Flash 에서 우상단에서의 위치임. toMat4()에서 y 값을 flip 시키고 있음(음수로)
      DEBUG_LOG("placed_inst> %p [%d/%d] bmcMatrix -> tx=%f, ty=%f", this, i, instanceSize - 1, bmcMatrix.tx(), bmcMatrix.ty());
    } else {
      inst->setAdditionalTransform(AffineTransform::IDENTITY);
      DEBUG_LOG("placed_inst> %p [%d/%d] bmcMatrix -> IDENTITY", this, i, instanceSize - 1);
    }
    if (placedInstance.has_colortrans_index() == false) {
      inst->setAdditionalColorTransform(ColorTransform::Identity());
    } else {
      const bmc::BmcColorTransform& s = _timeline.colortrans_instance_table(placedInstance.colortrans_index());
      inst->setAdditionalColorTransform(ColorTransform(s));
    }
    if (placedInstance.has_blend_mode()) {
      if (placedInstance.blend_mode() != bmc::BmcBlendMode_Normal) {
        const bmc::BmcInstance& instance = _timeline.instance_table(i);
//        xmc_dlog("%p: %s skc none BmcBlendMode_Normal(0) blend_mode=%d",
//                 inst.get(),
//                 instance.instance_name().c_str(), placedInstance.blend_mode());
        //xmc_assert(false);
      }
      inst->setBlendMode(placedInstance.blend_mode());
    }
    
    // Add the Z order as a minus value and organize the order of objects added later
    //int zOrder = -(instanceSize - i);
    int zOrder = i;
    if (inst->asNode()->getParent() == nullptr) {
      asNode()->addChild(inst->asNode(), zOrder, INSTANCE_TAG_OFFSET + placedInstance.instance_index());
      DEBUG_LOG("%p: %s addChild",
                inst.get(),
                instance.instance_name().c_str());
    } else {
      // For those already added, change ZOrder
      inst->asNode()->setLocalZOrder(zOrder);
    }
    liveObject.insert(inst);
    
    // if inst is XmcMovieClip, we should call it's updateFrameForView()
    //XmcMovieClipAdapter* instAsMc = dynamic_cast<XmcMovieClipAdapter*>(inst.get()); // org
    XmcMovieClipAdapter* instAsMc = inst.get()->query<XmcMovieClipAdapter>();
    if (instAsMc) {
      //DEBUG_LOG("placed_instance> %p: child ++++++++++++++++++++++++++++++++", this);
      instAsMc->updateFrameForView();
      instAsMc->play();
      //DEBUG_LOG("placed_instance> %p: child --------------------------------", this);
      DEBUG_LOG("%p: %s=%d/%d",
                instAsMc,
                instance.instance_name().c_str(),
                instAsMc->getCurrentFrame(),
                instAsMc->getTotalFrames() - 1);
    } else {
      DEBUG_LOG("%p: %s",
                inst.get(),
                instance.instance_name().c_str());
    }
  }
  
  // Delete unnecessary instances in this frame
  // Delete only the object managed by this symbol appropriately from the parent (Do not delete the object added later)
  for (int i = 0; i < _instances.size(); ++i) {
    XmcSymbol* p = _instances[i];
    if (p == nullptr) {
      //xmc_assert(false); //skc ok
      continue;
    }
    if (p->asNode()->getParent() == nullptr) {
      continue;
    }
    // 현재 timeline 의 인스턴스들 중에 현재 프레임에 포함되지 않는 노드는 부모에게서 삭제
    if (liveObject.find(p) == liveObject.end()) {
      //XmcMovieClipAdapter* instAsMc = dynamic_cast<XmcMovieClipAdapter*>(p); // org
      XmcMovieClipAdapter* instAsMc = p->query<XmcMovieClipAdapter>();
      if (instAsMc != nullptr) {
        instAsMc->moveCurrentFrame(0);
        DEBUG_LOG("%p: rewinded and removed", static_cast<XmcMovieClip*>(instAsMc));
      } else {
        DEBUG_LOG("%p: removed", p);
      }
      p->asNode()->removeFromParent();
    }
  }
}

void XmcMovieClipAdapter::setupSymbols(XmcLoader* source) {
  _instances.resize(_timeline.instance_table().size());
  // Extract and save the symbol to use
  const std::vector<XmcSymbol::Ptr>& symbolTable = source->getSymbolTable();
  for (int i = 0; i < _timeline.instance_table().size(); ++i) {
    const bmc::BmcInstance& instance = _timeline.instance_table(i);
    XmcSymbol* ptr = symbolTable[instance.symbol_index()];
    auto s = symbolTable[instance.symbol_index()]->asRef();
    DEBUG_LOG("[%d] %p (%s): refcnt=%d", i, ptr, typeid(*ptr).name(), ptr->asRef()->getReferenceCount());
    if (symbolTable[instance.symbol_index()]->asRef()->getReferenceCount() == 1) {
      _instances[i] = symbolTable[instance.symbol_index()];
    } else {
#if 1
      _instances[i] = symbolTable[instance.symbol_index()]->clone();
#else
      XmcSymbol::Ptr p = symbolTable[instance.symbol_index()]->clone(); // if newP return, XmcSymbol::Ptr(&) p -> 2, else Ptr return p -> 1
      // const & 일 경우에도 위와 동일한 주소가 리턴(return value optimization???), 하지만 p = nullptr; 를 호출할수 없어서 아래 xmc_assert() 에 걸리게 됨
      //const XmcSymbol::Ptr& p = symbolTable[instance.symbol_index()]->clone();
      _instances[i] = p;
      p = nullptr;
#endif
    }
  }
}

void XmcMovieClipAdapter::setPlayFrameRange(int startFrame, int count) {
  _playStartFrame = startFrame;
  _playFrameCount = count;
  if (_playStartFrame >= _timeline.frames().size()) {
    xmc_assert(false);
    _playStartFrame = _timeline.frames().size() - 1;
    _playFrameCount = 1;
  }
  if (_playStartFrame + _playFrameCount > _timeline.frames().size()) {
    xmc_assert(false);
    _playFrameCount = _timeline.frames().size() - _playStartFrame;
  }
}

// Use nextFrame and prevFrame to move to the designated frame
// It does not move by direct designation because it moves the frame of the hierarchical child Symbol correctly.
void XmcMovieClipAdapter::moveCurrentFrame(int frameNumber) {
  DEBUG_LOG("%p: move=%d->%d", static_cast<XmcMovieClip*>(this), _currentFrame, frameNumber);
  xmc_assert(_playFrameCount > 0);
  xmc_assert(0 <= _playStartFrame && _playStartFrame < _timeline.frames().size());
  if (xmc_wcond(frameNumber < 0)) {
    return;
  }
  if (_currentFrame < frameNumber) {
    int callCount = frameNumber - _currentFrame;
    for (int i = 0; i < callCount; ++i) {
      nextFrame(true);
    }
  } else if (_currentFrame > frameNumber) {
    int callCount = _currentFrame - frameNumber;
    for (int i = 0; i < callCount; ++i) {
      prevFrame(true);
    }
  }
}

cocos2d::Size XmcMovieClipAdapter::setupContentSize() {
  //skc 이 함수에서 자식 인스턴스들의 크기까지 계산해서 리턴해주지 않으면, 버튼 클릭이 되지 않게 된다.
  if (asNode()->getChildren().empty()) {
    updateFrameForView();
    if (asNode()->getChildren().empty()) {
      return asNode()->getContentSize();
    }
  }
  cocos2d::Rect rect;
  int childrenCnt = asNode()->getChildrenCount();
  for (int i = 0; i < childrenCnt; ++i) {
    cocos2d::Node* child = asNode()->getChildren().at(i);
    if (i == 0) {
      rect = child->getBoundingBox();
      continue;
    }
    Rect cr = child->getBoundingBox();
    rect.origin.x = std::min(cr.getMinX(), rect.getMinX());
    rect.origin.y = std::min(cr.getMinY(), rect.getMinY());
    if (rect.getMaxX() < cr.getMaxX()) {
      rect.size.width = cr.getMaxX() - rect.origin.x;
    }
    if (rect.getMaxY() < cr.getMaxY()) {
      rect.size.height = cr.getMaxY() - rect.origin.y;
    }
  }
  if (true) {
    cocos2d::Vec2 pos = asNode()->getPosition();
    cocos2d::Size size = asNode()->getContentSize();
    if (size.width > 0 || size.height > 0) {
      xmc_assert(false);
    }
    xmc_dlog("org: (%.1f, %.1f) %.1f - %.1f ==> (%.1f, %.1f) %.1f - %.1f", pos.x, pos.y, size.width, size.height
             , rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
  }
  asNode()->setContentSize(rect.size);
  return rect.size;
}

void XmcMovieClipAdapter::setBlendMode(bmc::BmcBlendMode blendMode) {
  _blendMode = blendMode;
  if (_blendMode == bmc::BmcBlendMode_Add) {
    for (size_t i = 0; i < _instances.size(); ++i) {
      if (_instances[i] == nullptr) {
        xmc_assert(false);
        continue;
      }
      _instances[i]->setBlendMode(_blendMode);
    }
  }
}

void XmcMovieClipAdapter::setEventListener(std::function<void (EventType, XmcMovieClip*)> listener) {
  _listener = listener;
}

void XmcMovieClipAdapter::clearEventListener() {
  _listener = nullptr;
}

XmcSymbol::Ptr XmcMovieClipAdapter::getInstanceImpl(const std::string& instanceName) const {
  for (int i = 0; i < _timeline.instance_table_size(); ++i) {
    const bmc::BmcInstance& instInfo = _timeline.instance_table(i);
    if (instInfo.has_instance_name()) {
      if (instInfo.instance_name() == instanceName) {
        XmcSymbol::Ptr symbol = _instances[i];
        if (!symbol) {
          xmc_elog("%s: not found. destroyed???", instanceName.c_str());
          return getNullObject();
        }
        return symbol;
      }
    }
  }
  xmc_wlog("%s: not found!", instanceName.c_str()); //ok
  return getNullObject();
}

XmcSymbol::Ptr XmcMovieClipAdapter::getNullObject() const {
  return new XmcNullMovieClip();
}

XmcSymbol::Ptr XmcMovieClipAdapter::destroyInstance(const std::string& instanceName) {
  for (int i = 0; i < _instances.size(); ++i) {
    const bmc::BmcInstance& instInfo = _timeline.instance_table(i);
    if (instInfo.has_instance_name() && instInfo.instance_name() == instanceName) {
      XmcSymbol::Ptr inst = _instances[i];
      if (inst == nullptr) {
        xmc_assert(false, "%s: already destroyed", instanceName.c_str());
        return nullptr; //skc todo getNullObject();
      }
      _instances[i] = nullptr;
      asNode()->removeChildByTag(INSTANCE_TAG_OFFSET + i);
      //this->release(); //skc add
      return inst;
    }
  }
  xmc_assert(false, "%s: not found!", instanceName.c_str());
  return nullptr; //skc todo getNullObject();
}

XmcSymbol::Ptr XmcMovieClipAdapter::destroyInstance(XmcSymbol::Ptr instance) {
  bool found = false;
  for (int i = 0; i < _instances.size(); ++i) {
    if (_instances[i] == instance) {
      _instances[i] = nullptr;
      asNode()->removeChildByTag(INSTANCE_TAG_OFFSET + i);
      //this->release(); //skc add
      found = true;
    }
  }
  xmc_assert(found);
  return instance;
}

void XmcMovieClipAdapter::iterateInstances(std::function<void(XmcSymbol*)> func) {
  for (int i = 0; i < _instances.size(); ++i) {
    if (_instances[i] == nullptr) {
      //xmc_assert(false); // destroyInstance()이 호출됐던 경우 가능함
      continue;
    }
    func(_instances[i].get());
  }
}

//static const std::string EMPTY_STRING = "";
void XmcMovieClipAdapter::iterateInstancesForName(std::function<void(const std::string&, XmcSymbol*)> func) {
  for (int i = 0; i < _instances.size(); ++i) {
    if (_instances[i] == nullptr) {
      continue;
    }
    const bmc::BmcInstance& instInfo = _timeline.instance_table(i);
    func(instInfo.has_instance_name() ? instInfo.instance_name() : "", _instances[i].get());
  }
}

void XmcMovieClipAdapter::iterateInstancesRecursively(std::function<void(XmcSymbol*)> func) {
  for (int i = 0; i < _instances.size(); ++i) {
    if (_instances[i] == nullptr) {
      //xmc_assert(false); //skc ok destroyInstance()이 호출됐던 경우 가능함
      continue;
    }
    func(_instances[i].get());
    _instances[i]->iterateInstancesRecursively(func);
  }
}

void XmcMovieClipAdapter::setLabelCallback(const std::string& labelName, std::function<void(XmcMovieClip* mc)> listener) {
  _labelCallback[labelName.c_str()] = listener;
}

void XmcMovieClipAdapter::setLabelCallback(std::function<void(const std::string& label, XmcMovieClip* mc)> listener) {
  _anyLabelCallback = listener;
}

void XmcMovieClipAdapter::clearLabelCallback() {
  _labelCallback.clear();
  _anyLabelCallback = nullptr;
}

int XmcMovieClipAdapter::searchFrameLabelIndexByLabel(const char* label) const {
  for (int i = 0; i < _timeline.frame_label_table_size(); ++i) {
    const std::string& str = _timeline.frame_label_table(i);
    if (str == label) {
      return i;
    }
  }
  return -1;
}

int XmcMovieClipAdapter::searchFrameNumberByFrameLabelIndex(int searchStartFrame, int frameLabelIndex) const {
  for (int i = searchStartFrame; i < _timeline.frames_size(); ++i) {
    const bmc::BmcFrame& frame = _timeline.frames(i);
    if (frame.has_frame_label_index() && frame.frame_label_index() == frameLabelIndex) {
      return i;
    }
  }
  return -1;
}

cocos2d::Rect XmcMovieClipAdapter::getCurrentFrameBoundingBox() const {
  if (asNode()->getChildrenCount() == 0) {
    return cocos2d::Rect::ZERO;
  }
  const float kContentScale = cocos2d::Director::getInstance()->getContentScaleFactor();
  if (kContentScale != 1.f) {
    xmc_assert(false);
  }
  int childrenCount = static_cast<int>(asNode()->getChildrenCount());
  float minX = 0, minY = 0, maxX = 0, maxY = 0;
  bool first = true;
  for (int i = 0; i < childrenCount; ++i) {
    auto child = asNode()->getChildren().at(i);
    if (child->isVisible() == false) {
      continue;
    }
    const XmcSymbol* asSymbol = areYouSymbol(child);
    //skc moved up common code
    auto cr = asSymbol ? asSymbol->getCurrentFrameBoundingBox() : child->getBoundingBox();
    if (cr.size.width == 0 || cr.size.height == 0) {
      //xmc_assert(false); //ok?
      continue;
    }
    if (first) {
//      auto cr = asSymbol ? asSymbol->getCurrentFrameBoundingBox() : child->getBoundingBox();
//      if (cr.size.width == 0 || cr.size.height == 0) {
//        xmc_assert(false);
//        continue;
//      }
      first = false;
      minX = cr.getMinX() * kContentScale;
      minY = cr.getMinY() * kContentScale;
      maxX = cr.getMaxX() * kContentScale;
      maxY = cr.getMaxY() * kContentScale;
      continue;
    }
    auto crMinX = cr.getMinX();
    if (crMinX < minX) {
      minX = crMinX;
    }
    auto crMinY = cr.getMinY();
    if (crMinY < minY) {
      minY = crMinY;
    }
    auto crMaxX = cr.getMaxX();
    if (maxX < crMaxX) {
      maxX = crMaxX;
    }
    auto crMaxY = cr.getMaxY();
    if (maxY < crMaxY) {
      maxY = crMaxY;
    }
  }
  
  if (first) {
    //xmc_assert(false); // 리틀돌 인벤토리에서 발생하고 있음
    return cocos2d::Rect::ZERO;
  }
  
  auto size = cocos2d::Size(maxX - minX, maxY - minY) / kContentScale;
  auto rect = cocos2d::Rect(minX / kContentScale, minY / kContentScale, size.width, size.height);
  rect = cocos2d::RectApplyAffineTransform(rect, asNode()->getNodeToParentAffineTransform());
  return rect;
}

NS_XMC_END
