#include "XmcShader.h"
#include "XmcFactory.h"
#include "shaders/Shaders.h"

USING_NS_CC;

NS_XMC_BEGIN

class XmcShaderImpl : public XmcShader {
public:
  XmcShaderImpl() {}
  virtual ~XmcShaderImpl() = default;
  
  virtual bool operator == (const std::nullptr_t) const override {
    //xmc_assert(false); //ok
    return false;
  }
  virtual bool operator != (const std::nullptr_t) const override {
    xmc_assert(false);
    return true;
  }
  virtual explicit operator bool() const override {
    xmc_assert(false);
    return true;
  }

  virtual void init() override;
  virtual void setTexture(XmcTexturePart* tex) override;
  virtual void setColorTransform(const ColorTransform& colorTransform) override;

  virtual void useMostValuableProgram() override;
  
private:
  /*
   const char *const Uniform_ColorMultiplierIdentify = "u_colorMultiplier";
   const char *const Uniform_ColorOffsetIdentify = "u_colorOffset";
   const char *const Uniform_AlphaMultiplierIdentify = "u_alphaMultiplier";
   extern const char *const kCCShader_Vector_PositionTextureColor_UseOffsetAlpha;
   extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetAlpha;
   extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor;
   extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha;
   extern const char *const kCCShader_Vector_PositionTextureColor_OnlyTexture;
   */
  GLProgram*   _useOffsetAlphaP;
  GLint        _useOffsetAlphaP_u_colorMultiplier;
  GLint        _useOffsetAlphaP_u_colorOffset;
  
  GLProgram*   _noOffsetAlphaP;
  GLint        _noOffsetAlphaP_u_colorMultiplier;
  GLint        _noOffsetAlphaP_u_colorOffset;
  
  GLProgram*   _noOffsetOnlyMultiplierClor;
  GLint        _noOffsetOnlyMultiplierClor_u_colorMultiplier;
  
  GLProgram*   _noOffsetOnlyMultiplierALpha;
  GLint        _noOffsetOnlyMultiplierALpha_u_alphaMultiplier;
  
  GLProgram*   _onlyTexture;
  
  ColorTransform _colorTransform;
  
};

void XmcShaderImpl::init() {
  _useOffsetAlphaP = GLProgramCache::getInstance()->getGLProgram(NS_XMC::kCCShader_Vector_PositionTextureColor_UseOffsetAlpha); // "Vector_PositionTextureColor_UseOffsetAlpha.xmc"
  _noOffsetAlphaP = GLProgramCache::getInstance()->getGLProgram(NS_XMC::kCCShader_Vector_PositionTextureColor_NoOffsetAlpha);
  _noOffsetOnlyMultiplierClor = GLProgramCache::getInstance()->getGLProgram(NS_XMC::kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor);
  _noOffsetOnlyMultiplierALpha = GLProgramCache::getInstance()->getGLProgram(NS_XMC::kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha);
  _onlyTexture = GLProgramCache::getInstance()->getGLProgram(NS_XMC::kCCShader_Vector_PositionTextureColor_OnlyTexture); // "Vector_PositionTextureColor_OnlyTexture.xmc"
  
  _useOffsetAlphaP_u_colorMultiplier = glGetUniformLocation(_useOffsetAlphaP->getProgram(), Uniform_ColorMultiplierIdentify); // "u_colorMultiplier"
  _useOffsetAlphaP_u_colorOffset = glGetUniformLocation(_useOffsetAlphaP->getProgram(), Uniform_ColorOffsetIdentify);
  
  _noOffsetAlphaP_u_colorMultiplier = glGetUniformLocation(_noOffsetAlphaP->getProgram(), Uniform_ColorMultiplierIdentify);
  _noOffsetAlphaP_u_colorOffset = glGetUniformLocation(_noOffsetAlphaP->getProgram(), Uniform_ColorOffsetIdentify);
  
  _noOffsetOnlyMultiplierClor_u_colorMultiplier = glGetUniformLocation(_noOffsetOnlyMultiplierClor->getProgram(), Uniform_ColorMultiplierIdentify);
  
  _noOffsetOnlyMultiplierALpha_u_alphaMultiplier = glGetUniformLocation(_noOffsetOnlyMultiplierALpha->getProgram(), Uniform_AlphaMultiplierIdentify);
}

void XmcShaderImpl::setTexture(XmcTexturePart* tex) {
  GL::bindTexture2D(tex->getTexture()->getCCTexture2D()->getName());
}

void XmcShaderImpl::setColorTransform(const ColorTransform& colorTransform) {
  _colorTransform = colorTransform;
}

void XmcShaderImpl::useMostValuableProgram() {
  const float* m = _colorTransform.multiplier();
  const float* a = _colorTransform.offset();
  GLProgram* program = nullptr;
  if (a[3] == 0.f) {
    if (a[0] == 0.f && a[1] == 0.f && a[2] == 0.f) {
      if (m[0] == 1.f && m[1] == 1.f && m[2] == 1.f) {
        if (m[3] == 1.f) {
          //xmc_dlog("_onlyTexture");
          program = _onlyTexture;
          program->use();
        } else {
          //xmc_dlog("_noOffsetOnlyMultiplierALpha");
          program = _noOffsetOnlyMultiplierALpha;
          program->use();
          glUniform1f(_noOffsetOnlyMultiplierALpha_u_alphaMultiplier, m[3]);
        }
      } else {
        //xmc_dlog("_noOffsetOnlyMultiplierClor");
        program = _noOffsetOnlyMultiplierClor;
        program->use();
        glUniform4fv(_noOffsetOnlyMultiplierClor_u_colorMultiplier, 1, m);
      }
    } else {
      //xmc_dlog("_noOffsetAlphaP");
      program = _noOffsetAlphaP;
      program->use();
      glUniform4fv(_noOffsetAlphaP_u_colorMultiplier, 1, m);
      glUniform4fv(_noOffsetAlphaP_u_colorOffset, 1, a);
    }
  } else {
    //xmc_dlog("_useOffsetAlphaP");
    program = _useOffsetAlphaP;
    program->use();
    glUniform4fv(_useOffsetAlphaP_u_colorMultiplier, 1, m);
    glUniform4fv(_useOffsetAlphaP_u_colorOffset, 1, a);
  }
  program->setUniformsForBuiltins();
}


XMC_FACTORY_CREATE_FUNC_IMPL(XmcShader);
NS_XMC_END
