#include "XmcTexturePart.h"
#include "XmcFactory.h"

NS_XMC_BEGIN

class XmcTexturePartImpl : public XmcTexturePart, public XmcRef {
public:
  XMC_DECLARE_ASREF_IMPL();

  XmcTexturePartImpl() {}
  virtual ~XmcTexturePartImpl() = default;

  virtual void init(XmcTexture::Ptr tex, const bmc::BmcTextureRef& ref) override;
  virtual XmcTexture::Ptr getTexture() override {
    return _texture;
  }
  virtual const bmc::BmcRectangle& getRectangle() const override {
    return _ref.rect();
  }
  virtual const cocos2d::Vec2& getOffset() const override {
    return _offset;
  }
  
  virtual const bmc::BmcRectangle& getScale9GridRectangle() const override {
    if (_ref.has_scale9grid()) {
      return _ref.scale9grid();
    }
    xmc_assert(false);
    return *(const bmc::BmcRectangle*)nullptr;
  }
  
private:
  XmcTexture::Ptr _texture;
  bmc::BmcTextureRef _ref;
  Vec2 _offset; // XmcShapeImpl::XmcSprite::draw() 등에서 x, y 축으로 이동시키는데 사용됨
};

void XmcTexturePartImpl::init(XmcTexture::Ptr tex, const bmc::BmcTextureRef& ref) {
  _texture = tex;
  _ref = ref;
  // Flash 의 display object positioning system 은 stage 를 Cartesian coordinate system 으로 처리한다.
  // Flash 는 stage 의 left-top 을 origin(4 사분면 좌표계), cocos2d, OpenGL은 left-bottom 을 origin(1사분면 좌표계)으로 처리함
  // Flash 는 모든 Display Object(Shape, Bitmap, Symbol)에 대해 left-top 을 origin 으로 처리하는듯함
  // cocos2d 에서는 XmcShapeImpl::init() 에서 _sprite->setAnchorPoint(Vec2::ZERO);로 설정(left-bottom)중임
  // cocos2d 의 Sprite 는 left-bottom 이 origin 이므로 _offset 을 left-bottom 위치((y + height) * -1) 로 저장시킴
  // cocos2d 의 texture origin 은 left-top 여서(texture coordinates (UV)), XmcSpriteShapeImpl::draw()에서 이 _offset 을 더해서 계산중임?
  _offset = CC_POINT_PIXELS_TO_POINTS(Vec2(_ref.rect().x()
                                           , (_ref.rect().y() + _ref.rect().height()) * -1.f)); // x, y 는 width, height 의 절반(음수)
}

XMC_FACTORY_CREATE_FUNC_IMPL(XmcTexturePart);
NS_XMC_END
