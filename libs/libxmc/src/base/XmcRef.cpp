#if 0
#include "XmcRef.h"
#include "foundation/Log.h"
#include <list>     // for std::list

#if XMC_REF_LEAK_DETECTION
#include <algorithm>    // std::find
#endif

NS_XMC_BEGIN

#if XMC_REF_LEAK_DETECTION
static void trackRef(XmcRef* ref);
static void untrackRef(XmcRef* ref);
#endif

XmcRef::XmcRef()
: _referenceCount(1) // when the XmcRef is created, the reference count of it is 1
{
#if XMC_REF_LEAK_DETECTION
  trackRef(this);
#endif
}

XmcRef::~XmcRef()
{
#if XMC_REF_LEAK_DETECTION
  if (_referenceCount != 0)
    untrackRef(this);
#endif
}

void XmcRef::retain()
{
  //    CCASSERT(_referenceCount > 0, "reference count should be greater than 0");
  ++_referenceCount;
}

void XmcRef::release()
{
  xmc_assert(_referenceCount > 0, "reference count should be greater than 0");
  --_referenceCount;
  
  if (_referenceCount == 0)
  {
#if XMC_REF_LEAK_DETECTION
    untrackRef(this);
#endif
    delete this;
  }
}

unsigned int XmcRef::getReferenceCount() const
{
  return _referenceCount;
}

#if XMC_REF_LEAK_DETECTION

static std::list<XmcRef*> __refAllocationList;

void XmcRef::printLeaks()
{
  // Dump XmcRef object memory leaks
  if (__refAllocationList.empty())
  {
    xmc_dlog("[memory] All XmcRef objects successfully cleaned up (no leaks detected).\n");
  }
  else
  {
    xmc_elog("[memory] WARNING: %d XmcRef objects still active in memory.\n", (int)__refAllocationList.size());
    
    for (const auto& ref : __refAllocationList)
    {
      xmc_assert(ref);
      const char* type = typeid(*ref).name();
      xmc_elog("[memory] LEAK: XmcRef object '%s' still active with reference count %d.\n", (type ? type : ""), ref->getReferenceCount());
    }
  }
}

static void trackRef(XmcRef* ref)
{
  xmc_assert(ref, "Invalid parameter, ref should not be null!");
  
  // Create memory allocation record.
  __refAllocationList.push_back(ref);
}

static void untrackRef(XmcRef* ref)
{
  auto iter = std::find(__refAllocationList.begin(), __refAllocationList.end(), ref);
  if (iter == __refAllocationList.end())
  {
    xmc_elog("[memory] CORRUPTION: Attempting to free (%s) with invalid ref tracking record.\n", typeid(*ref).name());
    return;
  }
  
  __refAllocationList.erase(iter);
}

#endif // #if XMC_REF_LEAK_DETECTION

NS_XMC_END

#endif //0
