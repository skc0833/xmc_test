#include <string>
#include <vector>
#include <platform/CCFileUtils.h>
#include "protocol/BmcFile.pb.h"
#include "XmcLoader.h"
#include "XmcFactory.h"
#include "XmcSymbol.h"
#include "XmcTexture.h"
#include "foundation/Log.h"
#include "foundation/FileHelper.h"
#include "deprecated/CCString.h" // for StringUtils
#include "2d/CCSpriteFrameCache.h"

USING_NS_CC;
//using namespace cocone;
using namespace google_public::protobuf; // for RepeatedPtrField

NS_XMC_BEGIN

#define SUPPORTED_BMC_VERSION   (2)

class XmcLoaderImpl : public XmcLoader {
public:
  XmcLoaderImpl();
  virtual ~XmcLoaderImpl();

  virtual bool operator == (const std::nullptr_t) const override {
    xmc_assert(false);
    return false;
  }
  virtual bool operator != (const std::nullptr_t) const override {
    //xmc_assert(false); //ok
    return true;
  }
  virtual explicit operator bool() const override {
    //xmc_assert(false); //ok
    return true;
  }
  
  virtual bool init(const std::string& path) override;
  virtual const std::vector<XmcSymbol::Ptr>& getSymbolTable() const override {
    return _symbols;
  }
  virtual XmcSymbol::Ptr getStage() override {
    return _stage;
  }
  virtual XmcClockGenerator::Ptr getClockGenerator() override {
    return _clockGenerator;
  }
  
  virtual void setSoundHandler(std::function<void(void)> playSound) override {
    _playSound = playSound;
  }
  
  virtual XmcShader::Ptr getShader() override {
    return s_shader;
  }
  
private:
  void initTextures();
  void initSymbols();
  void initStage(const bmc::BmcFile* bmcFile);
  
  bool isShape(const bmc::BmcSymbol& symbol);
  bool isMovieClip(const bmc::BmcSymbol& symbol);
  bool isButton(const bmc::BmcSymbol& symbol);
  bool isMaskSymbol(const bmc::BmcTimeline& timeline);

  XmcSymbol::Ptr createShape(const bmc::BmcSymbol& symbol);
  XmcSymbol::Ptr createMovieClip(const bmc::BmcSymbol& symbol);
  XmcSymbol::Ptr createButton(const bmc::BmcSymbol& symbol);
  
  XmcTexturePart::Ptr createTexturePart(XmcTexture::Ptr tex, const bmc::BmcTextureRef& ref);
  
  bool readHeader(const bmc::BmcFile* bmcFile);
  
public:
  static XmcShader::Ptr s_shader; // todo: should be static???
  
private:
  bmc::BmcFile _bmc;
  std::vector<XmcTexture::Ptr> _textures;
  std::vector<XmcSymbol::Ptr> _symbols;
  XmcSymbol::Ptr _stage;
  XmcClockGenerator::Ptr _clockGenerator;
  int _frameRate;
  std::string _filePath;
  std::function<void(void)> _playSound;
};

XmcShader::Ptr XmcLoaderImpl::s_shader;

XmcLoaderImpl::XmcLoaderImpl() : _frameRate(24) {
}

XmcLoaderImpl::~XmcLoaderImpl() {
  //s_shader = nullptr;
}

bool XmcLoaderImpl::init(const std::string& path) {
  xmc_dlog("path=%s", path.c_str());
  _filePath = path;
  
//  const Data fileData = CCFileUtils::getInstance()->getDataFromFile(path);
  const Data fileData = FileUtils::getInstance()->getDataFromFile(path);
  if (fileData.isNull()) {
    xmc_elog("Bmc File is null : path=%s", path.c_str());
    return false;
  }
  // IMPORTANT: Since the checkum data is contained in the last 4 bytes of the bmc file, 4 bytes are subtracted and executed
  if (!_bmc.ParseFromArray(fileData.getBytes(), static_cast<int>(fileData.getSize()) -4)) {
    xmc_elog("It is not Bmc File : path=%s", path.c_str());
    return false;
  }
  
  _clockGenerator = XmcFactory::createXmcClockGenerator();
  
  // init _bmc
  if (xmc_econd(readHeader(&_bmc) == false, "unsupported file")) {
    return false;
  }
  
  if (s_shader == nullptr) {
    s_shader = XmcFactory::createXmcShader();
    s_shader->init();
  }
  
  initTextures();
  initSymbols();
  initStage(&_bmc);

  _stage->query<XmcMovieClip>()->updateFrameForView(); // dynamic_cast<XmcMovieClip*>(_stage.get())->updateFrameForView();
  return true;
}

bool XmcLoaderImpl::readHeader(const bmc::BmcFile* bmcFile) {
  if (bmcFile->bmc_file_version() > SUPPORTED_BMC_VERSION) {
    return false;
  }
  if (bmcFile->has_frame_rate()) {
    _frameRate = bmcFile->frame_rate();
  }
  if (_clockGenerator) {
    _clockGenerator->setOneFrameDelta(1.f / float(bmcFile->frame_rate()));
  }
  return true;
}

void XmcLoaderImpl::initTextures() {
  // load spritesheet
  const auto ext = filehelper::extname(_filePath);
  const auto baseName = filehelper::basename(_filePath, "." + ext);
  if (ext == "mc") {
    xmc_assert(false);
    const auto dir = filehelper::dirname(_filePath);
#if (XMC_TARGET_PLATFORM == XMC_PLATFORM_IOS)
    const std::string os_image_ext = "pvr.plist";
    // NOTE Since pvr's software decoding is dirty, osx and win32 use ktx
#elif (XMC_TARGET_PLATFORM == XMC_PLATFORM_ANDROID || XMC_TARGET_PLATFORM == XMC_PLATFORM_MAC)
    const std::string os_image_ext = "ktx.plist";
#endif
    const auto plist = StringUtils::format("%s/images_%s.%s", dir.c_str(), baseName.c_str(), os_image_ext.c_str());
    if (FileUtils::getInstance()->isFileExist(plist)) {
      cocos2d::SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plist);
      xmc_dlog("loaded spriteframe sheet=%s", plist.c_str());
    }
    // TODO Processing when there is no plist
  }
  
  auto image_table = _bmc.image_table();
  _textures.resize(image_table.size());
  for (int i = 0; i < image_table.size(); ++i) {
    auto bmcImage = image_table.Get(i);
    XmcTexture::Ptr tex = XmcFactory::createXmcTexture();
    _textures[i] = tex;
    tex->init(i, &bmcImage, baseName); // create Texture2D
  }
}

void XmcLoaderImpl::initSymbols() {
  const RepeatedPtrField<bmc::BmcSymbol>& symbol_table = _bmc.symbol_table();
  _symbols.resize(symbol_table.size());
  for (int i = 0; i < symbol_table.size(); ++i) {
    const bmc::BmcSymbol& symbol = symbol_table.Get(i);
    if (isButton(symbol)) {
      _symbols[i] = createButton(symbol);
    } else if (isMovieClip(symbol)) {
      _symbols[i] = createMovieClip(symbol);
    } else if (isShape(symbol)) {
      _symbols[i] = createShape(symbol);
    } else {
      xmc_assert(false, "not implemented symbol type %d", symbol.symbol_type());
    }
  }
}

bool XmcLoaderImpl::isShape(const bmc::BmcSymbol& symbol) {
  return symbol.symbol_type() == bmc::BmcSymbol_Type_TypeShape || symbol.has_shape();
}

bool XmcLoaderImpl::isMovieClip(const bmc::BmcSymbol& symbol) {
  return symbol.symbol_type() == bmc::BmcSymbol_Type_TypeMovieClip || symbol.has_timeline();
}

bool XmcLoaderImpl::isButton(const bmc::BmcSymbol& symbol) {
  if (symbol.symbol_type() == bmc::BmcSymbol_Type_TypeButton) {
    return true;
  } else if (symbol.has_timeline() == false) {
    return false;
  }
  const bmc::BmcTimeline& timeline = symbol.timeline();
  bool up = false, upnormal = false, down = false, downnormal = false, disable = false;
  for (int i = 0; i < timeline.frame_label_table_size(); ++i) {
    const std::string& label = timeline.frame_label_table(i);
    if (label == "up") up = true;
    else if (label == "upnormal") upnormal = true;
    else if (label == "down") down = true;
    else if (label == "downnormal") downnormal = true;
    else if (label == "disable") disable = true;
  }
  if (up && upnormal && down && downnormal && disable) {
    return true;
  }
  return false;
}

bool XmcLoaderImpl::isMaskSymbol(const bmc::BmcTimeline& timeline) {
  const auto& frame0 = timeline.frames(0).placed_instances();
  if (frame0.size() < 1) {
    return false;
  }
  const auto& inst = timeline.instance_table(frame0.Get(frame0.size() - 1).instance_index());
  if (inst.instance_name().find("$range_mask") == 0) {
    xmc_assert(false);
    return true;
  }
  return false;
}

void XmcLoaderImpl::initStage(const bmc::BmcFile* bmcFile) {
  XmcMovieClip::Ptr mc = XmcFactory::createXmcMovieClip();
  mc->init(this, bmcFile->stage());
  _stage = mc; //mc->asSymbol();
}

XmcTexturePart::Ptr XmcLoaderImpl::createTexturePart(XmcTexture::Ptr tex, const bmc::BmcTextureRef& ref) {
  XmcTexturePart::Ptr p = XmcFactory::createXmcTexturePart();
  p->init(tex, ref);
  return p;
}

XmcSymbol::Ptr XmcLoaderImpl::createShape(const bmc::BmcSymbol& symbol) {
  const bmc::BmcTextureRef& textureRef = symbol.shape();
  const auto index = textureRef.texture_index();
  XmcTexture::Ptr tex = _textures[index];
  XmcTexturePart::Ptr part = createTexturePart(tex, textureRef); // connect BmcTextureRef to Texture2D
  auto image_table = _bmc.image_table();
  if (textureRef.has_scale9grid()) {
    //xmc_assert(false, "not implemented scale9grid");
    XmcScalingGridShape::Ptr grid = XmcFactory::createXmcScalingGridShape();
    if (image_table.Get(index).type() == bmc::BmcImage_Type_TypeCocos2dxAtlas) {
      grid->initWithSpriteFramename(s_shader, part, tex->getSpriteFrameName());
    } else {
      grid->init(s_shader, part);
    }
    return static_cast<XmcSymbol*>(grid);
  } else {
    if (image_table.Get(index).type() == bmc::BmcImage_Type_TypeCocos2dxAtlas) {
      xmc_assert(false, "not implemented createXmcSpriteShape");
      XmcShape::Ptr shape = XmcFactory::createXmcSpriteShape();
      //shape->initWithSpriteFrameName(s_shader, part, tex->getSpriteFrameName());
      return static_cast<XmcSymbol*>(shape);
    } else {
      XmcShape::Ptr shape = XmcFactory::createXmcShape();
      shape->init(s_shader, part);
      return static_cast<XmcSymbol*>(shape);
    }
  }
  return nullptr;
}

XmcSymbol::Ptr XmcLoaderImpl::createMovieClip(const bmc::BmcSymbol& symbol) {
  const bmc::BmcTimeline& timeline = symbol.timeline();
  XmcMovieClip::Ptr mc;
  if (isMaskSymbol(timeline)) {
    //mc = new XmcMCMaskImpl(); //skc todo
    xmc_assert(false);
    return nullptr;
  } else {
    mc = XmcFactory::createXmcMovieClip();
  }
  mc->init(this, timeline);
  return static_cast<XmcSymbol*>(mc); //skc instead of mc->asSymbol()
}

XmcSymbol::Ptr XmcLoaderImpl::createButton(const bmc::BmcSymbol& symbol) {
  const bmc::BmcTimeline& timeline = symbol.timeline();
  XmcButton::Ptr btn = XmcFactory::createXmcButton();
  btn->setPushSoundHandler(_playSound);
//  XmcMovieClip::Ptr mc = dynamic_cast<XmcMovieClip*>(btn.get()); // XmcMovieClip::Ptr button = rawButton->query<XmcMovieClip>();
//  mc->init(this, timeline); //skc XmcButton has no init()
  btn->query<XmcMovieClip>()->init(this, timeline);
  return static_cast<XmcSymbol*>(btn);
}

XMC_FACTORY_CREATE_FUNC_IMPL(XmcLoader);

NS_XMC_END
