#include "XmcTexture.h"
#include "XmcFactory.h"
#include "foundation/Log.h"
#include "foundation/StringHelper.h"
#include "protocol/BmcFile.pb.h"
#include "renderer/CCTexture2D.h"
#include <string>
#include "platform/CCImage.h"


NS_XMC_BEGIN

class XmcTextureImpl : public XmcTexture, public XmcRef {
public:
  XMC_DECLARE_ASREF_IMPL();

  XmcTextureImpl();
  virtual ~XmcTextureImpl() = default;
  
  virtual void init(int imageIndex, const bmc::BmcImage* bmcImage, const std::string& baseName) override;
  virtual cocos2d::Texture2D* getCCTexture2D() override {
    return _rawTexture;
  }
  virtual std::string getSpriteFrameName() override {
    xmc_assert(false, "not impl");
    return stringhelper::strsprintf("images_%s/%s_%d.png", _baseName.c_str(), _baseName.c_str(), _imageIndex);
  }
  
private:
  cocos2d::RefPtr<Texture2D> _rawTexture;
  int _imageIndex;
  std::string _baseName;
};

XmcTextureImpl::XmcTextureImpl()
: _imageIndex(-1)
{
}

void XmcTextureImpl::init(int imageIndex, const bmc::BmcImage* image, const std::string& baseName) {
  _imageIndex = imageIndex;
  _baseName = baseName;
  // In case of TextureAtlas do not manage anything
  if (image->type() == bmc::BmcImage_Type_TypeCocos2dxAtlas) {
    xmc_assert(false);
    return;
  }
  Image* pImage = new Image();
  if (!pImage->initWithImageData((const uint8_t*)image->image_bytes().data(), image->image_bytes().size())) {
    xmc_elog("failed to init image");
    pImage->release();
    //_rawTexture = new Texture2D(); //skc don't need???
    //_rawTexture->release();
    return;
  }
  _rawTexture = new Texture2D();
  _rawTexture->release(); // ref = 1
  _rawTexture->initWithImage(pImage);
  pImage->release(); // ref = 0
}

XMC_FACTORY_CREATE_FUNC_IMPL(XmcTexture);

NS_XMC_END
