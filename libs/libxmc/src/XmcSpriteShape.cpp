#include "XmcFactory.h"
#include "foundation/FileHelper.h"
#include "shaders/Shaders.h"
#include <cocos2d.h>

USING_NS_CC;
NS_XMC_BEGIN

class XmcSpriteShapeImpl : public XmcShape {
public:
  class XmcSprite : public XmcManagedObjectNode<cocos2d::Sprite> {
  public:
    XmcSprite(XmcSymbol* s = nullptr);
    XmcSprite(const XmcSymbol* s);
    virtual ~XmcSprite();
    virtual void visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags) override;
    virtual void draw(Renderer* renderer, const Mat4& transform, uint32_t flags) override;
    virtual cocos2d::Rect getBoundingBox() const override;
    
    virtual bool isVisible() const override { return _visible_input; }
    virtual void setVisible(bool visible) override {
      _visible_input = visible;
      cocos2d::Sprite::setVisible(visible);
    }
    virtual GLubyte getOpacity() const override { return _opacity_input; }
    virtual void setOpacity(GLubyte opacity) override {
      _opacity_input = opacity;
      affectColorTransform();
    }
    virtual const Color3B& getColor() const override { return _color_input; }
    virtual void setColor(const Color3B& color) override {
      _color_input = color;
      affectColorTransform();
    }
    
    void affectColorTransform();

  private:
    bool _visible_input;
    GLubyte _opacity_input;
    Color3B _color_input;
  };
  
  XMC_DECLARE_ASREF_BY_NODE_IMPL();
  
public:
  XmcSpriteShapeImpl();
  virtual ~XmcSpriteShapeImpl();
  
  virtual void init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) override;
  virtual void initWithSpriteFramename(XmcShader::Ptr shader, XmcTexturePart::Ptr texture, const std::string& spriteFramename) override;
  virtual XmcSymbol::Ptr clone() const override;
  
  virtual cocos2d::Size setupContentSize() override;
  
  virtual void setAdditionalColorTransform(const ColorTransform& additionalColorTransform) override;
  
  virtual void setBlendMode(bmc::BmcBlendMode blendMode) override {
    xmc_assert(false, "test needs!");
    _blendMode = blendMode;
  }
  
  virtual const cocos2d::Node* asNode() const override { return _sprite; }
  virtual cocos2d::Node* asNode() override { return _sprite; }
  
  virtual void* query(IIDType iid) override {
    if (iid == XmcShape::iid()) { // XmcShape 을 상속받으므로 XmcSpriteShape 가 아니라 XmcShape 타입을 리턴
      return static_cast<XmcShape*>(this);
    }
    if (iid == XmcSymbol::iid()) {
      return static_cast<XmcSymbol*>(this);
    }
#ifdef XMC_BMC_COMPATIBLE
    if (iid == "BmcShape@xmc") {
      return static_cast<XmcShape*>(this);
    }
#endif
    //xmc_assert(false); //ok
    return nullptr;
  }
  
private:
  //void onDraw(const Mat4& transform, uint32_t flags);
  
private:
  XmcShader::Ptr _shader;
  XmcTexturePart::Ptr _texture;
  float _vertices[8];
  float _texCoords[8];
  bmc::BmcBlendMode _blendMode;
  cocos2d::CustomCommand _customCommand;
  
//  bool _visible_input;
//  GLubyte _opacity_input;
//  Color3B _color_input;
  std::string _spriteFrameName;
  
  mutable XmcSprite* _sprite;
};

XmcSpriteShapeImpl::XmcSpriteShapeImpl() {
  _blendMode = bmc::BmcBlendMode_Normal;
  _sprite = new XmcSprite(this);
}

XmcSpriteShapeImpl::~XmcSpriteShapeImpl() {
}

void XmcSpriteShapeImpl::init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) {
  _sprite->initWithTexture(texture->getTexture()->getCCTexture2D());
  _shader = shader;
  _texture = texture;
  _spriteFrameName = "";
  _sprite->setAnchorPoint(Vec2::ZERO);
}

void XmcSpriteShapeImpl::initWithSpriteFramename(XmcShader::Ptr shader, XmcTexturePart::Ptr texture, const std::string& spriteFramename) {
  _spriteFrameName = spriteFramename;
  _sprite->initWithSpriteFrameName(_spriteFrameName);
  _shader = shader;
  _texture = texture;
  const auto path = cocos2d::Director::getInstance()->getTextureCache()->getTextureFilePath(_sprite->getTexture());
  const auto ext = filehelper::extname(path);
  if (ext == "pvrz" || ext == "ktxz") {
    // 우선 pvrz or ktxz의 경우 오른쪽 절반 마스크 텍스처로 처리???
    _sprite->setGLProgram(cocos2d::GLProgramCache::getInstance()->getGLProgram(kCCShader_PositionTextureColor_AlphaMask_noMVP));
  }
  _sprite->setAnchorPoint(Vec2::ZERO);
}

XmcSymbol::Ptr XmcSpriteShapeImpl::clone() const {
  XmcSpriteShapeImpl* newP = new XmcSpriteShapeImpl();
  if (_spriteFrameName != "") {
    newP->initWithSpriteFramename(_shader, _texture, _spriteFrameName);
  } else {
    newP->init(_shader, _texture);
  }
  newP->_additionalColorTransform = _additionalColorTransform;
  newP->_blendMode = _blendMode;
  XmcSymbol::Ptr ptr = newP;
  ptr->asRef()->release();
  return ptr; //skc static_cast<XmcSymbol*>(newP);
}

Size XmcSpriteShapeImpl::setupContentSize() {
  xmc_assert(false);
  return _sprite->getContentSize();
}

void XmcSpriteShapeImpl::setAdditionalColorTransform(const ColorTransform& additionalColorTransform) {
  if (_additionalColorTransform == additionalColorTransform) {
    return;
  }
  _additionalColorTransform = additionalColorTransform;
  _sprite->affectColorTransform();
}

// XmcSpriteShapeImpl::XmcSprite
//

XmcSpriteShapeImpl::XmcSprite::XmcSprite(XmcSymbol* s)
: XmcManagedObjectNode<cocos2d::Sprite>(s)
, _visible_input(true)
, _opacity_input(255)
, _color_input(cocos2d::Color3B::WHITE) {
}

XmcSpriteShapeImpl::XmcSprite::XmcSprite(const XmcSymbol* s)
: XmcManagedObjectNode<cocos2d::Sprite>(const_cast<XmcSymbol*>(s)) {
  xmc_assert(false);
}

XmcSpriteShapeImpl::XmcSprite::~XmcSprite() {
}

void XmcSpriteShapeImpl::XmcSprite::affectColorTransform() {
  XmcSpriteShapeImpl* symbol = dynamic_cast<XmcSpriteShapeImpl*>(getSymbol());
  ColorTransform ct = symbol->getAscendColorTransform();
  const auto& mul = ct.multiplier();
  const auto& ofs = ct.offset();
  
  GLubyte opacity = _opacity_input * mul[3] + 255 * ofs[3];
  bool visible = opacity > 0;
  if (visible && _visible_input) {
    cocos2d::Sprite::setVisible(true);
  } else {
    cocos2d::Sprite::setVisible(false);
    return;
  }
  cocos2d::Sprite::setColor({(GLubyte)(_color_input.r * mul[0])
    , (GLubyte)(_color_input.g * mul[1])
    , (GLubyte)(_color_input.b * mul[2])
  });
  cocos2d::Sprite::setOpacity(opacity);
}

void XmcSpriteShapeImpl::XmcSprite::visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags) {
  this->affectColorTransform();
  cocos2d::Sprite::visit(renderer, parentTransform, parentFlags);
}

void XmcSpriteShapeImpl::XmcSprite::draw(Renderer* renderer, const Mat4& transform, uint32_t flags) {
  const XmcSpriteShapeImpl* symbol = dynamic_cast<XmcSpriteShapeImpl*>(getSymbol());
  Mat4 offset = Mat4::IDENTITY;
  offset.m[12] = symbol->_texture->getOffset().x;
  offset.m[13] = symbol->_texture->getOffset().y;
  cocos2d::Sprite::draw(renderer, transform * offset, flags);
}

cocos2d::Rect XmcSpriteShapeImpl::XmcSprite::getBoundingBox() const {
  const XmcSpriteShapeImpl* symbol = dynamic_cast<const XmcSpriteShapeImpl*>(getSymbol());
  //const XmcSpriteShapeImpl* symbol = getSymbol()->query<XmcShape>(); // const compile error!
  return cocos2d::RectApplyTransform(
                                     { symbol->_vertices[0], symbol->_vertices[1], _contentSize.width, _contentSize.height}
                                     , getNodeToParentTransform()
                                     );
}

//XMC_FACTORY_CREATE_FUNC_IMPL(XmcSpriteShape);
XmcShape::Ptr XmcFactory::createXmcSpriteShape() {
  XmcShape::Ptr p = new XmcSpriteShapeImpl();
  p->asRef()->release();
  return p;
}

NS_XMC_END
