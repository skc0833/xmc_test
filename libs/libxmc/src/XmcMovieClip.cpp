#include "XmcMovieClipAdapter.h"
#include "XmcFactory.h"

USING_NS_CC;
NS_XMC_BEGIN

class XmcMovieClipImpl : public XmcMovieClipAdapter {
public:
  XmcMovieClipImpl();
  virtual ~XmcMovieClipImpl();
  
  class XmcNode : public XmcManagedObjectNode<cocos2d::Node> {
  public:
    XmcNode(XmcSymbol* s);
    XmcNode(const XmcSymbol* s);
    virtual ~XmcNode();
  };

  void addChild(XmcSymbol::Ptr child, int localZOrder, int tag);
  
  virtual void setAdditionalColorTransform(const ColorTransform& additionalColorTransform) override;
  
  virtual void* query(IIDType iid) override {
    if (iid == XmcMovieClip::iid()) { // "\x10XmcMovieClip@xmc"
      return static_cast<XmcMovieClip*>(this);
    }
    if (iid == XmcSymbol::iid()) {
      return static_cast<XmcSymbol*>(this);
    }
#ifdef XMC_BMC_COMPATIBLE
    if (iid == "BmcMovieClip@xmc") {
      return static_cast<XmcMovieClip*>(this);
    }
#endif
    //xmc_assert(false); //ok
    return nullptr;
  }

protected:
  virtual XmcSymbol* callNewAndNodeInit() const override;
  virtual bool callNodeInit() override;
  
private:
  void applyAdditionalColorTransformToChild(cocos2d::Node* child) const;
};

XmcMovieClipImpl::XmcMovieClipImpl()
: XmcMovieClipAdapter()
{
  _node = new XmcNode(this);
}

XmcMovieClipImpl::~XmcMovieClipImpl() {
}

XmcSymbol* XmcMovieClipImpl::callNewAndNodeInit() const {
  XmcMovieClipAdapter* p = new XmcMovieClipImpl();
  p->callNodeInit();
  return p;
}

bool XmcMovieClipImpl::callNodeInit() {
  return asNode()->init();
}

XmcMovieClipImpl::XmcNode::XmcNode(XmcSymbol* s)
: XmcManagedObjectNode<cocos2d::Node>(s) {
}

XmcMovieClipImpl::XmcNode::XmcNode(const XmcSymbol* s)
: XmcManagedObjectNode<cocos2d::Node>(const_cast<XmcSymbol*>(s)) {
  xmc_assert(false);
}

XmcMovieClipImpl::XmcNode::~XmcNode() {
}

void XmcMovieClipImpl::addChild(XmcSymbol::Ptr child, int localZOrder, int tag) {
  asNode()->addChild(child->asNode(), localZOrder, tag);
  applyAdditionalColorTransformToChild(child->asNode());
}

// 자식들에도 색상을 적용해줘야 함
void XmcMovieClipImpl::setAdditionalColorTransform(const ColorTransform& additionalColorTransform) {
  //skc called from XmcMovieClipImpl::updateFrameForView()
  XmcSymbol::setAdditionalColorTransform(additionalColorTransform);
  
  // NOTE: Apply color, opacity to child other than BmcSymbol
  for (auto child : asNode()->getChildren()) {
    applyAdditionalColorTransformToChild(child);
  }
}

// NOTE: When asNode()-> addChild a Node other than BmcSymbol to BmcMovieClip, apply the current color and opacity
void XmcMovieClipImpl::applyAdditionalColorTransformToChild(cocos2d::Node* child) const {
  if (areYouSymbol(child)) {
    return;
  }
  //xmc_assert(false); // todo check
  // If offset (coloring) is set, there is priority
  const float* mul;
  int opacity;
  if (_additionalColorTransform.offset()[0]) {
    //xmc_assert(false); //???
    mul = _additionalColorTransform.offset();
    opacity = 255 * (1.f - mul[3]);
  } else {
    mul = _additionalColorTransform.multiplier();
    opacity = 255 * mul[3];
  }
  if (auto lbl = dynamic_cast<cocos2d::Label*>(child)) {
    cocos2d::Color4B color;
    if (mul[0] == 1.f && mul[1] == 1.f && mul[2] == 1.f) {
      color = lbl->getTextColor();
    } else {
      color.r = 255 * mul[0];
      color.g = 255 * mul[1];
      color.b = 255 * mul[2];
    }
    color.a = opacity;
    lbl->setTextColor(color);
  } else {
    child->setOpacity(opacity);
    cocos2d::Color3B color;
    color.r = 255 * mul[0];
    color.g = 255 * mul[1];
    color.b = 255 * mul[2];
    child->setColor(color);
  }
}

XMC_FACTORY_CREATE_FUNC_IMPL(XmcMovieClip);

NS_XMC_END
