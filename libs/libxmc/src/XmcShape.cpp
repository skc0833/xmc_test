#include "XmcShape.h"
#include "XmcFactory.h"
#include <cocos2d.h>

USING_NS_CC;
NS_XMC_BEGIN

class XmcShapeImpl : public XmcShape {
public:
  class XmcSprite : public XmcManagedObjectNode<cocos2d::Sprite>  {
  public:
    XmcSprite(XmcSymbol* s = nullptr) : XmcManagedObjectNode<cocos2d::Sprite>(s) {}
    XmcSprite(const XmcSymbol* s) : XmcManagedObjectNode<cocos2d::Sprite>(const_cast<XmcSymbol*>(s)) {}
    virtual ~XmcSprite(); // = default;
    virtual void draw(Renderer* renderer, const Mat4& transform, uint32_t flags) override;
    virtual cocos2d::Rect getBoundingBox() const override;
  };
  
  XMC_DECLARE_ASREF_BY_NODE_IMPL();

public:
  XmcShapeImpl();
  virtual ~XmcShapeImpl();
  
  virtual void init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) override;
  virtual void initWithSpriteFramename(XmcShader::Ptr shader, XmcTexturePart::Ptr texture, const std::string& spriteFramename) override {
    xmc_assert(false, "not impl");
  }
  virtual XmcSymbol::Ptr clone() const override;
  
  virtual cocos2d::Size setupContentSize() override;

  virtual void setBlendMode(bmc::BmcBlendMode blendMode) override {
    _blendMode = blendMode;
  }
  
  // do nothing! called from XmcMovieClipAdapter::iterateInstancesRecursively()
  virtual void iterateInstancesRecursively(std::function<void(XmcSymbol*)> func) override {}

  virtual const cocos2d::Node* asNode() const override { return _sprite; }
  virtual cocos2d::Node* asNode() override { return _sprite; }
  
  virtual void* query(IIDType iid) override {
    if (iid == XmcShape::iid()) {
      return static_cast<XmcShape*>(this);
    }
    if (iid == XmcSymbol::iid()) {
      return static_cast<XmcSymbol*>(this);
    }
#ifdef XMC_BMC_COMPATIBLE
    if (iid == "BmcShape@xmc") {
      return static_cast<XmcShape*>(this);
    }
#endif
    //xmc_assert(false); //ok
    return nullptr;
  }

private:
  void setupVertices();
  void onDraw(const Mat4& transform, uint32_t flags);

private:
  XmcShader::Ptr _shader;
  XmcTexturePart::Ptr _texture;
  float _vertices[8];
  float _texCoords[8];
  bmc::BmcBlendMode _blendMode;
  cocos2d::CustomCommand _customCommand;
  
  mutable XmcSprite* _sprite;
};

XmcShapeImpl::XmcShapeImpl()
: _blendMode(bmc::BmcBlendMode_Normal)
{
  _sprite = new XmcSprite(this);
}

XmcShapeImpl::~XmcShapeImpl() {
  xmc_assert(asNode()->getReferenceCount() == 0);
}

XmcShapeImpl::XmcSprite::~XmcSprite() {
}

void XmcShapeImpl::init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) {
  _sprite->initWithTexture(texture->getTexture()->getCCTexture2D());
  _shader = shader;
  _texture = texture;
  _sprite->setAnchorPoint(Vec2::ZERO);
  setupVertices();
}

XmcSymbol::Ptr XmcShapeImpl::clone() const {
  XmcShapeImpl* newP = new XmcShapeImpl();
  newP->init(_shader, _texture);
  newP->_additionalColorTransform = _additionalColorTransform;
  newP->_blendMode = _blendMode;
  newP->_renderOnFBO = _renderOnFBO;
  XmcSymbol::Ptr ptr = newP;
  ptr->asRef()->release();
  return ptr; // ref = 1
}

void XmcShapeImpl::setupVertices() {
#if 1
  // Shape, Bitmap 객체의 경우, Flash 좌표계에서 부모의 기준점 (0,0)에서(center) left-top 이 (-50, -25) 인 경우
  double offsetX = double(_texture->getRectangle().x() / CC_CONTENT_SCALE_FACTOR()); // -50
  double offsetY = double(_texture->getRectangle().y() / CC_CONTENT_SCALE_FACTOR()); // -25
  double clipW = double(_texture->getRectangle().width() / CC_CONTENT_SCALE_FACTOR()); // 100
  double clipH = double(_texture->getRectangle().height() / CC_CONTENT_SCALE_FACTOR()); // 50

/*
(-50,-25) _____________
         |             |
         |      +-->   | 50   flash coord
         |      |      |
          -------------
               100

 (-50,25) _____________ (50,25)
         |      |      |
         |      +-->   |      cocos2d coord(Anchor = Zero(left-bottom))
         |             |
(-50,-25) ------------- (50,-25)

*/
  
  // Flash 좌표계(4사분면) -> cocos2d 좌표계(1사분면)
  _vertices[0] = float(0.f + offsetX);      // -50  -> left-bottom
  _vertices[1] = float(-clipH - offsetY);   // -25   (-50 - (-25)) -> 아래 left-top 위치에서 -height
  
  _vertices[2] = float(clipW + offsetX);    // 50   -> right-bottom
  _vertices[3] = float(-clipH - offsetY);   // -25  -> 아래 right-top 위치에서 -height
  
  _vertices[4] = float(0.f + offsetX);      // -50  -> left-top
  _vertices[5] = float(0.f - offsetY);      // 25
  
  _vertices[6] = float(clipW + offsetX);    // 50  -> right-top
  _vertices[7] = float(0.f - offsetY);      // 25
#else
  double offsetX = double(_texture->getRectangle().x() / CC_CONTENT_SCALE_FACTOR()); // -50
  double offsetY = double(-_texture->getRectangle().y() / CC_CONTENT_SCALE_FACTOR()); // 25 flipY
  double clipW = double(_texture->getRectangle().width() / CC_CONTENT_SCALE_FACTOR()); // 100
  double clipH = double(_texture->getRectangle().height() / CC_CONTENT_SCALE_FACTOR()); // 50

  _vertices[0] = float(0.f + offsetX);      // -50  -> left-bottom
  _vertices[1] = float(-clipH + offsetY);   // -25  (-50 + 25)
  
  _vertices[2] = float(clipW + offsetX);    // 50   -> right-bottom
  _vertices[3] = float(-clipH + offsetY);   // -25
  
  _vertices[4] = float(0.f + offsetX);      // -50  -> left-top
  _vertices[5] = float(0.f + offsetY);      // 25
  
  _vertices[6] = float(clipW + offsetX);    // 50   -> right-top
  _vertices[7] = float(0.f + offsetY);      // 25
#endif
  
  // In OpenGL, texture coordinates are sometimes referred to in coordinates (s, t) instead of (x, y).
  // (s, t) represents a texel on the texture, which is then mapped to the polygon.
  // 보통 이미지 데이터는 UV 좌표계에서 낮은 주소부터 (0,0) 위치부터 저장돼 있으므로??? polygon 좌표에 매핑시 y축으로 뒤짚어줘야 한다.
  /*
   1:(0,1)---(1,1):2
       |       |
       |       |
   3:(0,0)---(1,0):4
   */
  _texCoords[0] = 0.f;    // left-top
  _texCoords[1] = 1.f;
  
  _texCoords[2] = 1.f;    // right-top
  _texCoords[3] = 1.f;
  
  _texCoords[4] = 0.f;    // left-bottom
  _texCoords[5] = 0.f;
  
  _texCoords[6] = 1.f;    // right-bottom
  _texCoords[7] = 0.f;
}

Size XmcShapeImpl::setupContentSize() {
  xmc_assert(false);
  return Size(
              _texture->getRectangle().width() / CC_CONTENT_SCALE_FACTOR(),
              _texture->getRectangle().height() / CC_CONTENT_SCALE_FACTOR()
              );
}

// XmcShapeImpl::XmcSprite
//

// Node::visit() -> processParentFlags() -> transform() -> getNodeToParentTransform() 에서
// 현재 노드의 _additionalTransform 값과 부모(anchor zero)의 parentTransform(0, 1136)를 곱해서 draw() 함수의 transform 인자로 전달되고 있다.
// Flash 에서 심볼의 중심점이 (0, 1136) 위치에서 y축으로 내려온 위치가 (0, 0) 기준으로 변환된 좌표로 transform 에 전달된다(e.g, (275, 270) -> (275, 936 (1136-200)))
void XmcShapeImpl::XmcSprite::draw(Renderer* renderer, const Mat4& transform, uint32_t flags) {
  if (this->isVisible() == false) {
    xmc_assert(false);
    return;
  }
  
  XmcShapeImpl* symbol = getSymbol()->query<XmcShapeImpl>(); // XmcShape::iid()로 비교 후, XmcShapeImpl 로 casting 됨

  if (flags & FLAGS_DIRTY_MASK) {
    // FIXME I'm having problems with scaling
    Mat4 offset = Mat4::IDENTITY;
    offset.m[12] = symbol->_texture->getOffset().x;
    offset.m[13] = symbol->_texture->getOffset().y;
    // 좌상단 위치(transform * offset)와 사이즈로 포함여부 계산
    Sprite::_insideBounds = renderer->checkVisibility(transform * offset, Sprite::_contentSize);
  } else {
    //xmc_assert(false); //ok
  }
  if (!_insideBounds) {
    //xmc_assert(false); //ok
    return;
  }
  
  // TODO I want to change to setGLProgram
  symbol->_customCommand.init(_globalZOrder);
  symbol->_customCommand.func = CC_CALLBACK_0(XmcShapeImpl::onDraw, symbol, transform, flags);
  renderer->addCommand(&symbol->_customCommand);
}

cocos2d::Rect XmcShapeImpl::XmcSprite::getBoundingBox() const {
  const XmcShapeImpl* symbol = dynamic_cast<const XmcShapeImpl*>(getSymbol());
  //const XmcShapeImpl* symbol = getSymbol()->query<XmcShape>(); // const compile error!

  return cocos2d::RectApplyTransform(
    { symbol->_vertices[0], symbol->_vertices[1], _contentSize.width, _contentSize.height}
    , getNodeToParentTransform()
  );
}

void XmcShapeImpl::onDraw(const Mat4& transform, uint32_t flags) {
  Director::getInstance()->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW); //skc instead of _director
  Director::getInstance()->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);
  
  ColorTransform my = getAscendColorTransform();
  
  if (_blendMode == bmc::BmcBlendMode_Add) {
    GL::blendFunc(GL_ONE, GL_ONE);
    if (_renderOnFBO) {
      xmc_assert(false);
      glBlendFuncSeparate(GL_ONE, GL_ONE, GL_SRC_ALPHA, GL_ONE);
    } else {
      glBlendFunc(GL_ONE, GL_ONE);
    }
  } else {
    GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    if (_renderOnFBO) {
      xmc_assert(false);
      glBlendFuncSeparate(GL_ONE, GL_ONE_MINUS_SRC_ALPHA, GL_SRC_ALPHA, GL_ONE);
    } else {
      glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    }
  }
  
  _shader->setColorTransform(my);
  _shader->useMostValuableProgram();
  _shader->setTexture(_texture);
  
  GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_TEX_COORD);
  glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, _vertices);
  glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORDS, 2, GL_FLOAT, GL_FALSE, 0, _texCoords);

  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
  
  CHECK_GL_ERROR_DEBUG();
  Director::getInstance()->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
}

XMC_FACTORY_CREATE_FUNC_IMPL(XmcShape);

NS_XMC_END
