#include "XmcSymbol.h"
#include "XmcFactory.h"
#include "XmcNullSymbol.h"

// implement instead of BmcSymbolDefaultImpl
NS_XMC_BEGIN

XmcSymbol::XmcSymbol()
: _additionalColorTransform(ColorTransform::Identity())
{}

XmcSymbol::~XmcSymbol()
{
}

// 전부 디폴트로 처리하고 XmcNullSymbol, XmcNullMovieClip 만 true 로 리턴하면 되겠다.
bool XmcSymbol::operator == (const std::nullptr_t) const {
  //xmc_assert(false); //ok
  return false;
}

bool XmcSymbol::operator != (const std::nullptr_t) const {
  //xmc_assert(false); //ok
  return true;
}

XmcSymbol::operator bool() const {
  //xmc_assert(false); //ok
  return true;
}

cocos2d::Size XmcSymbol::setupContentSize() {
  xmc_assert(false);
  return cocos2d::Size::ZERO;
}

void XmcSymbol::setBlendMode(bmc::BmcBlendMode blendMode) {
  xmc_assert(false);
}

void XmcSymbol::setAdditionalTransform(const cocos2d::Mat4& additionalTransform) {
#if 1 //skc test 원래는 XmcShapeImpl, XmcMovieClipImpl 와 구현이 약간 달랐으나 결국은 아래와 같은 코드였음
  auto t = const_cast<cocos2d::Mat4*>(&additionalTransform);
  asNode()->setAdditionalTransform(t);
#endif
}

void XmcSymbol::setAdditionalTransform(const cocos2d::AffineTransform& additionalTransform) {
  asNode()->setAdditionalTransform(additionalTransform);
}

const cocos2d::Mat4& XmcSymbol::getAdditionalTransform() const {
  const cocos2d::Node* node = asNode();
  const XmcSymbol::XmcManagedObject* obj = dynamic_cast<const XmcSymbol::XmcManagedObject*>(node);
  if (obj) {
    return obj->getAdditionalTransform();
  } else {
    xmc_assert(false);
    return cocos2d::Mat4::IDENTITY;
  }
}

void XmcSymbol::clearAdditionalTransform() {
  return asNode()->setAdditionalTransform(cocos2d::AffineTransform::IDENTITY);
}

// Overwrite the color change setting specified in Flash
void XmcSymbol::setAdditionalColorTransform(const ColorTransform& additionalColorTransform) {
  _additionalColorTransform = additionalColorTransform;
}

ColorTransform XmcSymbol::getAscendColorTransform() const {
  ColorTransform ct = getAdditionalColorTransform();
  for (auto parent = asNode()->getParent(); parent; parent = parent->getParent()) {
    const XmcSymbol* p = areYouSymbol(parent);
    if (p) {
      ct = p->getColorTransform().mutiply(ct); //skc todo getColorTransform() 를 getAdditionalColorTransform() 로 대체하자!!!
    }
  }
  return ct;
}

const cocos2d::Node* XmcSymbol::asNode() const {
  xmc_assert(false);
  return nullptr;
}

cocos2d::Node* XmcSymbol::asNode() {
  xmc_assert(false);
  return nullptr;
}

cocos2d::Rect XmcSymbol::getCurrentFrameBoundingBox() const {
  return asNode()->getBoundingBox();
}

bool XmcSymbol::inHitArea(cocos2d::Point point) {
  auto node = asNode();
  if (node == nullptr || node->getParent() == nullptr) {
    xmc_assert(false);
    return false;
  }
  point = node->getParent()->convertToNodeSpace(point);
  auto hitArea = getCurrentFrameBoundingBox();
  return hitArea.containsPoint(point);
}

XmcSymbol::Ptr XmcSymbol::getInstanceImpl(const std::string& instanceName) const {
  xmc_assert(false);
  return getNullObject();
}

XmcSymbol::Ptr XmcSymbol::getNullObject() const {
  return new XmcNullSymbol();
}

XmcSymbol::Ptr XmcSymbol::destroyInstance(const std::string& instanceName) {
  xmc_assert(false);
  return getNullObject();
}

XmcSymbol::Ptr XmcSymbol::destroyInstance(XmcSymbol::Ptr instance) {
  xmc_assert(false);
  return getNullObject();
}

static bool isCascadeVisible(cocos2d::Node* node) {
  while (node) {
    if (!node->isVisible()) {
      return false;
    }
    node = node->getParent();
  }
  return true;
}

void XmcSymbol::setTouchBlockEnabled(bool enabled) {
  if (enabled) {
    if (_touchBlockListener != nullptr) {
      xmc_assert(false);
      return;
    }
    _touchBlockListener = cocos2d::EventListenerTouchOneByOne::create();
    _touchBlockListener->setSwallowTouches(true);
    
    _touchBlockListener->onTouchBegan = [=](cocos2d::Touch* touch, cocos2d::Event* event) {
      auto node = asNode();
      if (node == nullptr || !isCascadeVisible(node)) {
        xmc_assert(false);
        return false;
      }
      auto loc = touch->getLocation();
      auto bb = getCurrentFrameBoundingBox();
      bb.origin = node->getParent()->convertToWorldSpace(bb.origin);
      return bb.containsPoint(loc);
    };
    cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touchBlockListener, asNode());
  } else {
    if (_touchBlockListener == nullptr) {
      return;
    }
    cocos2d::Director::getInstance()->getEventDispatcher()->removeEventListener(_touchBlockListener);
    _touchBlockListener = nullptr;
  }
}

void XmcSymbol::setColorAdd(const cocos2d::Color4F c) {
  float* m = _additionalColorTransform.mutableOffset();
  m[0] = c.r;
  m[1] = c.g;
  m[2] = c.b;
  m[3] = c.a;
}

cocos2d::Color4F XmcSymbol::getColorAdd() const {
  const float* m = _additionalColorTransform.offset();
  return cocos2d::Color4F(m[0], m[1], m[2], m[3]);
}

void XmcSymbol::setColorMultiply(const cocos2d::Color4F c) {
  float* m = _additionalColorTransform.mutableMultiplier();
  m[0] = c.r;
  m[1] = c.g;
  m[2] = c.b;
  m[3] = c.a;
}

NS_XMC_END
