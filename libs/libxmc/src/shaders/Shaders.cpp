#include "Shaders.h"
#include "../../include/foundation/Log.h"

using namespace cocos2d;
NS_XMC_BEGIN

#if 0 //skc
const GLchar * ccVector_PositionColor_frag =
#include "ccShader_Vector_PositionColor_frag.h"
;
const GLchar * ccVector_PositionColor_vert =
#include "ccShader_Vector_PositionColor_vert.h"
;
#endif //0

//
const GLchar* ccVector_PositionTextureColor_vert=
"attribute vec4 a_position;\n"
"attribute vec2 a_texCoord;\n"
/*"uniform   highp   mat4 u_worldMatrix;"*/
"varying   vec2 v_texCoord;\n"
"void main()\n"
"{\n"
"gl_Position = CC_MVPMatrix * a_position;\n"
"v_texCoord = a_texCoord;\n"
"}\n";

const GLchar * ccVector_PositionTextureColorUseOffsetAlpha_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform vec4      u_colorMultiplier;\n"
"uniform vec4      u_colorOffset;\n"
"void main()\n"
"{\n"
"vec4 texcolor = texture2D(u_texture, v_texCoord);\n"
"if(texcolor.a>0.0001){"
"texcolor = vec4( texcolor.rgb/texcolor.a, texcolor.a );\n"
"}"
"texcolor = texcolor*u_colorMultiplier + u_colorOffset;\n"
"gl_FragColor = vec4( texcolor.rgb*texcolor.a, texcolor.a );\n"
"}\n";

const GLchar * ccVector_PositionTextureColorNoOffsetAlpha_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform vec4      u_colorMultiplier;\n"
"uniform vec4      u_colorOffset;\n"
"void main()\n"
"{\n"
"vec4 texcolormul = texture2D(u_texture, v_texCoord)*u_colorMultiplier;\n"
"gl_FragColor = vec4( texcolormul.rgb*u_colorMultiplier.a + u_colorOffset.rgb*texcolormul.a, texcolormul.a );\n"
"}\n";

const GLchar * ccVector_PositionTextureColorNoOffsetOnlyMultiplierColor_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform vec4      u_colorMultiplier;\n"
"void main()\n"
"{\n"
"vec4 texcolormul = texture2D(u_texture, v_texCoord)*u_colorMultiplier;\n"
"gl_FragColor = vec4( texcolormul.rgb*u_colorMultiplier.a, texcolormul.a );\n"
"}\n";

const GLchar * ccVector_PositionTextureColorNoOffsetOnlyMultiplierAlpha_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform float     u_alphaMultiplier;\n"
"void main()\n"
"{\n"
"gl_FragColor = texture2D(u_texture, v_texCoord)*u_alphaMultiplier;\n"
"}\n";

const GLchar * ccVector_PositionTextureColorOnlyTexture_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"void main()\n"
"{\n"
"gl_FragColor = texture2D(u_texture, v_texCoord);\n"
"}\n";

static const GLchar* kPositionTexture_AlphaMask_frag = R"(
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;       // 外部のカラー値(setColorとsetOpacityでセットされたもの)
varying vec2 v_texCoord;            // テクスチャーのUV座標

void main()
{
  // RGBはテクスチャーのものを使用
  gl_FragColor.rgb = texture2D(CC_Texture0, v_texCoord).rgb;
  // 右半分のマスクの色をアルファとして使用
  gl_FragColor.a = texture2D(CC_Texture0, vec2(v_texCoord.x + 0.5, v_texCoord.y)).r;
  // 外部で設定したカラーを適応
  gl_FragColor *= v_fragmentColor;
}
)";

// ccShader_UI_Gray.frag
static const char* kPositionTexture_GrayScale_AlphaMask_frag = R"(
#ifdef GL_ES
precision mediump float;
#endif

//varying vec4 v_fragmentColor;       // 外部のカラー値(setColorとsetOpacityでセットされたもの)
varying vec2 v_texCoord;            // テクスチャーのUV座標

void main(void)
{
  vec4 c = texture2D(CC_Texture0, v_texCoord);
  gl_FragColor.xyz = vec3(0.2126*c.r + 0.7152*c.g + 0.0722*c.b);
  gl_FragColor.w = c.w;
  
  // 右半分のマスクの色をアルファとして使用
  gl_FragColor.a = texture2D(CC_Texture0, vec2(v_texCoord.x + 0.5, v_texCoord.y)).r;
}
)";


const char *const Uniform_ColorMultiplierIdentify = "u_colorMultiplier";
const char *const Uniform_ColorOffsetIdentify = "u_colorOffset";
const char *const Uniform_AlphaMultiplierIdentify = "u_alphaMultiplier";
const char *const kCCShader_Vector_PositionTextureColor_UseOffsetAlpha = "Vector_PositionTextureColor_UseOffsetAlpha.xmc";
const char *const kCCShader_Vector_PositionTextureColor_NoOffsetAlpha = "Vector_PositionTextureColor_NoOffsetAlpha.xmc";
const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor = "Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor.xmc";
const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha = "Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha.xmc";
const char *const kCCShader_Vector_PositionTextureColor_OnlyTexture = "Vector_PositionTextureColor_OnlyTexture.xmc";
const char *const kCCShader_PositionTextureColor_AlphaMask_noMVP = "PositionTextureColor_AlphaMask_noMVP.xmc";
const char *const kCCShader_PositionTextureColor_GrayScale_AlphaMask_noMVP = "PositionTextureColor_GrayScale_AlphaMask_noMVP.xmc";

void addShaders(GLProgramCache *shaderCache)
{
  GLProgram *p;
  // FlashのColorTransformを完全に再現させるためのシェーダ。cocos2dのtextureデータがプリマルチプライドになってしまうのでいったん割り算してからやり直すというとても非効率なシェーダ
  {
    p = new GLProgram();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorUseOffsetAlpha_frag);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);   // "a_position", 0
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD); // "a_texCoord", 2
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
    shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_UseOffsetAlpha);
    p->release();
  }
  // ColorTransformのうち、Add する alpha 値がなければ、プリマルチプライドのカラーを使用できるので、それに限定したシェーダ。出来るだけこれを使用したい。
  {
    p = new GLProgram();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetAlpha_frag);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
    shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetAlpha);
    p->release();
  }
  // ColorTransformのうち、Add するカラー値がない場合に使用する。実際、RGBについて、掛け算だけすることはあまりないので使用頻度はすくないかも
  {
    p = new GLProgram();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierColor_frag);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
    shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor);
    p->release();
  }
  // ColorTransformのうち、alpha の掛け算だけを使用するもの。一般にはこのくらいのケースが多いだろう。
  {
    p = new GLProgram();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierAlpha_frag);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
    shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha);
    p->release();
  }
  // 何もしないテクスチャカラーのみのもの。テクスチャカラーのみといってもプリマルチプライドなので、ブレンドモードは必要になる。
  {
    p = new GLProgram();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorOnlyTexture_frag);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
//    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
    shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_OnlyTexture); // "Vector_PositionTextureColor_OnlyTexture.xmc"
    p->release();
  }
  {
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, kPositionTexture_AlphaMask_frag);
    GLProgramCache::getInstance()->addGLProgram(program, kCCShader_PositionTextureColor_AlphaMask_noMVP);
  }
  {
    // ccPositionTexture_GrayScale_frag
    auto program = GLProgram::createWithByteArrays(ccPositionTextureColor_noMVP_vert, kPositionTexture_GrayScale_AlphaMask_frag);
    GLProgramCache::getInstance()->addGLProgram(program, kCCShader_PositionTextureColor_GrayScale_AlphaMask_noMVP);
  }
}

void reloadShaders(GLProgramCache *shaderCache)
{
  xmc_dlog("");
  GLProgram *p;
  // FlashのColorTransformを完全に再現させるためのシェーダ。cocos2dのtextureデータがプリマルチプライドになってしまうのでいったん割り算してからやり直すというとても非効率なシェーダ
  {
    p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_UseOffsetAlpha);
    p->reset();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorUseOffsetAlpha_frag);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
  }
  // ColorTransformのうち、Add する alpha 値がなければ、プリマルチプライドのカラーを使用できるので、それに限定したシェーダ。出来るだけこれを使用したい。
  {
    p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_NoOffsetAlpha);
    p->reset();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetAlpha_frag);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
  }
  // ColorTransformのうち、Add するカラー値がない場合に使用する。実際、RGBについて、掛け算だけすることはあまりないので使用頻度はすくないかも
  {
    p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor);
    p->reset();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierColor_frag);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
  }
  // ColorTransformのうち、alpha の掛け算だけを使用するもの。一般にはこのくらいのケースが多いだろう。
  {
    p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha);
    p->reset();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierAlpha_frag);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
  }
  // 何もしないテクスチャカラーのみのもの。テクスチャカラーのみといってもプリマルチプライドなので、ブレンドモードは必要になる。
  {
    p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_OnlyTexture);
    p->reset();
    p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorOnlyTexture_frag);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
    p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    p->link();
    p->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
  }
  {
    auto program = GLProgramCache::getInstance()->getGLProgram(kCCShader_PositionTextureColor_AlphaMask_noMVP);
    program->reset();
    program->initWithByteArrays(ccPositionTextureColor_noMVP_vert, kPositionTexture_AlphaMask_frag);
    program->link();
    program->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
  }
  {
    // ccPositionTexture_GrayScale_frag
    auto program = GLProgramCache::getInstance()->getGLProgram(kCCShader_PositionTextureColor_GrayScale_AlphaMask_noMVP);
    program->reset();
    program->initWithByteArrays(ccPositionTextureColor_noMVP_vert, kPositionTexture_GrayScale_AlphaMask_frag);
    program->link();
    program->updateUniforms();
    CHECK_GL_ERROR_DEBUG();
  }
}

NS_XMC_END
