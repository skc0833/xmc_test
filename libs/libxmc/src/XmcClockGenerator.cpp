#include "XmcClockGenerator.h"
#include "XmcFactory.h"
#include "foundation/Log.h"
#include "base/CCScheduler.h"

#if 0
#define DEBUG_LOG(...) { NS_XMC::logOutput(false, NS_XMC::LogPrefix_Debug,__func__,__FILE__,__LINE__,__VA_ARGS__); }
#else
#define DEBUG_LOG(...) ((void)0)
#endif

USING_NS_CC;

NS_XMC_BEGIN
  
XmcClockGenerator::XmcClockGenerator()
  : _scheduled(false), _stockTime(.0f) {
}

XmcClockGenerator::~XmcClockGenerator()
{
  DEBUG_LOG("%p", this);
  Director::getInstance()->getScheduler()->unscheduleUpdate(this);
  DEBUG_LOG("unscheduled: %p", this);
}

#if 0
//inline
bool XmcClockGenerator::operator == (const std::nullptr_t) const {
  xmc_assert(false);
  return false;
}
//inline
bool XmcClockGenerator::operator != (const std::nullptr_t) const {
  xmc_assert(false);
  return true;
}
//inline /*explicit*/
XmcClockGenerator::operator bool() const { // explicit should be declared class definition
  //xmc_assert(false); //ok
  return true;
}
#endif

void XmcClockGenerator::update(float delta) {
  // Considering the unregisterListener that occurred during the loop, unregister it in the next frame
  for (auto pair: _unregister_reservation) {
    auto mc = pair.first;
    _listeners.erase(std::remove(begin(_listeners), std::end(_listeners), mc), std::end(_listeners));
    DEBUG_LOG("removed: %p", mc);
  }
  _unregister_reservation.clear();
  
  if (_listeners.size() == 0) {
    Director::getInstance()->getScheduler()->unscheduleUpdate(this);
    _scheduled = false;
    DEBUG_LOG("unscheduled: %p", this);
    return;
  }
  
  _stockTime += delta * _speedScale;
  
  static int frame = 0;
  
  //DEBUG_LOG("%p:%d", this, _listeners.size());
  
  retain();
  while (_stockTime > _oneFrameDelta) {
    DEBUG_LOG("******************************************************************************************");
    DEBUG_LOG("%d: stockTime=%.5f delta=%.5f frameDelta=%.5f frame=%.0f",
              frame, _stockTime, delta, _oneFrameDelta, floor(_stockTime / _oneFrameDelta));
    _stockTime -= _oneFrameDelta;
    notify_listeners();
    frame++;
  }
  release();
}

void XmcClockGenerator::notify_listeners() {
  // NOTE Consider registerListener of another mc in processFrame
  auto copy_listeners = _listeners;
  for (auto mc: copy_listeners) {
    // NOTE Consider unregisterListener different mc in processFrame
    if (_unregister_reservation.find(mc) != std::end(_unregister_reservation)) {
      continue;
    }
    mc->processFrame();
  }
}

void XmcClockGenerator::registerListener(XmcMovieClip* mc) {
  if (mc == nullptr) {
    xmc_assert(false);
    return;
  }
  bool test_added = false;
  if (std::find(std::begin(_listeners), std::end(_listeners), mc) == std::end(_listeners)) {
#if 1 //skc push_back() 으로 추가시에는 자식들 먼저 호출되게 되므로 역순으로 저장시켜야 부모, 자식 순서로 제대로 재생된다.
    _listeners.insert(std::begin(_listeners), mc);
#else
    _listeners.push_back(mc);
#endif
    DEBUG_LOG("added: %p", mc);
    test_added = true;
  } else {
    //ok 이미 play() 중인 객체에 다시 play()가 호출될 수 있음(from XmcMovieClipImpl::updateFrameForView())
    //xmc_assert(false);
  }
  // play(), stop(), play() 순으로 연속 호출될 경우 처리(stop 무효화)
  // 이렇게 안하면 stop() 후 play() 가 되질 않는다.
  _unregister_reservation.erase(mc);
  
  if (!_scheduled && _listeners.size() > 0) {
    Director::getInstance()->getScheduler()->scheduleUpdate(this, /* priority = */ 1, /* paused = */ false);
    _scheduled = true;
    xmc_assert(test_added);
    DEBUG_LOG("scheduled: %p", this);
  }
}

void XmcClockGenerator::unregisterListener(XmcMovieClip* mc) {
  if (_unregister_reservation.find(mc) == std::end(_unregister_reservation)) {
    _unregister_reservation[mc] = true;
    DEBUG_LOG("reserved: %p", mc);
  }
}

void XmcClockGenerator::unregisterAllListener() {
  for (auto listener: _listeners) {
    _unregister_reservation[listener] = true;
  }
}

bool XmcClockGenerator::isRegistered(XmcMovieClip* mc) const {
  if (std::find(std::begin(_listeners), std::end(_listeners), mc) != std::end(_listeners)) {
    return true;
  }
  return false;
}

//XMC_FACTORY_CREATE_FUNC_IMPL(XmcClockGenerator); // can't use, it's not XmcClockGeneratorImpl
XmcClockGenerator::Ptr XmcFactory::createXmcClockGenerator() {
    XmcClockGenerator::Ptr p = new XmcClockGenerator();
    p->release();
    return p;
}

NS_XMC_END
