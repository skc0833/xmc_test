#include "XmcButton.h"
#include "XmcMovieClipAdapter.h"
#include "XmcFactory.h"
#include <cocos2d.h>

USING_NS_CC;
NS_XMC_BEGIN

#define DRAW_HITAREA      1

#define LABEL_UP          "up"
#define LABEL_UPNORMAL    "upnormal"
#define LABEL_DOWN        "down"
#define LABEL_DOWNNORMAL  "downnormal"
#define LABEL_DISABLE     "disable"

static std::unordered_map<XmcButton::LabelType, std::string> _label_map = {
  { XmcButton::LabelType_Up, LABEL_UP },
  { XmcButton::LabelType_UpNormal, LABEL_UPNORMAL },
  { XmcButton::LabelType_Down, LABEL_DOWN },
  { XmcButton::LabelType_DownNormal, LABEL_DOWNNORMAL },
  { XmcButton::LabelType_Disable, LABEL_DISABLE },
};

static const char* LableStr(XmcButton::LabelType label) {
  return _label_map[label].c_str();
}

class XmcButtonImpl : public XmcButton {
public:
  class XmcButtonNode : public XmcManagedObjectNode<cocos2d::Node> {
  public:
    XmcButtonNode(XmcSymbol* s = nullptr) : XmcManagedObjectNode<cocos2d::Node>(s) {}
    virtual ~XmcButtonNode() = default;
    virtual void onEnter() override;
    virtual void onExit() override;
    virtual void update(float delta) override;
    
  private:
    XmcButtonImpl* getButtonSymbol() {
      auto p = getSymbol();
      XmcButtonImpl* symbol = static_cast<XmcButtonImpl*>(p);
      xmc_assert(symbol);
      return symbol;
    }
  };

public:
  XmcButtonImpl();
  virtual ~XmcButtonImpl();

  virtual void init(XmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override;
  virtual XmcSymbol::Ptr clone() const override;
  
  virtual bool inHitArea(cocos2d::Point point) override;
  
  virtual void setEnabled(bool enabled) override;
  virtual void setTouchEnabled(bool enabled) override;
  virtual void setPushHandler(std::function<bool(XmcButton*)> handler) override {
    _handler = handler;
  }
  virtual void setPushingHandler(std::function<bool(XmcButton*)> handler) override {
    _pushingHandler = handler;
  }
  
  virtual void setCheckBoxMode(bool checkbox) override {
    _checkBoxMode = checkbox;
  }
  virtual void setCheck(bool check, bool playSound) override {
    setCheck(check, playSound, true);
  }
  virtual void setCheck(bool check, bool playSound, bool animated) override;
  virtual bool getCheck() const override {
    return _check;
  }
  
  virtual void setSwallowTouches(bool needSwallow) override {
    _touchListener->setSwallowTouches(needSwallow);
  }
  
  virtual void setPushSoundHandler(std::function<void(void)> soundPlay) override {
    _soundPlay = soundPlay;
  }
  
  virtual void setEventBlock(bool block) override {
    if (!_enabled) return;
    _touchListener->setEnabled(!block);
  }

  virtual void setBarrageBlockTime(float blockSec) override {
    _barrageBlockSec = blockSec;
  }
  
  virtual void touchDownAction() override;
  
  virtual void* query(IIDType iid) override {
    if (iid == XmcButton::iid()) {
      return static_cast<XmcButton*>(this);
    }
    if (iid == XmcMovieClip::iid()) {
      return static_cast<XmcMovieClip*>(this);
    }
    if (iid == XmcSymbol::iid()) {
      return static_cast<XmcSymbol*>(this);
    }
#ifdef XMC_BMC_COMPATIBLE
    if (iid == "BmcButton@xmc") {
      return static_cast<XmcButton*>(this);
    }
#endif
    //xmc_assert(false); //ok
    return nullptr;
  }
  
protected:
  virtual XmcSymbol* callNewAndNodeInit() const override;
  virtual bool callNodeInit() override;

private:
  virtual void gotoAndPlayToNextLabel(const char* label) override;
  
  void mcEventListener(XmcMovieClip::EventType eventType, XmcMovieClip* mc);
  bool onTouchBegan(cocos2d::Touch* pTouch, cocos2d::Event* pEvent);
  void onTouchMoved(cocos2d::Touch* pTouch, cocos2d::Event* pEvent);
  void onTouchEnded(cocos2d::Touch* pTouch, cocos2d::Event* pEvent);
  bool pushFire();

  bool hasVisibleParents();
  void childTouchDisable();

  void setCurrentLabel(LabelType label);

  bool _enabled;
  LabelType _currentPlayingLabel;
  cocos2d::RefPtr<cocos2d::EventListenerTouchOneByOne> _touchListener;
  static int _buttonPressingCounter; //skc needs static???
  bool _simultaneousPress;
  bool _onPressing;
  float _pushingElapsed;
  float _barrageBlockSec;
  double _barrageBlockCounter;
  cocos2d::Rect _hitArea;
  bool _checkBoxMode;
  bool _check;
  std::function<bool(XmcButton*)> _touchBeganHandler;
  std::function<bool(XmcButton*)> _pushingHandler;
  std::function<bool(XmcButton*)> _handler;
  std::function<void(void)>       _soundPlay;
};

#pragma mark - XmcButton
XmcButton::XmcButton()
: XmcMovieClipAdapter()
{
}

XmcButton::~XmcButton() {
}

void XmcButtonImpl::XmcButtonNode::onEnter() {
  cocos2d::Node::onEnter();
  XmcButtonImpl* s = getButtonSymbol();
  s->play();
  s->childTouchDisable();
  cocos2d::Node::scheduleUpdate();
  xmc_dlog("%p: %d/%d", this, s->_onPressing, s->_buttonPressingCounter);
}

void XmcButtonImpl::XmcButtonNode::onExit() {
  XmcButtonImpl* s = getButtonSymbol();
  s->_buttonPressingCounter -= s->_onPressing;
  s->_onPressing = false;
  cocos2d::Node::onExit();
  xmc_dlog("%p: %d/%d", this, s->_onPressing, s->_buttonPressingCounter);
}

void XmcButtonImpl::XmcButtonNode::update(float delta)  {
  XmcButtonImpl* s = getButtonSymbol();
  if (s->_barrageBlockCounter > 0.f) {
    //xmc_dlog("delta=%.5f, _barrageBlockCounter=%f", delta, s->_barrageBlockCounter);
    s->_barrageBlockCounter -= delta;
  } else {
    //xmc_assert(false);
  }
  cocos2d::Node::update(delta);
}

#pragma mark - XmcButtonImpl
int XmcButtonImpl::_buttonPressingCounter = 0;

XmcButtonImpl::XmcButtonImpl()
: XmcButton()
, _enabled(false)
, _currentPlayingLabel(LabelType_Up)
, _simultaneousPress(true)
, _pushingElapsed(0.f)
, _barrageBlockSec(.2f)
, _barrageBlockCounter(0.f)
, _hitArea(cocos2d::Rect::ZERO)
, _checkBoxMode(false)
, _check(false)
, _touchBeganHandler(nullptr)
, _pushingHandler(nullptr)
, _handler(nullptr)
, _onPressing(false)
{
  _node = new XmcButtonNode(this);
}

XmcButtonImpl::~XmcButtonImpl() {
  asNode()->getEventDispatcher()->removeEventListener(_touchListener);
}

XmcSymbol* XmcButtonImpl::callNewAndNodeInit() const {
  XmcMovieClipAdapter* p = new XmcButtonImpl();
  p->asNode()->init();
  return p;
}

bool XmcButtonImpl::callNodeInit() {
  return asNode()->init();
}

void XmcButtonImpl::setCurrentLabel(LabelType label) {
  _currentPlayingLabel = label;
  if (_label_map.find(label) != std::end(_label_map)) {
    const std::string& label_str = _label_map[label];
    gotoAndPlayToNextLabel(label_str.c_str());
    return;
  }
  xmc_assert(false);
}

void XmcButtonImpl::gotoAndPlayToNextLabel(const char* label) {
  xmc_dlog("%p: %s", this, label);
  XmcMovieClipAdapter::gotoAndPlayToNextLabel(label);
  // Change if there is a button type child
  iterateInstancesRecursively([label](XmcSymbol* symbol) {
    //XmcButton* child = dynamic_cast<XmcButton*>(symbol);
    XmcButton* child = symbol->query<XmcButton>();
    if (child) {
      //xmc_assert(false); //skc ok
      child->gotoAndPlayToNextLabel(label);
    }
  });
}

void XmcButtonImpl::childTouchDisable() {
  // Keep children out of touch
  iterateInstancesRecursively([](XmcSymbol* symbol) -> void {
    //XmcButton* child = dynamic_cast<XmcButton*>(symbol);
    XmcButton* child = symbol->query<XmcButton>();
    if (child) {
      child->setTouchEnabled(false);
    }
  });
}

void XmcButtonImpl::setTouchEnabled(bool enabled) {
  _touchListener->setEnabled(enabled);
  setLoop(enabled);
}

class TestSprite : public cocos2d::Sprite {
public:
//  virtual void visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags) override {
//    cocos2d::Sprite::visit(renderer, parentTransform, parentFlags);
//  }
  virtual void draw(Renderer* renderer, const Mat4& transform, uint32_t flags) override {
    //setColor(Color3B::BLUE);
    cocos2d::Sprite::draw(renderer, transform, flags);
  }
  virtual const Color3B& getColor() const override {
    return cocos2d::Sprite::getColor();
  }
  virtual void setColor(const Color3B& color) override {
    cocos2d::Sprite::setColor(color);
  }
  static TestSprite* create()
  {
    TestSprite *sprite = new (std::nothrow) TestSprite();
    if (sprite && sprite->init())
    {
      sprite->autorelease();
      return sprite;
    }
    CC_SAFE_DELETE(sprite);
    return nullptr;
  }
};

void XmcButtonImpl::init(XmcLoader* source, const bmc::BmcTimeline& bmcTimeline) {
  XmcMovieClipAdapter::init(source, bmcTimeline);
  this->setEventListener(CC_CALLBACK_2(XmcButtonImpl::mcEventListener, this));
  
  _touchListener = cocos2d::EventListenerTouchOneByOne::create();
  _touchListener->setSwallowTouches(true);

  _touchListener->onTouchBegan = CC_CALLBACK_2(XmcButtonImpl::onTouchBegan, this);
  _touchListener->onTouchMoved = CC_CALLBACK_2(XmcButtonImpl::onTouchMoved, this);
  _touchListener->onTouchEnded = CC_CALLBACK_2(XmcButtonImpl::onTouchEnded, this);
  asNode()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touchListener.get(), asNode());
  _touchListener->setEnabled(true);
  _enabled = true;
  
#if 1 //skc test
  _hitArea = this->getCurrentFrameBoundingBox();
//  destroyInstance(hitArea);
  
  TestSprite* spr = (TestSprite*)TestSprite::create();
  spr->setTextureRect({ .0f, .0f, _hitArea.size.width, _hitArea.size.height });
  spr->setPosition(_hitArea.origin);
  spr->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
  spr->setColor(Color3B::BLUE);
  spr->setOpacity(0xFF * .3f);
  asNode()->addChild(spr);
#else
  XmcSymbol::Ptr hitArea = XmcSymbol::getInstance("$hitArea");
  if (hitArea != nullptr) {
    _hitArea = hitArea->getCurrentFrameBoundingBox();
    destroyInstance(hitArea);
#if defined(DRAW_HITAREA) && (DRAW_HITAREA)
    auto spr = Sprite::create();
    spr->setTextureRect({ .0f, .0f, _hitArea.size.width, _hitArea.size.height });
    spr->setPosition(_hitArea.origin);
    spr->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
    spr->setColor(Color3B::BLUE);
    spr->setOpacity(0xFF * .3f);
    asNode()->addChild(spr);
    //this->retain(); //skc add
#endif
  }
#endif //skc

  //gotoAndPlayToNextLabel(LABEL_UPNORMAL);
  setCurrentLabel(LabelType_UpNormal); // 위에서 직접 _currentPlayingLabel = LabelType_UpNormal; 하고 있었음
  //play(); //skc org commented
}

XmcSymbol::Ptr XmcButtonImpl::clone() const {
  //xmc_assert(false);
  XmcSymbol::Ptr ret = XmcMovieClipAdapter::clone();
  XmcButtonImpl* me = static_cast<XmcButtonImpl*>(ret.get());
  me->setEventListener(CC_CALLBACK_2(XmcButtonImpl::mcEventListener, me));
  
  me->_touchListener = cocos2d::EventListenerTouchOneByOne::create();
  me->_touchListener->setSwallowTouches(true);
  me->_touchListener->onTouchBegan = CC_CALLBACK_2(XmcButtonImpl::onTouchBegan, me);
  me->_touchListener->onTouchMoved = CC_CALLBACK_2(XmcButtonImpl::onTouchMoved, me);
  me->_touchListener->onTouchEnded = CC_CALLBACK_2(XmcButtonImpl::onTouchEnded, me);
  //me->asNode()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(me->_touchListener.get(), me->asNode());
  me->_touchListener->setEnabled(this->_touchListener->isEnabled());
  me->asNode()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(me->_touchListener.get(), me->asNode()); //skc order change
  
  me->_enabled = _enabled;
  me->_hitArea = _hitArea;
  me->setCurrentLabel(LabelType_UpNormal);
  me->_checkBoxMode = _checkBoxMode;
  me->_check = _check;
  me->_soundPlay = _soundPlay;
  
//  XmcSymbol::Ptr ptr = newP->query<XmcSymbol>(); // same above
//  ptr->release();
//  return ptr; // caller 에서 리턴받은 XmcSymbol::Ptr 객체의 refcnt는 1 이됨
  return ret; // 이미 refCnt == 1
}

void XmcButtonImpl::setEnabled(bool enabled) {
  _enabled = enabled;
  _touchListener->setEnabled(_enabled);
  setLoop(_enabled);
  if (_enabled) {
    setCurrentLabel(LabelType_Disable);
  } else {
    setCurrentLabel(LabelType_UpNormal);
  }
}

bool XmcButtonImpl::inHitArea(cocos2d::Point point) {
  if (_hitArea.equals(cocos2d::Rect::ZERO)) {
    return XmcSymbol::inHitArea(point);
  }
  auto node = asNode();
  if (node == nullptr) {
    xmc_assert(false);
    return false;
  }
  point = asNode()->convertToNodeSpace(point);
  return _hitArea.containsPoint(point);
}

bool XmcButtonImpl::hasVisibleParents() {
  for (cocos2d::Node* node = asNode()->getParent(); node; node = node->getParent()) {
    if (!node->isVisible()) {
      //xmc_assert(false); //ok
      return false;
    }
  }
  return true;
}

void XmcButtonImpl::setCheck(bool check, bool playSound, bool animated) {
  if (_checkBoxMode == false || _touchListener->isEnabled() == false) {
    xmc_assert(false);
    return;
  }
  setLoop(false);
  if (check) {
    if (animated) {
      setCurrentLabel(LabelType_Down);
    } else {
      setCurrentLabel(LabelType_DownNormal);
    }
  } else {
    if (animated) {
      setCurrentLabel(LabelType_Up);
    } else {
      setCurrentLabel(LabelType_UpNormal);
    }
  }
  //_touchBegan = false;
  if (playSound && _soundPlay != nullptr) {
    _soundPlay();
  }
  _check = check;
}

bool XmcButtonImpl::onTouchBegan(cocos2d::Touch* pTouch, cocos2d::Event* pEvent) {
  xmc_dlog("%p: +++ %d/%d, %s", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
  if (asNode()->isVisible() == false || hasVisibleParents() == false
      || (_buttonPressingCounter > 0 && _simultaneousPress == false)) {
    //xmc_assert(false); //ok
    return false;
  }
  _pushingElapsed = 0.f;
  
  // Check for successive hits
  if (_barrageBlockSec > 0.f) {
    if (_barrageBlockCounter > 0.f) {
      xmc_dlog("successive click blocked!!! %p: _barrageBlockSec=%.5f, _barrageBlockCounter=%.5f", this, _barrageBlockSec, _barrageBlockCounter);
      return false; // Still in successive blocking time
    }
  }
  
  cocos2d::Point touchLocation = pTouch->getLocation();
  if (inHitArea(touchLocation) == false) {
    if (_checkBoxMode == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)) {
      xmc_assert(false);
      setCurrentLabel(LabelType_Up);
    }
    return false;
  }
  
  bool ret = true;
  if (_checkBoxMode == false) {
    if (_touchBeganHandler) {
      ret = _touchBeganHandler(this);
    }
    if (ret) {
      setCurrentLabel(LabelType_Down); // common case
    }
  } else {
    if (_touchBeganHandler) {
      ret = _touchBeganHandler(this);
    }
    if (ret) {
      if (_check) {
        setCurrentLabel(LabelType_Up);
      } else {
        setCurrentLabel(LabelType_Down);
      }
    }
  }
  
  if (ret) {
    _buttonPressingCounter++;
    _onPressing = true;
    xmc_dlog("%p: --- %d/%d, %s\n", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
  } else {
    xmc_assert(false);
  }
  return ret;
}

void XmcButtonImpl::onTouchMoved(Touch *pTouch, Event *pEvent) {
  if (asNode()->isVisible() == false || hasVisibleParents() == false) {
    xmc_assert(false, "%p: skip!!! asNode()->isVisible()=%d, hasVisibleParents()=%d", this, asNode()->isVisible(), hasVisibleParents());
    return;
  }
  
  Point touchLocation = pTouch->getLocation();
  //xmc_dlog("%f, %f", touchLocation.x, touchLocation.y );
  bool hitArea = inHitArea(touchLocation);
  xmc_dlog("(%f, %f) -> hitArea=%d, %s", touchLocation.x, touchLocation.y, hitArea, LableStr(_currentPlayingLabel));
  if (_checkBoxMode == false) {
    if (hitArea == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)) {
      setLoop(false);
      setCurrentLabel(LabelType_Up);
    } else if (hitArea && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal) == false) {
      setLoop(false);
      setCurrentLabel(LabelType_Down);
    }
  } else {
    //xmc_dlog( "DO Moved");
    if (_check) {
      if (hitArea == false && (_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal)) {
        setLoop(false);
        setCurrentLabel(LabelType_Down);
      } else if (hitArea && (_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal) == false) {
        setLoop(false);
        setCurrentLabel(LabelType_Up);
      }
    } else {
      if (hitArea == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)) {
        setLoop(false);
        setCurrentLabel(LabelType_Up);
      } else if (hitArea && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal) == false) {
        setLoop(false);
        setCurrentLabel(LabelType_Down);
      }
    }
  }
}

void XmcButtonImpl::onTouchEnded(Touch *pTouch, Event *pEvent) {
  xmc_dlog("%p: +++ %d/%d, %s", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
  if (asNode()->isVisible() == false || hasVisibleParents() == false) {
    xmc_assert(false);
    _buttonPressingCounter -= _onPressing;
    _onPressing = false;
    return;
  }
  
  Point touchLocation = pTouch->getLocation();
  if (inHitArea(touchLocation)) {
    if (_checkBoxMode == false) {
      setLoop(false);
      setCurrentLabel(LabelType_Up); // common case
      pushFire();
    } else {
      _check = !_check;
      if (!pushFire()) {
        _check = !_check;
      }
    }
  } else if (_checkBoxMode) {
    //skc checkbox toggle when touchdown -> drag -> outside -> up ???
    if (_check) {
      if ((_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal)) {
        setLoop(false);
        setCurrentLabel(LabelType_Down);
      }
    } else {
      if ((_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)) {
        setLoop(false);
        setCurrentLabel(LabelType_Up);
      }
    }
  }
  
  _buttonPressingCounter -= _onPressing;
  _onPressing = false;
  xmc_dlog("%p: --- %d/%d, %s\n", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
}

bool XmcButtonImpl::pushFire() {
  _barrageBlockCounter = _barrageBlockSec;

  auto soundPlayFunc = _soundPlay;
  if (_handler != nullptr) {
    //xmc_assert(false);
    if (_handler(this) == false) {
      return false;
    }
  }
  if (soundPlayFunc != nullptr) {
    //xmc_assert(false); //ok
    soundPlayFunc();
  }
  return true;
}

void XmcButtonImpl::mcEventListener(XmcMovieClip::EventType eventType, XmcMovieClip* mc) {
  //xmc_dlog("%p, eventType=%d, _currentPlayingLabel=%s", this, eventType, LableStr(_currentPlayingLabel));
  xmc_assert(mc == this);
  switch (eventType) {
    case XmcMovieClip::EventType_PlayEndFrame:
    {
      switch (_currentPlayingLabel) {
        case LabelType_Up:
          xmc_dlog("%p, %s -> %s", this, LableStr(LabelType_Up), LableStr(LabelType_UpNormal));
          setLoop(true);
          setCurrentLabel(LabelType_UpNormal);
          break;
        case LabelType_Down:
          xmc_dlog("%p, %s -> %s", this, LableStr(LabelType_Down), LableStr(LabelType_DownNormal));
          setLoop(true);
          setCurrentLabel(LabelType_DownNormal);
          break;
        default:
          break;
      }
    }
      break;
      
    default:
      break;
  }
}

void XmcButtonImpl::touchDownAction() {
  setLoop(false);
  _currentPlayingLabel = LabelType_Up;
  gotoAndPlayLabel(LABEL_DOWNNORMAL);
}

XMC_FACTORY_CREATE_FUNC_IMPL(XmcButton);

NS_XMC_END
