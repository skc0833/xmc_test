#include "XmcScalingGridShape.h"
#include "XmcFactory.h"
#include <cocos2d.h>
#include <ui/UIScale9Sprite.h>

USING_NS_CC;
NS_XMC_BEGIN

class XmcScalingGridShapeImpl : public XmcScalingGridShape {
public:
  class XmcSprite : public XmcManagedObjectNode<cocos2d::ui::Scale9Sprite>  {
  public:
    XmcSprite(XmcSymbol* s = nullptr) : XmcManagedObjectNode<cocos2d::ui::Scale9Sprite>(s) {}
    XmcSprite(const XmcSymbol* s) : XmcManagedObjectNode<cocos2d::ui::Scale9Sprite>(const_cast<XmcSymbol*>(s)) {}
    virtual ~XmcSprite();
    virtual cocos2d::Rect getBoundingBox() const override;
  };
  
  XMC_DECLARE_ASREF_BY_NODE_IMPL(); // XmcScalingGridShape() 생성자에서 retain() 시에 호출되고 있음

public:
  XmcScalingGridShapeImpl();
  virtual ~XmcScalingGridShapeImpl();
  
  virtual void init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) override;
  virtual void initWithSpriteFramename(XmcShader::Ptr shader, XmcTexturePart::Ptr texture, const std::string& spriteFrameName) override;

  virtual XmcSymbol::Ptr clone() const override;
  
  virtual cocos2d::Size setupContentSize() override;
  
  virtual void setAdditionalTransform(const Mat4& additionalTransform) override;
  virtual void setAdditionalTransform(const cocos2d::AffineTransform& additionalTransform) override;

  virtual void setBlendMode(bmc::BmcBlendMode blendMode) override {
    _blendMode = blendMode;
  }
  
  virtual const cocos2d::Node* asNode() const override { return _sprite; }
  virtual cocos2d::Node* asNode() override { return _sprite; }
  
  virtual void* query(IIDType iid) override {
    if (iid == XmcScalingGridShape::iid()) {
      return static_cast<XmcScalingGridShape*>(this);
    }
    if (iid == XmcSymbol::iid()) {
      return static_cast<XmcSymbol*>(this);
    }
#ifdef XMC_BMC_COMPATIBLE
    if (iid == "BmcScalingGridShape@xmc") {
      return static_cast<XmcScalingGridShape*>(this);
    }
#endif
    //xmc_assert(false); //ok
    return nullptr;
  }
  
private:
  Rect getCapInsets();
  
private:
  XmcShader::Ptr _shader;
  XmcTexturePart::Ptr _texture;
  bmc::BmcBlendMode _blendMode;
  
  std::string _spriteFrameName;
  mutable XmcSprite* _sprite;
};

XmcScalingGridShapeImpl::XmcScalingGridShapeImpl()
: _blendMode(bmc::BmcBlendMode_Normal)
{
  _sprite = new XmcSprite(this);
}

XmcScalingGridShapeImpl::~XmcScalingGridShapeImpl() {
  xmc_assert(asNode()->getReferenceCount() == 0);
}

Rect XmcScalingGridShapeImpl::getCapInsets() {
  const auto& texRect = _texture->getRectangle();
  const auto& scale9Rect = _texture->getScale9GridRectangle();
  Rect capInsets(scale9Rect.x() - texRect.x()
                 , scale9Rect.y() - texRect.y()
                 , scale9Rect.width() == 0 ? 1 : scale9Rect.width()
                 , scale9Rect.height() == 0 ? 1 : scale9Rect.height()
                 );
  return capInsets;
}

void XmcScalingGridShapeImpl::init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) {
  //_sprite->initWithTexture(texture->getTexture()->getCCTexture2D());
  _shader = shader;
  _texture = texture;
  
  //_sprite->setAnchorPoint(Vec2::ZERO);
  //setupVertices();
  
  auto spr = Sprite::createWithTexture(_texture->getTexture()->getCCTexture2D());
  _sprite->init(spr, Rect::ZERO, getCapInsets());
}

void XmcScalingGridShapeImpl::initWithSpriteFramename(XmcShader::Ptr shader, XmcTexturePart::Ptr texture, const std::string& spriteFrameName) {
  _spriteFrameName = spriteFrameName;
  _shader = shader;
  _texture = texture;
  _sprite->initWithSpriteFrameName(_spriteFrameName, getCapInsets());
}

XmcSymbol::Ptr XmcScalingGridShapeImpl::clone() const {
  XmcScalingGridShapeImpl* newP = new XmcScalingGridShapeImpl(); //skc todo _sprite 객체는 생성을 해야할듯함!!!
  newP->init(_shader, _texture);
  newP->_additionalColorTransform = _additionalColorTransform;
  newP->_blendMode = _blendMode;
  XmcSymbol::Ptr ptr = newP;
  ptr->asRef()->release();
  return ptr; //skc static_cast<XmcSymbol*>(newP);
}

Size XmcScalingGridShapeImpl::setupContentSize() {
  xmc_assert(false);
  return Size(
              _texture->getRectangle().width() / CC_CONTENT_SCALE_FACTOR(),
              _texture->getRectangle().height() / CC_CONTENT_SCALE_FACTOR()
              );
}

XmcScalingGridShapeImpl::XmcSprite::~XmcSprite() {
}

void XmcScalingGridShapeImpl::setAdditionalTransform(const Mat4& additionalTransform) {
  Mat4 trans = additionalTransform;
  float xRate = additionalTransform.m[0];
  float yRate = additionalTransform.m[5];
  trans.m[0] = 1.f;
  trans.m[5] = 1.f;
  
  _sprite->setContentSize({_texture->getRectangle().width() * xRate, _texture->getRectangle().height() * yRate});
  _sprite->setAdditionalTransform(&trans);
}

void XmcScalingGridShapeImpl::setAdditionalTransform(const cocos2d::AffineTransform& additionalTransform) {
  Mat4 tmp;
  CGAffineToGL(additionalTransform, tmp.m);
  setAdditionalTransform(tmp);
}

// XmcScalingGridShapeImpl::XmcSprite
//

cocos2d::Rect XmcScalingGridShapeImpl::XmcSprite::getBoundingBox() const {
  const XmcScalingGridShapeImpl* symbol = dynamic_cast<const XmcScalingGridShapeImpl*>(getSymbol());
  return cocos2d::RectApplyTransform(
                                     { symbol->_texture->getOffset().x, symbol->_texture->getOffset().y
                                       , _contentSize.width, _contentSize.height }
                                     , getNodeToParentTransform()
                                     );
}

XMC_FACTORY_CREATE_FUNC_IMPL(XmcScalingGridShape);

NS_XMC_END
