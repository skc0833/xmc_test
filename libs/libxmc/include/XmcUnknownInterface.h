#pragma once

NS_XMC_BEGIN

typedef const std::string& IIDType; // typedef const char* IIDType; 와 동일

// 현재 "\x10XmcMovieClip@xmc" 식으로 리턴되고 있다(Bmc의 경우, \x10 가 없는 정상 문자열을 리턴함)
#define XMC_DECLARE_IID(_class) static IIDType iid() { \
  static IIDType _iid = #_class"@xmc"; \
  return _iid; \
}

class XmcUnknownInterface {
public:
  virtual void* query(IIDType iid) = 0;
  // auto mc = dynamic_cast<XmcMovieClip*>(stage.get());
  // VS auto mc = stage->query<XmcMovieClip>();
  template<class _I> inline _I* query() {
    //return static_cast<_I*>(this); //ok
    auto iid = _I::iid();
    auto ret = query(iid);
    return static_cast<_I*>(ret);
  }
};

NS_XMC_END
