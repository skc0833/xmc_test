#if 0
#pragma once

#include "XmcCommon.h"
#include "XmcSymbol.h"
#include "XmcTexturePart.h"

NS_XMC_BEGIN

class XmcSpriteShape : public XmcSymbol {
public:
  XMC_SHARABLEPTR_DEFINE(XmcSpriteShape);
  XMC_DECLARE_IID(XmcSpriteShape);

  virtual void init(XmcTexturePart::Ptr texture) = 0;
  
protected:
  XmcSpriteShape() = default;
  virtual ~XmcSpriteShape() = default;
};

NS_XMC_END
#endif
