#pragma once
#include <cocos2d.h>
USING_NS_CC;

#include "../XmcCommon.h"

NS_XMC_BEGIN
  
  extern const char *const Uniform_ColorMultiplierIdentify;
  extern const char *const Uniform_ColorOffsetIdentify;
  extern const char *const Uniform_AlphaMultiplierIdentify;
  
  extern const char *const kCCShader_Vector_PositionTextureColor_UseOffsetAlpha;
  extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetAlpha;
  extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor;
  extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha;
  extern const char *const kCCShader_Vector_PositionTextureColor_OnlyTexture;
  extern const char *const kCCShader_PositionTextureColor_AlphaMask_noMVP;
  extern const char *const kCCShader_PositionTextureColor_GrayScale_AlphaMask_noMVP;
  
  extern void addShaders(GLProgramCache *shaderCache);
  extern void reloadShaders(GLProgramCache *shaderCache);
  
NS_XMC_END
