#include "../../foundation/Log.h" // XMC_BMC_COMPATIBLE

//#define xmc_econd(cond,...)  (cond)
//#define xmc_elog(...)        ((void)0)
//#define xmc_assert(cond,...) ((void)0)
//#define xmc_wcond(cond,...)  (cond)
//#define xmc_wlog(...)        ((void)0)
//#define xmc_ilog(...)        ((void)0)
//#define xmc_slog(...)        ((void)0)
//#define xmc_dlog(...)        ((void)0)

#define ccn_assert    xmc_assert
#define ccn_wlog      xmc_wlog
#define ccn_dlog      xmc_dlog
#define ccn_elog      xmc_elog
