#pragma once

/// @cond DO_NOT_SHOW

#include "XmcRef.h"
#include "../foundation/Log.h"
#include <functional>   // for std::less
#include <type_traits>  // for std::remove_const
#include <cstddef>      // for std::nullptr_t

NS_XMC_BEGIN

/**
 * Utility/support macros. Defined to enable XmcRefPtr<T> to contain types like 'const T' because we do not
 * regard retain()/release() as affecting mutability of state.
 */
#define XMC_REF_PTR_SAFE_RETAIN(ptr)\
  \
  do\
  {\
    if (ptr)\
    {\
      ptr->asRef()->retain();\
    }\
  \
  }   while (0);

#define XMC_REF_PTR_SAFE_RELEASE(ptr)\
  \
  do\
  {\
    if (ptr)\
    {\
      ptr->asRef()->release();\
    }\
  \
  }   while (0);

#define XMC_REF_PTR_SAFE_RELEASE_NULL(ptr)\
  \
  do\
  {\
    if (ptr)\
    {\
      ptr->asRef()->release();\
      ptr = nullptr;\
    }\
  \
  }   while (0);

class XmcAsRef
{
public:
  virtual const XmcRef* asRef() const = 0;
  virtual XmcRef* asRef() = 0;
  
  inline explicit operator const XmcRef*() const { return asRef(); }
  inline explicit operator XmcRef*() { return asRef(); }
};

/**
 * Wrapper class which maintains a strong reference to a cocos2dx cocos2d::XmcRef* type object.
 * Similar in concept to a boost smart pointer.
 *
 * Enables the use of the RAII idiom with Cocos2dx objects and helps automate some of the more
 * mundane tasks of pointer initialization and cleanup.
 *
 * The class itself is modelled on C++ 11 std::shared_ptr, and trys to keep some of the methods
 * and functionality consistent with std::shared_ptr.
 */
template <typename T> class XmcRefPtr
{
public:
  
    XmcRefPtr()
        : _ptr(nullptr)
    {
      
    }
  
    XmcRefPtr(XmcRefPtr<T> && other)
    {
        _ptr = other._ptr;
        other._ptr = nullptr;
    }

    XmcRefPtr(T * ptr)
        : _ptr(ptr)
    {
        XMC_REF_PTR_SAFE_RETAIN(_ptr);
    }
  
    XmcRefPtr(std::nullptr_t ptr)
        : _ptr(nullptr)
    {
      
    }
  
    XmcRefPtr(const XmcRefPtr<T> & other)
        : _ptr(other._ptr)
    {
        XMC_REF_PTR_SAFE_RETAIN(_ptr);
    }
  
    ~XmcRefPtr()
    {
        XMC_REF_PTR_SAFE_RELEASE_NULL(_ptr);
    }
  
    XmcRefPtr<T> & operator = (const XmcRefPtr<T> & other)
    {
        if (other._ptr != _ptr)
        {
            XMC_REF_PTR_SAFE_RETAIN(other._ptr);
            XMC_REF_PTR_SAFE_RELEASE(_ptr);
            _ptr = other._ptr;
        }
      
        return *this;
    }
  
    XmcRefPtr<T> & operator = (XmcRefPtr<T> && other)
    {
        if (&other != this)
        {
            XMC_REF_PTR_SAFE_RELEASE(_ptr);
            _ptr = other._ptr;
            other._ptr = nullptr;
        }
      
        return *this;
    }
  
    XmcRefPtr<T> & operator = (T * other)
    {
        if (other != _ptr)
        {
            XMC_REF_PTR_SAFE_RETAIN(other);
            XMC_REF_PTR_SAFE_RELEASE(_ptr);
            _ptr = other;
        }
      
        return *this;
    }
  
    XmcRefPtr<T> & operator = (std::nullptr_t other)
    {
        XMC_REF_PTR_SAFE_RELEASE_NULL(_ptr);
        return *this;
    }
  
    operator T * () const { return _ptr; }
  
    T & operator * () const
    {
        xmc_assert(_ptr, "Attempt to dereference a null pointer!");
        return *_ptr;
    }
  
    T * operator->() const
    {
        xmc_assert(_ptr, "Attempt to dereference a null pointer!");
        return _ptr;
    }
  
    T * get() const { return _ptr; }
  
  
    bool operator == (const XmcRefPtr<T> & other) const { return _ptr == other._ptr; }
    bool operator == (const T * other) const { return _ptr == other; }
    bool operator == (typename std::remove_const<T>::type * other) const { return _ptr == other; }
    //bool operator == (const std::nullptr_t other) const { return _ptr == other; } //skc modify
    bool operator == (const std::nullptr_t other) const { return _ptr == other || *_ptr == other; }

    bool operator != (const XmcRefPtr<T> & other) const { return _ptr != other._ptr; }
    bool operator != (const T * other) const { return _ptr != other; }
    bool operator != (typename std::remove_const<T>::type * other) const { return _ptr != other; }
    //bool operator != (const std::nullptr_t other) const { return _ptr != other; }
    inline bool operator != (const std::nullptr_t other) const { return _ptr != other && *_ptr != other; } //skc modify
  
    bool operator > (const XmcRefPtr<T> & other) const { return _ptr > other._ptr; }
    bool operator > (const T * other) const { return _ptr > other; }
    bool operator > (typename std::remove_const<T>::type * other) const { return _ptr > other; }
    bool operator > (const std::nullptr_t other) const { xmc_assert(false); return _ptr > other; } //skc add

    bool operator < (const XmcRefPtr<T> & other) const { return _ptr < other._ptr; }
    bool operator < (const T * other) const { return _ptr < other; }
    bool operator < (typename std::remove_const<T>::type * other) const { return _ptr < other; }
    bool operator < (const std::nullptr_t other) const { xmc_assert(false); return _ptr < other; } //skc add

    bool operator >= (const XmcRefPtr<T> & other) const { return _ptr >= other._ptr; }
    bool operator >= (const T * other) const { return _ptr >= other; }
    bool operator >= (typename std::remove_const<T>::type * other) const { return _ptr >= other; }
    bool operator >= (const std::nullptr_t other) const { xmc_assert(false); return _ptr >= other; } //skc add

    bool operator <= (const XmcRefPtr<T> & other) const { return _ptr <= other._ptr; }
    bool operator <= (const T * other) const { return _ptr <= other; }
    bool operator <= (typename std::remove_const<T>::type * other) const { return _ptr <= other; }
    bool operator <= (const std::nullptr_t other) const { xmc_assert(false); return _ptr <= other; } //skc add

    //operator bool() const { return _ptr != nullptr; }
    operator bool() const { return _ptr != nullptr && bool(*_ptr); }
  
    void reset()
    {
        XMC_REF_PTR_SAFE_RELEASE_NULL(_ptr);
    }
  
    void swap(XmcRefPtr<T> & other)
    {
        if (&other != this)
        {
            T * tmp = _ptr;
            _ptr = other._ptr;
            other._ptr = tmp;
        }
    }
  
    /**
     * This function assigns to this XmcRefPtr<T> but does not increase the reference count of the object pointed to.
     * Useful for assigning an object created through the 'new' operator to a XmcRefPtr<T>. Basically used in scenarios
     * where the XmcRefPtr<T> has the initial ownership of the object.
     *
     * E.G:
     *      XmcRefPtr<cocos2d::Image> image;
     *      image.weakAssign(new cocos2d::Image());
     *
     * Instead of:
     *      XmcRefPtr<cocos2d::Image> image;
     *      image = new cocos2d::Image();
     *      image->release();               // Required because new'd object already has a reference count of '1'.
     */
    void weakAssign(const XmcRefPtr<T> & other)
    {
        XMC_REF_PTR_SAFE_RELEASE(_ptr);
        _ptr = other._ptr;
    }
  
private:
    T * _ptr;

    // NOTE: We can ensure T is derived from cocos2d::XmcRef at compile time here.
    //static_assert(std::is_base_of<XmcRef, typename std::remove_const<T>::type>::value, "T must be derived from XmcRef");
};

template <class T> inline
XmcRefPtr<T> makeRef(T *ptr)
{
    return XmcRefPtr<T>(ptr);
}

template<class T> inline
bool operator<(const XmcRefPtr<T>& r, std::nullptr_t)
{
    return std::less<T*>()(r.get(), nullptr);
}

template<class T> inline
bool operator<(std::nullptr_t, const XmcRefPtr<T>& r)
{
    return std::less<T*>()(nullptr, r.get());
}

template<class T> inline
bool operator>(const XmcRefPtr<T>& r, std::nullptr_t)
{
    return nullptr < r;
}

template<class T> inline
bool operator>(std::nullptr_t, const XmcRefPtr<T>& r)
{
    return r < nullptr;
}

template<class T> inline
bool operator<=(const XmcRefPtr<T>& r, std::nullptr_t)
{
    return !(nullptr < r);
}

template<class T> inline
bool operator<=(std::nullptr_t, const XmcRefPtr<T>& r)
{
    return !(r < nullptr);
}

template<class T> inline
bool operator>=(const XmcRefPtr<T>& r, std::nullptr_t)
{
    return !(r < nullptr);
}

template<class T> inline
bool operator>=(std::nullptr_t, const XmcRefPtr<T>& r)
{
    return !(nullptr < r);
}

/**
 * Cast between XmcRefPtr types statically.
 */
template<class T, class U> XmcRefPtr<T> static_pointer_cast(const XmcRefPtr<U> & r)
{
    return XmcRefPtr<T>(static_cast<T*>(r.get()));
}

/**
 * Cast between XmcRefPtr types dynamically.
 */
template<class T, class U> XmcRefPtr<T> dynamic_pointer_cast(const XmcRefPtr<U> & r)
{
    return XmcRefPtr<T>(dynamic_cast<T*>(r.get()));
}

/**
 * Done with these macros.
 */
#undef XMC_REF_PTR_SAFE_RETAIN
#undef XMC_REF_PTR_SAFE_RELEASE
#undef XMC_REF_PTR_SAFE_RELEASE_NULL

//#define XMC_DECLARE_ASNODE_IMPL() \
//  cocos2d::Node* asNode() override { return this; } \
//  const cocos2d::Node* asNode() const override { return this; } \
//  XmcRef* asRef() override { return asNode(); } \
//  const XmcRef* asRef() const override { return asNode(); }

// asNode()는 따로 정의해야 함(_node, _sprite 등을 리턴)
#define XMC_DECLARE_ASREF_BY_NODE_IMPL() \
  XmcRef* asRef() override { return asNode(); } \
  const XmcRef* asRef() const override { return asNode(); }

#define XMC_DECLARE_ASREF_IMPL() \
  XmcRef* asRef() override { return this; } \
  const XmcRef* asRef() const override { return this; }

NS_XMC_END

/// @endcond
