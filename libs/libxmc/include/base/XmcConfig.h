#pragma once

#include "../platform/XmcPlatformConfig.h"

/** @def CC_ENABLE_PROFILERS
 * If enabled, will activate various profilers within cocos2d. This statistical data will be output to the console
 * once per second showing average time (in milliseconds) required to execute the specific routine(s).
 * Useful for debugging purposes only. It is recommended to leave it disabled.
 * To enable set it to a value different than 0. Disabled by default.
 */
//#ifndef CC_ENABLE_PROFILERS
//#define CC_ENABLE_PROFILERS 0
//#endif
