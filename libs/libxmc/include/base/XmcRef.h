#pragma once

#if 1
#include "base/CCRef.h" // just use cocos2d::Ref class

// XmcMovieClipAdapter 등의 클래스는 cocos2d::Node 를 상속 또는 포함하고 있으며, XmcAsRef 인터페이스의 asRef() 함수로
// cocos2d::Node 객체를 cocos2d::Ref* 타입으로 반환해야 하므로, XmcRef 는 cocos2d::Ref 타입과 동일해야한다.
// (cocos2d::Ref 클래스를 상속받아 XmcRef 타입을 사용할 경우, cocos2d::Node 는 XmcRef 클래스를 상속받지 않으므로 빌드에러임.
// XmcRef 클래스를 cocos2d::Ref 클래스와 동일하게 정의해도 마찬가지 이유로 빌드에러임)
typedef cocos2d::Ref XmcRef;
//class XmcRef : public cocos2d::Ref {
//public:
//  XmcRef() = default;
//  ~XmcRef() = default;
//};
#else

#include "XmcCommon.h"

// from CCRef.h
#if !defined(XMC_DEBUG) || XMC_DEBUG == 0 || defined(NDEBUG)
#define XMC_REF_LEAK_DETECTION 0
#else
#define XMC_REF_LEAK_DETECTION 1
#endif

NS_XMC_BEGIN

/**
 * XmcRef is used for reference count management. If a class inherits from XmcRef,
 * then it is easy to be shared in different places.
 * @js NA
 */
class XmcRef
{
public:
  /**
   * Retains the ownership.
   *
   * This increases the XmcRef's reference count.
   *
   * @see release, autorelease
   * @js NA
   */
  void retain();
  
  /**
   * Releases the ownership immediately.
   *
   * This decrements the XmcRef's reference count.
   *
   * If the reference count reaches 0 after the decrement, this XmcRef is
   * destructed.
   *
   * @see retain, autorelease
   * @js NA
   */
  void release();
  
  /**
   * Returns the XmcRef's current reference count.
   *
   * @returns The XmcRef's reference count.
   * @js NA
   */
  unsigned int getReferenceCount() const;
  
#if 1 // for BmcCompatible
  XmcRef* asRef() { return this; }
#endif
  
protected:
  /**
   * Constructor
   *
   * The XmcRef's reference count is 1 after construction.
   * @js NA
   */
  XmcRef();
  
public:
  /**
   * Destructor
   *
   * @js NA
   * @lua NA
   */
  virtual ~XmcRef();
  
protected:
  /// count of references
  unsigned int _referenceCount;
  
  //friend class AutoreleasePool;
  
  // Memory leak diagnostic data (only included when CC_REF_LEAK_DETECTION is defined and its value isn't zero)
#if 1 // XMC_REF_LEAK_DETECTION
public:
  static void printLeaks();
#endif
};

NS_XMC_END

#endif
