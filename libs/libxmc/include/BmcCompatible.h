#pragma once
#include "Xmc.h"
#include "cocone/foundation/Log.h"

#ifndef XMC_BMC_COMPATIBLE
#error XMC_BMC_COMPATIBLE should be defined!
#endif

USING_NS_XMC;

namespace cocone {
#define BmcSymbol   XmcSymbol
#define BmcMovieClip   XmcMovieClip
#define BmcButton   XmcButton
#define BmcShader   XmcShader
#define BmcLoader   XmcLoader

// 오타 강제 수정
#define clearAdditonalTransform   clearAdditionalTransform

  class BmcFactory
  {
  public:
    static BmcLoader::Ptr createBmcLoader() {
      return XmcFactory::createXmcLoader();
    }
    
  protected:
    BmcFactory(){}
    virtual ~BmcFactory(){}
  };
}
