#pragma once

#include "XmcSymbol.h"
#include "XmcMovieClipAdapter.h"

NS_XMC_BEGIN

class XmcButton : public XmcMovieClipAdapter {
public:
  XMC_SHARABLEPTR_DEFINE(XmcButton);
  XMC_DECLARE_IID(XmcButton);

public:
  enum LabelType {
    LabelType_Up,
    LabelType_UpNormal,
    LabelType_Down,
    LabelType_DownNormal,
    LabelType_Disable,
    LabelType_HitArea,
  };
  
  XmcButton();
  virtual ~XmcButton();
  
  virtual void setTouchEnabled(bool enabled) = 0;
  virtual void setEnabled(bool enabled) = 0;
  virtual void setPushHandler(std::function<bool(XmcButton*)> handler) = 0;
  virtual void setPushingHandler(std::function<bool(XmcButton*)> handler) = 0;
  virtual void setCheckBoxMode(bool checkbox) = 0;
  virtual void setCheck(bool check, bool playSound) = 0;
  virtual void setCheck(bool check, bool playSound, bool animated) = 0;
  virtual bool getCheck() const = 0;
  virtual void setSwallowTouches(bool swallow) = 0;
  virtual void setPushSoundHandler(std::function<void(void)> soundPlay) = 0;
  virtual void setEventBlock(bool block) = 0;
  virtual void setBarrageBlockTime(float blockSec = 0.2f) = 0; // 연타방지 시간 설정
  virtual void touchDownAction() = 0;
};

NS_XMC_END
