#pragma once

#include "XmcCommon.h"
#include "XmcSymbol.h"
#include "XmcShader.h"
#include "XmcTexturePart.h"

NS_XMC_BEGIN

class XmcShape : public XmcSymbol {
public:
  XMC_SHARABLEPTR_DEFINE(XmcShape);
  XMC_DECLARE_IID(XmcShape);
  
  //inline XmcSymbol* asSymbol() { return query<XmcSymbol>(); } // XmcLoaderImpl에서만 사용중(static_cast<XmcSymbol*>(xxx) 로 대체함)

  virtual void init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) = 0;
  virtual void initWithSpriteFramename(XmcShader::Ptr shader, XmcTexturePart::Ptr texture, const std::string& spriteFramename) = 0;

protected:
  XmcShape() = default;
  virtual ~XmcShape() = default;
};

NS_XMC_END
