#pragma once

#include "XmcMovieClip.h"
#include "XmcButton.h"

NS_XMC_BEGIN

class XmcNullMovieClip : public XmcButton {
public:
  XMC_DECLARE_ASREF_BY_NODE_IMPL();
  XMC_SHARABLEPTR_DEFINE(XmcNullMovieClip);

  XmcNullMovieClip() {
    _node = new XmcManagedObjectNode<cocos2d::Node>(this);
  }
  virtual ~XmcNullMovieClip() = default;
  
  virtual const cocos2d::Node* asNode() const override { return _node; }
  virtual cocos2d::Node* asNode() override { return _node; }

  virtual bool operator == (const std::nullptr_t) const override
  {
    //xmc_assert(false);
    return true;
  }
  virtual bool operator != (const std::nullptr_t) const override {
    //xmc_assert(false); //ok
    return false;
  }
  virtual explicit operator bool() const override {
    //xmc_assert(false); //ok
    return false;
  }

  void* query(IIDType iid) override {
    if (iid == XmcSymbol::iid()) {
      return static_cast<XmcSymbol*>(this);
    } else if (iid == XmcMovieClip::iid()) {
      return static_cast<XmcMovieClip*>(this);
    } else if (iid == XmcButton::iid()) {
      return static_cast<XmcButton*>(this);
    }
    return nullptr;
  }
  
  virtual XmcSymbol* callNewAndNodeInit() const override {
    xmc_assert(false);
    return new XmcNullMovieClip();
  }
  virtual bool callNodeInit() override {
    xmc_assert(false);
    return true;
  }
  virtual cocos2d::Rect getCurrentFrameBoundingBox() const {
    return asNode()->getBoundingBox();
  }
  
  virtual void init(XmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override {
    return;
  }
  virtual XmcSymbol::Ptr clone() const override {
    return new XmcNullMovieClip();
  }
//  virtual const cocone::bmc::BmcTimeline& getTimeline() const override {
//    return _timeline;
//  }
  virtual XmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const override {
    return clone();
  }
//  virtual XmcSymbol::Ptr getInstanceOne() const override {
//    return clone();
//  }
  virtual XmcSymbol::Ptr destroyInstance(const std::string& instanceName) override {
    return clone();
  }
  virtual int getCurrentFrame() const override {
    return 0;
  }
  virtual const std::string& getCurrentLabel() const override {
    static const std::string kEmptyString = "";
    return kEmptyString;
  }
  virtual int getTotalFrames() const override {
    return 0;
  }
  virtual int getFrameByLabel(const char* label) const override {
    return 0;
  }
  virtual void gotoAndPlay(int frameNumber) override {}
//  virtual void gotoTopAndPlayRecursively() override {}
  virtual void gotoAndPlay(int frameNumber, int count) override {}
  virtual void gotoAndPlayToNextLabel( const char* label ) override {}
  virtual void gotoAndPlayLabel( const char* labelFrom, const char* labelTo = 0 ) override {}
//  virtual void gotoAndPlayToNextFrame( int frameNumber ) override {}
  virtual void gotoAndStop( int frameNumber ) override {}
  virtual void play() override {}
  virtual void stop() override {}
  virtual void stopAll() override {}
  virtual void setLoop( bool isLoop ) override {}
  virtual void setSpeedScale(float scale) override {}
  virtual float getSpeedScale() const override {
    return .0f;
  }
  virtual void setEventListener( std::function<void (EventType, XmcMovieClip*)> listener ) override {}
  virtual void clearEventListener() override {}
  // label callback
  virtual void setLabelCallback( const std::string& labelName, std::function<void(XmcMovieClip*)> listener ) override {}
  virtual void setLabelCallback(std::function<void(const std::string&, XmcMovieClip*)> listener) override {}
  virtual void clearLabelCallback() override {}
  
  virtual void nextFrame( bool child ) override {}
  virtual void prevFrame( bool child ) override {}
  virtual void updateFrameForView() override {}
  virtual void processFrame() override {}
  
  virtual cocos2d::Size setupContentSize() override {
    return cocos2d::Size::ZERO;
  }
  
  virtual const cocos2d::Mat4& getAdditionalTransform() const override {
    return cocos2d::Mat4::IDENTITY; // asNode() 거치지 않고 바로 리턴시킴
  }

  // XmcButton
  virtual void setPushSoundHandler(std::function<void(void)> soundPlay) override {}
  virtual void setPushHandler( std::function<bool (XmcButton*)> handler ) override {}
  virtual void setPushingHandler( std::function<bool (XmcButton*)> handler ) override {}
//  virtual void setTouchBeganHandler(std::function<bool(XmcButton*)> handler) override {}
  virtual void setEnabled( bool enabled ) override {}
  virtual void setEventBlock( bool block ) override {}
  virtual void setCheckBoxMode( bool checkbox ) override {}
//  virtual bool getCheckBoxMode()const override { return false; }
  virtual void setCheck( bool check, bool playSound ) override {}
  virtual void setCheck( bool check, bool playSound , bool animated) override {}
  virtual bool getCheck()const override { return false; }
  virtual void setBarrageBlockTime(float blockSec=0.2f) override {}
//  virtual float getBarrageBlockTime() const override { return .2f; }
  virtual void setTouchEnabled(bool) override {}
  virtual void setSwallowTouches( bool swallow ) override {}
  virtual void touchDownAction() override {}
//  virtual void setSimultaneousPress(bool enabled) override {}
//  virtual bool getSimultaneousPress() const { return false; }
  
private:
  mutable XmcManagedObjectNode<cocos2d::Node>* _node;
};

NS_XMC_END
