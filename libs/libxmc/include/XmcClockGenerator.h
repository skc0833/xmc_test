#pragma once

#include <unordered_map> // not need?
#include <vector>
#include "base/XmcRefPtr.h"

// XmcClockGenerator.h -> XmcMovieClip.h -> XmcLoader.h -> XmcClockGenerator.h
//#include "XmcMovieClip.h" // undeclared identifier XmcLoader in XmcMovieClip.h.

NS_XMC_BEGIN
  
class XmcMovieClip;

class XmcClockGenerator : public XmcRef, public XmcAsRef {
public:
  XMC_SHARABLEPTR_DEFINE(XmcClockGenerator);
  XMC_DECLARE_ASREF_IMPL();

  XmcClockGenerator(); // should be public for create()
  virtual ~XmcClockGenerator();
  
  inline bool operator == (const std::nullptr_t) const {
    xmc_assert(false);
    return false;
  }
  inline bool operator != (const std::nullptr_t) const {
    xmc_assert(false);
    return true;
  }
  inline explicit operator bool() const {
    /*xmc_assert(false);*/
    return true;
  }
  
  void setOneFrameDelta(float oneFrameDelta) { _oneFrameDelta = oneFrameDelta; }
  float getOneFrameDelta() const { return _oneFrameDelta; }
  void update(float delta);
  void registerListener(XmcMovieClip* mc);
  void unregisterListener(XmcMovieClip* mc);
  void unregisterAllListener();
  
  void setSpeedScale(float scale) { _speedScale = scale; }
  float getSpeedScale() const { return _speedScale; }
  
  bool isRegistered(XmcMovieClip* mc) const;

private:
  void notify_listeners();
  
private:
  float _oneFrameDelta = 1.f / 30.f; // set to 30fps by default
  float _speedScale = 1.f;
  
  bool _scheduled;
  float _stockTime;
  std::vector<XmcMovieClip*> _listeners;
  std::unordered_map<XmcMovieClip*, bool> _unregister_reservation;
};
  
NS_XMC_END
