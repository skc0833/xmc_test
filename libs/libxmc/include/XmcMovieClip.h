#pragma once

#include "XmcCommon.h"
#include "XmcLoader.h"
#include "XmcSymbol.h"
#include "protocol/BmcFile.pb.h"

NS_XMC_BEGIN

class XmcMovieClip : public XmcSymbol {
public:
  XMC_SHARABLEPTR_DEFINE(XmcMovieClip);
  XMC_DECLARE_IID(XmcMovieClip);
  
  enum EventType {
    EventType_PlayEndFrame, // called when last frame played
  };
  
  virtual void init(XmcLoader* source, const bmc::BmcTimeline& bmcTimeline) = 0;
  
  // Playback starts with the specified frame number. Ignored if an invalid number is passed.
  virtual void gotoAndPlay(int frameNumber) = 0;
  virtual void gotoAndPlay(int frameNumber, int count) = 0;
  virtual void gotoAndStop(int frameNumber) = 0;
  inline void gotoAndStop(const std::string& label) {
    gotoAndStop(getFrameByLabel(label.c_str()));
  }
  // Plays from the frame with the specified frame label to the previous frame (or last frame) with the next frame label.
  // When loop setting is made, loop playback is performed with the specified frame label standard.
  virtual void gotoAndPlayToNextLabel(const char* label) = 0;
  virtual void gotoAndPlayLabel(const char* labelFrom, const char* labelTo = nullptr) { xmc_assert(false); }
  inline void gotoAndPlayLabel(const std::string& labelFrom, const std::string& labelTo) {
    gotoAndPlayLabel(labelFrom.c_str(), labelTo.c_str());
  }

  // Playback starts from the current frame.
  virtual void play() = 0;
  // Stops playback in the current frame.
  virtual void stop() = 0;
  virtual void stopAll() = 0;
  
  // After playing back to the last frame, specify whether or not to move the playhead to the first frame. The default is to move (loop).
  // This flag also affects frame label specification.
  virtual void setLoop(bool isLoop) = 0;
  
  // It is the total number of frames.
  virtual int getTotalFrames() const = 0;
  virtual int getCurrentFrame() const = 0;
  virtual const std::string& getCurrentLabel() const = 0;
  virtual int getFrameByLabel(const char* label) const = 0;
  
  virtual void nextFrame(bool child) = 0;
  virtual void prevFrame(bool child) = 0;
  virtual void updateFrameForView() = 0;
  virtual void processFrame() = 0;
  
  virtual void setEventListener(std::function<void(EventType, XmcMovieClip*)> listener) = 0;
  virtual void clearEventListener() = 0;
  
  // Set the ratio by which the default speed is multiplied.
  virtual void setSpeedScale(float scale) = 0;
  virtual float getSpeedScale() const = 0;
  
  virtual float getOneFrameDelta() const { xmc_assert(false); return 0.f; } //skc add
  
  // label callback
  virtual void setLabelCallback(const std::string& labelName, std::function<void(XmcMovieClip* mc)> listener) = 0;
  virtual void setLabelCallback(std::function<void(const std::string& label, XmcMovieClip* mc)> listener) = 0;
  virtual void clearLabelCallback() = 0;
  
//  virtual bool inHitArea(cocos2d::Point point) = 0;
//  virtual cocos2d::Rect getCurrentFrameBoundingBox() const = 0;
  
protected:
  XmcMovieClip() = default;
  virtual ~XmcMovieClip() = default;
};

NS_XMC_END
