#pragma once

#include "base/XmcRefPtr.h"
#include "XmcGeom.h"

NS_XMC_BEGIN

class XmcTexturePart;
class XmcShader : public XmcRef, public XmcAsRef {
public:
  XMC_SHARABLEPTR_DEFINE(XmcShader);
  XMC_DECLARE_ASREF_IMPL();

  virtual bool operator == (const std::nullptr_t) const = 0;
  virtual bool operator != (const std::nullptr_t) const = 0;
  virtual explicit operator bool() const = 0;
  
  virtual void init() = 0;
  virtual void setColorTransform(const ColorTransform& colorTransform) = 0;
  virtual void setTexture(XmcTexturePart* tex) = 0;

  virtual void useMostValuableProgram() = 0;

protected:
  XmcShader() = default;
  virtual ~XmcShader() = default;
};

NS_XMC_END
