#pragma once

#include "XmcCommon.h"
#include "XmcLoader.h"
#include "XmcMovieClip.h"
#include "protocol/BmcFile.pb.h"

NS_XMC_BEGIN

class XmcMovieClipAdapter : public XmcMovieClip {
public:
  XMC_DECLARE_ASREF_BY_NODE_IMPL();
  
public:
  XmcMovieClipAdapter();
  virtual ~XmcMovieClipAdapter();
  
  virtual XmcSymbol* callNewAndNodeInit() const = 0;
  virtual bool callNodeInit() = 0;

  virtual const cocos2d::Node* asNode() const override { return _node; }
  virtual cocos2d::Node* asNode() override { return _node; }
  
  //skc workaround cross reference by XmcLoader* instead of XmcLoader::Ptr source
  virtual void init(XmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override;
  virtual XmcSymbol::Ptr clone() const override;
  
  virtual void updateFrameForView() override;
  
  virtual cocos2d::Size setupContentSize() override;
  virtual void setBlendMode(bmc::BmcBlendMode blendMode) override;
  
  virtual void gotoAndPlay(int frameNumber) override;
  virtual void gotoAndPlay(int frameNumber, int count) override;
  virtual void gotoAndStop(int frameNumber) override;
  virtual void gotoAndPlayToNextLabel(const char* label) override;
  virtual void gotoAndPlayLabel(const char* labelFrom, const char* labelTo = nullptr) override;

  virtual void play() override;
  virtual void stop() override;
  virtual void stopAll() override;
  
  virtual void setLoop(bool isLoop) override {
    _isLoop = isLoop;
  }
  
  virtual int getTotalFrames() const override {
    return _timeline.frames().size();
  }
  virtual int getCurrentFrame() const override {
    return _currentFrame;
  }
  virtual const std::string& getCurrentLabel() const override;
  virtual int getFrameByLabel(const char* label) const override;
  
  virtual void nextFrame(bool child) override;
  virtual void prevFrame(bool child) override;
  virtual void processFrame() override;
  
  virtual void setEventListener(std::function<void (EventType, XmcMovieClip*)> listener) override;
  virtual void clearEventListener() override;
  
  virtual XmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const override;
  virtual void getAllInstances(std::vector<XmcSymbol::Ptr>* out) const override {
    *out = _instances;
  }
  virtual XmcSymbol::Ptr getNullObject() const override;
  
  virtual XmcSymbol::Ptr destroyInstance(const std::string& instanceName) override;
  virtual XmcSymbol::Ptr destroyInstance(XmcSymbol::Ptr instance) override;
  
  virtual void iterateInstances(std::function<void(XmcSymbol*)> func) override;
  virtual void iterateInstancesForName(std::function<void(const std::string&, XmcSymbol*)> func) override;
  virtual void iterateInstancesRecursively(std::function<void(XmcSymbol*)> func) override;

  virtual void setSpeedScale(float scale) override {
    _clockGenerator->setSpeedScale(scale); //skc 필요할 경우,
  }
  virtual float getSpeedScale() const override {
    return _clockGenerator->getSpeedScale();
  }
  virtual float getOneFrameDelta() const override {
    return _clockGenerator->getOneFrameDelta();
  }
  
  virtual void setLabelCallback(const std::string& labelName, std::function<void(XmcMovieClip* mc)> listener) override;
  virtual void setLabelCallback(std::function<void(const std::string& label, XmcMovieClip* mc)> listener) override;
  virtual void clearLabelCallback() override;
  
  virtual cocos2d::Rect getCurrentFrameBoundingBox() const override;
  
protected:
  mutable XmcManagedObjectNode<cocos2d::Node>* _node; //skc todo 상속받아서 onExit()을 구현해야할까???(멤버변수 초기화 필요?)

private:
  void setupSymbols(XmcLoader* source);
  void setPlayFrameRange(int startFrame, int count);
  void moveCurrentFrame(int frameNumber);
  
  //void applyAdditionalColorTransformToChild(cocos2d::Node* child) const;
  void processActionScriptByLabels();
  
  int searchFrameLabelIndexByLabel(const char* label) const;
  int searchFrameNumberByFrameLabelIndex(int searchStartFrame, int frameLabelIndex) const;

  std::vector<XmcSymbol::Ptr> _instances;
  std::vector<int> _indexhash;
  bmc::BmcTimeline _timeline;
  int _currentFrame;
  int _prevConstructFrame;
  int _playStartFrame;
  int _playFrameCount;
  bool _isLoop;
  XmcClockGenerator::Ptr _clockGenerator;
  bmc::BmcBlendMode _blendMode;
  
  std::function<void (EventType, XmcMovieClip*)> _listener;
  std::function<void(const std::string&, XmcMovieClip*)> _anyLabelCallback;
  std::map<std::string, std::function<void(XmcMovieClip*)>> _labelCallback;
};

NS_XMC_END

