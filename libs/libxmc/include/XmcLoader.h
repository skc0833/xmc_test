#pragma once

//#include "XmcCommon.h"
#include "base/XmcRefPtr.h"
#include "XmcSymbol.h"
#include "XmcClockGenerator.h"
#include "XmcShader.h"

USING_NS_CC;

NS_XMC_BEGIN
  
class XmcLoader : public XmcRef, public XmcAsRef {
public:
  XMC_SHARABLEPTR_DEFINE(XmcLoader);
  XMC_DECLARE_ASREF_IMPL();

  virtual bool operator == (const std::nullptr_t) const = 0;
  virtual bool operator != (const std::nullptr_t) const = 0;
  virtual explicit operator bool() const = 0;

  virtual bool init(const std::string& path) = 0;
  
  // Get the symbol table.
  virtual const std::vector<XmcSymbol::Ptr>& getSymbolTable() const = 0;
  
  // Acquire the stage (top MovieClip)
  virtual XmcSymbol::Ptr getStage() = 0;
  virtual XmcClockGenerator::Ptr getClockGenerator() = 0;

  virtual void setSoundHandler(std::function<void(void)> playSound) = 0;
  
  virtual XmcShader::Ptr getShader() = 0; // for debug
  
protected:
  XmcLoader() = default;
  virtual ~XmcLoader() = default;
};
  
NS_XMC_END
