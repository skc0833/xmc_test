#pragma once

// 0x00 HI ME LO
// 00   01 00 00
#define XMC_VERSION                     0x00010000
#define XMC_SHARABLEPTR_DEFINE(clazz)   typedef NS_XMC::XmcRefPtr<clazz> Ptr;

#define XMC_BMC_COMPATIBLE
//#define XMC_USE_MI

#include "platform/XmcPlatformMacros.h" //skc todo: 여기서 3개를 포함시키자!
//#include "foundation/Log.h" // for xmc_assert()
//#include "base/XmcRefPtr.h"
