#pragma once

#include "base/XmcRefPtr.h"
#include "protocol/BmcFile.pb.h"
#include "renderer/CCTexture2D.h"

USING_NS_CC;

using namespace cocone;
NS_XMC_BEGIN

class XmcTexture : public XmcAsRef {
public:
  XMC_SHARABLEPTR_DEFINE(XmcTexture);
  
  virtual void init(int imageIndex, const bmc::BmcImage* bmcImage, const std::string& baseName) = 0;
  virtual cocos2d::Texture2D* getCCTexture2D() = 0;
  virtual std::string getSpriteFrameName() = 0;
  
protected:
  XmcTexture() = default;
  virtual ~XmcTexture() = default;
};

NS_XMC_END
