#pragma once

#include "XmcCommon.h"
#include "XmcSymbol.h"
#include "XmcShader.h"
#include "XmcTexturePart.h"

NS_XMC_BEGIN

class XmcScalingGridShape : public XmcSymbol {
public:
  XMC_SHARABLEPTR_DEFINE(XmcScalingGridShape);
  XMC_DECLARE_IID(XmcScalingGridShape);
  
  virtual void init(XmcShader::Ptr shader, XmcTexturePart::Ptr texture) = 0;
  virtual void initWithSpriteFramename(XmcShader::Ptr shader, XmcTexturePart::Ptr texture, const std::string& spriteFramename) = 0;

protected:
  XmcScalingGridShape() = default;
  virtual ~XmcScalingGridShape() = default;
};

NS_XMC_END
