#pragma once

/**
 * Define some platform specific macros.
 */
//#include "base/ccConfig.h"
#include "XmcPlatformConfig.h"
#include "XmcPlatformDefine.h"

/** @def CREATE_FUNC(__TYPE__)
 * Define a create function for a specific type, such as Layer.
 *
 * @param __TYPE__  class type to add create(), such as Layer.
 */
//#define CREATE_FUNC(__TYPE__) \
//static __TYPE__* create() \
//{ \
//    __TYPE__ *pRet = new(std::nothrow) __TYPE__(); \
//    if (pRet && pRet->init()) \
//    { \
//        pRet->autorelease(); \
//        return pRet; \
//    } \
//    else \
//    { \
//        delete pRet; \
//        pRet = nullptr; \
//        return nullptr; \
//    } \
//}


// Generic macros

/// @name namespace xmc
/// @{
#ifdef __cplusplus
#ifdef XMC_BMC_COMPATIBLE // #ifdef XMC_BMC_COMPATIBLE 로 판단시에는 적용안되는 소스가 있는듯함(compile error)
    #define NS_XMC_BEGIN                     namespace cocone {
    #define NS_XMC_END                       }
    #define USING_NS_XMC                     using namespace cocone
    #define NS_XMC                           ::cocone
#else
#error skc111
    #define NS_XMC_BEGIN                     namespace xmc {
    #define NS_XMC_END                       }
    #define USING_NS_XMC                     using namespace xmc
    #define NS_XMC                           ::xmc
#endif
#else
    #define NS_XMC_BEGIN 
    #define NS_XMC_END 
    #define USING_NS_XMC 
    #define NS_XMC
#endif 
//  end of namespace group
/// @}


#define XMC_SAFE_DELETE(p)           do { delete (p); (p) = nullptr; } while(0)
#define XMC_SAFE_DELETE_ARRAY(p)     do { if(p) { delete[] (p); (p) = nullptr; } } while(0)
#define XMC_SAFE_FREE(p)             do { if(p) { free(p); (p) = nullptr; } } while(0)
#define XMC_SAFE_RELEASE(p)          do { if(p) { (p)->release(); } } while(0)
#define XMC_SAFE_RELEASE_NULL(p)     do { if(p) { (p)->release(); (p) = nullptr; } } while(0)
#define XMC_SAFE_RETAIN(p)           do { if(p) { (p)->retain(); } } while(0)
#define XMC_BREAK_IF(cond)           if(cond) break

#define __XMCLOGWITHFUNCTION(s, ...) \
  NS_XMC::log("%s : %s",__FUNCTION__, StringUtils::format(s, ##__VA_ARGS__).c_str())

#if 0 //skc not used(using xmc_dlog(), xmc_assert())
/// @name Cocos2d debug
/// @{
#if !defined(COCOS2D_DEBUG) || COCOS2D_DEBUG == 0
#define XMCLOG(...)       do {} while (0)
#define XMCLOGINFO(...)   do {} while (0)
#define XMCLOGERROR(...)  do {} while (0)
#define XMCLOGWARN(...)   do {} while (0)

#elif COCOS2D_DEBUG == 1
#define XMCLOG(format, ...)      NS_XMC::log(format, ##__VA_ARGS__)
#define XMCLOGERROR(format,...)  NS_XMC::log(format, ##__VA_ARGS__)
#define XMCLOGINFO(format,...)   do {} while (0)
#define XMCLOGWARN(...) __XMCLOGWITHFUNCTION(__VA_ARGS__)

#elif COCOS2D_DEBUG > 1
#define XMCLOG(format, ...)      NS_XMC::log(format, ##__VA_ARGS__)
#define XMCLOGERROR(format,...)  NS_XMC::log(format, ##__VA_ARGS__)
#define XMCLOGINFO(format,...)   NS_XMC::log(format, ##__VA_ARGS__)
#define XMCLOGWARN(...) __XMCLOGWITHFUNCTION(__VA_ARGS__)
#endif // COCOS2D_DEBUG
#endif //0

//  end of debug group
/// @}

/** @def XMC_DISALLOW_COPY_AND_ASSIGN(TypeName)
 * A macro to disallow the copy constructor and operator= functions.
 * This should be used in the private: declarations for a class
 */
#if defined(__GNUC__) && ((__GNUC__ >= 5) || ((__GNUG__ == 4) && (__GNUC_MINOR__ >= 4))) \
    || (defined(__clang__) && (__clang_major__ >= 3)) || (_MSC_VER >= 1800)
#define XMC_DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName &) = delete; \
    TypeName &operator =(const TypeName &) = delete;
#else
#define XMC_DISALLOW_COPY_AND_ASSIGN(TypeName) \
    TypeName(const TypeName &); \
    TypeName &operator =(const TypeName &);
#endif

/** @def XMC_DISALLOW_IMPLICIT_CONSTRUCTORS(TypeName)
 * A macro to disallow all the implicit constructors, namely the
 * default constructor, copy constructor and operator= functions.
 *
 * This should be used in the private: declarations for a class
 * that wants to prevent anyone from instantiating it. This is
 * especially useful for classes containing only static methods. 
 */
#define XMC_DISALLOW_IMPLICIT_CONSTRUCTORS(TypeName)    \
    TypeName();                                        \
    XMC_DISALLOW_COPY_AND_ASSIGN(TypeName)

/** @def XMC_DEPRECATED_ATTRIBUTE
 * Only certain compilers support __attribute__((deprecated)).
 */
#if defined(__GNUC__) && ((__GNUC__ >= 4) || ((__GNUC__ == 3) && (__GNUC_MINOR__ >= 1)))
    #define XMC_DEPRECATED_ATTRIBUTE __attribute__((deprecated))
#elif _MSC_VER >= 1400 //vs 2005 or higher
    #define XMC_DEPRECATED_ATTRIBUTE __declspec(deprecated)
#else
    #define XMC_DEPRECATED_ATTRIBUTE
#endif 

/** @def XMC_DEPRECATED(...)
 * Macro to mark things deprecated as of a particular version
 * can be used with arbitrary parameters which are thrown away.
 * e.g. XMC_DEPRECATED(4.0) or XMC_DEPRECATED(4.0, "not going to need this anymore") etc.
 */
#define XMC_DEPRECATED(...) XMC_DEPRECATED_ATTRIBUTE

/** @def XMC_FORMAT_PRINTF(formatPos, argPos)
 * Only certain compiler support __attribute__((format))
 *
 * @param formatPos 1-based position of format string argument.
 * @param argPos    1-based position of first format-dependent argument.
 */
#if defined(__GNUC__) && (__GNUC__ >= 4)
#define XMC_FORMAT_PRINTF(formatPos, argPos) __attribute__((__format__(printf, formatPos, argPos)))
#elif defined(__has_attribute)
  #if __has_attribute(format)
  #define XMC_FORMAT_PRINTF(formatPos, argPos) __attribute__((__format__(printf, formatPos, argPos)))
  #endif // __has_attribute(format)
#else
#define XMC_FORMAT_PRINTF(formatPos, argPos)
#endif

#if defined(_MSC_VER)
#define XMC_FORMAT_PRINTF_SIZE_T "%08lX"
#else
#define XMC_FORMAT_PRINTF_SIZE_T "%08zX"
#endif

#ifdef __GNUC__
#define XMC_UNUSED __attribute__ ((unused))
#else
#define XMC_UNUSED
#endif

/** @def XMC_REQUIRES_NULL_TERMINATION
 * 
 */
#if !defined(XMC_REQUIRES_NULL_TERMINATION)
    #if defined(__APPLE_CC__) && (__APPLE_CC__ >= 5549)
        #define XMC_REQUIRES_NULL_TERMINATION __attribute__((sentinel(0,1)))
    #elif defined(__GNUC__)
        #define XMC_REQUIRES_NULL_TERMINATION __attribute__((sentinel))
    #else
        #define XMC_REQUIRES_NULL_TERMINATION
    #endif
#endif
