#pragma once

#include "../XMCPlatformConfig.h"
#if XMC_TARGET_PLATFORM == XMC_PLATFORM_IOS

#include <assert.h>

#define XMC_DLL

#define XMC_ASSERT(cond) assert(cond)


#define XMC_UNUSED_PARAM(unusedparam) (void)unusedparam

/* Define NULL pointer value */
#ifndef NULL
#ifdef __cplusplus
#define NULL    0
#else
#define NULL    ((void *)0)
#endif
#endif


#endif // XMC_PLATFORM_IOS
