#pragma once

/// @cond DO_NOT_SHOW

/**
  Config of cocos2d-x project, per target platform.
  
  THIS FILE MUST NOT INCLUDE ANY OTHER FILE
*/

//////////////////////////////////////////////////////////////////////////
// pre configure
//////////////////////////////////////////////////////////////////////////

// define supported target platform macro which XMC uses.
#define XMC_PLATFORM_UNKNOWN            0
#define XMC_PLATFORM_IOS                1
#define XMC_PLATFORM_ANDROID            2
#define XMC_PLATFORM_WIN32              3
#define XMC_PLATFORM_MARMALADE          4
#define XMC_PLATFORM_LINUX              5
#define XMC_PLATFORM_BADA               6
#define XMC_PLATFORM_BLACKBERRY         7
#define XMC_PLATFORM_MAC                8
#define XMC_PLATFORM_NACL               9
#define XMC_PLATFORM_EMSCRIPTEN        10
#define XMC_PLATFORM_TIZEN             11
#define XMC_PLATFORM_QT5               12
#define XMC_PLATFORM_WINRT             13

// Determine target platform by compile environment macro.
#define XMC_TARGET_PLATFORM             XMC_PLATFORM_UNKNOWN

// mac
#if defined(XMC_TARGET_OS_MAC) /*|| defined(__APPLE__)*/ //skc remove __APPLE__
#undef  XMC_TARGET_PLATFORM
#define XMC_TARGET_PLATFORM         XMC_PLATFORM_MAC
#endif

// iphone
#if defined(XMC_TARGET_OS_IPHONE)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM         XMC_PLATFORM_IOS
#else
//#error skc0000000000111
#define XMC_TARGET_PLATFORM         XMC_PLATFORM_IOS //skc todo Preprocessor Macros 에 XMC_TARGET_OS_IPHONE 가 정의됐는데... 왜?
#endif

// android
#if defined(ANDROID)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM         XMC_PLATFORM_ANDROID
#endif

// win32
#if defined(_WIN32) && defined(_WINDOWS)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM         XMC_PLATFORM_WIN32
#endif

// linux
#if defined(LINUX) && !defined(__APPLE__)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM         XMC_PLATFORM_LINUX
#endif

// marmalade
#if defined(MARMALADE)
#undef  XMC_TARGET_PLATFORM
#define XMC_TARGET_PLATFORM         XMC_PLATFORM_MARMALADE
#endif

// bada
#if defined(SHP)
#undef  XMC_TARGET_PLATFORM
#define XMC_TARGET_PLATFORM         XMC_PLATFORM_BADA
#endif

// qnx
#if defined(__QNX__)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM     XMC_PLATFORM_BLACKBERRY
#endif

// native client
#if defined(__native_client__)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM     XMC_PLATFORM_NACL
#endif

// Emscripten
#if defined(EMSCRIPTEN)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM     XMC_PLATFORM_EMSCRIPTEN
#endif

// tizen
#if defined(TIZEN)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM     XMC_PLATFORM_TIZEN
#endif

// qt5
#if defined(XMC_TARGET_QT5)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM     XMC_PLATFORM_QT5
#endif

// WinRT (Windows 8.1 Store/Phone App)
#if defined(WINRT)
    #undef  XMC_TARGET_PLATFORM
    #define XMC_TARGET_PLATFORM          XMC_PLATFORM_WINRT
#endif

//////////////////////////////////////////////////////////////////////////
// post configure
//////////////////////////////////////////////////////////////////////////

// check user set platform
#if ! XMC_TARGET_PLATFORM
    #error  "Cannot recognize the target platform; are you targeting an unsupported platform?"
#endif 

#if (XMC_TARGET_PLATFORM == XMC_PLATFORM_WIN32)
#ifndef __MINGW32__
#pragma warning (disable:4127) 
#endif 
#endif  // XMC_PLATFORM_WIN32

#if XMC_TARGET_PLATFORM != XMC_PLATFORM_IOS
#error "not supported os! " XMC_TARGET_PLATFORM
#endif

/// @endcond

