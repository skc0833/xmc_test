#pragma once

#include "XmcSymbol.h"

NS_XMC_BEGIN

class XmcNullSymbol : public XmcSymbol {
public:
  XMC_DECLARE_ASREF_BY_NODE_IMPL();
  XMC_SHARABLEPTR_DEFINE(XmcNullSymbol);

  XmcNullSymbol() = default;
  virtual ~XmcNullSymbol() = default;

  virtual bool operator == (const std::nullptr_t) const override { xmc_assert(false); return true; }
  virtual bool operator != (const std::nullptr_t) const override { xmc_assert(false); return false; }
  virtual explicit operator bool() const override { xmc_assert(false); return false; }
  
  void* query(IIDType iid) override {
    if (iid == XmcSymbol::iid()) {
      return static_cast<XmcSymbol*>(this);
    }
    return nullptr;
  }
  
  virtual XmcSymbol::Ptr clone() const override {
    return new XmcNullSymbol();
  }

  virtual XmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const override {
    return clone();
  }
  virtual XmcSymbol::Ptr destroyInstance(const std::string& instanceName) override {
    return clone();
  }
  virtual cocos2d::Size setupContentSize() override {
    return cocos2d::Size::ZERO;
  }
  
  virtual const cocos2d::Mat4& getAdditionalTransform() const override {
    return cocos2d::Mat4::IDENTITY;
  }

private:
  mutable cocos2d::Node* _node;
};

NS_XMC_END
