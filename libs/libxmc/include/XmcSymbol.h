#pragma once

#include "base/XmcRefPtr.h"
#include "XmcGeom.h"
#include "XmcUnknownInterface.h"
#include <cocos2d.h> // for cocos2d::EventListenerTouchOneByOne, cocos2d::Color4F

NS_XMC_BEGIN

class XmcSymbol : public XmcAsRef, public XmcUnknownInterface {
public:
  class XmcManagedObject { // XmcManagedObjectNode 가 템플릿이라서 따로 부모 클래스를 정의함
  public:
    const XmcSymbol* getSymbol() const {
      xmc_assert(_symbol);
      return _symbol;
    }
    XmcSymbol* getSymbol() { return _symbol; }
    virtual const cocos2d::Mat4& getAdditionalTransform() const = 0;
  protected:
    XmcManagedObject(XmcSymbol* s)
    : _symbol(s) {
    }
    virtual ~XmcManagedObject() = default;
  private:
    XmcSymbol* _symbol; // owner object
  };
  
  // T should be cocos2d::Node inherited type
  template <class T>
  class XmcManagedObjectNode : public XmcManagedObject, public T { // 상속 순서가 상관이 있나?
  public:
    XmcManagedObjectNode(XmcSymbol* s)
    : XmcManagedObject(s) {
      xmc_assert(s != nullptr);
    }
    virtual ~XmcManagedObjectNode() {
      // cocos2d::Node 객체이므로 적절한 시점에 소멸자가 호출되게 된다(removeChild() 등)
      // 이 시점에 XmcNode 을 포함하고 있는 XmcSymbol 객체도 해제시키게 처리함
      if (areYouSymbol(this)) {
        auto s = areYouSymbol(this);
        delete s;
      } else {
        xmc_assert(false);
      }
    }
    virtual const cocos2d::Mat4& getAdditionalTransform() const override { // cocos2d 에는 없는 함수임
#if COCOS2D_VERSION < 0x00031600
      return T::_additionalTransform;
#else
      if (T::_additionalTransform != nullptr) {
        return *(T::_additionalTransform);
      } else {
        return cocos2d::Mat4::IDENTITY;
      }
#endif
    }
  };

  XMC_SHARABLEPTR_DEFINE(XmcSymbol);
  
  XmcSymbol();
  virtual ~XmcSymbol();

  virtual bool operator == (const std::nullptr_t) const;
  virtual bool operator != (const std::nullptr_t) const;
  virtual explicit operator bool() const;
  
  virtual XmcSymbol::Ptr clone() const = 0;
  
  virtual const cocos2d::Node* asNode() const;
  virtual cocos2d::Node* asNode();
  
  inline XmcSymbol* asSymbol() { return query<XmcSymbol>(); }
  
  virtual cocos2d::Size setupContentSize();
  virtual void setBlendMode(bmc::BmcBlendMode blendMode);
  
  virtual void setAdditionalTransform(const cocos2d::Mat4& additionalTransform);
  virtual void setAdditionalTransform(const cocos2d::AffineTransform& additionalTransform);

  virtual const cocos2d::Mat4& getAdditionalTransform() const;
  /*virtual*/ void clearAdditionalTransform();
  
  // Overwrite the color change setting specified in Flash
  virtual void setAdditionalColorTransform(const NS_XMC::ColorTransform& additionalColorTransform);
  
  // Acquire the color change setting specified in Flash
  /*virtual*/ const ColorTransform& getAdditionalColorTransform() const {
    return _additionalColorTransform;
  }
  // Multiply the color change setting on the Node side and the color change setting on the Flash
  // to obtain the final color change setting.
  virtual ColorTransform getColorTransform() const {
    return _additionalColorTransform;
  }
  
  ColorTransform getAscendColorTransform() const;
  
  // Retrieves an instance that is placed with the specified instance name.
  virtual XmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const;
  
  XmcSymbol::Ptr getInstance() {
    return this;
  }
  
  template<class First, class... Rest>
  XmcSymbol::Ptr getInstance(const First& first, const Rest& ... rest) {
    return getInstanceImpl(first)->getInstance(rest...); // getInstance() 함수가 정의돼 있어야 빌드 에러 발생안하게 됨
  }
  
  template<class T>
  T* getInstance(const std::string& instanceName) const {
    auto inst = getInstanceImpl(instanceName);
    return inst->query<T>(); // dynamic_cast<T*>(inst.get());
  }
  
  XmcSymbol::Ptr getInstance(const std::vector<std::string>& instancePath) {
    XmcSymbol::Ptr symbol = this;
    for (auto& name : instancePath) {
      symbol = symbol->getInstance(name);
    }
    return symbol;
  }
  
  virtual XmcSymbol::Ptr getNullObject() const;
  
  virtual void getAllInstances(std::vector<XmcSymbol::Ptr>* out) const { xmc_assert(false); }
  
  XmcSymbol::Ptr pluckInstance() {
    return this;
  }
  
  template<class First, class... Rest>
  XmcSymbol::Ptr pluckInstance(const First& first, const Rest& ... rest) {
    if (sizeof...(rest) > 0) {
      return getInstanceImpl(first)->pluckInstance(rest...);
    } else {
      return destroyInstance(first);
    }
  }
  
  virtual XmcSymbol::Ptr destroyInstance(const std::string& instanceName);
  virtual XmcSymbol::Ptr destroyInstance(XmcSymbol::Ptr instance);
  
  virtual void iterateInstances(std::function<void(XmcSymbol*)> func) { xmc_assert(false); }
  virtual void iterateInstancesForName(std::function<void(const std::string&, XmcSymbol*)> func) { xmc_assert(false); }

  // Process child objects including recursively all child instances
  virtual void iterateInstancesRecursively(std::function<void(XmcSymbol*)> func) { xmc_assert(false); }

  virtual cocos2d::Rect getCurrentFrameBoundingBox() const;
  virtual bool inHitArea(cocos2d::Point point);
  
  virtual void setTouchBlockEnabled(bool enabled);
  virtual void setRenderOnFBO(bool renderOnFBO) { _renderOnFBO = renderOnFBO; }
  virtual void setColorAdd(const cocos2d::Color4F color);
  virtual cocos2d::Color4F getColorAdd() const;
  virtual void setColorMultiply(const cocos2d::Color4F color);
  
  XMC_DECLARE_IID(XmcSymbol);

protected:
  ColorTransform _additionalColorTransform;
  cocos2d::EventListenerTouchOneByOne* _touchBlockListener = nullptr;
  bool _renderOnFBO = false;
};

inline XmcSymbol* areYouSymbol(cocos2d::Node* node) {
  // node->getName() 은 이후에 재설정될수 있으므로 사용하지 않게 함
  XmcSymbol::XmcManagedObject* obj = dynamic_cast<XmcSymbol::XmcManagedObject*>(node);
  if (obj) {
    return obj->getSymbol();
  } else {
    //xmc_assert(false);
    return nullptr;
  }
}

inline const XmcSymbol* areYouSymbol(const cocos2d::Node* node) {
  // node->getName() 은 이후에 재설정될수 있으므로 사용하지 않게 함
  const XmcSymbol::XmcManagedObject* obj = dynamic_cast<const XmcSymbol::XmcManagedObject*>(node);
  if (obj) {
    return obj->getSymbol();
  } else {
    //xmc_assert(false);
    return nullptr;
  }
}

NS_XMC_END
