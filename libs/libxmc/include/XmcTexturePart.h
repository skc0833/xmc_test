#pragma once

#include "base/XmcRefPtr.h"
#include "XmcTexture.h" // for XmcTexture::Ptr
#include "protocol/BmcFile.pb.h"
#include <math/Vec2.h>

NS_XMC_BEGIN

class XmcTexturePart : public XmcAsRef {
public:
  XMC_SHARABLEPTR_DEFINE(XmcTexturePart);
  
  virtual void init(XmcTexture::Ptr tex, const bmc::BmcTextureRef& ref) = 0;
  virtual XmcTexture::Ptr getTexture() = 0;
  virtual const bmc::BmcRectangle& getRectangle() const = 0;
  virtual const cocos2d::Vec2& getOffset() const = 0;
  
  virtual const bmc::BmcRectangle& getScale9GridRectangle() const = 0;
  
protected:
  XmcTexturePart() = default;
  virtual ~XmcTexturePart() = default;
};

NS_XMC_END
