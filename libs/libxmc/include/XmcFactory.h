#pragma once
#include "XmcLoader.h"
#include "XmcTexture.h"
#include "XmcTexturePart.h"
#include "XmcShader.h"
#include "XmcShape.h"
//#include "XmcSpriteShape.h"
#include "XmcScalingGridShape.h"
#include "XmcMovieClip.h"
#include "XmcButton.h"
#include "XmcClockGenerator.h"


NS_XMC_BEGIN

#define XMC_FACTORY_CREATE_FUNC_IMPL(clazz) \
clazz::Ptr XmcFactory::create##clazz() \
{ \
  clazz::Ptr p = new clazz##Impl(); \
  p->asRef()->release(); \
  return p; \
}

  class XmcFactory {
  public:
    static XmcLoader::Ptr createXmcLoader();
    static XmcTexture::Ptr createXmcTexture();
    static XmcTexturePart::Ptr createXmcTexturePart();
    static XmcShader::Ptr createXmcShader();
    static XmcShape::Ptr createXmcShape();
    static XmcShape::Ptr createXmcSpriteShape(); //skc return type is not XmcSpriteShape
    static XmcScalingGridShape::Ptr createXmcScalingGridShape();
    static XmcMovieClip::Ptr createXmcMovieClip();
    static XmcButton::Ptr createXmcButton();
    static XmcClockGenerator::Ptr createXmcClockGenerator();
    
  private:
    XmcFactory() = default;
    virtual ~XmcFactory() = default;
  };

NS_XMC_END
