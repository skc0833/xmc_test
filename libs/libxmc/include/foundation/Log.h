// Log.h
// log macros
//
// include example.
// #define XMC_DLOG_DISABLED // if this define is enabled, all xmc_dlog called in this file is ignored.
// #include "Log.h"

#pragma once
#include "../XmcCommon.h" // XmcCommon.h 에서도 Log.h 를 포함하고 있으므로 여기서 포함시키면 XmcRefPtr.h 에서 xmc_assert() undefined 에러
//#include "../platform/XmcPlatformMacros.h" // for NS_XMC_BEGIN
#include <string>

#if !defined(__func__)
#define __func__ __FUNCTION__
#endif

NS_XMC_BEGIN
enum LogPrefix {
  LogPrefix_Error=0,
  LogPrefix_Warning,
  LogPrefix_Info,
  LogPrefix_Save,
  LogPrefix_Debug,
  LogPrefix_CountOfLogPrefix,
};
void consoleOutput( LogPrefix prefix, const char* str );
std::string formatString( const char* format, ... );
std::string* formatString( std::string* out, const char* format, ... );
bool logOutputCond( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* condStr, const char* format, ... );
bool logOutputCond( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* condStr);
bool logOutput( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* format, ... );
bool logOutput( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt );
NS_XMC_END

#ifdef NDEBUG

#define xmc_econd(cond,...)  (cond)
#define xmc_elog(...)        ((void)0)
#define xmc_assert(cond,...) ((void)0)
#define xmc_wcond(cond,...)  (cond)
#define xmc_wlog(...)        ((void)0)
#define xmc_ilog(...)        ((void)0)
#define xmc_slog(...)        ((void)0)
#define xmc_dlog(...)        ((void)0)

#else

// error
#define xmc_econd(cond,...) ( (cond) && NS_XMC::logOutputCond(true, NS_XMC::LogPrefix_Error, __func__,__FILE__,__LINE__,#cond,##__VA_ARGS__))
#define xmc_elog(...) { NS_XMC::logOutput(true, NS_XMC::LogPrefix_Error, __func__,__FILE__,__LINE__,__VA_ARGS__); }
#define xmc_assert(cond,...) ( (cond) ? (void)0 : (void)NS_XMC::logOutputCond(true, NS_XMC::LogPrefix_Error, __func__,__FILE__,__LINE__,#cond,##__VA_ARGS__) )

// warning
#define xmc_wcond(cond,...) ( (cond) && NS_XMC::logOutputCond(false,NS_XMC::LogPrefix_Warning, __func__,__FILE__,__LINE__,#cond,##__VA_ARGS__) )
#define xmc_wlog(...) { NS_XMC::logOutput(false, NS_XMC::LogPrefix_Warning, __func__,__FILE__,__LINE__,__VA_ARGS__); }

// info
#define xmc_ilog(...) { NS_XMC::logOutput(false, NS_XMC::LogPrefix_Info, __func__,__FILE__,__LINE__,__VA_ARGS__); }

// save
#define xmc_slog(...) { NS_XMC::logOutput(false, NS_XMC::LogPrefix_Save, __func__,__FILE__,__LINE__,__VA_ARGS__); }

// debug
#define xmc_dlog(...) { NS_XMC::logOutput(false, NS_XMC::LogPrefix_Debug,__func__,__FILE__,__LINE__,__VA_ARGS__); }
#endif

//// for BmcCompatible.h
//#define ccn_assert    xmc_assert
//#define ccn_wlog      xmc_wlog
//#define ccn_dlog      xmc_dlog

