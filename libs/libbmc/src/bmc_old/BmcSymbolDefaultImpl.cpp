﻿// BmcSymbol.cpp.cpp
//

#include "BmcSymbolDefaultImpl.h"
#include "BmcNullSymbolImpl.h"

USING_NS_CC;

namespace cocone {

    BmcSymbol::Ptr BmcSymbolDefaultImpl::getInstance(const std::string& instanceName) const
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::getInstanceOne() const
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::destroyInstance(const std::string& instanceName)
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::getNullObject() const
    {
        return new BmcNullSymbolImpl();
    }
}
