﻿// BmcShaderDefaultImpl.cpp
//
#include <cocone/bmc/BmcShader.h>

#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/bmc/Shaders.h>
#include <cocone/foundation/Log.h>

#include "BmcFactoryImpl.h"

USING_NS_CC;

namespace cocone {

    class BmcShaderImpl : public BmcShader, public ZeroRefCount<cocos2d::Ref>
    {
        /*
        const char *const Uniform_ColorMultiplierIdentify = "u_colorMultiplier";
        const char *const Uniform_ColorOffsetIdentify = "u_colorOffset";
        const char *const Uniform_AlphaMultiplierIdentify = "u_alphaMultiplier";
        extern const char *const kCCShader_Vector_PositionTextureColor_UseOffsetAlpha;
        extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetAlpha;
        extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor;
        extern const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha;
        extern const char *const kCCShader_Vector_PositionTextureColor_OnlyTexture;
        */
        GLProgram* _useOffsetAlphaP;
        GLint        _useOffsetAlphaP_u_colorMultiplier;
        GLint        _useOffsetAlphaP_u_colorOffset;

        GLProgram* _noOffsetAlphaP;
        GLint        _noOffsetAlphaP_u_colorMultiplier;
        GLint        _noOffsetAlphaP_u_colorOffset;

        GLProgram* _noOffsetOnlyMultiplierClor;
        GLint        _noOffsetOnlyMultiplierClor_u_colorMultiplier;

        GLProgram* _noOffsetOnlyMultiplierALpha;
        GLint        _noOffsetOnlyMultiplierALpha_u_alphaMultiplier;

        GLProgram* _onlyTexture;

        ColorTransform _colorTransform;
    public:
        CCN_DECLARE_ASREF_IMPL();

        BmcShaderImpl()
        {

        }

        virtual ~BmcShaderImpl()
        {
            //		ccn_dlog( "" );
        }
        void init() override
        {
            _useOffsetAlphaP = GLProgramCache::getInstance()->getGLProgram(cocone::kCCShader_Vector_PositionTextureColor_UseOffsetAlpha);
            _noOffsetAlphaP = GLProgramCache::getInstance()->getGLProgram(cocone::kCCShader_Vector_PositionTextureColor_NoOffsetAlpha);
            _noOffsetOnlyMultiplierClor = GLProgramCache::getInstance()->getGLProgram(cocone::kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor);
            _noOffsetOnlyMultiplierALpha = GLProgramCache::getInstance()->getGLProgram(cocone::kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha);
            _onlyTexture = GLProgramCache::getInstance()->getGLProgram(cocone::kCCShader_Vector_PositionTextureColor_OnlyTexture);

            _useOffsetAlphaP_u_colorMultiplier = glGetUniformLocation(_useOffsetAlphaP->getProgram(), Uniform_ColorMultiplierIdentify);
            _useOffsetAlphaP_u_colorOffset = glGetUniformLocation(_useOffsetAlphaP->getProgram(), Uniform_ColorOffsetIdentify);

            _noOffsetAlphaP_u_colorMultiplier = glGetUniformLocation(_noOffsetAlphaP->getProgram(), Uniform_ColorMultiplierIdentify);
            _noOffsetAlphaP_u_colorOffset = glGetUniformLocation(_noOffsetAlphaP->getProgram(), Uniform_ColorOffsetIdentify);

            _noOffsetOnlyMultiplierClor_u_colorMultiplier = glGetUniformLocation(_noOffsetOnlyMultiplierClor->getProgram(), Uniform_ColorMultiplierIdentify);

            _noOffsetOnlyMultiplierALpha_u_alphaMultiplier = glGetUniformLocation(_noOffsetOnlyMultiplierALpha->getProgram(), Uniform_AlphaMultiplierIdentify);

        }
        virtual void setTexture(Texture2D* tex) override
        {
            GL::bindTexture2D(tex->getName());
        }
        virtual void setTexture(BmcTexturePart* tex) override
        {
            GL::bindTexture2D(tex->getTexture()->getCCTexture2D()->getName());
        }

        virtual void setColorTransform(const ColorTransform& colorTransform) override
        {
            _colorTransform = colorTransform;
            //float* m=const_cast<float*>(colorTransform.multiplier());
            //float* a=const_cast<float*>(colorTransform.offset());
            //ccn_dlog( "ct(%f,%f,%f,%f,%f,%f,%f,%f)", m[0], m[1], m[2], m[3], a[0], a[1], a[2], a[3]);
            //        m[3] = 0.5f;
            //        m[0] = 0.1f;
            //        m[1] = 0.1f;
            //        m[2] = 0.1f;
        }

        virtual void useMostValuableProgram() override
        {
            const float* m = _colorTransform.multiplier();
            const float* a = _colorTransform.offset();
            GLProgram* program = nullptr;
#ifdef USE_COCOS_SPRITE_LOGIC
            program = _onlyTexture;
            program->use();
#else
            if (a[3] == 0.f){
                if (a[0] == 0.f && a[1] == 0.f && a[2] == 0.f){
                    if (m[0] == 1.f && m[1] == 1.f && m[2] == 1.f){
                        if (m[3] == 1.f){
                            //ccn_dlog("_onlyTexture");
                            program = _onlyTexture;
                            program->use();
                        }
                        else{
                            //ccn_dlog("_noOffsetOnlyMultiplierALpha");
                            program = _noOffsetOnlyMultiplierALpha;
                            program->use();
                            glUniform1f(_noOffsetOnlyMultiplierALpha_u_alphaMultiplier, m[3]);
                        }
                    }
                    else{
                        //ccn_dlog("_noOffsetOnlyMultiplierClor");
                        program = _noOffsetOnlyMultiplierClor;
                        program->use();
                        glUniform4fv(_noOffsetOnlyMultiplierClor_u_colorMultiplier, 1, m);
                    }
                }
                else{
                    //ccn_dlog("_noOffsetAlphaP");
                    program = _noOffsetAlphaP;
                    program->use();
                    glUniform4fv(_noOffsetAlphaP_u_colorMultiplier, 1, m);
                    glUniform4fv(_noOffsetAlphaP_u_colorOffset, 1, a);
                }
            }
            else{
                //ccn_dlog("_useOffsetAlphaP");
                program = _useOffsetAlphaP;
                program->use();
                glUniform4fv(_useOffsetAlphaP_u_colorMultiplier, 1, m);
                glUniform4fv(_useOffsetAlphaP_u_colorOffset, 1, a);
            }
#endif
            program->setUniformsForBuiltins();
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcShader);

} // cocone
