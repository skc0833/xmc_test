﻿// BmcSymbol.cpp.cpp
//
#pragma once

#include "BmcSymbolDefaultImpl.h"

namespace cocone
{
    /// アクセス不可を起こさないための NullObject
    class BmcNullMovieClipImpl : public BmcMovieClip, public BmcSymbolDefaultImpl, public ZeroRefCount<cocos2d::Node>
    {
        cocone::bmc::BmcTimeline _timeline;
    public:
        // TODO impl assert
        BmcNullMovieClipImpl(){
        }
        virtual ~BmcNullMovieClipImpl() = default;

        CCN_SHARABLEPTR_DEFINE(BmcNullMovieClipImpl);
        CCN_DECLARE_ASNODE_IMPL();

        void* query(IIDType iid) override {
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

        virtual void init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override
        {
            return;
        }

        virtual BmcSymbol::Ptr clone() const override
        {
            return new BmcNullMovieClipImpl();
        }

        virtual const cocone::bmc::BmcTimeline& getTimeline() const override
        {
            return _timeline;
        }

        virtual BmcSymbol::Ptr getInstance(const std::string& instanceName) const override
        {
            return clone();
        }
        virtual BmcSymbol::Ptr getInstanceOne() const override
        {
            return clone();
        }

        virtual BmcSymbol::Ptr destroyInstance(const std::string& instanceName) override
        {
            return clone();
        }

        virtual int getCurrentFrame() const override
        {
            return 0;
        }
        virtual const std::string& getCurrentLabel() const override
        {
            static const std::string kEmptyString = "";
            return kEmptyString;
        }
        virtual int getTotalFrames() const override
        {
            return 0;
        }
        virtual int getFrameByLabel(const char* label) const override
        {
            return 0;
        }
        virtual void gotoAndPlay(int frameNumber) override {}
        virtual void gotoTopAndPlayRecursively() override {}
        virtual void gotoAndPlay(int frameNumber, int count) override {}
        virtual void gotoAndPlayToNextLabel( const char* label ) override {}
        virtual void gotoAndPlayLabel( const char* labelFrom, const char* labelTo = 0 ) override {}
        virtual void gotoAndPlayToNextFrame( int frameNumber ) override {}
        virtual void gotoAndStop( int frameNumber ) override {}
        virtual void play() override {}
        virtual void stop() override {}
        virtual void stopAll() override {}
        virtual void setLoop( bool isLoop ) override {}
        virtual void setSpeedScale(float scale) override {}
        virtual float getSpeedScale() const override
        {
            return .0f;
        }
        virtual void setEventListener( std::function<void (EventType, BmcMovieClip*)> listener ) override {}
        virtual void clearEventListener() override {}
        // label callback
        virtual void setLabelCallback( const std::string& labelName, std::function<void(BmcMovieClip*)> listener ) override {}
        virtual void clearLabelCallback() override {}

        virtual void nextFrame( bool child ) override {}
        virtual void prevFrame( bool child ) override {}
        virtual void updateFrameForView() override {}
        virtual void processFrame() override {}

        virtual cocos2d::Size setupContentSize() override
        {
            return cocos2d::Size::ZERO;
        }

        virtual const cocos2d::Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }

        inline bool operator == (const std::nullptr_t) const { return true; }
        inline bool operator != (const std::nullptr_t) const { return false; }
        inline operator bool() const { return false; }
    };

} // cocone

// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored. 
