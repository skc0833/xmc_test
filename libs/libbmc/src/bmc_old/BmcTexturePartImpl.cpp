﻿// BmcTexturePartDefaultImpl.cpp
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored. 

#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>

#include "BmcFactoryImpl.h"

USING_NS_CC;

namespace cocone {

    class BmcTexturePartImpl : public BmcTexturePart, public ZeroRefCount<cocos2d::Ref>
    {
        BmcTexture::Ptr _texture;
        bmc::BmcTextureRef _ref;
        Vec2 _offset;
    public:
        virtual cocos2d::Ref* asRef() override { return this; };
        virtual const cocos2d::Ref* asRef() const override { return this; }

        BmcTexturePartImpl()
        {

        }
        virtual ~BmcTexturePartImpl()
        {
            //        ccn_dlog( "" );
        }
        virtual BmcTexture* getTexture() override
        {
            return _texture.get();
        }
        virtual cocos2d::Size getTextureSize() const override
        {
            return _texture->getCCTexture2D()->getContentSize();
        }
        virtual const bmc::BmcRectangle& getRectangle() const override
        {
            return _ref.rect();
        }
        virtual const Vec2& getOffset() const override
        {
            return _offset;
        }
        virtual int getTexLeft() const override
        {
            return _ref.tex_left();
        }
        virtual int getTexTop() const override
        {
            return _ref.tex_top();
        }
        virtual bool hasScale9Grid() const override
        {
            return _ref.has_scale9grid();
        }
        virtual const bmc::BmcRectangle& getScale9GridRectangle() const override
        {
            if (_ref.has_scale9grid()){
                return _ref.scale9grid();
            }
            return *(const bmc::BmcRectangle*)nullptr;
        }

        virtual void init(BmcTexture* tex, const bmc::BmcTextureRef& ref) override
        {
            _texture = tex;
            _ref = ref;

            _offset = CC_POINT_PIXELS_TO_POINTS(
                Vec2(
                    _ref.rect().x(),
                    (_ref.rect().y() + _ref.rect().height()) * -1.f
                )
            );
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcTexturePart);
} // cocone
