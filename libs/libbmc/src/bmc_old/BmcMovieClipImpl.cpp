﻿// BmcMovieClipDefaultImpl.cpp.cpp
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored. 
#include "BmcMovieClipAdapter.h"

#include "BmcFactoryImpl.h"

//#define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored. 
//#include <cocone/foundation/Log.h>

USING_NS_CC;

namespace cocone {
    class BmcMovieClipImpl : public ZeroRefCount<Node>, public BmcMovieClipAdapter
    {
        bool _visible_input;
        GLubyte _opacity_input;
        Color3B _color_input;

    protected:
        void* query(IIDType iid) override {
            if (iid == BmcMovieClip::iid()) {
                return static_cast<BmcMovieClip*>(this);
            }
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }
        BmcMovieClipAdapter* callNewAndInit() const override
        {
            auto t = new BmcMovieClipImpl();
            t->ZeroRefCount<Node>::init();
            return t;
        }
        bool callNodeInit() override {
            return ZeroRefCount<Node>::init();
        }
    public://////////////////////////////////////////////////////////////////////
        CCN_DECLARE_ASNODE_IMPL();

        BmcMovieClipImpl()
        {
        }
        virtual ~BmcMovieClipImpl()
        {
        }
/*
        virtual void onEnter() override
        {
            ZeroRefCount<Node>::onEnter();
            if (_timeline.frames().size() > 1){
//                play();
            }
        }
*/
        virtual void update(float dt) override
        {
            nodeFuncUpdate(dt);
        }
        virtual void onExit() override
        {
            nodeFuncOnExit();
            ZeroRefCount<Node>::onExit();
        }

        virtual bool isVisible() const override
        {
            return _visible_input;
        }

        virtual void setVisible(bool visible) override
        {
            _visible_input = visible;
            Node::setVisible(visible);
        }

        virtual GLubyte getOpacity() const override
        {
            return _opacity_input;
        }

        virtual void setOpacity(GLubyte opacity) override
        {
            _opacity_input = opacity;

            const ColorTransform ct = getAscendColorTransform();
            const auto& mul = ct.multiplier();
            const auto& ofs = ct.offset();
            opacity = opacity * (mul[3] + ofs[3]);
            Node::setOpacity(opacity);
        }

        virtual const Color3B& getColor() const override
        {
            return _color_input;
        }

        virtual void setColor(const Color3B& color) override
        {
            _color_input = color;

            const ColorTransform ct = getAscendColorTransform();
            const auto& mul = ct.multiplier();
            Node::setColor({
                (GLubyte)(color.r * mul[0]),
                (GLubyte)(color.g * mul[1]),
                (GLubyte)(color.b * mul[2]),
            });
        }

        virtual const Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }

        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }

        virtual void setAdditonalColorTransform(const ColorTransform& additionalColorTransform) override
        {
            if (_additionalColorTransform == additionalColorTransform) {
                return;
            }
            _additionalColorTransform = additionalColorTransform;

            ColorTransform ct = getAscendColorTransform();
            const auto& mul = ct.multiplier();
            const auto& ofs = ct.offset();

            GLubyte opacity = _opacity_input * mul[3] + 255 * ofs[3];
            bool visible = opacity > 0;
            if (visible && _visible_input) {
                Node::setVisible(true);
            } else if (_visible) {
                Node::setVisible(false);
                return;
            }

            Node::setColor({
                (GLubyte)(_color_input.r * mul[0]),
                (GLubyte)(_color_input.g * mul[1]),
                (GLubyte)(_color_input.b * mul[2]),
            });
            Node::setOpacity(opacity);
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcMovieClip);

} // cocone
