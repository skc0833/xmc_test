// BmcSpriteShapeDefaultImpl.cpp
//
// cocos 側のシェーダを使って描画する
//

#include <cocone/bmc/BmcShape.h>

#include <cocone/bmc/BmcShader.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/foundation/StringHelper.h>

#include "BmcSymbolDefaultImpl.h"
#include "BmcFactoryImpl.h"

namespace cocone {

    using namespace std;
    namespace sh = cocone::stringhelper;
    USING_NS_CC;

    class BmcSpriteShapeImpl : public ZeroRefCount<Sprite>, public BmcShape, public BmcSymbolDefaultImpl
    {
        CCN_DECLARE_ASNODE_IMPL();

    protected:///////////////////////////////////////////////////////////////////
        virtual void* query(IIDType iid) override
        {
            if (iid == BmcShape::iid()) {
                return static_cast<BmcShape*>(this);
            } else if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

        virtual const cocos2d::Mat4& getAdditionalTransform() const override
        {
#if 1 //skc test
            return std::move(_additionalTransform);
#else
            Mat4 trans = _additionalTransform;
            return std::move(trans);
#endif
        }

        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(getAdditionalTransform().m, &out);
            return out;
        }
        ///////////////////////////////////////////////////////////////////////////
    private:
        BmcShader::Ptr _shader;
        BmcTexturePart::Ptr _texture;
        bmc::BmcBlendMode _blendMode;
        CustomCommand _customCommand;

        bool _visible_input;
        GLubyte _opacity_input;
        Color3B _color_input;

    public:

        BmcSpriteShapeImpl() :
            _visible_input(true),
            _opacity_input(255),
            _color_input(Color3B::WHITE)
        {
            _blendMode = bmc::BmcBlendMode_Normal;
        }
        virtual ~BmcSpriteShapeImpl()
        {
            //		ccn_dlog( "" );
        }
        virtual BmcSymbol::Ptr clone() const override
        {
            BmcSpriteShapeImpl* newP = new BmcSpriteShapeImpl();
            newP->init(_shader, _texture);
            newP->_additionalColorTransform = _additionalColorTransform;
            newP->_blendMode = _blendMode;

            return static_cast<BmcSymbol*>(newP);
        }
  
        virtual void init(BmcShader::Ptr shader, BmcTexturePart::Ptr texture) override
        {
            Sprite::initWithTexture(texture->getTexture()->getCCTexture2D());
//            Sprite::initWithSpriteFrameName(sh::strsprintf("tutorialQuest_kisekae_%d.png", texture->getImageIndex()));
//            debugDraw(true);
            _shader = shader;
            _texture = texture;

            setAnchorPoint(Vec2::ZERO);
        }
        virtual Size setupContentSize() override
        {
            // Sprite がやってくれるのでなにもしない
            return _contentSize;
        }
        virtual void setBlendMode(cocone::bmc::BmcBlendMode blendMode) override
        {
            _blendMode = blendMode;
        }

        virtual bool isVisible() const override
        {
            return _visible_input;
        }

        virtual void setVisible(bool visible) override
        {
            _visible_input = visible;
            Sprite::setVisible(visible);
        }

        virtual GLubyte getOpacity() const override
        {
            return _opacity_input;
        }

        virtual void setOpacity(GLubyte opacity) override
        {
            _opacity_input = opacity;

            const ColorTransform ct = getAscendColorTransform();
            const auto& mul = ct.multiplier();
            const auto& ofs = ct.offset();
            opacity = opacity * (mul[3] + ofs[3]);
            Sprite::setOpacity(opacity);
        }

        virtual const Color3B& getColor() const override
        {
            return _color_input;
        }

        virtual void setColor(const Color3B& color) override
        {
            _color_input = color;

            const ColorTransform ct = getAscendColorTransform();
            const auto& mul = ct.multiplier();
            Sprite::setColor({
                (GLubyte)(color.r * mul[0]),
                (GLubyte)(color.g * mul[1]),
                (GLubyte)(color.b * mul[2]),
            });           
        }

        virtual void setAdditonalColorTransform(const ColorTransform& additionalColorTransform) override
        {
            if (_additionalColorTransform == additionalColorTransform) {
                return;
            }
            _additionalColorTransform = additionalColorTransform;

            ColorTransform ct = getAscendColorTransform();
            const auto& mul = ct.multiplier();
            const auto& ofs = ct.offset();

            GLubyte opacity = _opacity_input * mul[3] + 255 * ofs[3];
            bool visible = opacity > 0;
            if (visible && _visible_input) {
                Sprite::setVisible(true);
            } else if (_visible) {
                Sprite::setVisible(false);
                return;
            }

            // TODO color offset -> shader でやる必要がある
            // TODO プリマルチプライドに対応
            // TODO 変更チェック
            Sprite::setColor({
                (GLubyte)(_color_input.r * mul[0]),
                (GLubyte)(_color_input.g * mul[1]),
                (GLubyte)(_color_input.b * mul[2]),
            });           
            Sprite::setOpacity(opacity);
        }

        virtual Rect getBoundingBox() const override
        {
            return RectApplyTransform(
                { _texture->getOffset().x, _texture->getOffset().y, _contentSize.width, _contentSize.height },
                getNodeToParentTransform()
            );
        }

        virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override
        {
            Mat4 offset = Mat4::IDENTITY;
            offset.m[12] = _texture->getOffset().x;
            offset.m[13] = _texture->getOffset().y;
            Sprite::draw(renderer, transform * offset, flags);
        }
    };

//    BMC_FACTORY_CREATE_FUNC_IMPL(BmcShape);
    BmcShape::Ptr BmcFactory::createBmcShape()
    {
        return new BmcSpriteShapeImpl();
    }

} // cocone
