
#pragma once

#include <cocone/bmc/BmcFactory.h>

#define BMC_FACTORY_CREATE_FUNC_IMPL(clazz) \
    clazz::Ptr BmcFactory::create##clazz() \
    { \
        return new clazz##Impl(); \
    }
