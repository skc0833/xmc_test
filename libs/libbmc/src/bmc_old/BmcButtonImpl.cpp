﻿// BmcButtonDefaultImpl.cpp
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored. 

#include <cocone/bmc/BmcButton.h>
#include <cocone/foundation/Log.h>

#include "BmcMovieClipAdapter.h"
#include "BmcFactoryImpl.h"

using namespace std;
USING_NS_CC;

#define LABEL_UP "up"
#define LABEL_UPNORMAL "upnormal"
#define LABEL_DOWN "down"
#define LABEL_DOWNNORMAL "downnormal"
#define LABEL_DISABLE "disable"

namespace cocone {
    class BmcButtonImpl : public BmcButton, public BmcMovieClipAdapter, public ZeroRefCount<Node>
    {
        CCN_SHARABLEPTR_DEFINE(BmcButtonImpl);
        CCN_DECLARE_ASNODE_IMPL();

        enum LabelType
        {
            LabelType_Up,
            LabelType_UpNormal,
            LabelType_Down,
            LabelType_DownNormal,
            LabelType_Disable,
            LabelType_HitArea,
        };
        typedef BmcButtonImpl _myt;
        LabelType _currentPlayingLabel;
        std::function<bool(BmcButton*)> _handler;
        std::function<void(void)> _soundPlay;
        bool _enabled;
        bool _touchBegan;
        bool _checkBoxMode;
        bool _check;
        RefPtr<EventListenerTouchOneByOne> _touchListener;

    protected:///////////////////////////////////////////////////////////////////
        void* query(IIDType iid) override {
            if (iid == BmcMovieClip::iid()) {
                return static_cast<BmcMovieClip*>(this);
            }
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            if (iid == BmcButton::iid()) {
                return static_cast<BmcButton*>(this);
            }
            return nullptr;
        }
        BmcMovieClipAdapter* callNewAndInit() const override
        {
            auto t = new BmcButtonImpl();
            t->ZeroRefCount<Node>::init();
            return t;
        }
        bool callNodeInit() override {
            return ZeroRefCount<Node>::init();
        }
        virtual const Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }
        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }
    public://////////////////////////////////////////////////////////////////////
    public:
        BmcButtonImpl()
            : _enabled(false), _currentPlayingLabel(LabelType_Up), _touchBegan(false), _checkBoxMode(false), _check(false)
        {

        }
        virtual ~BmcButtonImpl()
        {
            asNode()->getEventDispatcher()->removeEventListener(_touchListener);
        }
        virtual BmcSymbol::Ptr clone() const override
        {
            BmcSymbol::Ptr ret = BmcMovieClipAdapter::clone();
            BmcButtonImpl* me = static_cast<BmcButtonImpl*>(ret.get());
            me->setEventListener(std::bind(&_myt::mcEventListener, me, placeholders::_1, placeholders::_2));

            // タッチ設定
            me->_touchListener = EventListenerTouchOneByOne::create();
            me->_touchListener->setSwallowTouches(true);
            me->_touchListener->onTouchBegan = bind(&_myt::onTouchBegan, me, placeholders::_1, placeholders::_2);
            me->_touchListener->onTouchMoved = bind(&_myt::onTouchMoved, me, placeholders::_1, placeholders::_2);
            me->_touchListener->onTouchEnded = bind(&_myt::onTouchEnded, me, placeholders::_1, placeholders::_2);
            me->asNode()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(me->_touchListener.get(), me->asNode());
            me->_touchListener->setEnabled(this->_touchListener->isEnabled());

            me->_enabled = _enabled;
            me->_currentPlayingLabel = LabelType_UpNormal;
            me->gotoAndPlayToNextLabel(LABEL_UPNORMAL);
            me->_checkBoxMode = _checkBoxMode;
            me->_check = _check;
            me->_soundPlay = _soundPlay;
            return ret;
        }
        virtual void init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override
        {
            BmcMovieClipAdapter::init(source, bmcTimeline);
            this->setEventListener(std::bind(&_myt::mcEventListener, this, placeholders::_1, placeholders::_2));
            _touchListener = EventListenerTouchOneByOne::create();
            _touchListener->setSwallowTouches(true);
            _touchListener->onTouchBegan = bind(&_myt::onTouchBegan, this, placeholders::_1, placeholders::_2);
            _touchListener->onTouchMoved = bind(&_myt::onTouchMoved, this, placeholders::_1, placeholders::_2);
            _touchListener->onTouchEnded = bind(&_myt::onTouchEnded, this, placeholders::_1, placeholders::_2);
            asNode()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touchListener.get(), asNode());
            _touchListener->setEnabled(true);
            _enabled = true;

            _currentPlayingLabel = LabelType_UpNormal;
            gotoAndPlayToNextLabel(LABEL_UPNORMAL);
            //play();
        }
        virtual void onEnter() override
        {
            Node::onEnter();
            play();
            childTouchDisable();
        }
        virtual void scheduleUpdate()
        {
            Node::scheduleUpdate();
        }
        virtual void update(float delta) override
        {
            nodeFuncUpdate(delta);
            Node::update(delta);
        }
        virtual void setPushSoundHandler(std::function<void(void)> soundPlay) override
        {
            _soundPlay = soundPlay;
        }
        virtual void setPushHandler(std::function<bool(BmcButton*)> handler) override
        {
            _handler = handler;
        }
        virtual void setEnabled(bool enabled) override
        {
            _enabled = enabled;
            _touchListener->setEnabled(enabled);
            setLoop(enabled);
            if (_enabled == false){
                _currentPlayingLabel = LabelType_Disable;
                gotoAndPlayToNextLabel(LABEL_DISABLE);
            }
            else{
                _currentPlayingLabel = LabelType_UpNormal;
                gotoAndPlayToNextLabel(LABEL_UPNORMAL);
            }
        }
        virtual void setCheckBoxMode(bool checkbox) override
        {
            _checkBoxMode = checkbox;
        }
        virtual bool getCheckBoxMode()const override
        {
            return _checkBoxMode;
        }
        virtual void setCheck(bool check, bool playSound) override
        {
            setCheck(check, playSound, true);
        }
        virtual void setCheck(bool check, bool playSound, bool animated) override
        {
            if (_checkBoxMode == false || _touchListener->isEnabled() == false) {
                return;
            }

            setLoop(false);

            if (check) {
                if (animated) {
                    _currentPlayingLabel = LabelType_Down;
                    gotoAndPlayToNextLabel(LABEL_DOWN);
                }
                else {
                    _currentPlayingLabel = LabelType_DownNormal;
                    gotoAndPlayToNextLabel(LABEL_DOWNNORMAL);
                }
            }
            else {
                if (animated) {
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                }
                else {
                    _currentPlayingLabel = LabelType_UpNormal;
                    gotoAndPlayToNextLabel(LABEL_UPNORMAL);
                }
            }

            _touchBegan = false;
            if (playSound && _soundPlay != nullptr){
                _soundPlay();
            }
            _check = check;
        }
        virtual bool getCheck() const override
        {
            return _check;
        }
        virtual void setTouchDisable() override
        {
            _touchListener->setEnabled(false);
            setLoop(false);
        }
    private:
        virtual void gotoAndPlayToNextLabel(const char* label)override{
            BmcMovieClipAdapter::gotoAndPlayToNextLabel(label);
            // ボタンタイプの子供がいれば変更する
            iterateInstancesRecursively([label](BmcSymbol* symbol){
                BmcButtonImpl* child = dynamic_cast<BmcButtonImpl*>(symbol);
                if (child != nullptr) {
                    child->gotoAndPlayToNextLabel(label);
                }
            });
        }
        void childTouchDisable()
        {
            // 子供たちがタッチに反応しないようにする
            iterateInstancesRecursively([](BmcSymbol* symbol){
                BmcButtonImpl* child = dynamic_cast<BmcButtonImpl*>(symbol);
                if (child != nullptr) {
                    child->setTouchDisable();
                }
            });
        }
        void mcEventListener(BmcMovieClip::EventType eventType, BmcMovieClip* mc)
        {
            switch (eventType){
            case EventType_PlayEndFrame:
            {
                //ccn_dlog( "%p, _currentPlayingLabel=%d", this, _currentPlayingLabel);
                switch (_currentPlayingLabel){
                case LabelType_Up:
                    //ccn_dlog("LabelType_Up");
                    setLoop(true);
                    _currentPlayingLabel = LabelType_UpNormal;
                    gotoAndPlayToNextLabel(LABEL_UPNORMAL);
                    break;
                case LabelType_Down:
                    //ccn_dlog("LabelType_Down");
                    setLoop(true);
                    _currentPlayingLabel = LabelType_DownNormal;
                    gotoAndPlayToNextLabel(LABEL_DOWNNORMAL);
                    break;
                default:
                    break;
                }
            }
                break;
            default:
                break;
            }
        }
        void pushFire()
        {
            if (_handler != nullptr){
                if (_handler(this) == false){
                    return;
                }
            }
            if (_soundPlay != nullptr){
                _soundPlay();
            }
        }
        virtual void gotoTopAndPlayRecursively() override
        {
            // 何もしない
        }
    private: /// Layer -------------------------------------------------------------------------------------

        bool hasVisibleParents()
        {
            Node* pParent = this->getParent();
            for (Node *c = pParent; c != NULL; c = c->getParent())
            {
                if (!c->isVisible())
                {
                    return false;
                }
            }
            return true;
        }
        virtual bool onTouchBegan(Touch *pTouch, Event *pEvent)
        {
            if (isVisible() == false || hasVisibleParents() == false){ return false; }
            _touchBegan = false;

            Point touchLocation = pTouch->getLocation();
            if (inHitArea(touchLocation) == false){
                if (_checkBoxMode == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                }
                return false;
            }
            _touchBegan = true;
            if (_checkBoxMode == false){
                _currentPlayingLabel = LabelType_Down;
                gotoAndPlayToNextLabel(LABEL_DOWN);
            }
            else{
                //ccn_dlog( "DO Began");
                if (_check){
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                }
                else{
                    _currentPlayingLabel = LabelType_Down;
                    gotoAndPlayToNextLabel(LABEL_DOWN);
                }
            }
            return true;
        }
        virtual void onTouchMoved(Touch *pTouch, Event *pEvent){
            if (isVisible() == false || hasVisibleParents() == false){ return; }

            Point touchLocation = pTouch->getLocation();
            //ccn_dlog("%f, %f", touchLocation.x, touchLocation.y );
            bool hitArea = inHitArea(touchLocation);
            if (_checkBoxMode == false){
                if (_touchBegan && hitArea == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                    setLoop(false);
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                }
                else if (_touchBegan && hitArea && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal) == false){
                    setLoop(false);
                    _currentPlayingLabel = LabelType_Down;
                    gotoAndPlayToNextLabel(LABEL_DOWN);
                }
            }
            else{
                //ccn_dlog( "DO Moved");
                if (_check){
                    if (_touchBegan && hitArea == false && (_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Down;
                        gotoAndPlayToNextLabel(LABEL_DOWN);
                    }
                    else if (_touchBegan && hitArea && (_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal) == false){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Up;
                        gotoAndPlayToNextLabel(LABEL_UP);
                    }
                }
                else{
                    if (_touchBegan && hitArea == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Up;
                        gotoAndPlayToNextLabel(LABEL_UP);
                    }
                    else if (_touchBegan && hitArea && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal) == false){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Down;
                        gotoAndPlayToNextLabel(LABEL_DOWN);
                    }
                }
            }
        }
        virtual void onTouchEnded(Touch *pTouch, Event *pEvent){
            if (isVisible() == false || hasVisibleParents() == false){ return; }

            Point touchLocation = pTouch->getLocation();
            if (_touchBegan && inHitArea(touchLocation)){
                if (_checkBoxMode == false){
                    setLoop(false);
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                    pushFire();
                }
                else{
                    //ccn_dlog( "DO Ended");
                    _check = !_check;
                    pushFire();
                }
            }
            else if (_touchBegan && inHitArea(touchLocation) == false && _checkBoxMode){
                if (_check){
                    if ((_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Down;
                        gotoAndPlayToNextLabel(LABEL_DOWN);
                    }
                }
                else{
                    if ((_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Up;
                        gotoAndPlayToNextLabel(LABEL_UP);
                    }
                }
            }

            _touchBegan = false;
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcButton);

} // cocone
