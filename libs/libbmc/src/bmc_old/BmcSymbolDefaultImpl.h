﻿// BmcSymbol.h
//
#pragma once

#include <cocone/bmc/BmcSymbol.h>
#include <cocone/foundation/Log.h>
//#include <cocone/display/bmc/BmcLibrary.h>

namespace cocone
{
/*
#define CCN_DECLARE_ASNODE_IMPL() \
    cocos2d::Node* asNode() override{ return this; } \
    const cocos2d::Node* asNode() const override{ return this; }
*/

    class BmcLibrary;
    class BmcSymbolDefaultImpl : public BmcSymbol
    {
    protected:
        ColorTransform _additionalColorTransform;
    protected:
        BmcSymbolDefaultImpl()
            : _additionalColorTransform(ColorTransform::Identity())
        {
        }
        virtual ~BmcSymbolDefaultImpl(){
        }

        ColorTransform getAscendColorTransform() const
        {
            ColorTransform ct = getAdditionalColorTransform();
            for (auto parent = asNode()->getParent(); parent; parent = parent->getParent()) {
                const BmcSymbol* p = areYouSymbol(parent);
                if (p) {
                    ct = p->getColorTransform().mutiply(ct);
                }
            }
            return std::move(ct);
        }
    public:
        CCN_SHARABLEPTR_DEFINE(BmcSymbolDefaultImpl);

        bool hasTimeline()const override {
            return false;
        }

        const bmc::BmcTimeline& getTimeline()const override {
            ccn_assert(false, "unsupported.");
            return *(const bmc::BmcTimeline*)nullptr;
        }

        BmcSymbol::Ptr getInstance(const std::string& instanceName) const override; // at cpp
        BmcSymbol::Ptr getInstanceOne() const override; // at cpp
        BmcSymbol::Ptr getNullObject() const override; // at cpp
        void getAllInstances(std::vector<BmcSymbol::Ptr>* out) const override{}

        void overwriteInstance(BmcSymbol::Ptr current, BmcSymbol::Ptr over) override{}
        void overwriteInstances(const std::string& instanceName, BmcSymbol::Ptr over) override{}

        BmcSymbol::Ptr destroyInstance(const std::string& instanceName) override; // at cpp
        void destroyInstance(BmcSymbol::Ptr instance) override{}

        void iterateInstances(std::function<void(BmcSymbol*)> func) override{}
        void iterateInstancesForName(std::function<void(const std::string& instanceName, BmcSymbol*)> func) override{}
        void iterateInstancesByName(const std::string& instanceName, std::function<void(BmcSymbol*)> func) override{}
        void iterateInstancesRecursively(std::function<void(BmcSymbol*)> func) override{}
        void iterateInstancesByNameRecursively(const std::string& instanceName, std::function<void(BmcSymbol*)> func) override{}
        
        cocos2d::Size setupContentSize() override{
            return cocos2d::Size::ZERO;
        }
        void setBlendMode( bmc::BmcBlendMode blendMode) override{}


        virtual void setAdditionalTransform(const cocos2d::Mat4& mat) override
        {
            const auto trans = getAdditionalTransform();
            if (std::equal(std::begin(trans.m), std::end(trans.m), mat.m)) {
                return;
            }
            auto t = const_cast<cocos2d::Mat4&>(mat);
            asNode()->setAdditionalTransform(&t);
        }
        virtual void setAdditionalTransform(const cocos2d::AffineTransform& t) override
        {
            asNode()->setAdditionalTransform(t);
        }
        ///virtual const cocos2d::Mat4& getAdditionalTransformMat4()const =0; 下に同じ
        ///virtual const AffineTransform& getAdditionalTransform()const = 0; /// 派生側、Nodeを直接継承した人が実装しなければならない
        virtual void clearAdditonalTransform() override
        {
            asNode()->setAdditionalTransform(cocos2d::AffineTransform::IDENTITY);
        }


        void setAdditonalColorTransform(const ColorTransform& additionalColorTransform) override{
            _additionalColorTransform = additionalColorTransform;
        }
        const ColorTransform& getAdditionalColorTransform()const override{
            return _additionalColorTransform;
        }
        ColorTransform getColorTransform()const override{
//            ccn_assert(false, "unsupported node color.");
            return _additionalColorTransform;
//            return _additionalColorTransform.mutiply(_localColor);
        }


        virtual cocos2d::Rect getCurrentFrameBoundingBox() const override
        {
            return asNode()->getBoundingBox();
        }
        
        virtual bool inHitArea(cocos2d::Point point) override
        {
            cocos2d::Node* node = asNode();
            if (node == nullptr){
                return false;
            }

            point = asNode()->convertToNodeSpace(point);
            cocos2d::Size size = asNode()->getContentSize();
            size.width = size.width / 2;
            size.height = size.height / 2;
            //ccn_dlog("     tag=%d, touchMode=%s, local touch %f, %f", getTag(), checkBoxModeString, point.x, point.y);
            if (-size.width <= point.x && point.x <= size.width
                && -size.height <= point.y && point.y <= size.height){
                //ccn_dlog("tag=%d, touchMode=%s, HHHHHHHHHHHHHHIIIIIIIIIIIIIIIIIITTTTTTTTTTTTTTTTTTTTTT", getTag(), checkBoxModeString);
                return true;
            }
            return false;
        }
        
        virtual void addLibrary(BmcLibrary& library) override
        {
        }
        
    };


} // cocone
