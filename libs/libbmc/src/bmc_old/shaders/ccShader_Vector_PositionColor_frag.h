﻿"							\n\
#ifdef GL_ES				\n\
precision lowp float;		\n\
#endif						\n\
\n\
#ifdef GL_ES                                \n\
varying lowp vec4 v_color;                  \n\
varying lowp vec4 v_colorRatio;             \n\
varying lowp vec4 v_colorAmount;            \n\
varying mediump vec2 v_texCoord;            \n\
#else                                       \n\
varying vec4 v_color;                       \n\
varying vec4 v_colorRatio;                  \n\
varying vec4 v_colorAmount;                 \n\
varying vec2 v_texCoord;                    \n\
#endif                                      \n\
\n\
uniform sampler2D u_texture;\n\
uniform bool u_useTexture;	\n\
\n\
void main()					\n\
{							\n\
    if (u_useTexture) {     \n\
        vec4 texColor = texture2D(u_texture, v_texCoord) * v_colorRatio.a; \n\
        gl_FragColor = texColor + v_colorAmount * texColor.a; \n\
    } else {                \n\
        gl_FragColor = v_color * v_colorRatio + v_colorAmount; \n\
    }                       \n\
}							\n\
"
