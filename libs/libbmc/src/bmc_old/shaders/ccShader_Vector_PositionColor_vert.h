﻿"                                           \n\
attribute vec4 a_position;                  \n\
attribute vec2 a_texCoord;                  \n\
                                            \n\
uniform vec4 u_color;                       \n\
uniform vec4 u_colorRatio;                  \n\
uniform vec4 u_colorAmount;                 \n\
                                            \n\
#ifdef GL_ES                                \n\
varying lowp vec4 v_color;                  \n\
varying lowp vec4 v_colorRatio;             \n\
varying lowp vec4 v_colorAmount;            \n\
varying mediump vec2 v_texCoord;            \n\
#else                                       \n\
varying vec4 v_color;                       \n\
varying vec4 v_colorRatio;                  \n\
varying vec4 v_colorAmount;                 \n\
varying vec2 v_texCoord;                    \n\
#endif                                      \n\
                                            \n\
                                            \n\
void main()                                 \n\
{                                           \n\
    gl_Position = CC_MVPMatrix * a_position; \n\
    v_color = u_color;                      \n\
    v_colorRatio= u_colorRatio;             \n\
    v_colorAmount = u_colorAmount;          \n\
    v_texCoord = a_texCoord;                \n\
}                                           \n\
"
