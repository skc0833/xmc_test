//
//  Shaders.cpp
//  pokepoke
//
//  Created by TANAKAHiroshi on 2013/05/13.
//
//

#include <cocone/bmc/Shaders.h>
// TODO VBO に書き換えたい

using namespace cocos2d;
namespace cocone {

const GLchar * ccVector_PositionColor_frag =
#include "ccShader_Vector_PositionColor_frag.h"
;
const GLchar * ccVector_PositionColor_vert =
#include "ccShader_Vector_PositionColor_vert.h"
;

//
const GLchar* ccVector_PositionTextureColor_vert=
"attribute vec4 a_position;\n"
"attribute vec2 a_texCoord;\n"
/*"uniform   highp   mat4 u_worldMatrix;"*/
"varying   vec2 v_texCoord;\n"
"void main()\n"
"{\n"
"gl_Position = CC_MVPMatrix * a_position;\n"
"v_texCoord = a_texCoord;\n"
"}\n";

const GLchar * ccVector_PositionTextureColorUseOffsetAlpha_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform vec4      u_colorMultiplier;\n"
"uniform vec4      u_colorOffset;\n"
"void main()\n"
"{\n"
"vec4 texcolor = texture2D(u_texture, v_texCoord);\n"
"texcolor = vec4( texcolor.rgb/texcolor.a, texcolor.a );\n"
"texcolor = texcolor*u_colorMultiplier + u_colorOffset;\n"
"gl_FragColor = vec4( texcolor.rgb*texcolor.a, texcolor.a );\n"
"}\n";

const GLchar * ccVector_PositionTextureColorNoOffsetAlpha_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform vec4      u_colorMultiplier;\n"
"uniform vec4      u_colorOffset;\n"
"void main()\n"
"{\n"
"vec4 texcolormul = texture2D(u_texture, v_texCoord)*u_colorMultiplier;\n"
"gl_FragColor = vec4( texcolormul.rgb*u_colorMultiplier.a + u_colorOffset.rgb*texcolormul.a, texcolormul.a );\n"
"}\n";

const GLchar * ccVector_PositionTextureColorNoOffsetOnlyMultiplierColor_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform vec4      u_colorMultiplier;\n"
"void main()\n"
"{\n"
"vec4 texcolormul = texture2D(u_texture, v_texCoord)*u_colorMultiplier;\n"
"gl_FragColor = vec4( texcolormul.rgb*u_colorMultiplier.a, texcolormul.a );\n"
"}\n";

const GLchar * ccVector_PositionTextureColorNoOffsetOnlyMultiplierAlpha_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"uniform float     u_alphaMultiplier;\n"
"void main()\n"
"{\n"
"gl_FragColor = texture2D(u_texture, v_texCoord)*u_alphaMultiplier;\n"
"}\n";

const GLchar * ccVector_PositionTextureColorOnlyTexture_frag =
"varying vec2      v_texCoord;\n"
"uniform sampler2D u_texture;\n"
"void main()\n"
"{\n"
"gl_FragColor = texture2D(u_texture, v_texCoord);\n"
"}\n";


const char *const Uniform_ColorMultiplierIdentify = "u_colorMultiplier";
const char *const Uniform_ColorOffsetIdentify = "u_colorOffset";
const char *const Uniform_AlphaMultiplierIdentify = "u_alphaMultiplier";
const char *const kCCShader_Vector_PositionTextureColor_UseOffsetAlpha = "Vector_PositionTextureColor_UseOffsetAlpha.cocone";
const char *const kCCShader_Vector_PositionTextureColor_NoOffsetAlpha = "Vector_PositionTextureColor_NoOffsetAlpha.cocone";
const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor = "Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor.cocone";
const char *const kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha = "Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha.cocone";
const char *const kCCShader_Vector_PositionTextureColor_OnlyTexture = "Vector_PositionTextureColor_OnlyTexture.cocone";

void addShaders(GLProgramCache *shaderCache)
{
    GLProgram *p;
    // FlashのColorTransformを完全に再現させるためのシェーダ。cocos2dのtextureデータがプリマルチプライドになってしまうのでいったん割り算してからやり直すというとても非効率なシェーダ
    {
        p = new GLProgram();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorUseOffsetAlpha_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_UseOffsetAlpha);
        p->release();
    }
    // ColorTransformのうち、Add する alpha 値がなければ、プリマルチプライドのカラーを使用できるので、それに限定したシェーダ。出来るだけこれを使用したい。
    {
        p = new GLProgram();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetAlpha_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetAlpha);
        p->release();
    }
    // ColorTransformのうち、Add するカラー値がない場合に使用する。実際、RGBについて、掛け算だけすることはあまりないので使用頻度はすくないかも
    {
        p = new GLProgram();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierColor_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor);
        p->release();
    }
    // ColorTransformのうち、alpha の掛け算だけを使用するもの。一般にはこのくらいのケースが多いだろう。
    {
        p = new GLProgram();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierAlpha_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha);
        p->release();
    }
    // 何もしないテクスチャカラーのみのもの。テクスチャカラーのみといってもプリマルチプライドなので、ブレンドモードは必要になる。
    {
        p = new GLProgram();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorOnlyTexture_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_OnlyTexture);
        p->release();
    }
}
void reloadShaders(GLProgramCache *shaderCache)
{
    GLProgram *p;
    // FlashのColorTransformを完全に再現させるためのシェーダ。cocos2dのtextureデータがプリマルチプライドになってしまうのでいったん割り算してからやり直すというとても非効率なシェーダ
    {
        p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_UseOffsetAlpha);
        p->reset();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorUseOffsetAlpha_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_UseOffsetAlpha);
    }
    // ColorTransformのうち、Add する alpha 値がなければ、プリマルチプライドのカラーを使用できるので、それに限定したシェーダ。出来るだけこれを使用したい。
    {
        p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_NoOffsetAlpha);
        p->reset();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetAlpha_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetAlpha);
    }
    // ColorTransformのうち、Add するカラー値がない場合に使用する。実際、RGBについて、掛け算だけすることはあまりないので使用頻度はすくないかも
    {
        p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor);
        p->reset();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierColor_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierColor);
    }
    // ColorTransformのうち、alpha の掛け算だけを使用するもの。一般にはこのくらいのケースが多いだろう。
    {
        p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha);
        p->reset();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorNoOffsetOnlyMultiplierAlpha_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_NoOffsetOnlyMultiplierAlpha);
    }
    // 何もしないテクスチャカラーのみのもの。テクスチャカラーのみといってもプリマルチプライドなので、ブレンドモードは必要になる。
    {
        p = shaderCache->getGLProgram(kCCShader_Vector_PositionTextureColor_OnlyTexture);
        p->reset();
        p->initWithByteArrays(ccVector_PositionTextureColor_vert, ccVector_PositionTextureColorOnlyTexture_frag);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        p->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        p->link();
        p->updateUniforms();
        CHECK_GL_ERROR_DEBUG();
        shaderCache->addGLProgram(p, kCCShader_Vector_PositionTextureColor_OnlyTexture);
    }
}

/*
static void addShader_PositionColor(CCShaderCache *shaderCache)
{
    CCGLProgram *p;
    
    p = new CCGLProgram();
    p->initWithVertexShaderByteArray(ccVector_PositionColor_vert, ccVector_PositionColor_frag);
    p->addAttribute(kCCAttributeNamePosition, kCCVertexAttrib_Position);
    p->addAttribute(kCCAttributeNameTexCoord, kCCVertexAttrib_TexCoords);
    p->link();
    p->updateUniforms();
    
    CHECK_GL_ERROR_DEBUG();
    
    shaderCache->addProgram(p, kCCShader_Vector_PositionColor);
    p->release();
}
*/

}
