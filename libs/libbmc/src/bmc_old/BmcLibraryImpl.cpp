﻿//
//  BmcLibraryImpl.cpp
//  PocketColony
//
//  Created by 洪 起萬 on 2014/05/30.
//
//

#include "BmcLibraryImpl.h"

#include <cocone/bmc/BmcLoader.h>
#include <cocone/bmc/BmcFactory.h>
#include <cocone/bmc/BmcSymbol.h>

USING_NS_CC;

namespace cocone
{
    BmcLibraryImpl::BmcLibraryImpl() {
    }
    
    BmcLibraryImpl::~BmcLibraryImpl() {
        _symbolTable.clear();
    }
    
    void BmcLibraryImpl::loadLibraryByName(const std::vector<std::string>& names) {
        for (int i=0; i<names.size(); i++) {
            loadLibraryByName(names.at(i));
        }
    }

    void BmcLibraryImpl::loadLibrary(const std::string& fullPath) {
        BmcLoader::Ptr loader = BmcFactory::createBmcLoader();
        loader->init(fullPath);
        BmcSymbol::Ptr symbol = loader->getStage();
        
        const cocone::bmc::BmcTimeline& timeLine = symbol->getTimeline();
        std::vector<BmcSymbol::Ptr> instances;
        symbol->getAllInstances(&instances);
        
        for (int i = 0; i < timeLine.instance_table_size(); ++i)
        {
            const bmc::BmcInstance& instanceInfo = timeLine.instance_table(i);
            
            if( _symbolTable.find(instanceInfo.instance_name()) != _symbolTable.end() )
            {
                _symbolTable.erase("$$" + instanceInfo.instance_name());
            }
            
            _symbolTable.insert(std::make_pair("$$" + instanceInfo.instance_name(), instances.at(i)));
        }
        
    }

    void BmcLibraryImpl::loadLibrary(const std::vector<std::string>& fullPath) {
        for (int i=0; i<fullPath.size(); i++) {
            loadLibrary(fullPath.at(i));
        }
    }
    
    BmcSymbol::Ptr BmcLibraryImpl::getSymbol(const std::string &referenced_key) {
        std::map<std::string, BmcSymbol::Ptr>::iterator it = _symbolTable.find(referenced_key);
        
        if( it != _symbolTable.end() ) {
            BmcSymbol::Ptr symbol = it->second;
            return symbol->clone();
        }
        
        return nullptr;
    }
    void BmcLibraryImpl::iterateBySymbol(std::function<void(const std::string&, BmcSymbol::Ptr)> f)
    {
        for (std::map<std::string, BmcSymbol::Ptr>::iterator it = _symbolTable.begin(), ed = _symbolTable.end();
            it != ed; ++it){
            f(it->first, it->second);
        }
    }

}
