﻿// BmcTextureDefaultImpl.cpp
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.

#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/foundation/Log.h>

#include "BmcFactoryImpl.h"

//#define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.

USING_NS_CC;

namespace cocone {

    class BmcTextureImpl : public BmcTexture, public ZeroRefCount<cocos2d::Ref>
    {
#ifdef _DEBUG
        static int static_active_texure_count;
#endif
        RefPtr<Texture2D> _rawTexture;
        int _imageIndex;
        BmcTextureReloader::Ptr _reloader;
    public:
        CCN_DECLARE_ASREF_IMPL();

        BmcTextureImpl() : _imageIndex(-1)
        {
#ifdef _DEBUG
            //ccn_dlog("%d", ++static_active_texure_count);
#endif
        }

        virtual ~BmcTextureImpl()
        {
#ifdef _DEBUG
            //ccn_dlog("%d", --static_active_texure_count);
#endif
            if (_reloader){
                ccn_assert(_imageIndex != -1);
                _reloader->unregist(_imageIndex);
            }
        }

        virtual void init(int imageIndex, const bmc::BmcImage* image) override
        {
            _imageIndex = imageIndex;
            _rawTexture = nullptr;
            Image* pImage = new Image();
            if (ccn_econd(pImage->initWithImageData((const uint8_t*)image->image_bytes().data(), image->image_bytes().size()) == false, "faild init image") ){
                pImage->release();
                _rawTexture = new Texture2D();
                _rawTexture->release();
                return;
            }
            _rawTexture = new Texture2D();
            _rawTexture->release();
            _rawTexture->initWithImage(pImage);
            pImage->release();
        }
        virtual void reInit(int imageIndex, const bmc::BmcImage* bmcImage) override
        {
            Image* pImage = new Image();
            if (pImage && pImage->initWithImageData((const uint8_t*)bmcImage->image_bytes().data(), bmcImage->image_bytes().size()))
            {
                Texture2D::PixelFormat oldPixelFormat = Texture2D::getDefaultAlphaPixelFormat();
                Texture2D::setDefaultAlphaPixelFormat(Texture2D::PixelFormat::RGBA8888);
                _rawTexture->initWithImage(pImage);
                Texture2D::setDefaultAlphaPixelFormat(oldPixelFormat);
                pImage->release();
            }
        }
        virtual void registReloader(BmcTextureReloader::Ptr reloader) override
        {
            ccn_assert(_imageIndex != -1);
            _reloader = reloader;
            _reloader->regist(_imageIndex, this);
        }
        virtual Size getContentSize()
        {
            return _rawTexture->getContentSize();
        }

        virtual Texture2D* getCCTexture2D() override
        {
            return _rawTexture;
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcTexture);

} // cocone

#ifdef _DEBUG
int cocone::BmcTextureImpl::static_active_texure_count;
#endif
