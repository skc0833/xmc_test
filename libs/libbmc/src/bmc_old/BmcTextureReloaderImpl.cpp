﻿// BmcTextureReloaderDefaultImpl.cpp
//

#include <cocone/foundation/Sharable.h>
#include <cocone/bmc/BmcSymbol.h>
#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/bmc/BmcTextureReloadManager.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/protocol/BmcFile.pb.h>
#include <cocone/bmc/BmcScalingGridShape.h>
#include <cocone/foundation/Log.h>

#include "BmcFactoryImpl.h"

USING_NS_CC;
using namespace google_public::protobuf;

namespace cocone {

    class BmcTextureReloaderImpl : public BmcTextureReloader, public ZeroRefCount<cocos2d::Ref>
    {
        std::string _bmcFilePath;
        std::vector<BmcTexture*> _texWeakPtrArray;
        std::list<BmcScalingGridShape*> _9gridWeakPtrList;
        BmcTextureReloadManager::Ptr _reloaderManager;

    public:
        CCN_DECLARE_ASREF_IMPL();

        BmcTextureReloaderImpl()
        {

        }

        virtual ~BmcTextureReloaderImpl()
        {
            if (_reloaderManager){
                _reloaderManager->unregist(this);
            }
        }
        virtual void init(const std::string& bmcFilePath, int imageCount) override
        {
            _bmcFilePath = bmcFilePath;
            _texWeakPtrArray.resize(imageCount, nullptr);
        }
        virtual void registReloaderManager(BmcTextureReloadManager::Ptr reloaderManager) override
        {
            _reloaderManager = reloaderManager;
            _reloaderManager->regist(this);
        }
        virtual void regist(int index, BmcTexture* weakPtr) override
        {
            _texWeakPtrArray[index] = weakPtr;
        }
        virtual void unregist(int index) override
        {
            _texWeakPtrArray[index] = nullptr;
        }
        virtual void regist(BmcScalingGridShape* weakPtr) override
        {
            _9gridWeakPtrList.push_back(weakPtr);
        }
        virtual void unregist(BmcScalingGridShape* weakPtr) override
        {
            std::list<BmcScalingGridShape*>::iterator it = _9gridWeakPtrList.begin(), ed = _9gridWeakPtrList.end();
            for (; it != ed; ++it){
                if ((*it) == weakPtr){
                    _9gridWeakPtrList.erase(it);
                    return;
                }
            }
        }
        virtual void reload() override
        {
            ccn_dlog("start %s", _bmcFilePath.c_str());
            Data fileData = FileUtils::getInstance()->getDataFromFile(_bmcFilePath);

            if (ccn_econd(fileData.isNull(), "path=%s", _bmcFilePath.c_str())){
                return;
            }
            bmc::BmcFile bmcFile;
            bmcFile.ParseFromArray(fileData.getBytes(), static_cast<int>(fileData.getSize()));

            // テクスチャをリロード
            RepeatedPtrField<bmc::BmcImage>* image_table = bmcFile.mutable_image_table();
            for (int i = 0; i < image_table->size(); ++i)
            {
                BmcTexture* weakPtr = _texWeakPtrArray[i];
                if (weakPtr == nullptr){
                    continue;
                }
                weakPtr->reInit(i, image_table->Mutable(i));
            }
            std::list<BmcScalingGridShape*>::iterator it = _9gridWeakPtrList.begin(), ed = _9gridWeakPtrList.end();
            for (; it != ed; ++it){
                (*it)->reload();
            }
            ccn_dlog("end %s", _bmcFilePath.c_str());
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcTextureReloader);

} // cocone
