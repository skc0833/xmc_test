// BmcShapeDefaultImpl.cpp
//
// BmcSpriteShapeImpl を使うのでこちらは使わない
//
#if 0 //skc test
#include <cocone/bmc/BmcShape.h>

#include <cocone/bmc/BmcShader.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/foundation/StringHelper.h>

#include "BmcSymbolDefaultImpl.h"
#include "BmcFactoryImpl.h"

namespace cocone {

    using namespace std;
    USING_NS_CC;

    class BmcShapeImpl : public ZeroRefCount<Sprite>, public BmcShape, public BmcSymbolDefaultImpl
    {
        CCN_DECLARE_ASNODE_IMPL();

    protected:///////////////////////////////////////////////////////////////////
        virtual void* query(IIDType iid) override
        {
            if (iid == BmcShape::iid()) {
                return static_cast<BmcShape*>(this);
            } else if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

        virtual const cocos2d::Mat4& getAdditionalTransform() const override
        {
            Mat4 trans = _additionalTransform;
            return std::move(trans);
        }

        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(getAdditionalTransform().m, &out);
            return out;
        }
        ///////////////////////////////////////////////////////////////////////////
    private:
        BmcShader::Ptr _shader;
        BmcTexturePart::Ptr _texture;
        float _vertices[8];
        float _texCoords[8];
        bmc::BmcBlendMode _blendMode;
        CustomCommand _customCommand;
        bool _visible_userinput;

        ColorTransform getAscendColorTransform() const
        {
            ColorTransform ct = getAdditionalColorTransform();
            for (auto parent = getParent(); parent; parent = parent->getParent()) {
                const BmcSymbol* p = areYouSymbol(parent);
                if (p) {
                    ct = p->getColorTransform().mutiply(ct);
                }
            }
            return ct;
        }

    public:

        BmcShapeImpl()
        {
            _blendMode = bmc::BmcBlendMode_Normal;
        }
        virtual ~BmcShapeImpl()
        {
            //		ccn_dlog( "" );
        }
        virtual BmcSymbol::Ptr clone() const override
        {
            BmcShapeImpl* newP = new BmcShapeImpl();
            newP->init(_shader, _texture);
            newP->_additionalColorTransform = _additionalColorTransform;
            newP->_blendMode = _blendMode;

            return static_cast<BmcSymbol*>(newP);
        }
  
        virtual void init(BmcShader::Ptr shader, BmcTexturePart::Ptr texture) override
        {
            Sprite::initWithTexture(texture->getTexture()->getCCTexture2D());
            _shader = shader;
            _texture = texture;
            _visible_userinput = true;

            setAnchorPoint(Vec2::ZERO);
            setupVertices();
        }
        virtual Size setupContentSize() override
        {
            return Size(
                _texture->getRectangle().width() / CC_CONTENT_SCALE_FACTOR(),
                _texture->getRectangle().height() / CC_CONTENT_SCALE_FACTOR()
            );
        }
        virtual void setBlendMode(cocone::bmc::BmcBlendMode blendMode) override
        {
            _blendMode = blendMode;
        }

        virtual void setVisible(bool visible) override
        {
            _visible_userinput = visible;
            Sprite::setVisible(visible);
        }

        virtual void setAdditionalTransform(const Mat4& additionalTransform) override
        {
            Sprite::setAdditionalTransform(const_cast<Mat4*>(&additionalTransform));
        }

        void setAdditonalColorTransform(const ColorTransform& additionalColorTransform) override
        {
            if (_additionalColorTransform == additionalColorTransform) {
                return;
            }
            _additionalColorTransform = additionalColorTransform;

            ColorTransform ct = getAscendColorTransform();
            bool visible = !(ct.multiplier()[3] == 0 && ct.offset()[3] == 0);
            if (visible && _visible_userinput) {
                Sprite::setVisible(true);
            } else if (_visible) {
                Sprite::setVisible(false);
                return;
            }
        }

        virtual Rect getBoundingBox() const override
        {
            return RectApplyTransform(
                { _vertices[0], _vertices[1], _contentSize.width, _contentSize.height },
                getNodeToParentTransform()
            );
        }

        void setupVertices()
        {
            double offsetX = double(_texture->getRectangle().x() / CC_CONTENT_SCALE_FACTOR());
            double offsetY = double(-_texture->getRectangle().y() / CC_CONTENT_SCALE_FACTOR());
            double clipW = double(_texture->getRectangle().width() / CC_CONTENT_SCALE_FACTOR());
            double clipH = double(_texture->getRectangle().height() / CC_CONTENT_SCALE_FACTOR());
            //            double texLeft = double( _texture->getTexLeft());
            //            double texTop = double( _texture->getTexTop() );

            _vertices[0] = float(0.f + offsetX);
            _vertices[1] = float(-clipH + offsetY);

            _vertices[2] = float(clipW + offsetX);
            _vertices[3] = float(-clipH + offsetY);

            _vertices[4] = float(0.f + offsetX);
            _vertices[5] = float(0.f + offsetY);

            _vertices[6] = float(clipW + offsetX);
            _vertices[7] = float(0.f + offsetY);
/*
            ccn_dlog( "vertices( %.2f,%.2f,  %.2f,%.2f,  %.2f,%.2f,  %.2f,%.2f )"
            , _vertices[0], _vertices[1]
            , _vertices[2], _vertices[3]
            , _vertices[4], _vertices[5]
            , _vertices[6], _vertices[7]
            );
*/
            //_texCoords[0] = float(  (texLeft+0.5) * invW );
            //_texCoords[1] = float(( (texTop+0.5) + clipH -1.0 ) * invH );

            //_texCoords[2] = float(( (texLeft+0.5) + clipW -1.0 ) * invW );
            //_texCoords[3] = float(( (texTop+0.5) + clipH -1.0 ) * invH );

            //_texCoords[4] = float(( texLeft+0.5 ) * invW );
            //_texCoords[5] = float(( texTop+0.5 )*invH );

            //_texCoords[6] = float(( texLeft+0.5 + clipW-1.0 ) * invW );
            //_texCoords[7] = float(( texTop+0.5 ) * invH );

            _texCoords[0] = 0.f;//float(  (texLeft+0.5) * invW );
            _texCoords[1] = 1.f;//float(( (texTop+0.5) + clipH -1.0 ) * invH );

            _texCoords[2] = 1.f;//float(( (texLeft+0.5) + clipW -1.0 ) * invW );
            _texCoords[3] = 1.f;//float(( (texTop+0.5) + clipH -1.0 ) * invH );

            _texCoords[4] = 0.f;//float(( texLeft+0.5 ) * invW );
            _texCoords[5] = 0.f;//float(( texTop+0.5 )*invH );

            _texCoords[6] = 1.f;//float(( texLeft+0.5 + clipW-1.0 ) * invW );
            _texCoords[7] = 0.f;//float(( texTop+0.5 ) * invH );
/*
            ccn_dlog( "uv( %.2f,%.2f,  %.2f,%.2f,  %.2f,%.2f,  %.2f,%.2f )"
            , _texCoords[0], _texCoords[1]
            , _texCoords[2], _texCoords[3]
            , _texCoords[4], _texCoords[5]
            , _texCoords[6], _texCoords[7]
            );
*/
        }

        ///// このシンボルが単体でノードツリーにaddChildされた時
        virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override
        {
            if (this->isVisible() == false) {
                return;
            }

            if (flags & FLAGS_DIRTY_MASK) {
                Mat4 trans = transform;
                trans.m[12] += (float)(_texture->getRectangle().x() * trans.m[0] / CC_CONTENT_SCALE_FACTOR());
                trans.m[13] -= (float)(_texture->getRectangle().y() * trans.m[5] / CC_CONTENT_SCALE_FACTOR());
                _insideBounds = renderer->checkVisibility(trans, _contentSize);
            }
            if (!_insideBounds) {
                return;
            }

            // TODO setGLProgram に載せ替えたい
            _customCommand.init(_globalZOrder);
            _customCommand.func = CC_CALLBACK_0(BmcShapeImpl::onDraw, this, transform, flags);
            renderer->addCommand(&_customCommand);
        }

        void onDraw(const Mat4 &transform, uint32_t flags)
        {
            _director->pushMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
            _director->loadMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW, transform);

            ColorTransform my = getAdditionalColorTransform();
            Node* pParent = nullptr;
            for (pParent = getParent(); pParent != nullptr; pParent = pParent->getParent()) {
                BmcSymbol* p = areYouSymbol(pParent);
                if (p == nullptr) {
                    continue;
                }
                my = p->getColorTransform().mutiply(my);
            }

            // ブレンディング設定。プリマルチプライドに対応
            // ccGLBlendFunc( CC_BLEND_SRC, CC_BLEND_DST ); と同じ。
            // これだと分かりにくいのでOpenGLの値をそのまま渡す。cocos2dxが余計に分かりにくい定義をするのが意味分からない。
            // ccGLBlendFuncは内部でglEnable(GL_BLEND);を呼ぶ。前と同じStateであれば変更されないので毎回呼んだほうが安全
            if (_blendMode == bmc::BmcBlendMode_Add) {
                GL::blendFunc(GL_ONE, GL_ONE);
            } else {
                GL::blendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
            }
            _shader->setColorTransform(my);
            _shader->useMostValuableProgram();
            _shader->setTexture(_texture);

            GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POSITION | GL::VERTEX_ATTRIB_FLAG_TEX_COORD);
            glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 2, GL_FLOAT, GL_FALSE, 0, _vertices);
            glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, 0, _texCoords);

            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

            CHECK_GL_ERROR_DEBUG();
            _director->popMatrix(MATRIX_STACK_TYPE::MATRIX_STACK_MODELVIEW);
        }
    };

// BmcSpriteShapeImpl を使うのでこちらは使わない
//    BMC_FACTORY_CREATE_FUNC_IMPL(BmcShape);

} // cocone
#endif
