﻿// BmcSymbol.cpp.cpp
//
#pragma once

#include "BmcSymbolDefaultImpl.h"

namespace cocone
{

    /// アクセス不可を起こさないための NullObject
    class BmcNullSymbolImpl : public BmcSymbolDefaultImpl, public ZeroRefCount<cocos2d::Node>
    {
        cocone::bmc::BmcTimeline _timeline;
    public:
        // TODO impl assert
        BmcNullSymbolImpl() = default;
        virtual ~BmcNullSymbolImpl() = default;

        CCN_SHARABLEPTR_DEFINE(BmcNullSymbolImpl);
        CCN_DECLARE_ASNODE_IMPL();

        void* query(IIDType iid) override
        {
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

        virtual BmcSymbol::Ptr clone() const override
        {
            return new BmcNullSymbolImpl();
        }

        virtual const cocone::bmc::BmcTimeline& getTimeline()const override
        {
            return _timeline;
        }

        virtual BmcSymbol::Ptr getInstance(const std::string& instanceName) const override
        {
            return clone();
        }
        virtual BmcSymbol::Ptr getInstanceOne() const override
        {
            return clone();
        }
        virtual BmcSymbol::Ptr destroyInstance(const std::string& instanceName) override
        {
            return clone();
        }

        virtual cocos2d::Size setupContentSize() override {
            return cocos2d::Size::ZERO;
        }

        virtual const cocos2d::Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }

        inline bool operator == (const std::nullptr_t) const { return true; }
        inline bool operator != (const std::nullptr_t) const { return false; }
        inline operator bool() const { return false; }
    };


} // cocone

// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored. 
