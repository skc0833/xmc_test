﻿// BmcScalingGridShapeDefaultImpl.cpp
//

#include <cocone/bmc/BmcScalingGridShape.h>

#include <cocos-ext.h>
#include <cocone/bmc/BmcSymbol.h>
#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>

#include "BmcFactoryImpl.h"
#include "BmcSymbolDefaultImpl.h"

USING_NS_CC;
USING_NS_CC_EXT;

namespace cocone {

    class BmcScalingGridShapeImpl : public BmcSymbolDefaultImpl, public BmcScalingGridShape, public ui::Scale9Sprite
    {
        CCN_DECLARE_ASNODE_IMPL();
    protected:///////////////////////////////////////////////////////////////////
        void* query(IIDType iid) override {
            if (iid == BmcScalingGridShape::iid()) {
                return static_cast<BmcScalingGridShape*>(this);
            }
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

        virtual const Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }

        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }
        ///////////////////////////////////////////////////////////////////////////
    private:
        BmcShader::Ptr _shader;
        BmcTexturePart::Ptr _texture;
        bmc::BmcBlendMode _blendMode;
        bool _isInit;
        BmcTextureReloader::Ptr _reloader;
    public:

        BmcScalingGridShapeImpl()
        {
            _blendMode = bmc::BmcBlendMode_Normal;
            _isInit = false;
        }
        virtual ~BmcScalingGridShapeImpl()
        {
            //		ccn_dlog( "" );
            if (_reloader){
                _reloader->unregist(this);
            }
        }
        virtual BmcSymbol::Ptr clone() const override
        {
            BmcScalingGridShapeImpl* newP = new BmcScalingGridShapeImpl();
            newP->_shader = _shader;
            newP->_texture = _texture;
            newP->_blendMode = _blendMode;
            //            newP->_batchNode = _batchNode;
            newP->_additionalColorTransform = _additionalColorTransform;
            newP->_isInit = false;
            newP->_reloader = _reloader;
            _reloader->regist(newP);
            return newP;
        }
        virtual void init(BmcShader::Ptr shader, BmcTexturePart::Ptr texture) override
        {
            _shader = shader;
            _texture = texture;
        }
        virtual void onEnter() override
        {
            if (_isInit == false){
                _isInit = true;
                initInternal();
            }
            Scale9Sprite::onEnter();
        }
        void initInternal()
        {
            SpriteBatchNode *batchnode = SpriteBatchNode::createWithTexture(_texture->getTexture()->getCCTexture2D(), 9);
            //    bool pReturn = this->initWithFile(file, CCRectZero, capInsets);
            const bmc::BmcRectangle& texRect = _texture->getRectangle();
            const bmc::BmcRectangle& rect9scale = _texture->getScale9GridRectangle();
            Rect capInsets = Rect(
                rect9scale.x() - texRect.x()
                , (rect9scale.y() - texRect.y())
                , rect9scale.width() == 0 ? 1 : rect9scale.width()
                , rect9scale.height() == 0 ? 1 : rect9scale.height()
                );

            bool pReturn = this->initWithBatchNode(batchnode, Rect::ZERO, capInsets);
            if (ccn_econd(pReturn == false)){
            }
//            setContentSize(Size(_texture->getRectangle().width(), _texture->getRectangle().height()));
            /// いったん外す。TODO:
            //            float anchorX = float(texRect.x()) / float( texRect.width() );
            //            float anchorY = -float(texRect.y()) / float( texRect.height() );
            //            setAnchorPoint( ccp( anchorX, anchorY) );
        }
        virtual void registReloader(BmcTextureReloader::Ptr reloader) override
        {
            _reloader = reloader;
            _reloader->regist(this);
        }
        virtual void reload() override
        {
            initInternal();
        }
        virtual Size setupContentSize() override
        {
            return Size(_texture->getRectangle().width(), _texture->getRectangle().height());
        }
        virtual void setBlendMode(cocone::bmc::BmcBlendMode blendMode) override
        {
            _blendMode = blendMode;
        }
        void setAdditionalTransform(const AffineTransform& additionalTransform) override
        {
            AffineTransform affine = additionalTransform;

            float xRate = affine.a;
            float yRate = affine.d;
            affine.a = 1.f;
            affine.d = 1.f;
            setContentSize(Size(_texture->getRectangle().width()*xRate, _texture->getRectangle().height()*yRate));

            this->setAdditionalTransform(affine);
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcScalingGridShape);

} // cocone
