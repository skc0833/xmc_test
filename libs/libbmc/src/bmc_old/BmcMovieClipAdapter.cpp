// BmcMovieClipAdapter.cpp
//
#include "BmcMovieClipAdapter.h"

#include <cocone/bmc/BmcGeom.h>
#include <cocone/bmc/BmcSoundPlayer.h>
#include <cocone/bmc/BmcLibrary.h>
#include <cocone/bmc/BmcClockGenerator.h>

//#define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
#include <cocone/foundation/Log.h>
#include <cocone/foundation/ClockMeter.h>

#include "BmcNullMovieClipImpl.h"

//#define DEBUG_LOG_ENABLED
#define DEBUG_LOG_ENABLED //skc test

#ifdef DEBUG_LOG_ENABLED
#define DEBUG_LOG(...) { cocone::logOutput(false, cocone::LogPrefix_Debug,__func__,__FILE__,__LINE__,__VA_ARGS__); }
#else
#define DEBUG_LOG(...) ((void)0)
#endif


using namespace cocos2d;
using namespace google_public::protobuf;
namespace cocone {

    static const std::string EMPTY_STRING = "";
    static const int INSTANCE_TAG_OFFSET = 10000;
    BmcMovieClipAdapter::BmcMovieClipAdapter()
        : _speedScale(1.f), _blendMode(bmc::BmcBlendMode_Normal)
    {
      ccn_dlog("[skc] MC ++ %p", this);
    }

    BmcMovieClipAdapter::~BmcMovieClipAdapter()
    {
      ccn_dlog("[skc] MC -- %p", this);
    }
    BmcSymbol::Ptr BmcMovieClipAdapter::clone() const
    {
        BmcMovieClipAdapter* newP = callNewAndInit();
        newP->asNode()->setContentSize(Size::ZERO);
        newP->_timeline.CopyFrom(_timeline);
        newP->_speedScale = _speedScale;

        newP->_oneFrameDelta = _oneFrameDelta;
        newP->_currentFrame = _currentFrame;
        newP->_stockTime = _stockTime;
        newP->_isLoop = _isLoop;
        newP->_playStartFrame = _playStartFrame;
        newP->_playFrameCount = _playFrameCount;
        newP->_prevConstructFrame = -1;
        newP->setAdditionalTransform(getAdditionalTransform());
        newP->_additionalColorTransform = _additionalColorTransform;
        newP->_blendMode = _blendMode;
        newP->_soundPlayer = _soundPlayer;
        newP->_clockGenerator = _clockGenerator;

        newP->_instances.resize(_timeline.instance_table().size());
        for (int i = 0; i < _timeline.instance_table().size(); ++i) {
            newP->_instances[i] = _instances[i]->clone();
        }
        newP->_indexhash.resize(_indexhash.size());
        for (uint32_t i = 0; i < _indexhash.size(); ++i) {
            newP->_indexhash[i] = _indexhash[i];
        }
        newP->setupContentSize();
        newP->clearLabelCallback();

        return newP;
    }
    void BmcMovieClipAdapter::init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline)
    {
        callNodeInit();
        asNode()->setContentSize(Size::ZERO);
        _oneFrameDelta = 1.f / float(source->getFrameRate());
        _timeline.CopyFrom(bmcTimeline);
        _currentFrame = 0;
        _stockTime = 0.f;
        _isLoop = true;
        _prevConstructFrame = -1;
        _playStartFrame = 0;
        _playFrameCount = _timeline.frames().size();
        _soundPlayer = source->getSoundPlayer();
        _clockGenerator = source->getClockGenerator();

        // 使用するシンボルを抽出する
        setupSymbols(source);

        //make hash table for overrapped named instances
        std::map<string, int> nameTable;
        _indexhash.clear();
        nameTable.clear();
        for (int i = 0; i < _timeline.instance_table_size(); ++i) {
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
#if 0 //skc test
            _indexhash.push_back(i);
            nameTable[instanceInfo.instance_name()] = i;
#else
            if (nameTable.find(instanceInfo.instance_name()) != nameTable.end()) {
                _indexhash.push_back(nameTable[instanceInfo.instance_name()]);
            } else {
                _indexhash.push_back(i);
                nameTable[instanceInfo.instance_name()] = i;
            }
#endif
DEBUG_LOG("%d: %s", i, instanceInfo.instance_name().c_str());
        }

        setupContentSize();
        clearLabelCallback();
    }
    void BmcMovieClipAdapter::processFrame()
    {
        if (asNode()->getParent() == nullptr) {
            return;
        }
        if (_prevConstructFrame != _currentFrame) {
            updateFrameForView();
            {
                const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
                // サウンドプレイ
                if (frame.has_play_sound_index() && asNode()->isVisible()) {
                    _soundPlayer->playEffect(frame.play_sound_index());
                }
            }
            processActionScriptByLabels();
            if (_currentFrame == (_playStartFrame + _playFrameCount - 1)) {
                if (_listener != nullptr) {
                    _listener(EventType_PlayEndFrame, this);
                }
                if (!_isLoop) {
//                    stop();
                }
            }
        }
        nextFrame(false);
    }

    void BmcMovieClipAdapter::nodeFuncUpdate(float deltaTime)
    {
        _stockTime += deltaTime*_speedScale;
#ifdef DEBUG_LOG_ENABLED
int frame = floor(_stockTime / _oneFrameDelta);
#endif
DEBUG_LOG("%p: currentFrame=%d stockTime=%.5f deltaTime=%.5f frameDelta=%.5f frame=%d", static_cast<BmcMovieClip*>(this), _currentFrame, _stockTime, deltaTime, _oneFrameDelta, frame);

        while (_stockTime > _oneFrameDelta) {
            _stockTime -= _oneFrameDelta;
            updateFrameForView();
            {
                const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
                // サウンドプレイ
                if (frame.has_play_sound_index() && asNode()->isVisible()) {
                    _soundPlayer->playEffect(frame.play_sound_index());
                }
            }
            int prevFrame = _currentFrame;
            nextFrame(false);
            if (prevFrame == _currentFrame) {
                _stockTime = 0;
                break;
            }
            processActionScriptByLabels();
            if (_listener != nullptr && _currentFrame == (_playStartFrame+_playFrameCount)) {
                _listener(EventType_PlayEndFrame, this);
                break;
            }
        }
    }

    void BmcMovieClipAdapter::nodeFuncOnExit()
    {
        _currentFrame = _playStartFrame;
        _stockTime = 0.f;
        _prevConstructFrame = -1;
    }

    // BmcSymbol
    const bmc::BmcTimeline& BmcMovieClipAdapter::getTimeline() const
    {
        return _timeline;
    }
    // BmcSymbol
    bool BmcMovieClipAdapter::hasTimeline() const
    {
        return true;
    }
    /// 指定したインスタンス名で配置されているシンボルを取得する。
    BmcSymbol::Ptr BmcMovieClipAdapter::getInstance(const std::string& instanceName) const
    {
        //            ccn_dlog("updateFrameForView start==============================%d", instanceSize);
        for (int i = 0; i < _timeline.instance_table_size(); ++i) {
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
            BmcSymbol::Ptr symbol = _instances[i];

            if (instanceInfo.has_instance_name() && instanceInfo.instance_name() == instanceName) {
                return symbol;
            }
        }
        return nullptr;
    }
    BmcSymbol::Ptr BmcMovieClipAdapter::getInstanceOne() const
    {
        ccn_assert(_instances.size() == 1);
        return _instances[0];
    }
    BmcSymbol::Ptr BmcMovieClipAdapter::getNullObject() const
    {
        return new BmcNullMovieClipImpl();
    }

    void BmcMovieClipAdapter::getAllInstances(std::vector<BmcSymbol::Ptr>* out) const
    {
        *out = _instances;
    }
    void BmcMovieClipAdapter::overwriteInstance(BmcSymbol::Ptr current, BmcSymbol::Ptr over)
    {
        bool overwrite = false;
        for (int i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == current) {
                _instances[i] = over;
                asNode()->removeChildByTag(INSTANCE_TAG_OFFSET + i);
                overwrite = true;
                break;
            }
        }
        if (overwrite) {
            _prevConstructFrame = -1;
            updateFrameForView();
        }
    }
    void BmcMovieClipAdapter::overwriteInstances(const std::string& instanceName, BmcSymbol::Ptr over)
    {
        innerOverrideInstances(instanceName, over, false);
    }
    bool BmcMovieClipAdapter::innerOverrideInstances(const std::string& instanceName, BmcSymbol::Ptr _template, bool overwrite)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
            if (instanceInfo.has_instance_name() && instanceInfo.instance_name() == instanceName) {
                if (overwrite) {
                    _instances[i] = _template->clone();
                }
                else{
                    _instances[i] = _template;
                    overwrite = true;
                }
                asNode()->removeChildByTag(INSTANCE_TAG_OFFSET + i);
            }
            else{
                BmcMovieClipAdapter* adapter = dynamic_cast<BmcMovieClipAdapter*>(_instances[i].get());
                if (adapter != nullptr) {
                    overwrite = adapter->innerOverrideInstances(instanceName, _template, overwrite);
                }
            }
        }
        if (overwrite) {
            _prevConstructFrame = -1;
            updateFrameForView();
        }
        return overwrite;
    }

    BmcSymbol::Ptr BmcMovieClipAdapter::destroyInstance(const std::string& instanceName)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
            if (instanceInfo.has_instance_name() && instanceInfo.instance_name() == instanceName) {
                BmcSymbol::Ptr inst = _instances[i];
                _instances[i] = nullptr;
                asNode()->removeChildByTag(INSTANCE_TAG_OFFSET + i);
                return inst;
            }
        }
        return nullptr;
    }
    void BmcMovieClipAdapter::destroyInstance(BmcSymbol::Ptr instance)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == instance) {
                _instances[i] = nullptr;
                asNode()->removeChildByTag(INSTANCE_TAG_OFFSET + i);
            }
        }
    }
    void BmcMovieClipAdapter::iterateInstances(std::function<void(BmcSymbol*)> func)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == nullptr) {
                continue;
            }
            func(_instances[i].get());
        }
    }
    void BmcMovieClipAdapter::iterateInstancesForName(std::function<void(const std::string& instanceName, BmcSymbol*)> func)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == nullptr) {
                continue;
            }
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
            func(instanceInfo.has_instance_name()? instanceInfo.instance_name() : EMPTY_STRING,_instances[i].get());
        }
    }
    void BmcMovieClipAdapter::iterateInstancesByName(const std::string& instanceName, std::function<void(BmcSymbol*)> func)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == nullptr) {
                continue;
            }
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
            if (instanceInfo.has_instance_name() && instanceInfo.instance_name() == instanceName) {
                func(_instances[i].get());
            }
        }
    }
    void BmcMovieClipAdapter::iterateInstancesRecursively(std::function<void(BmcSymbol*)> func)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == nullptr) {
                continue;
            }
            func(_instances[i].get());
            _instances[i]->iterateInstances(func);
        }
    }
    void BmcMovieClipAdapter::iterateInstancesByNameRecursively(const std::string& instanceName, std::function<void(BmcSymbol*)> func)
    {
        for (int i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == nullptr) {
                continue;
            }
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);
            if (instanceInfo.has_instance_name() && instanceInfo.instance_name() == instanceName) {
                func(_instances[i].get());
            }
            _instances[i]->iterateInstancesByName(instanceName, func);
        }
    }


    int BmcMovieClipAdapter::getCurrentFrame() const
    {
        return _currentFrame;
    }

    const std::string& BmcMovieClipAdapter::getCurrentLabel() const
    {
        return EMPTY_STRING;
    }

    int BmcMovieClipAdapter::getTotalFrames() const
    {
        return _timeline.frames().size();
    }

    int BmcMovieClipAdapter::getFrameByLabel(const char* label) const
    {
        int labelIndex = searchFrameIndexByLabel(label);
        if (ccn_wcond(labelIndex == -1, "label=%s", label)) {
            return labelIndex;
        }
        int labelFrameNumber = searchFrameNumberByFrameLabelIndex(0, labelIndex);
        if (ccn_wcond(labelFrameNumber == -1, "label=%s", label)) {
            return -1;
        }
        return labelFrameNumber;
    }
    void BmcMovieClipAdapter::gotoAndPlay(int frameNumber)
    {
        setPlayFrameRange(frameNumber, _timeline.frames().size() - frameNumber);
        moveCurrentFrame(frameNumber);
        play();
    }
    void BmcMovieClipAdapter::gotoTopAndPlayRecursively()
    {
        _currentFrame = 0;
        _playStartFrame = 0;
        _playFrameCount = _timeline.frames().size();
        iterateInstances([](BmcSymbol* symbol) {
            BmcMovieClipAdapter* child = dynamic_cast<BmcMovieClipAdapter*>(symbol);
            if (child != nullptr) {
                child->gotoTopAndPlayRecursively();
            }
        });
        play();
    }
    void BmcMovieClipAdapter::gotoAndPlay(int frameNumber, int count)
    {
        if (ccn_econd(frameNumber + count > _timeline.frames().size(), "frameNumber=%d, count=%d, total=%d", frameNumber, count, _timeline.frames().size())) {
            count = _timeline.frames().size() - frameNumber;
        }
        if (count <= 0) {
            count = 1;
        }
        setPlayFrameRange(frameNumber, count);
        moveCurrentFrame(frameNumber);
        play();
    }
    void BmcMovieClipAdapter::gotoAndPlayToNextLabel(const char* label)
    {
        int labelIndex = searchFrameIndexByLabel(label);
        if (ccn_wcond(labelIndex == -1, "label=%s", label)) {
            return;
        }
        int startFrameNumber = searchFrameNumberByFrameLabelIndex(0, labelIndex); // 指定したラベルがある開始フレーム
        if (ccn_wcond(startFrameNumber==-1, "label=%s", label)) {
            return;
        }
        int tailIndex = labelIndex+1;
        int frameCount = -1;
        if (tailIndex == _timeline.frame_label_table().size()) {
            frameCount = getTotalFrames() - startFrameNumber; // 次のラベルがなければ最終フレームまでのフレーム数
        } else {
            frameCount = searchFrameNumberByFrameLabelIndex(startFrameNumber+1, tailIndex);
            if (ccn_econd(frameCount==-1, "label=%s, labelIndex=%d, startFrameNumber=%d", label, labelIndex, startFrameNumber)) {
                return;
            }
            frameCount -= startFrameNumber; // 次のラベルがある位置までのフレーム数(開始フレームからの）
        }
        // 開始位置と再生数を指定して指定位置に移動する
        setPlayFrameRange(startFrameNumber, frameCount);
        //        ccn_dlog("start=%d, count=%d", _playStartFrame, _playFrameCount);
        moveCurrentFrame(_playStartFrame);
        //        ccn_dlog("current=%d", _currentFrame);
        play();
    }
    void BmcMovieClipAdapter::gotoAndPlayLabel(const char* labelFrom, const char* labelTo)
    {
        int labelIndexFrom = searchFrameIndexByLabel(labelFrom);
        if (ccn_wcond(labelIndexFrom == -1, "label=%s", labelFrom)) {
            return;
        }
        int startFrameNumber = searchFrameNumberByFrameLabelIndex(0, labelIndexFrom); // 指定したラベルがある開始フレーム
        if (ccn_wcond(startFrameNumber==-1, "label=%s", labelFrom)) {
            return;
        }

        int frameCount = -1;
        if (labelTo == 0) {
            frameCount = getTotalFrames() - startFrameNumber; // 次のラベルがなければ最終フレームまでのフレーム数
        } else {
            int labelIndexTo = searchFrameIndexByLabel(labelTo);
            if (ccn_wcond(labelIndexTo == -1, "label=%s", labelTo)) {
                return;
            }
            int endFrameNumber = searchFrameNumberByFrameLabelIndex(0, labelIndexTo); // 指定したラベルがある開始フレーム
            if (ccn_wcond(endFrameNumber==-1, "label=%s", labelIndexTo)) {
                return;
            }
            if (ccn_wcond(startFrameNumber > endFrameNumber, "start frame is bigger than end frame")) {
                return;
            }
            frameCount = endFrameNumber - startFrameNumber; // 次のラベルがある位置までのフレーム数(開始フレームからの）
        }
        // 開始位置と再生数を指定して指定位置に移動する
        setPlayFrameRange(startFrameNumber, frameCount);
        //        ccn_dlog("start=%d, count=%d", _playStartFrame, _playFrameCount);
        moveCurrentFrame(_playStartFrame);
        //        ccn_dlog("current=%d", _currentFrame);
        play();
    }
    void BmcMovieClipAdapter::gotoAndPlayToNextFrame(int frameNumber)
    {
        setPlayFrameRange(_playStartFrame, frameNumber);
        moveCurrentFrame(_currentFrame);
        play();
    }
    void BmcMovieClipAdapter::gotoAndStop(int frameNumber)
    {
        setPlayFrameRange(frameNumber, 1);
        moveCurrentFrame(frameNumber);
        stop();
    }
    void BmcMovieClipAdapter::play()
    {
//        asNode()->unscheduleUpdate();
//        asNode()->scheduleUpdate();
        if (getTotalFrames() > 1) {
            _clockGenerator->registerListener(this);
        }
    }
    void BmcMovieClipAdapter::stop()
    {
        setPlayFrameRange(_currentFrame, 1);
//        asNode()->unscheduleUpdate();
        if (getTotalFrames() > 1) {
            _clockGenerator->unregisterListener(this);
        }
    }
    void BmcMovieClipAdapter::stopAll()
    {
        stop();
        _clockGenerator->unregisterAllListeners();
    }
    void BmcMovieClipAdapter::setLoop(bool isLoop)
    {
DEBUG_LOG("%p: isLoop=%d", static_cast<BmcMovieClip*>(this), isLoop);
        _isLoop = isLoop;
    }
    void BmcMovieClipAdapter::setSpeedScale(float scale)
    {
        _clockGenerator->setSpeedScale(scale);
        _speedScale = scale;
        for (size_t i = 0; i < _instances.size(); ++i) {
            if (_instances[i] == nullptr) { continue; }
            BmcMovieClip* p = _instances[i]->query<BmcMovieClip>();
            if (p == nullptr) { continue; }
            p->setSpeedScale(scale);
        }
    }
    float BmcMovieClipAdapter::getSpeedScale() const
    {
        return _speedScale;
    }
    void BmcMovieClipAdapter::setEventListener(std::function<void (EventType, BmcMovieClip*)> listener)
    {
        _listener = listener;
    }
    void BmcMovieClipAdapter::clearEventListener()
    {
		_listener = nullptr;
    }

    void BmcMovieClipAdapter::setLabelCallback(const std::string& labelName, std::function<void(BmcMovieClip*)> listener)
    {
        _labelCallback[labelName.c_str()] = listener;
    }
    void BmcMovieClipAdapter::clearLabelCallback()
    {
        _labelCallback.clear();
    }

    // BmcSymbol
    void BmcMovieClipAdapter::nextFrame(bool child)
    {
        ccn_assert(_playFrameCount > 0);
        ccn_assert(0<=_playStartFrame && _playStartFrame < _timeline.frames().size());
#ifdef DEBUG_LOG_ENABLED
        auto prevFrame = _currentFrame;
#endif
        if (++_currentFrame==(_playStartFrame+_playFrameCount)) {
DEBUG_LOG("%p: isLoop=%d", static_cast<BmcMovieClip*>(this), _isLoop);
            if (_isLoop) {
DEBUG_LOG("%p: looped", static_cast<BmcMovieClip*>(this));
                _currentFrame = _playStartFrame;
            } else {
DEBUG_LOG("%p: stop loop", static_cast<BmcMovieClip*>(this));
                _currentFrame = _playStartFrame+_playFrameCount-1;
            }
        }
DEBUG_LOG("%p: frame=%d->%d", static_cast<BmcMovieClip*>(this), prevFrame, _currentFrame);
        if (child) {
            // 子供たちにも知らせる
            const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
            int instanceSize = frame.placed_instances().size();
            for (int i = 0; i < instanceSize; ++i) {
                const bmc::BmcInstanceAtPlacedFrame& placedInstance = frame.placed_instances(i);
                int instance_index = _indexhash[placedInstance.instance_index()];
                BmcMovieClip* instAsMc = dynamic_cast<BmcMovieClip*>(_instances[instance_index].get());
                if (instAsMc != nullptr) {
                    instAsMc->nextFrame(true);
                }
            }
        }
    }
    void BmcMovieClipAdapter::prevFrame(bool child)
    {
        ccn_assert(_playFrameCount > 0);
        ccn_assert(0<=_playStartFrame && _playStartFrame < _timeline.frames().size());
        if (--_currentFrame==(_playStartFrame-1)) {
            if (_isLoop) {
                _currentFrame = _playStartFrame+_playFrameCount-1;
            } else {
                _currentFrame = _playStartFrame;
            }
        }
        if (child) {
            // 子供たちにも知らせる
            const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
            int instanceSize = frame.placed_instances().size();
            for (int i = 0; i < instanceSize; ++i) {
                const bmc::BmcInstanceAtPlacedFrame& placedInstance = frame.placed_instances(i);
                int instance_index = _indexhash[placedInstance.instance_index()];
                BmcMovieClip* instAsMc = dynamic_cast<BmcMovieClip*>(_instances[instance_index].get());
                if (instAsMc != nullptr) {
                    instAsMc->prevFrame(true);
                }
            }
        }
    }
    void BmcMovieClipAdapter::updateFrameForView()
    {
        if (_prevConstructFrame == _currentFrame) {
#ifdef DEBUG_LOG_ENABLED
            if (_playStartFrame+_playFrameCount-1 > 0) {
DEBUG_LOG("%p: frame=%d/%d ========================================", static_cast<BmcMovieClip*>(this), _currentFrame, _playStartFrame+_playFrameCount-1);
            }
#endif
           return;
        }
DEBUG_LOG("%p: frame=%d/%d ========================================", static_cast<BmcMovieClip*>(this), _currentFrame, _playStartFrame+_playFrameCount-1);

        _prevConstructFrame = _currentFrame;
        const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);

        std::set<BmcSymbol*> liveObject;

        int instanceSize = frame.placed_instances().size();
        //            ccn_dlog("updateFrameForView start==============================%d", instanceSize);
        for (int i = 0; i < instanceSize; ++i) {
            const bmc::BmcInstanceAtPlacedFrame& placedInstance = frame.placed_instances(i);
            int instance_index = _indexhash[placedInstance.instance_index()];
            BmcSymbol::Ptr inst = _instances[instance_index];

            if (inst == nullptr) { // 削除されたものなので無視してよい
                continue;
            }

            if (placedInstance.has_matrix_index()) {
                const bmc::BmcMatrix& bmcMatrix = _timeline.matrix_instance_table(placedInstance.matrix_index());
                inst->setAdditionalTransform(toMat4(bmcMatrix));
            } else {
                inst->setAdditionalTransform(AffineTransform::IDENTITY);
            }
#if 1 //skc test
            if (placedInstance.has_colortrans_index()==false) {
                inst->setAdditonalColorTransform(ColorTransform::Identity());
            } else {
                const bmc::BmcColorTransform& s = _timeline.colortrans_instance_table(placedInstance.colortrans_index());
                inst->setAdditonalColorTransform(ColorTransform(s));
            }
            if (placedInstance.has_blend_mode()) {
                inst->setBlendMode(placedInstance.blend_mode());
            }
#endif
            // Zオーダーをマイナス値で追加して後から追加されたオブジェクトとの順番を整理する
            int zOrder = -(instanceSize - i);
            if (inst->asNode()->getParent() == nullptr) {
                asNode()->addChild(inst->asNode(), zOrder, INSTANCE_TAG_OFFSET + placedInstance.instance_index());
            }
            else{
                // すでに追加されているものはZOrderを変更する
                inst->asNode()->setLocalZOrder(zOrder);
            }
            liveObject.insert(inst.get());

const bmc::BmcInstance& instance = _timeline.instance_table(i);
            BmcMovieClip* instAsMc = dynamic_cast<BmcMovieClip*>(inst.get());
            if (instAsMc != nullptr) {
                instAsMc->updateFrameForView();
                instAsMc->play();
DEBUG_LOG("%p: %s=%d/%d",
  instAsMc,
  instance.instance_name().c_str(),
  instAsMc->getCurrentFrame(),
  instAsMc->getTotalFrames() - 1);
            } else {
DEBUG_LOG("%p: %s",
  inst.get(),
  instance.instance_name().c_str());
            }
        }
        // このフレームで必要ないインスタンスを削除する
        // このシンボルが管理しているオブジェクトだけを親から適切に削除する（あとで追加されたオブジェクトを削除しない
        for (size_t i = 0; i < _instances.size(); ++i) {
            BmcSymbol* p = _instances[i];
            if (p == nullptr) { continue; }
            if (p->asNode()->getParent() == nullptr) {
                continue;
            }
            if (liveObject.find(p)==liveObject.end()) {
                BmcMovieClipAdapter* instAsMc = dynamic_cast<BmcMovieClipAdapter*>(p);
                if (instAsMc != nullptr) {
                    instAsMc->moveCurrentFrame(0);
DEBUG_LOG("%p: rewinded", static_cast<BmcMovieClip*>(instAsMc));
                } else {
DEBUG_LOG("%p: removed", p);
                }
                p->asNode()->removeFromParent();
            }
        }
    }
    void BmcMovieClipAdapter::stopFrameRecursively()
    {
        const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
        int instanceSize = frame.placed_instances().size();
        for (int i = 0; i < instanceSize; ++i) {
            const bmc::BmcInstanceAtPlacedFrame& placedInstance = frame.placed_instances(i);
            int instance_index = _indexhash[placedInstance.instance_index()];
            BmcSymbol::Ptr inst = _instances[instance_index];
            BmcMovieClip* mc = dynamic_cast<BmcMovieClip*>(inst.get());
            if (mc) {
                mc->stop();
            }
        }
    }
    void BmcMovieClipAdapter::setupSymbols(BmcLoader* source)
    {
        _instances.resize(_timeline.instance_table().size());
        // 使用するシンボルを抽出して保存する
        const std::vector<BmcSymbol::Ptr>& symbolTable = source->getSymbolTable();
        for (int i = 0; i < _timeline.instance_table().size(); ++i) {
            const bmc::BmcInstance& instance = _timeline.instance_table(i);
            if (symbolTable[instance.symbol_index()]->asRef()->getReferenceCount() == 1) {
                _instances[i] = symbolTable[instance.symbol_index()];
            } else {
                _instances[i] = symbolTable[instance.symbol_index()]->clone();
            }
        }
    }
    int BmcMovieClipAdapter::searchFrameIndexByLabel(const char* label) const
    {
        int labelIndex = -1;
        for (int i = 0; i < _timeline.frame_label_table().size(); ++i) {
            if (_timeline.frame_label_table(i)==label) {
                labelIndex = i;
                break;
            }
        }
        return labelIndex;
    }
    int BmcMovieClipAdapter::searchFrameNumberByFrameLabelIndex(int searchStartFrame, int frameLabelIndex) const
    {
        for (int i = searchStartFrame; i < _timeline.frames().size(); ++i) {
            const bmc::BmcFrame& frame = _timeline.frames(i);
            if (frame.has_frame_label_index() && frame.frame_label_index() == frameLabelIndex) {
                return i;
            }
        }
        return -1;
    }
    void BmcMovieClipAdapter::setPlayFrameRange(int startFrame, int count)
    {
        _playStartFrame = startFrame;
        _playFrameCount = count;
        if (_playStartFrame >= _timeline.frames().size()) {
            _playStartFrame = _timeline.frames().size()-1;
            _playFrameCount = 1;
        }
        if (_playStartFrame + _playFrameCount > _timeline.frames().size()) {
            _playFrameCount = _timeline.frames().size() - _playStartFrame;
        }
        //// 子供たちにも知らせる
        //const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
        //int instanceSize = frame.placed_instances().size();
        //for (int i = 0; i < instanceSize; ++i)
        //{
        //    const bmc::BmcInstanceAtPlacedFrame& placedInstance = frame.placed_instances(i);
        //    BmcMovieClipAdapter* child = dynamic_cast<BmcMovieClipAdapter*>(_instances[placedInstance.instance_index()].get());
        //    if (child != nullptr) {
        //        child->setPlayFrameRange(startFrame, count);
        //    }
        //}
    }

    // 指定フレームまで、nextFrameとprevFrameを駆使して移動させる
    // 直指定で移動しないのは階層化された子Symbolのフレームを正しく移動するため。
    void BmcMovieClipAdapter::moveCurrentFrame(int frameNumber)
    {
DEBUG_LOG("%p: move=%d->%d", static_cast<BmcMovieClip*>(this), _currentFrame, frameNumber);
        ccn_assert(_playFrameCount > 0);
        ccn_assert(0<=_playStartFrame && _playStartFrame < _timeline.frames().size());
        if (ccn_wcond(frameNumber < 0)) {
            return;
        }
        if (frameNumber > _currentFrame) {
            int callCount = frameNumber - _currentFrame;
            for (int i = 0; i < callCount; ++i) {
                nextFrame(true);
            }
        }else if (_currentFrame > frameNumber) {
            int callCount = _currentFrame - frameNumber;
            for (int i = 0; i < callCount; ++i) {
                prevFrame(true);
            }
        }
    }
    void BmcMovieClipAdapter::processActionScriptByLabels()
    {
        const bmc::BmcFrame& frame = _timeline.frames(_currentFrame);
        if (frame.has_frame_label_index() == false) {
            return;
        }
        const std::string& label = _timeline.frame_label_table(frame.frame_label_index());
        if (label.empty()) {
            return;
        }
DEBUG_LOG("%p: label=%s frame=%d", static_cast<BmcMovieClip*>(this), label.c_str(), _currentFrame);

        //reserved directive (goto, stop)
        static const std::string atStop = "@stop";
        static const std::string atGoto = "@goto_";
        if (label == atStop) {
//ccn_dlog("@stop called.")
            setPlayFrameRange(_currentFrame, 1);
            setLoop(false);
            return;
        }
        if (label.find(atGoto) != std::string::npos) {
///ccn_dlog("@goto called. goto %s", gotoLabel.c_str());
            std::string gotoLabel = label.substr(atGoto.size());
            gotoAndPlayToNextLabel(gotoLabel.c_str());
            return;
        }

        //custom callback
        if (_labelCallback.find(label) != _labelCallback.end()) {
            _labelCallback[label](this);
            return;
        }
    }

    ///////////////////////////////////////////////////////////////////////////

    Size BmcMovieClipAdapter::setupContentSize()
    {
        if (asNode()->getChildren().empty()) {
            updateFrameForView();
            if (asNode()->getChildren().empty()) {
                return asNode()->getContentSize();
            }
        }
        Rect rect;
        auto childrenCount = asNode()->getChildrenCount();
        for (int i = 0; i < childrenCount; ++i) {
            Node* child = static_cast<Node*>(asNode()->getChildren().at(i));
            if (i == 0) {
                rect = child->getBoundingBox();
                continue;
            }
            Rect cr = child->getBoundingBox();

            rect.origin.x = std::min(cr.getMinX(), rect.getMinX());
            rect.origin.y = std::min(cr.getMinY(), rect.getMinY());
            if (rect.getMaxX() < cr.getMaxX()) {
                rect.size.width = cr.getMaxX() - rect.origin.x;
            }
            if (rect.getMaxY() < cr.getMaxY()) {
                rect.size.height = cr.getMaxY() - rect.origin.y;
            }
        }
        asNode()->setContentSize(rect.size);
        return rect.size;
    }

    Rect BmcMovieClipAdapter::getCurrentFrameBoundingBox() const
    {
        if (asNode()->getChildrenCount() == 0) {
            return Rect::ZERO;
        }
        int childrenCount = static_cast<int>(asNode()->getChildrenCount());
        float minX = 0, minY = 0, maxX = 0, maxY = 0;
        bool first = true;
        for (int i = 0; i < childrenCount; ++i) {
            auto child = static_cast<Node*>(asNode()->getChildren().at(i));
            if (child->isVisible() == false) {
                continue;
            }
            BmcSymbol* asSymbol = dynamic_cast<BmcSymbol*>(child);
            if (first) {
                auto cr = asSymbol? asSymbol->getCurrentFrameBoundingBox() : child->getBoundingBox();
                if (cr.size.width == 0 && cr.size.height == 0) {
                    continue;
                }
                first = false;
                minX = cr.getMinX();
                minY = cr.getMinY();
                maxX = cr.getMaxX();
                maxY = cr.getMaxY();
                continue;
            }
            auto cr = asSymbol? asSymbol->getCurrentFrameBoundingBox() : child->getBoundingBox();
            if (cr.size.width == 0 && cr.size.height == 0) {
                continue;
            }
            if (cr.getMinX() < minX) {
                minX = cr.getMinX();
            }
            if (cr.getMinY() < minY) {
                minY = cr.getMinY();
            }
            if (maxX < cr.getMaxX()) {
                maxX = cr.getMaxX();
            }
            if (maxY < cr.getMaxY()) {
                maxY = cr.getMaxY();
            }
        }
        if (first) {
            return Rect::ZERO;
        }
        auto size = Size(maxX - minX, maxY - minY);
        auto rect = Rect(minX, minY, size.width, size.height);
        rect =  RectApplyAffineTransform(rect, asNode()->nodeToParentTransform());
        return rect;
    }

    void BmcMovieClipAdapter::setBlendMode(cocone::bmc::BmcBlendMode blendMode)
    {
        _blendMode = blendMode;
        if (_blendMode == bmc::BmcBlendMode_Add) {
            for (size_t i = 0; i < _instances.size(); ++i) {
                if (_instances[i] == nullptr) { continue; }
                _instances[i]->setBlendMode(_blendMode);
            }
        }
    }
    void BmcMovieClipAdapter::addLibrary(BmcLibrary& library)
    {
        for (int i = 0; i < _timeline.instance_table_size(); ++i) {
            const bmc::BmcInstance& instanceInfo = _timeline.instance_table(i);

            if (instanceInfo.instance_name().find("$$") != std::string::npos) {
                BmcSymbol::Ptr symbol = library.getSymbol(instanceInfo.instance_name());
                overwriteInstances(instanceInfo.instance_name(), symbol);
            }
        }
    }

}// cocone
