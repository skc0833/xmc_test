//
// BmcClockGenerator.cpp

#include <cocos2d.h>

#include <cocone/bmc/BmcClockGenerator.h>

#if 0
#include <cocone/foundation/Log.h>
#define DEBUG_LOG(...) { cocone::logOutput(false, cocone::LogPrefix_Debug,__func__,__FILE__,__LINE__,__VA_ARGS__); }
#else
#define DEBUG_LOG(...) ((void)0)
#endif

USING_NS_CC;

namespace cocone {
  
BmcClockGenerator::BmcClockGenerator()
  : _scheduled(false), _stock_time(.0f)
{
  DEBUG_LOG("++ %p", this);
}

BmcClockGenerator::~BmcClockGenerator() {
  DEBUG_LOG("%p", this);
  Director::getInstance()->getScheduler()->unscheduleUpdate(this);
  DEBUG_LOG("unscheduled: %p", this);
}

BmcClockGenerator::Ptr BmcClockGenerator::create()
{
  return new BmcClockGenerator();
}

float BmcClockGenerator::getSpeedScale() const
{
  return _speed_scale;
}

void BmcClockGenerator::setSpeedScale(float speedScale)
{
  _speed_scale = speedScale;
}

float BmcClockGenerator::getOneFrameDelta() const
{
  return _one_frame_delta;
}

void BmcClockGenerator::setOneFrameDelta(float oneFrameDelta)
{
  _one_frame_delta = oneFrameDelta;
}

void BmcClockGenerator::update(float delta)
{
  _stock_time += delta * _speed_scale;

  static int frame = 0;
  
  while (_stock_time > _one_frame_delta) {
  DEBUG_LOG("******************************************************************************************");
  DEBUG_LOG("%d: stockTime=%.5f delta=%.5f frameDelta=%.5f frame=%.0f",
    frame, _stock_time, delta, _one_frame_delta, floor(_stock_time / _one_frame_delta));
    _stock_time -= _one_frame_delta;
    notify_listeners();
    frame++;
  }
}

void BmcClockGenerator::notify_listeners()
{
  // ループ中に発生した unregisterListener を考慮して次のフレームで登録解除する
  for (auto& pair: _unregister_reservation) {
    auto& mc = pair.second;
    if (_listeners.find(mc.get()) != end(_listeners)) {
      _listeners.erase(mc.get());
    }
  }
  _unregister_reservation.clear();
  if (_scheduled && _listeners.size() == 0) {
    Director::getInstance()->getScheduler()->unscheduleUpdate(this);
    _scheduled = false;
  }
  for (auto& pair: _listeners) {
    auto& mc = pair.second;
    if (mc) {
      mc->processFrame();
    }
  }
}

void BmcClockGenerator::registerListener(const BmcMovieClip::Ptr& mc)
{
  _listeners[mc.get()] = mc;
  if (!_scheduled && _listeners.size() > 0) {
    Director::getInstance()->getScheduler()->scheduleUpdate(
      this, /* priority = */ 0, /* paused = */ false);
    _scheduled = true;
  }
}

void BmcClockGenerator::unregisterListener(const BmcMovieClip::Ptr& mc)
{
  _unregister_reservation[mc.get()] = mc;
}

void BmcClockGenerator::unregisterAllListeners()
{
  _unregister_reservation = _listeners;
}

}
