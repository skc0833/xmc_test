﻿//
//  BmcLibrary.h
//  PocketColony
//
//  Created by 洪 起萬 on 2014/05/30.
//
//

#pragma once
#include <cocone/bmc/BmcLibrary.h>

namespace cocone
{
    class BmcLibraryImpl : public BmcLibrary
    {
    protected:
        std::map<std::string, BmcSymbol::Ptr> _symbolTable;
        
        BmcLibraryImpl();
        virtual ~BmcLibraryImpl();
    public:
        CCN_SHARABLEPTR_DEFINE(BmcLibrary);
        
        virtual void loadLibraryByName(const std::string& name) = 0;
        virtual void loadLibraryByName(const std::vector<std::string>& names);
        virtual void loadLibrary(const std::string& fullPath);
        virtual void loadLibrary(const std::vector<std::string>& fullPath);
        
        virtual BmcSymbol::Ptr getSymbol(const std::string &referenced_key);
        virtual void iterateBySymbol(std::function<void(const std::string&, BmcSymbol::Ptr)> f);
    };
}
