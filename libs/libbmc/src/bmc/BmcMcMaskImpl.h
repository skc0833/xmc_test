//
//  BmcMCMaskImpl.h
//
//  Created by tanaka on 2016/04/21.
//  Copyright © 2016年 cocone. All rights reserved.
//
#pragma once
#include "BmcMovieClipImpl.h"

namespace cocone
{

    class BmcMCMaskImpl : public BmcMovieClipImpl
    {
    public://////////////////////////////////////////////////////////////////////
        BmcMCMaskImpl();
        virtual ~BmcMCMaskImpl();
        BmcMovieClipAdapter* callNewAndInit() const override ;
        BmcSymbol::Ptr clone() const override;
        virtual void init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override;
        virtual void visit(Renderer* renderer, const Mat4& transform, uint32_t flags) override;
    private:
        cocos2d::CustomCommand _beforeVisitCmdScissor, _afterVisitCmdScissor;
        BmcSymbol* _range_mask;
    };
}
