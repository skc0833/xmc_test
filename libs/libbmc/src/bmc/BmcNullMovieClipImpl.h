// BmcSymbol.cpp.cpp
//
#pragma once

#include "BmcSymbolDefaultImpl.h"
#include <cocone/bmc/BmcButton.h>

namespace cocone
{
    /// アクセス不可を起こさないための NullObject
    class BmcNullMovieClipImpl : public BmcMovieClip, public BmcButton, public BmcSymbolDefaultImpl, public ZeroRefCount<cocos2d::Node>
    {
        cocone::bmc::BmcTimeline _timeline;
    public:
        // TODO impl assert
        BmcNullMovieClipImpl(){
//          ccn_dlog("");
        }
//        virtual ~BmcNullMovieClipImpl() = default;
        virtual ~BmcNullMovieClipImpl()
        {
//          ccn_dlog("");
        }

        CCN_SHARABLEPTR_DEFINE(BmcNullMovieClipImpl);
        CCN_DECLARE_ASNODE_IMPL();

        virtual bool operator == (const std::nullptr_t) const override { return true; }
        virtual bool operator != (const std::nullptr_t) const override { return false; }
        virtual explicit operator bool() const override { return false; }

        void* query(IIDType iid) override {
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            } else if (iid == BmcMovieClip::iid()) {
                return static_cast<BmcMovieClip*>(this);
            } else if (iid == BmcButton::iid()) {
                return static_cast<BmcButton*>(this);
            }
            return nullptr;
        }

        virtual void init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override
        {
            return;
        }

        virtual BmcSymbol::Ptr clone() const override
        {
            return new BmcNullMovieClipImpl();
        }

        virtual const cocone::bmc::BmcTimeline& getTimeline() const override
        {
            return _timeline;
        }

        virtual BmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const override
        {
            return clone();
        }
        virtual BmcSymbol::Ptr getInstanceOne() const override
        {
            return clone();
        }

        virtual BmcSymbol::Ptr destroyInstance(const std::string& instanceName) override
        {
            return clone();
        }

        virtual int getCurrentFrame() const override
        {
            return 0;
        }
        virtual const std::string& getCurrentLabel() const override
        {
            static const std::string kEmptyString = "";
            return kEmptyString;
        }
        virtual int getTotalFrames() const override
        {
            return 0;
        }
        virtual int getFrameByLabel(const char* label) const override
        {
            return 0;
        }
        virtual void gotoAndPlay(int frameNumber) override {}
        virtual void gotoTopAndPlayRecursively() override {}
        virtual void gotoAndPlay(int frameNumber, int count) override {}
        virtual void gotoAndPlayToNextLabel( const char* label ) override {}
        virtual void gotoAndPlayLabel( const char* labelFrom, const char* labelTo = 0 ) override {}
        virtual void gotoAndPlayToNextFrame( int frameNumber ) override {}
        virtual void gotoAndStop( int frameNumber ) override {}
        virtual void play() override {}
        virtual void stop() override {}
        virtual void stopAll() override {}
        virtual void setLoop( bool isLoop ) override {}
        virtual void setSpeedScale(float scale) override {}
        virtual float getSpeedScale() const override
        {
            return .0f;
        }
        virtual void setEventListener( std::function<void (EventType, BmcMovieClip*)> listener ) override {}
        virtual void clearEventListener() override {}
        // label callback
        virtual void setLabelCallback( const std::string& labelName, std::function<void(BmcMovieClip*)> listener ) override {}
        virtual void setLabelCallback(std::function<void(const std::string&, BmcMovieClip*)> listener) override {}
        virtual void clearLabelCallback() override {}

        virtual void nextFrame( bool child ) override {}
        virtual void prevFrame( bool child ) override {}
        virtual void updateFrameForView() override {}
        virtual void processFrame() override {}

        virtual cocos2d::Size setupContentSize() override
        {
            return cocos2d::Size::ZERO;
        }

#ifdef skc_cocos_v3
        const cocos2d::Mat4& getAdditionalTransform() const
        {
            if (_additionalTransform) return *_additionalTransform;
            else return cocos2d::Mat4::IDENTITY;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform->m, &out);
            return out;
        }
#else
        virtual const cocos2d::Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }
#endif
        virtual void enableLog(bool enabled) override {}

        // BmcButton
        virtual void setPushSoundHandler(std::function<void(void)> soundPlay) override {}
        virtual void setPushHandler( std::function<bool (BmcButton*)> handler ) override {}
        virtual void setPushingHandler( std::function<bool (BmcButton*)> handler ) override {}
        virtual void setTouchBeganHandler(std::function<bool(BmcButton*)> handler) override {}
        virtual void setEnabled( bool enabled ) override {}
        virtual void setEventBlock( bool block ) override {}
        virtual void setCheckBoxMode( bool checkbox ) override {}
        virtual bool getCheckBoxMode()const override { return false; }
        virtual void setCheck( bool check, bool playSound ) override {}
        virtual void setCheck( bool check, bool playSound , bool animated) override {}
        virtual bool getCheck()const override { return false; }
        virtual void setBarrageBlockTime(float blockSec=0.2f) override {}
        virtual float getBarrageBlockTime() const override { return .2f; }
        virtual void setTouchEnabled(bool) override {}
        virtual void setSwallowTouches( bool swallow ) override {}
        virtual void touchDownAction() override {}
        virtual void setSimultaneousPress(bool enabled) override {}
        virtual bool getSimultaneousPress() const { return false; }
    };

} // cocone

// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
