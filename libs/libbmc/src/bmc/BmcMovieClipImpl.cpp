// BmcMovieClipDefaultImpl.cpp.cpp
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
#include "BmcMovieClipImpl.h"

#include "BmcFactoryImpl.h"

//#define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
//#include <cocone/foundation/Log.h>

USING_NS_CC;

namespace cocone {

void* BmcMovieClipImpl::query(IIDType iid)
{
    if (iid == BmcMovieClip::iid()) {
        return static_cast<BmcMovieClip*>(this);
    }
    if (iid == BmcSymbol::iid()) {
        return static_cast<BmcSymbol*>(this);
    }
    return nullptr;
}

BmcMovieClipAdapter* BmcMovieClipImpl::callNewAndInit() const
{
    auto t = new BmcMovieClipImpl();
    t->ZeroRefCount<Node>::init();
    return t;
}

bool BmcMovieClipImpl::callNodeInit()
{
    return ZeroRefCount<Node>::init();
}

void BmcMovieClipImpl::update(float dt)
{
    nodeFuncUpdate(dt);
}

void BmcMovieClipImpl::onExit()
{
    nodeFuncOnExit();
    ZeroRefCount<Node>::onExit();
}

void BmcMovieClipImpl::setOpacity(GLubyte opacity)
{
    ColorTransform color = getAdditionalColorTransform() ;
    color.mutableMultiplier()[3] = float(opacity)/255.f;
    setAdditionalColorTransform(color);

    for (auto child : _children){
        child->setOpacity(opacity);
    }
}

GLubyte BmcMovieClipImpl::getOpacity() const
{
    return getAdditionalColorTransform().multiplier()[3]*255.f;
}

#ifdef skc_cocos_v3
    const Mat4& BmcMovieClipImpl::getAdditionalTransform() const
    {
        if (_additionalTransform) return *_additionalTransform;
        else return Mat4::IDENTITY;
    }
    
    AffineTransform BmcMovieClipImpl::getAdditionalAffineTransform() const
    {
        AffineTransform out;
        GLToCGAffine(this->_additionalTransform->m, &out);
        return out;
    }
#else
const Mat4& BmcMovieClipImpl::getAdditionalTransform() const
{
    return _additionalTransform;
}

AffineTransform BmcMovieClipImpl::getAdditionalAffineTransform() const
{
    AffineTransform out;
    GLToCGAffine(this->_additionalTransform.m, &out);
    return out;
}
#endif

void BmcMovieClipImpl::setAdditionalColorTransform(const ColorTransform& additionalColorTransform)
{
    if (_additionalColorTransform == additionalColorTransform) {
        return;
    }
    _additionalColorTransform = additionalColorTransform;

    // NOTE: BmcSymbol以外のchildにcolor,opacityを適用する
    for (auto child : _children) {
        applyAdditionalColorTransformToChild(child);
    }
}

void BmcMovieClipImpl::addChild(Node *child, int localZOrder, int tag)
{
  Node::addChild(child, localZOrder, tag);

  applyAdditionalColorTransformToChild(child);
}

void BmcMovieClipImpl::addChild(Node* child, int localZOrder, const std::string &name)
{
  Node::addChild(child, localZOrder, name);

  applyAdditionalColorTransformToChild(child);
}

void BmcMovieClipImpl::addChild(Node *child, int zOrder)
{
  Node::addChild(child, zOrder);

  applyAdditionalColorTransformToChild(child);
}

void BmcMovieClipImpl::addChild(Node *child)
{
  Node::addChild(child);

  applyAdditionalColorTransformToChild(child);
}

// NOTE: BmcMovieClipにBmcSymbol以外のNodeをasNode()->addChildした時にその時点のcolor,opacityを適用する
void BmcMovieClipImpl::applyAdditionalColorTransformToChild(Node* child) const
{
  if (areYouSymbol(child)) {
    return;
  }

  // offset(着色) が設定されている場合はそちらを優先
  const float* mul;
  int opacity;
  if (_additionalColorTransform.offset()[0]) {
    mul = _additionalColorTransform.offset();
    opacity = 255 * (1.f - mul[3]);
  } else {
    mul = _additionalColorTransform.multiplier();
    opacity = 255 * mul[3];
  }
  if (auto lbl = dynamic_cast<Label*>(child)) {
    Color4B color;
    if (mul[0] == 1.f && mul[1] == 1.f && mul[2] == 1.f) {
      color = lbl->getTextColor();
    } else {
      color.r = 255 * mul[0];
      color.g = 255 * mul[1];
      color.b = 255 * mul[2];
    }
    color.a = opacity;
    lbl->setTextColor(color);
  } else {
    child->setOpacity(opacity);

    Color3B color;
    color.r = 255 * mul[0];
    color.g = 255 * mul[1];
    color.b = 255 * mul[2];
    child->setColor(color);
  }
}

BMC_FACTORY_CREATE_FUNC_IMPL(BmcMovieClip);

} // cocone
