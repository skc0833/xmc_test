//
//  BmcMCMaskImpl.cpp
//
//  Created by tanaka on 2016/04/21.
//  Copyright © 2016年 cocone. All rights reserved.
//

#include "BmcMCMaskImpl.h"

namespace cocone
{
    BmcMCMaskImpl::BmcMCMaskImpl()
    {

    }
    BmcMCMaskImpl::~BmcMCMaskImpl()
    {
    }
    BmcMovieClipAdapter* BmcMCMaskImpl::callNewAndInit() const
    {
        auto t = new BmcMCMaskImpl();
        t->ZeroRefCount<Node>::init();
        return t;
    }
    BmcSymbol::Ptr BmcMCMaskImpl::clone() const
    {
        BmcSymbol::Ptr ret = BmcMovieClipImpl::clone();

        BmcMCMaskImpl* _this = static_cast<BmcMCMaskImpl*>(ret.get());
        _this->_range_mask = _this->getInstanceBeginWith("$range_mask");
        auto temp = _this->getInstanceBeginWith("$range_mask$transparent");
        if (temp && _range_mask == temp) {
            _range_mask->asNode()->setOpacity(0);
        }

        return ret;
    }
    void BmcMCMaskImpl::init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline)
    {
        BmcMovieClipAdapter::init(source, bmcTimeline);
        _range_mask = this->getInstanceBeginWith("$range_mask");
        auto temp = this->getInstanceBeginWith("$range_mask$transparent");
        if (temp && _range_mask == temp) {
            _range_mask->asNode()->setOpacity(0);
        }
    }

    void BmcMCMaskImpl::visit(Renderer* renderer, const Mat4& transform, uint32_t flags)
    {
      // シザー開始
      _beforeVisitCmdScissor.init(_globalZOrder);
      _beforeVisitCmdScissor.func = [this](){
        glEnable(GL_SCISSOR_TEST);

        Rect clippingRect = _range_mask->getCurrentFrameBoundingBox();
        clippingRect = RectApplyAffineTransform(clippingRect, getNodeToWorldAffineTransform());

        _director->getOpenGLView()->setScissorInPoints(
          clippingRect.getMinX(), clippingRect.getMinY(),
          clippingRect.size.width, clippingRect.size.height
        );
      };
      renderer->addCommand(&_beforeVisitCmdScissor);

      BmcMovieClipImpl::visit(renderer, transform, flags);

      // シザー終了
      _afterVisitCmdScissor.init(_globalZOrder);
      _afterVisitCmdScissor.func = [](){ glDisable(GL_SCISSOR_TEST); };
      renderer->addCommand(&_afterVisitCmdScissor);
    }
}
