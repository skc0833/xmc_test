﻿// BmcSoundPlayerDefaultImpl.cpp
//

#include <cocone/bmc/BmcSoundPlayer.h>

#include <cocone/bmc/protocol/BmcFile.pb.h>

#include "BmcFactoryImpl.h"

// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
//#include <cocone/foundation/Log.h>

namespace cocone {


    class BmcSoundPlayerImpl : public BmcSoundPlayer, public ZeroRefCount<cocos2d::Ref>
    {
        std::function<void(const std::string& soundFilePath)> _playFunction;
        ::google_public::protobuf::RepeatedPtrField<std::string> _soundTable;
    public:
        CCN_DECLARE_ASREF_IMPL();

        virtual bool operator == (const std::nullptr_t) const override { return false; }
        virtual bool operator != (const std::nullptr_t) const override { return true; }
        virtual explicit operator bool() const override { return true; }

        BmcSoundPlayerImpl()
        {

        }

        virtual ~BmcSoundPlayerImpl()
        {

        }
        virtual void initSoundTable(const ::google_public::protobuf::RepeatedPtrField<std::string>& soundTable) override
        {
            _soundTable = soundTable;
        }
        virtual void initFunctions(
            std::function<void(const std::string& soundFilePath)> preloadFunction
            , std::function<void(const std::string& soundFilePath)> playFunction
        ) override
        {
            _playFunction = playFunction;
            for (int i = 0; i < _soundTable.size(); ++i){
                preloadFunction(_soundTable.Get(i));
            }
        }
        virtual void playEffect(int index) override
        {
            _playFunction(_soundTable.Get(index));
        }
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcSoundPlayer);
} // cocone
