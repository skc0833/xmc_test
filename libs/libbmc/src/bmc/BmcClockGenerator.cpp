//
// BmcClockGenerator.cpp

#include <cocos2d.h>

#include <cocone/bmc/BmcClockGenerator.h>

#include "BmcMovieClipAdapter.h"

#if 0
#include <cocone/foundation/Log.h>
#define DEBUG_LOG(...) { cocone::logOutput(false, cocone::LogPrefix_Debug,__func__,__FILE__,__LINE__,__VA_ARGS__); }
#else
#define DEBUG_LOG(...) ((void)0)
#endif

USING_NS_CC;

namespace cocone {

BmcClockGenerator::BmcClockGenerator()
  : _scheduled(false), _stock_time(.0f)
{
}

BmcClockGenerator::~BmcClockGenerator()
{
  DEBUG_LOG("%p", this);
  Director::getInstance()->getScheduler()->unscheduleUpdate(this);
  DEBUG_LOG("unscheduled: %p", this);
}

BmcClockGenerator::Ptr BmcClockGenerator::create()
{
  return new BmcClockGenerator();
}

float BmcClockGenerator::getSpeedScale() const
{
  return _speed_scale;
}

void BmcClockGenerator::setSpeedScale(float speedScale)
{
  _speed_scale = speedScale;
}

float BmcClockGenerator::getOneFrameDelta() const
{
  return _one_frame_delta;
}

void BmcClockGenerator::setOneFrameDelta(float oneFrameDelta)
{
  _one_frame_delta = oneFrameDelta;
}

void BmcClockGenerator::update(float delta)
{
  // ループ中に発生した unregisterListener を考慮して次のフレームで登録解除する
  for (auto pair: _unregister_reservation) {
    auto mca = pair.first;
    _listeners.erase(
      std::remove(begin(_listeners), end(_listeners), mca), end(_listeners)
    );
    DEBUG_LOG("removed: %p", mca);
  }
  _unregister_reservation.clear();

  if (_listeners.size() == 0) {
    Director::getInstance()->getScheduler()->unscheduleUpdate(this);
    _scheduled = false;
    DEBUG_LOG("unscheduled: %p", this);
    return;
  }

  _stock_time += delta * _speed_scale;

  static int frame = 0;

  DEBUG_LOG("%p:%d", this, _listeners.size());

  retain();
  while (_stock_time > _one_frame_delta) {
  DEBUG_LOG("******************************************************************************************");
  DEBUG_LOG("%d: stockTime=%.5f delta=%.5f frameDelta=%.5f frame=%.0f",
    frame, _stock_time, delta, _one_frame_delta, floor(_stock_time / _one_frame_delta));
    _stock_time -= _one_frame_delta;
    notify_listeners();
    frame++;
  }
  release();
}

void BmcClockGenerator::notify_listeners()
{
  // NOTE processFrame 内で別の mc を registerListener した場合を考慮
  auto copy_listeners = _listeners;
  for (auto mca: copy_listeners) {
    // NOTE processFrame 内で別の mc を unregisterListener した場合を考慮
      if (_unregister_reservation.find(mca) != end(_unregister_reservation)) {
      continue;
    }
    mca->processFrame();
  }
}

void BmcClockGenerator::registerListener(BmcMovieClipAdapter* mca)
{
  if (mca == nullptr) {
    return;
  }
  if (std::find(begin(_listeners), end(_listeners), mca) == end(_listeners)) {
    _listeners.push_back(mca);
    DEBUG_LOG("added: %p", mca);
  }
  _unregister_reservation.erase(mca);

  if (!_scheduled && _listeners.size() > 0) {
    Director::getInstance()->getScheduler()->scheduleUpdate(
      this, /* priority = */ 1, /* paused = */ false);
    _scheduled = true;
    DEBUG_LOG("scheduled: %p", this);
  }
}

void BmcClockGenerator::unregisterListener(BmcMovieClipAdapter* mca)
{
  if (_unregister_reservation.find(mca) == end(_unregister_reservation)) {
    _unregister_reservation[mca] = true;
    DEBUG_LOG("reserved: %p", mca);
  }
}

void BmcClockGenerator::unregisterAllListeners()
{
  for (auto listener: _listeners) {
    _unregister_reservation[listener] = true;
  }
}

}
