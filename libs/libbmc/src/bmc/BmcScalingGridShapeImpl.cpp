// BmcScalingGridShapeDefaultImpl.cpp
//
#include <cocone/bmc/BmcScalingGridShape.h>

#include <extensions/cocos-ext.h>
#include <cocone/bmc/BmcSymbol.h>
#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/bmc/Shaders.h>
#include <cocone/foundation/FileHelper.h>

#include "BmcFactoryImpl.h"
#include "BmcSymbolDefaultImpl.h"

// TODO additionalColorTransform

USING_NS_CC;
USING_NS_CC_EXT;

namespace cocone {

    class BmcScalingGridShapeImpl :  public ZeroRefCount<ui::Scale9Sprite>, public BmcScalingGridShape, public BmcSymbolDefaultImpl
    {
        CCN_DECLARE_ASNODE_IMPL();
    protected:///////////////////////////////////////////////////////////////////
        void* query(IIDType iid) override {
            if (iid == BmcScalingGridShape::iid()) {
                return static_cast<BmcScalingGridShape*>(this);
            }
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

#ifdef skc_cocos_v3
        const cocos2d::Mat4& getAdditionalTransform() const
        {
            if (_additionalTransform) return *_additionalTransform;
            else return cocos2d::Mat4::IDENTITY;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform->m, &out);
            return out;
        }
#else
        virtual const Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }

        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }
#endif
        ///////////////////////////////////////////////////////////////////////////
    private:
        BmcShader::Ptr _shader;
        BmcTexturePart::Ptr _texture;
        bmc::BmcBlendMode _blendMode;
        BmcTextureReloader::Ptr _reloader;
        std::string _spriteFrameName;

    public:

        BmcScalingGridShapeImpl()
        {
            _blendMode = bmc::BmcBlendMode_Normal;
        }
        virtual ~BmcScalingGridShapeImpl()
        {
            //		ccn_dlog( "" );
            if (_reloader){
                _reloader->unregist(this);
            }
        }
        virtual BmcSymbol::Ptr clone() const override
        {
            BmcScalingGridShapeImpl* newP = new BmcScalingGridShapeImpl();
            if (_spriteFrameName != "") {
              newP->initWithSpriteFrameName(_shader, _texture, _spriteFrameName);
            } else {
              newP->init(_shader, _texture);
            }
            newP->_blendMode = _blendMode;
            newP->_additionalColorTransform = _additionalColorTransform;
#if ENABLE_RELOADER
            newP->_reloader = _reloader;
            _reloader->regist(newP);
#endif
            return newP;
        }
        virtual void init(BmcShader::Ptr shader, BmcTexturePart::Ptr texture) override
        {
            _shader = shader;
            _texture = texture;
            initInternal();
        }
        virtual void onEnter() override
        {
            Scale9Sprite::onEnter();
        }
        void initInternal()
        {
            auto spr = Sprite::createWithTexture(_texture->getTexture()->getCCTexture2D());
            const auto& texRect    = _texture->getRectangle();
            const auto& rect9scale = _texture->getScale9GridRectangle();
            Rect capInsets = Rect(
                rect9scale.x() - texRect.x()
                , (rect9scale.y() - texRect.y())
                , rect9scale.width() == 0 ? 1 : rect9scale.width()
                , rect9scale.height() == 0 ? 1 : rect9scale.height()
                );

            Scale9Sprite::init(spr, Rect::ZERO, capInsets);
        }
        virtual void initWithSpriteFrameName(BmcShader::Ptr shader, BmcTexturePart::Ptr texture, const std::string& spriteFrameName) override
        {
            _spriteFrameName = spriteFrameName;
            _shader = shader;
            _texture = texture;

//            setAnchorPoint(Vec2::ZERO);

            const auto& texRect    = _texture->getRectangle();
            const auto& rect9scale = _texture->getScale9GridRectangle();
            Rect capInsets = Rect(
                rect9scale.x() - texRect.x()
                , (rect9scale.y() - texRect.y())
                , rect9scale.width() == 0 ? 1 : rect9scale.width()
                , rect9scale.height() == 0 ? 1 : rect9scale.height()
                );

//            Scale9Sprite::init(spr, Rect::ZERO, capInsets);
            Scale9Sprite::initWithSpriteFrameName(_spriteFrameName, capInsets);
        }
        virtual void registReloader(BmcTextureReloader::Ptr reloader) override
        {
            _reloader = reloader;
            _reloader->regist(this);
        }
        virtual void reload() override
        {
            initInternal();
        }
        virtual Size setupContentSize() override
        {
            return _contentSize;
        }
        virtual void setBlendMode(cocone::bmc::BmcBlendMode blendMode) override
        {
            _blendMode = blendMode;
        }

        virtual void setAdditionalTransform(const Mat4& additionalTransform) override
        {
            Mat4 trans = additionalTransform;
            float xRate = additionalTransform.m[0];
            float yRate = additionalTransform.m[5];
            trans.m[0] = 1.f;
            trans.m[5] = 1.f;

            Scale9Sprite::setContentSize({
              _texture->getRectangle().width()  * xRate,
              _texture->getRectangle().height() * yRate
            });
            Scale9Sprite::setAdditionalTransform(&trans);
        }

        void setAdditionalTransform(const AffineTransform& additionalTransform) override
        {
            Mat4 tmp;
            CGAffineToGL(additionalTransform, tmp.m);
            setAdditionalTransform(tmp);

//          AffineTransform affine;
//          GLToCGAffine(getNodeToParentTransform().m, &affine);
//
//            return BmcSymbolDefaultImpl::setAdditionalTransform(affine);
        }

        virtual Rect getBoundingBox() const override
        {
            return RectApplyTransform(
                { _texture->getOffset().x, _texture->getOffset().y, _contentSize.width, _contentSize.height },
                getNodeToParentTransform()
            );
        }

#if 1 //skc test
      virtual bool updateWithSprite(Sprite* sprite,
                                    const Rect& textureRect,
                                    bool rotated,
                                    const Vec2 &offset,
                                    const Size &originalSize,
                                    const Rect& capInsets) override {
        const auto path = Director::getInstance()->getTextureCache()->getTextureFilePath(sprite->getTexture());
        const auto ext = filehelper::extname(path);
        // pvrz or ktxz の場合は右半分マスクテクスチャとして扱う
        bool useAlphaMaskShader = false;
        if (ext == "pvrz" || ext == "ktxz") {
          useAlphaMaskShader = true;
          ccn_assert(false);
        }

        return Scale9Sprite::updateWithSprite(sprite, textureRect, rotated, offset, originalSize, capInsets);
      }
#else
        // NOTE setState の処理を乗っ取りたかった
        virtual bool updateWithSprite(Sprite* sprite,
                                      const Rect& textureRect,
                                      bool rotated,
                                      const Vec2 &offset,
                                      const Size &originalSize,
                                      const Rect& capInsets) override
        {
            const auto path = Director::getInstance()->getTextureCache()->getTextureFilePath(sprite->getTexture());
            const auto ext = filehelper::extname(path);
            // pvrz or ktxz の場合は右半分マスクテクスチャとして扱う
            bool useAlphaMaskShader = false;
            if (ext == "pvrz" || ext == "ktxz") {
              useAlphaMaskShader = true;
            }

            GLubyte opacity = getOpacity();
            Color3B color = getColor();

            // Release old sprites
            this->cleanupSlicedSprites();
            _protectedChildren.clear();

            updateBlendFunc(sprite?sprite->getTexture():nullptr);

            if(nullptr != sprite)
            {
                if (nullptr == sprite->getSpriteFrame())
                {
                    return false;
                }

                if (nullptr == _scale9Image)
                {
                    _scale9Image = sprite;
                    _scale9Image->retain();
                }
                else
                {
                    _scale9Image->setSpriteFrame(sprite->getSpriteFrame());
                }
            }

            if (!_scale9Image)
            {
                return false;
            }

            SpriteFrame *spriteFrame = _scale9Image->getSpriteFrame();

            if (!spriteFrame)
            {
                return false;
            }

            Rect rect(textureRect);
            Size size(originalSize);

            _capInsets = capInsets;

            // If there is no given rect
            if ( rect.equals(Rect::ZERO) )
            {
                // Get the texture size as original
                Size textureSize = _scale9Image->getTexture()->getContentSize();

                rect = Rect(0, 0, textureSize.width, textureSize.height);
            }

            if( size.equals(Size::ZERO) )
            {
                size = rect.size;
            }

            // Set the given rect's size as original size
            _spriteRect = rect;
            _offset = offset;
            _spriteFrameRotated = rotated;
            _originalSize = size;
            _preferredSize = size;

            _capInsetsInternal = capInsets;

            if (_scale9Enabled)
            {
                this->createSlicedSprites();
            }

            applyBlendFunc();
            if (useAlphaMaskShader) {
                this->setStateAlphaMaskTexture(_brightState);
            } else {
                this->setState(_brightState);
            }
            if(this->_isPatch9)
            {
                size.width = size.width - 2;
                size.height = size.height - 2;
            }
            this->setContentSize(size);

            if (_spritesGenerated)
            {
                // Restore color and opacity
                this->setOpacity(opacity);
                this->setColor(color);
            }
            _spritesGenerated = true;

            return true;
        }

        void setStateAlphaMaskTexture(cocos2d::ui::Scale9Sprite::State state)
        {
            GLProgramState *glState = nullptr;
            switch (state)
            {
            case State::NORMAL:
            {
                glState = GLProgramState::getOrCreateWithGLProgramName(kCCShader_PositionTextureColor_AlphaMask_noMVP);
            }
            break;
            case State::GRAY:
            {
                glState = GLProgramState::getOrCreateWithGLProgramName(kCCShader_PositionTextureColor_GrayScale_AlphaMask_noMVP);
            }
            default:
                break;
            }

            if (nullptr != _scale9Image)
            {
                _scale9Image->setGLProgramState(glState);
            }

            if (_scale9Enabled)
            {
                for (auto& sp : _protectedChildren)
                {
                    sp->setGLProgramState(glState);
                }
            }
            _brightState = state;
        }
#endif
    };

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcScalingGridShape);

} // cocone
