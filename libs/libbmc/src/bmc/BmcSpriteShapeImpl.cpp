// BmcSpriteShapeDefaultImpl.cpp
//
// cocos 側のシェーダを使って描画する
//

#include <cocone/bmc/BmcShape.h>

#include <cocone/bmc/BmcShader.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/bmc/Shaders.h>
#include <cocone/foundation/StringHelper.h>
#include <cocone/foundation/FileHelper.h>

#include "BmcSymbolDefaultImpl.h"
#include "BmcFactoryImpl.h"

namespace cocone {

    using namespace std;
    namespace sh = cocone::stringhelper;
    USING_NS_CC;

    class BmcSpriteShapeImpl : public ZeroRefCount<Sprite>, public BmcShape, public BmcSymbolDefaultImpl
    {
        CCN_DECLARE_ASNODE_IMPL();

    protected:///////////////////////////////////////////////////////////////////
        virtual void* query(IIDType iid) override
        {
            if (iid == BmcShape::iid()) {
                return static_cast<BmcShape*>(this);
            } else if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

#ifdef skc_cocos_v3
        const Mat4& getAdditionalTransform() const
        {
            if (_additionalTransform) return *_additionalTransform;
            else return Mat4::IDENTITY;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform->m, &out);
            return out;
        }
#else
        virtual const cocos2d::Mat4& getAdditionalTransform() const override
        {
            Mat4 trans = _additionalTransform;
            return std::move(trans);
        }

        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(getAdditionalTransform().m, &out);
            return out;
        }
#endif
        ///////////////////////////////////////////////////////////////////////////
    private:
        BmcShader::Ptr _shader;
        BmcTexturePart::Ptr _texture;
        bmc::BmcBlendMode _blendMode;
        CustomCommand _customCommand;

        bool _visible_input;
        GLubyte _opacity_input;
        Color3B _color_input;
        std::string _spriteFrameName;

    public:

        BmcSpriteShapeImpl() :
            _visible_input(true),
            _opacity_input(255),
            _color_input(Color3B::WHITE)
        {
            _blendMode = bmc::BmcBlendMode_Normal;
        }
        virtual ~BmcSpriteShapeImpl()
        {
            //		ccn_dlog( "" );
        }
        virtual BmcSymbol::Ptr clone() const override
        {
            BmcSpriteShapeImpl* newP = new BmcSpriteShapeImpl();
            if (_spriteFrameName != "") {
              newP->initWithSpriteFrameName(_shader, _texture, _spriteFrameName);
            } else {
              newP->init(_shader, _texture);
            }
            newP->_additionalColorTransform = _additionalColorTransform;
            newP->_blendMode = _blendMode;

            return static_cast<BmcSymbol*>(newP);
        }

        virtual void init(BmcShader::Ptr shader, BmcTexturePart::Ptr texture) override
        {
            Sprite::initWithTexture(texture->getTexture()->getCCTexture2D());
            //debugDraw(true);
            _shader = shader;
            _texture = texture;
            _spriteFrameName = "";

            setAnchorPoint(Vec2::ZERO);
        }
        virtual void initWithSpriteFrameName(BmcShader::Ptr shader, BmcTexturePart::Ptr texture, const std::string& spriteFrameName) override
        {
            _spriteFrameName = spriteFrameName;
            Sprite::initWithSpriteFrameName(_spriteFrameName);
            _shader = shader;
            _texture = texture;
            const auto path = Director::getInstance()->getTextureCache()->getTextureFilePath(getTexture());
            const auto ext = filehelper::extname(path);
            // とりあえず pvrz or ktxz の場合は右半分マスクテクスチャとして扱う
            if (ext == "pvrz" || ext == "ktxz") {
                setGLProgram(GLProgramCache::getInstance()->getGLProgram(kCCShader_PositionTextureColor_AlphaMask_noMVP));
            }

            setAnchorPoint(Vec2::ZERO);
        }
        virtual Size setupContentSize() override
        {
            // Sprite がやってくれるのでなにもしない
            return _contentSize;
        }
        virtual void setBlendMode(cocone::bmc::BmcBlendMode blendMode) override
        {
            _blendMode = blendMode;
        }

        virtual bool isVisible() const override
        {
            return _visible_input;
        }

        virtual void setVisible(bool visible) override
        {
            _visible_input = visible;
            Sprite::setVisible(visible);
        }

        virtual GLubyte getOpacity() const override
        {
            return _opacity_input;
        }

        virtual void setOpacity(GLubyte opacity) override
        {
            _opacity_input = opacity;
            affectColorTransform();
        }

        virtual const Color3B& getColor() const override
        {
            return _color_input;
        }

        virtual void setColor(const Color3B& color) override
        {
            _color_input = color;
            affectColorTransform();
        }

        virtual void setAdditionalColorTransform(const ColorTransform& additionalColorTransform) override
        {
            if (_additionalColorTransform == additionalColorTransform) {
                return;
            }
            _additionalColorTransform = additionalColorTransform;

            affectColorTransform();
        }

        virtual Rect getBoundingBox() const override
        {
            return RectApplyTransform(
                { _texture->getOffset().x, _texture->getOffset().y, _contentSize.width, _contentSize.height },
                getNodeToParentTransform()
            );
        }

        virtual void visit(Renderer* renderer, const Mat4 &parentTransform, uint32_t parentFlags) override
        {
            affectColorTransform();
            Sprite::visit(renderer, parentTransform, parentFlags);
        }

        virtual void draw(Renderer *renderer, const Mat4 &transform, uint32_t flags) override
        {
            Mat4 offset = Mat4::IDENTITY;
            offset.m[12] = _texture->getOffset().x;
            offset.m[13] = _texture->getOffset().y;
            Sprite::draw(renderer, transform * offset, flags);
        }
    private:
        void affectColorTransform()
        {
            ColorTransform ct = getAscendColorTransform();
            const auto& mul = ct.multiplier();
            const auto& ofs = ct.offset();

            GLubyte opacity = _opacity_input * mul[3] + 255 * ofs[3];
            bool visible = opacity > 0;
            if (visible && _visible_input) {
                Sprite::setVisible(true);
            } else if (_visible) {
                Sprite::setVisible(false);
                return;
            }

            // TODO color offset -> shader でやる必要がある
            // TODO 変更チェック
            Sprite::setColor({
                (GLubyte)(_color_input.r * mul[0]),
                (GLubyte)(_color_input.g * mul[1]),
                (GLubyte)(_color_input.b * mul[2]),
            });
            Sprite::setOpacity(opacity);
        }
    };

    BmcShape::Ptr BmcFactory::createBmcSpriteShape()
    {
        return new BmcSpriteShapeImpl();
    }
} // cocone
