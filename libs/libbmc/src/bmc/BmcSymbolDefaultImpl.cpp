﻿// BmcSymbol.cpp.cpp
//

#include "BmcSymbolDefaultImpl.h"
#include "BmcNullSymbolImpl.h"

USING_NS_CC;

namespace {
  bool isCascadeVisible(Node* node)
  {
      while (node) {
          if (!node->isVisible()) {
              return false;
          }
          node = node->getParent();
      }
      return true;
  }
};

namespace cocone {

    BmcSymbol::Ptr BmcSymbolDefaultImpl::getInstanceImpl(const std::string& instanceName) const
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::getInstanceOne() const
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::getInstanceBeginWith(const std::string& instanceNameBegin)
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::destroyInstance(const std::string& instanceName)
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::destroyInstance(BmcSymbol::Ptr instance)
    {
        return getNullObject();
    }
    BmcSymbol::Ptr BmcSymbolDefaultImpl::getNullObject() const
    {
        return new BmcNullSymbolImpl();
    }

    cocos2d::Color4F BmcSymbolDefaultImpl::getColorMultiply()const
    {
        const float* m = _additionalColorTransform.multiplier();
        return cocos2d::Color4F( m[0], m[1], m[2], m[3] );
    }
    void BmcSymbolDefaultImpl::setColorMultiply(const cocos2d::Color4F& c)
    {
        float* m = _additionalColorTransform.mutableMultiplier();
        m[0] = c.r;
        m[1] = c.g;
        m[2] = c.b;
        m[3] = c.a;
    }
    void BmcSymbolDefaultImpl::setColorMultiply(float r, float g, float b)
    {
        float* m = _additionalColorTransform.mutableMultiplier();
        m[0] = r;
        m[1] = g;
        m[2] = b;
    }
    cocos2d::Color4F BmcSymbolDefaultImpl::getColorAdd()const
    {
        const float* m = _additionalColorTransform.offset();
        return cocos2d::Color4F( m[0], m[1], m[2], m[3] );
    }
    void BmcSymbolDefaultImpl::setColorAdd(const cocos2d::Color4F& c)
    {
        float* m = _additionalColorTransform.mutableOffset();
        m[0] = c.r;
        m[1] = c.g;
        m[2] = c.b;
        m[3] = c.a;
    }
    void BmcSymbolDefaultImpl::setColorAdd(float r, float g, float b, float a)
    {
        float* m = _additionalColorTransform.mutableOffset();
        m[0] = r;
        m[1] = g;
        m[2] = b;
        m[3] = a;
    }

    void BmcSymbolDefaultImpl::setTouchBlockEnabled(bool enabled)
    {
      if (enabled) {
        if (_touchBlockListener != nullptr) {
          return;
        }
        _touchBlockListener = cocos2d::EventListenerTouchOneByOne::create();
        _touchBlockListener->setSwallowTouches(true);

        _touchBlockListener->onTouchBegan = [=](cocos2d::Touch* touch, cocos2d::Event* event) {
          auto node = asNode();
          if (node == nullptr || !isCascadeVisible(node)) {
            return false;
          }

          auto loc = touch->getLocation();
          auto bb = getCurrentFrameBoundingBox();
          bb.origin = node->getParent()->convertToWorldSpace(bb.origin);
          //ccn_dlog("%f,%f", loc.x, loc.y);
          //ccn_dlog("%f,%f:%f,%f", bb.origin.x, bb.origin.y, bb.size.width, bb.size.height);
          return bb.containsPoint(loc);
        };
        cocos2d::Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touchBlockListener, asNode());
      } else {
        if (_touchBlockListener == nullptr) {
          return;
        }
        cocos2d::Director::getInstance()->getEventDispatcher()->removeEventListener(_touchBlockListener);
        _touchBlockListener = nullptr;
      }
    }
}
