﻿// BmcTextureReloadManagerDefaultImpl.cpp
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.

#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/bmc/BmcTextureReloadManager.h>
#include <cocone/foundation/Component.h>
#include <cocone/foundation/Log.h>

namespace cocone {

    class BmcTextureReloadManagerImpl : public BmcTextureReloadManager, public Component
    {
        std::list<BmcTextureReloader*> _reloadersWeakPtr;
    public:
        static BmcTextureReloadManagerImpl* s_instance;
//        CCN_SHARABLEPTR_DEFINE(BmcTextureReloadManagerImpl);
        CCN_DECLARE_ASREF_IMPL();

        virtual bool operator == (const std::nullptr_t) const override { return false; }
        virtual bool operator != (const std::nullptr_t) const override { return true; }
        virtual explicit operator bool() const override { return true; }

        BmcTextureReloadManagerImpl()
        {
            ccn_dlog("call");
            ccn_assert(s_instance == 0);
            s_instance = this;
        }

        virtual ~BmcTextureReloadManagerImpl()
        {
            ccn_dlog("call");
            ccn_assert(s_instance == this);
            s_instance = 0;
        }

        virtual CLSID getCLSID()const override
        {
            return typeid(BmcTextureReloadManager);
        }

        virtual void inject() override
        {
            // _some_ptr = queryComponent<SomeComponent>();
        }

        virtual void regist(BmcTextureReloader* reloader) override
        {
            _reloadersWeakPtr.push_back(reloader);
        }
        virtual void unregist(BmcTextureReloader* reloader) override
        {
            std::list<BmcTextureReloader*>::iterator it = _reloadersWeakPtr.begin(), ed = _reloadersWeakPtr.end();
            for (; it != ed; ++it){
                if ((*it) == reloader){
                    _reloadersWeakPtr.erase(it);
                    return;
                }
            }
        }
        virtual void reload() override
        {
            ccn_dlog("start");
            std::list<BmcTextureReloader*>::iterator it = _reloadersWeakPtr.begin(), ed = _reloadersWeakPtr.end();
            for (; it != ed; ++it){
                ccn_dlog(">>>> %p", (*it));
                (*it)->reload();
            }
            ccn_dlog("end");
        }

    };

    BmcTextureReloadManagerImpl* BmcTextureReloadManagerImpl::s_instance = 0;
    BmcTextureReloadManager& BmcTextureReloadManager::getInstance()
    {
        ccn_dlog("%p", BmcTextureReloadManagerImpl::s_instance);
        ccn_assert(BmcTextureReloadManagerImpl::s_instance != 0);
        return *BmcTextureReloadManagerImpl::s_instance;
    }

} // cocone
