﻿// BmcSymbol.h
//
#pragma once

#include <cocone/bmc/BmcSymbol.h>
#include <cocone/foundation/Log.h>
//#include <cocone/display/bmc/BmcLibrary.h>

namespace cocone
{
/*
#define CCN_DECLARE_ASNODE_IMPL() \
    cocos2d::Node* asNode() override{ return this; } \
    const cocos2d::Node* asNode() const override{ return this; }
*/

    class BmcLibrary;
    class BmcSymbolDefaultImpl : public BmcSymbol
    {
    protected:
        ColorTransform _additionalColorTransform;
        cocos2d::EventListenerTouchOneByOne* _touchBlockListener = nullptr;

    protected:
        BmcSymbolDefaultImpl()
            : _additionalColorTransform(ColorTransform::Identity())
        {
        }
        virtual ~BmcSymbolDefaultImpl(){
        }

        ColorTransform getAscendColorTransform() const
        {
            ColorTransform ct = getAdditionalColorTransform();
            for (auto parent = asNode()->getParent(); parent; parent = parent->getParent()) {
                const BmcSymbol* p = areYouSymbol(parent);
                if (p) {
                    ct = p->getColorTransform().multiply(ct);
                }
            }
            return std::move(ct);
        }
    public:
        CCN_SHARABLEPTR_DEFINE(BmcSymbolDefaultImpl);

        virtual bool operator == (const std::nullptr_t) const override { return false; }
        virtual bool operator != (const std::nullptr_t) const override { return true; }
        virtual explicit operator bool() const override { return true; }

        bool hasTimeline()const override {
            return false;
        }

        const bmc::BmcTimeline& getTimeline()const override {
            ccn_assert(false, "unsupported.");
            return *(const bmc::BmcTimeline*)nullptr;
        }

        BmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const override; // at cpp
        BmcSymbol::Ptr getInstanceOne() const override; // at cpp
        virtual BmcSymbol::Ptr getInstanceBeginWith(const std::string& beginStr) override;
        BmcSymbol::Ptr getNullObject() const override; // at cpp
        void getAllInstances(std::vector<BmcSymbol::Ptr>* out) const override{}

        void overwriteInstance(BmcSymbol::Ptr current, BmcSymbol::Ptr over) override{}
        void overwriteInstances(const std::string& instanceName, BmcSymbol::Ptr over) override{}

        BmcSymbol::Ptr destroyInstance(const std::string& instanceName) override; // at cpp
        BmcSymbol::Ptr destroyInstance(BmcSymbol::Ptr instance) override;

        void iterateInstances(std::function<void(BmcSymbol*)> func) override{}
        void iterateInstancesForName(std::function<void(const std::string& instanceName, BmcSymbol*)> func) override{}
        void iterateInstancesByName(const std::string& instanceName, std::function<void(BmcSymbol*)> func) override{}
        void iterateInstancesRecursively(std::function<void(BmcSymbol*)> func) override{}
        void iterateInstancesByNameRecursively(const std::string& instanceName, std::function<void(BmcSymbol*)> func) override{}

        cocos2d::Size setupContentSize() override{
            return cocos2d::Size::ZERO;
        }
        void setBlendMode( bmc::BmcBlendMode blendMode) override{}


        virtual void setAdditionalTransform(const cocos2d::Mat4& mat) override
        {
            const auto trans = getAdditionalTransform();
            if (std::equal(std::begin(trans.m), std::end(trans.m), mat.m)) {
                return;
            }
            auto t = const_cast<cocos2d::Mat4&>(mat);
            asNode()->setAdditionalTransform(&t);
        }
        virtual void setAdditionalTransform(const cocos2d::AffineTransform& t) override
        {
            asNode()->setAdditionalTransform(t);
        }
        ///virtual const cocos2d::Mat4& getAdditionalTransformMat4()const =0; 下に同じ
        ///virtual const AffineTransform& getAdditionalTransform()const = 0; /// 派生側、Nodeを直接継承した人が実装しなければならない
        virtual void clearAdditonalTransform() override
        {
            asNode()->setAdditionalTransform(cocos2d::AffineTransform::IDENTITY);
        }


        void setAdditionalColorTransform(const ColorTransform& additionalColorTransform) override{
            _additionalColorTransform = additionalColorTransform;
        }
        const ColorTransform& getAdditionalColorTransform()const override{
            return _additionalColorTransform;
        }
        ColorTransform getColorTransform()const override{
//            ccn_assert(false, "unsupported node color.");
            return _additionalColorTransform;
//            return _additionalColorTransform.mutiply(_localColor);
        }


        virtual cocos2d::Rect getCurrentFrameBoundingBox() const override
        {
            return asNode()->getBoundingBox();
        }

        virtual bool inHitArea(cocos2d::Point point) override
        {
            auto node = asNode();
            if (node == nullptr || node->getParent() == nullptr) {
                return false;
            }

            point = node->getParent()->convertToNodeSpace(point);
            auto hitArea = getCurrentFrameBoundingBox();
            return hitArea.containsPoint(point);
        }

        virtual void addLibrary(BmcLibrary& library) override
        {
        }

        virtual cocos2d::Color4F getColorMultiply()const override;
        virtual void setColorMultiply(const cocos2d::Color4F& color) override;
        virtual void setColorMultiply(float r, float g, float b) override;
        virtual cocos2d::Color4F getColorAdd()const override;
        virtual void setColorAdd(const cocos2d::Color4F& color) override;
        virtual void setColorAdd(float r, float g, float b, float a=0.f) override;

        virtual void setTouchBlockEnabled(bool enabled) override;
    };


} // cocone
