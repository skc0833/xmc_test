// BmcLoaderDefaultImpl.cpp
//

#include <cocone/bmc/BmcLoader.h>
#include <cocone/bmc/BmcFactory.h>
#include <cocone/bmc/BmcSymbol.h>
#include <cocone/bmc/BmcShader.h>
#include <cocone/bmc/BmcShape.h>
#include <cocone/bmc/BmcMovieClip.h>
#include <cocone/bmc/BmcButton.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/bmc/BmcSoundPlayer.h>
#include <cocone/bmc/BmcScalingGridShape.h>
#include <cocone/bmc/BmcClockGenerator.h>
#include <cocone/foundation/FileHelper.h>

#include "BmcMCMaskImpl.h"

// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
#include <cocone/foundation/Log.h>

#include "BmcFactoryImpl.h"

namespace cocone {
    using namespace google_public::protobuf;
    USING_NS_CC;

#define SUPPORTED_BMC_VERSION (2)

    class BmcLoaderImpl : public BmcLoader, ZeroRefCount<cocos2d::Ref>
    {
    public:
        static BmcShader::Ptr s_shader;

    private:
        std::vector<BmcSymbol::Ptr> _symbols;
        std::vector<BmcTexture::Ptr> _textures;

        bmc::BmcFile _bmc;
        BmcSymbol::Ptr _stage;
        int _frameRate;
        std::function<void(void)> _playSound;
        BmcSoundPlayer::Ptr _soundPlayer;
        BmcTextureReloader::Ptr _reloader;
        BmcClockGenerator::Ptr _clock_generator;
        std::string _filePath;

    public:
        CCN_DECLARE_ASREF_IMPL();

        virtual bool operator == (const std::nullptr_t) const override { return false; }
        virtual bool operator != (const std::nullptr_t) const override { return true; }
        virtual explicit operator bool() const override { return true; }

        BmcLoaderImpl()
            :_frameRate(24)
        {

        }

        virtual ~BmcLoaderImpl()
        {
            ccn_dlog("");
        }
      
        //skc for debug
        virtual BmcShader::Ptr getShader() override {
          return s_shader;
        }

        virtual void setSoundHandler(std::function<void(void)> playSound) override
        {
            _playSound = playSound;
        }
        virtual bool init(const std::string& path) override
        {
            ccn_dlog("path=%s", path.c_str());
            _filePath = path;

            const Data fileData = FileUtils::getInstance()->getDataFromFile(path);
            if (fileData.isNull()){
                ccn_dlog("Bmc File is null : path=%s", path.c_str());
                return false;
            }
            //IMPORTANT:bmcファイル一番後ろ4バイトにチェクサムデーターが入っているので４バイト引いて実行する
            if (!_bmc.ParseFromArray(fileData.getBytes(), static_cast<int>(fileData.getSize())-4)){
                ccn_dlog("It is not Bmc File : path=%s", path.c_str());
                return false;
            }
#if ENABLE_RELOADER
            _reloader = BmcFactory::createBmcTextureReloader();
            _reloader->init(path, _bmc.image_table_size());
#endif
            _clock_generator = BmcClockGenerator::create();

            return init(&_bmc);
        }
        bool init(const bmc::BmcFile* bmcFile)
        {
            ccn_dlog("");
            if (!readHeader(bmcFile)){
                ccn_dlog("unsupported file");
                return false;
            }
            if (s_shader == nullptr){
                s_shader = BmcFactory::createBmcShader();
                s_shader->init();
            }
            if (bmcFile->sound_filename_table().size() > 0){
                _soundPlayer = BmcFactory::createBmcSoundPlayer();
                _soundPlayer->initSoundTable(bmcFile->sound_filename_table());
            }
          
            initTextures(bmcFile);
            initSymbols(bmcFile);
            initStage(bmcFile);
            _stage->query<BmcMovieClip>()->updateFrameForView();
            
            return true;
        }


        enum LazyInitState{
            Textures,
            Symbols,
            Stage,
        };
        LazyInitState _lazyInitState;
        int _lazyInitIndex;
        virtual void initLazy(const std::string& path) override
        {
            _filePath = path;

            Data fileData = FileUtils::getInstance()->getDataFromFile(path);
            if (ccn_econd(fileData.isNull(), "path=%s", path.c_str())){
                return;
            }
            _bmc.ParseFromArray(fileData.getBytes(), static_cast<int>(fileData.getSize()));

            if (ccn_econd(readHeader(&_bmc) == false, "unsupported file")){
                return;
            }

            const auto ext = filehelper::extname(_filePath);
            const auto baseName = filehelper::basename(_filePath, ext);
            if (ext == "mc") {
                const auto dir = filehelper::dirname(_filePath);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                const std::string os_image_ext = "pvr.plist";
    // NOTE pvr のソフトウェアデコードが汚いので、osx,win32 は ktx を使います
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
                const std::string os_image_ext = "ktx.plist";
#endif
                const auto plist = StringUtils::format("%s/images_%s.%s", dir.c_str(), baseName.c_str(), os_image_ext.c_str());
                if (FileUtils::getInstance()->isFileExist(plist)) {
                    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plist);
                    ccn_dlog("loaded spriteframe sheet=%s", plist.c_str());
                }
            }

#if ENABLE_RELOADER
            _reloader = BmcFactory::createBmcTextureReloader();
            _reloader->init(path, _bmc.image_table_size());
#endif
            if (s_shader == nullptr){
                s_shader = BmcFactory::createBmcShader();
                s_shader->init();
            }
            if (_bmc.sound_filename_table().size() > 0){
                _soundPlayer = BmcFactory::createBmcSoundPlayer();
                _soundPlayer->initSoundTable(_bmc.sound_filename_table());
            }
            _lazyInitState = Textures;
            _lazyInitIndex = 0;
        }
        virtual bool updateLazyInit() override
        {
            //initTextures(bmcFile);
            //initSymbols(bmcFile);
            //initStage(bmcFile);
            //_stage->updateFrameForView();
            bmc::BmcFile* bmcFile = &_bmc;
            const int STEP = 25;
            switch (_lazyInitState)
            {
                case Textures:
                {
                    RepeatedPtrField<bmc::BmcImage>* image_table = bmcFile->mutable_image_table();
                    if (_lazyInitIndex == 0){
                        _textures.resize(image_table->size(), nullptr);
                    }
                    int stepCountEnd = _lazyInitIndex + STEP;
                    if (stepCountEnd > image_table->size()){
                        stepCountEnd = image_table->size();
                    }

                    const auto ext = filehelper::extname(_filePath);
                    const auto baseName = filehelper::basename(_filePath, ext);
                    for (int i = _lazyInitIndex; i < stepCountEnd; ++i){
                        BmcTexture::Ptr tex = BmcFactory::createBmcTexture();
                        _textures[i] = tex;
                        tex->init(i, image_table->Mutable(i), baseName);
#if ENABLE_RELOADER
                        tex->registReloader(_reloader);
#endif
                    }
                    _lazyInitIndex = stepCountEnd;
                    if (_lazyInitIndex == image_table->size()){
                        _lazyInitState = Symbols;
                        _lazyInitIndex = 0;
                    }
                    return false;
                }
                case Symbols:
                {
                    const RepeatedPtrField<bmc::BmcSymbol>& symbol_table = bmcFile->symbol_table();
                    if (_lazyInitIndex == 0){
                        _symbols.resize(symbol_table.size(), nullptr);
                    }
                    int stepCountEnd = _lazyInitIndex + STEP;
                    if (stepCountEnd > symbol_table.size()){
                        stepCountEnd = symbol_table.size();
                    }

                    for (int i = _lazyInitIndex; i < stepCountEnd; ++i){
                        const bmc::BmcSymbol& symbol = symbol_table.Get(i);
                        if (isButton(symbol)){
                            _symbols[i] = createButton(symbol);
                        }
                        else if (isMovieClip(symbol))
                        {
                            _symbols[i] = createMovieClip(symbol);
                        }
                        else if (isShape(symbol))
                        {
                            _symbols[i] = createShape(symbol);
                        }
                    }
                    _lazyInitIndex = stepCountEnd;
                    if (_lazyInitIndex == symbol_table.size()){
                        _lazyInitState = Stage;
                        _lazyInitIndex = 0;
                    }
                    return false;
                }
                case Stage:
                {
                    BmcMovieClip::Ptr mc = BmcFactory::createBmcMovieClip();
                    mc->init(this, bmcFile->stage());
                    mc->updateFrameForView();
                    _stage = mc->asSymbol();
                    return true;
                }
            }
            ccn_assert(false, "never");
            return true;
        }

        virtual bool hasSound() override
        {
            return _soundPlayer != nullptr;
        }
        virtual void initSound(
            std::function<void(const std::string& soundFilePath)> preloadFunction
            , std::function<void(const std::string& soundFilePath)> playFunction
            ) override
        {
            _soundPlayer->initFunctions(preloadFunction, playFunction);
        }
        virtual int getFrameRate()const override
        {
            return _frameRate;
        }
    private:
        bool readHeader(const bmc::BmcFile* bmcFile)
        {
            if (bmcFile->bmc_file_version() > SUPPORTED_BMC_VERSION){
                return false;
            }
            if (bmcFile->has_frame_rate()){
                _frameRate = bmcFile->frame_rate();
            }
            if (_clock_generator) {
                _clock_generator->setOneFrameDelta(1.f / float(bmcFile->frame_rate()));
            }
            return true;
        }
        void initTextures(const bmc::BmcFile* bmcFile)
        {
            // load spritesheet
            const auto ext = filehelper::extname(_filePath);
            const auto baseName = filehelper::basename(_filePath, "." + ext);
            if (ext == "mc") {
                const auto dir = filehelper::dirname(_filePath);
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
                const std::string os_image_ext = "pvr.plist";
    // NOTE pvr のソフトウェアデコードが汚いので、osx,win32 は ktx を使います
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
                const std::string os_image_ext = "ktx.plist";
#endif
                const auto plist = StringUtils::format("%s/images_%s.%s", dir.c_str(), baseName.c_str(), os_image_ext.c_str());
                if (FileUtils::getInstance()->isFileExist(plist)) {
                    SpriteFrameCache::getInstance()->addSpriteFramesWithFile(plist);
                    ccn_dlog("loaded spriteframe sheet=%s", plist.c_str());
                }
                // TODO plist がない時の処理
            }

            auto image_table = bmcFile->image_table();
            _textures.resize(image_table.size());
            for (int i = 0; i < image_table.size(); ++i)
            {
                auto bmcImage = image_table.Get(i);
                BmcTexture::Ptr tex = BmcFactory::createBmcTexture();
                _textures[i] = tex;
                tex->init(i, &bmcImage, baseName);
#if ENABLE_RELOADER
                tex->registReloader(_reloader);
#endif
            }
        }
        void initSymbols(const bmc::BmcFile* bmcFile)
        {
            const RepeatedPtrField<bmc::BmcSymbol>& symbol_table = bmcFile->symbol_table();
            _symbols.resize(symbol_table.size());
            for (int i = 0; i < symbol_table.size(); ++i)
            {
                const bmc::BmcSymbol& symbol = symbol_table.Get(i);
                if (isButton(symbol)){
                    _symbols[i] = createButton(symbol);
                }
                else if (isMovieClip(symbol))
                {
                    _symbols[i] = createMovieClip(symbol);
                }
                else if (isShape(symbol))
                {
                    _symbols[i] = createShape(symbol);
                }
                else {
                    ccn_wlog("unknown symbol");
                }
            }
        }
        void initStage(const bmc::BmcFile* bmcFile)
        {
            BmcMovieClip::Ptr mc = BmcFactory::createBmcMovieClip();
            mc->init(this, bmcFile->stage());
            _stage = mc->asSymbol();
        }
    private: //////////////////////////////////////////////////////////////////////
        BmcTexturePart::Ptr createTexturePart(BmcTexture* tex, const bmc::BmcTextureRef& ref)
        {
            BmcTexturePart::Ptr p = BmcFactory::createBmcTexturePart();
            p->init(tex, ref);
            return p;
        }
        bool isShape(const bmc::BmcSymbol& symbol){
            return symbol.symbol_type() == bmc::BmcSymbol_Type_TypeShape || symbol.has_shape();
        }
        bool isMovieClip(const bmc::BmcSymbol& symbol){
            return symbol.symbol_type() == bmc::BmcSymbol_Type_TypeMovieClip || symbol.has_timeline();
        }
        bool isButton(const bmc::BmcSymbol& symbol){
            if (symbol.symbol_type() == bmc::BmcSymbol_Type_TypeButton) {
                return true;
            } else if (symbol.has_timeline() == false){
                return false;
            }
            const bmc::BmcTimeline& timeline = symbol.timeline();
            bool up = false, upnormal = false, down = false, downnormal = false, disable = false;
            for (int i = 0; i < timeline.frame_label_table().size(); ++i)
            {
                const std::string& label = timeline.frame_label_table(i);
                if (label == "up"){ up = true; }
                else if (label == "upnormal"){ upnormal = true; }
                else if (label == "down"){ down = true; }
                else if (label == "downnormal"){ downnormal = true; }
                else if (label == "disable"){ disable = true; }
            }
            // TODO: ↑判定に時間がかかるので、bmcconv側で情報を入れるようにする
            if (up && upnormal && down && downnormal && disable){
                return true;
            }
            //        for( int i = 0; i < timeline.instance_table().size(); ++i )
            //        {
            //            const bmc::BmcInstance& instance = timeline.instance_table(i);
            //            if( instance.has_instance_name() == false ){ continue; }
            //            if( instance.instance_name().find_first_of("btn") != std::string::npos ){
            //                return true;
            //            }
            //        }
            return false;
        }
        bool isMaskSymbol(const bmc::BmcTimeline& timeline)
        {
            // 1フレーム目の先頭にrange_mask があるかどうかで見る
            const auto& f = timeline.frames(0).placed_instances();
            if (f.size() < 1) {
                return false;
            }
            const auto& inst = timeline.instance_table(f.Get(f.size()-1).instance_index());
            if (inst.instance_name().find("$range_mask") == 0) {
                return true;
            }
            return false;
        }
        BmcSymbol::Ptr createShape(const bmc::BmcSymbol& symbol){
            const bmc::BmcTextureRef& textureRef = symbol.shape();
            const auto index = textureRef.texture_index();
            auto bmcTexture = _textures[index];
            BmcTexturePart::Ptr part = createTexturePart(bmcTexture, textureRef);
            auto image_table = _bmc.image_table();
            //if (false && textureRef.has_scale9grid()) //skc test
          if (textureRef.has_scale9grid())
            {
                BmcScalingGridShape::Ptr grid = BmcFactory::createBmcScalingGridShape();
                if (image_table.Get(index).type() == bmc::BmcImage_Type_TypeCocos2dxAtlas) {
                  grid->initWithSpriteFrameName(s_shader, part, bmcTexture->getSpriteFrameName());
                } else {
                  grid->init(s_shader, part);
                }
#if ENABLE_RELOADER
                grid->registReloader(_reloader);
#endif
                return grid->asSymbol();
            }
            else
            {
                if (image_table.Get(index).type() == bmc::BmcImage_Type_TypeCocos2dxAtlas) {
                    BmcShape::Ptr shape = BmcFactory::createBmcSpriteShape();
                    shape->initWithSpriteFrameName(s_shader, part, bmcTexture->getSpriteFrameName());
                    return shape->asSymbol();
                } else {
                    BmcShape::Ptr shape = BmcFactory::createBmcShape();
                    shape->init(s_shader, part);
                    return shape->asSymbol();
                }
            }
        }
        BmcSymbol::Ptr createMovieClip(const bmc::BmcSymbol& symbol){
            const bmc::BmcTimeline& timeline = symbol.timeline();
            BmcMovieClip::Ptr mc;
            if (isMaskSymbol(timeline)) {
                mc = new BmcMCMaskImpl();
            } else {
                mc = BmcFactory::createBmcMovieClip();
            }
            mc->init(this, timeline);
            return mc->asSymbol();
        }
        BmcSymbol::Ptr createButton(const bmc::BmcSymbol& symbol){
            const bmc::BmcTimeline& timeline = symbol.timeline();
            BmcButton::Ptr rawButton = BmcFactory::createBmcButton();
            rawButton->setPushSoundHandler(_playSound);
            BmcMovieClip::Ptr button = rawButton->query<BmcMovieClip>();
            button->init(this, timeline);
            return button->asSymbol();
        }
    public:
        virtual const std::vector<BmcSymbol::Ptr>& getSymbolTable()const override
        {
            return _symbols;
        }

        /////////////////////////////////////////////////
        virtual BmcSymbol::Ptr getStage() override
        {
            return _stage;
        }
        virtual BmcSoundPlayer::Ptr getSoundPlayer() override
        {
            return _soundPlayer;
        }
        virtual BmcTextureReloader::Ptr getReloader() override
        {
            return _reloader;
        }
        virtual BmcClockGenerator::Ptr getClockGenerator() override
        {
            return _clock_generator;
        }

    };

    BmcShader::Ptr BmcLoaderImpl::s_shader;

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcLoader);
} // cocone
