// BmcMovieClipAdapter.h
//
#pragma once
#include <cocone/bmc/BmcSymbol.h>
#include <cocone/bmc/BmcMovieClip.h>
#include <cocone/bmc/BmcLoader.h>
#include <cocone/bmc/BmcClockGenerator.h>
//#include <cocone/bmc/BmcLibrary.h>
#include "BmcSymbolDefaultImpl.h"
//#include <cocone/display/Cocos2dEx.h>

namespace cocone {
    class BmcLibrary;

    class BmcMovieClipAdapter : public BmcMovieClip, public BmcSymbolDefaultImpl
    {
    protected:
        virtual BmcMovieClipAdapter* callNewAndInit() const = 0;
        virtual bool callNodeInit() = 0;
        /// Node::update(float)内で呼び出してください。
        void nodeFuncUpdate(float deltaTime);
        void nodeFuncOnExit();
    public:
        BmcMovieClipAdapter();
        virtual ~BmcMovieClipAdapter();

        virtual bool operator == (const std::nullptr_t) const override { return false; }
        virtual bool operator != (const std::nullptr_t) const override { return true; }
        virtual explicit operator bool() const override { return true; }

        BmcSymbol::Ptr clone() const override;
        virtual void init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override;
        // BmcSymbol
        virtual const bmc::BmcTimeline& getTimeline() const override;
        // BmcSymbol
        virtual bool hasTimeline() const override;


        virtual BmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const override;
        virtual BmcSymbol::Ptr getInstanceOne() const override;
        virtual BmcSymbol::Ptr getInstanceBeginWith(const std::string& beginStr) override;
        virtual BmcSymbol::Ptr getNullObject() const override;
        virtual void getAllInstances(std::vector<BmcSymbol::Ptr>* out) const override;

        virtual void overwriteInstance(BmcSymbol::Ptr current, BmcSymbol::Ptr over) override;
        virtual void overwriteInstances(const std::string& instanceName, BmcSymbol::Ptr over) override;
        bool innerOverrideInstances(const std::string& instanceName, BmcSymbol::Ptr _template, bool alreadyOverride);
        virtual BmcSymbol::Ptr destroyInstance(const std::string& instanceName) override;
        virtual BmcSymbol::Ptr destroyInstance(BmcSymbol::Ptr instance) override;
        virtual void iterateInstances(std::function<void(BmcSymbol*)> func) override;
        virtual void iterateInstancesForName(std::function<void(const std::string& instanceName, BmcSymbol*)> func) override;
        virtual void iterateInstancesByName(const std::string& instanceName, std::function<void(BmcSymbol*)> func) override;
        virtual void iterateInstancesRecursively(std::function<void(BmcSymbol*)> func) override;
        virtual void iterateInstancesByNameRecursively(const std::string& instanceName, std::function<void(BmcSymbol*)> func) override;

        virtual int getCurrentFrame() const override;
        virtual const std::string& getCurrentLabel() const override;
        virtual int getTotalFrames() const override;
        virtual int getFrameByLabel(const char* label) const override;
        virtual void gotoAndPlay(int frameNumber) override;
        virtual void gotoTopAndPlayRecursively() override;
        virtual void gotoAndPlay(int frameNumber, int count) override;
        virtual void gotoAndPlayToNextLabel(const char* label) override;
        virtual void gotoAndPlayLabel(const char* labelFrom, const char* labelTo = 0) override;
        virtual void gotoAndPlayToNextFrame(int frameNumber) override;
        virtual void gotoAndStop(int frameNumber) override;
        virtual void play() override;
        virtual void stop() override;
        virtual void stopAll() override;
        virtual void setLoop(bool isLoop) override;
        virtual void setSpeedScale(float scale) override;
        virtual float getSpeedScale() const override;
        virtual void setEventListener(std::function<void (EventType, BmcMovieClip*)> listener) override;
        // NOTE イベントリスナー内でキャプチャされた変数を参照する可能性があるのでリスナー処理後にクリアします
        virtual void clearEventListener() override;
        // label callback
        virtual void setLabelCallback(const std::string& labelName, std::function<void(BmcMovieClip*)> listener) override;
        virtual void setLabelCallback(std::function<void(const std::string&, BmcMovieClip*)> listener) override;
        virtual void clearLabelCallback() override;
        // BmcSymbol
        virtual void nextFrame(bool child) override;
        virtual void prevFrame(bool child) override;
        virtual void updateFrameForView() override;
        virtual void stopFrameRecursively();
        virtual void processFrame() override;

        virtual cocos2d::Size setupContentSize() override;
        virtual cocos2d::Rect getCurrentFrameBoundingBox() const override;

        virtual void setBlendMode(bmc::BmcBlendMode blendMode) override;
        virtual void addLibrary(BmcLibrary& library) override;
        virtual void enableLog(bool enabled) override;

    private:
        void setupSymbols(BmcLoader* source);
        int searchFrameIndexByLabel(const char* label) const;
        int searchFrameNumberByFrameLabelIndex(int searchStartFrame, int frameLabelIndex) const;

        void setPlayFrameRange(int startFrame, int count);
        // 指定フレームまで、nextFrameとprevFrameを駆使して移動させる
        // 直指定で移動しないのは階層化された子Symbolのフレームを正しく移動するため。
        void moveCurrentFrame(int frameNumber);
        void processActionScriptByLabels();

    protected:
        bmc::BmcTimeline _timeline;
        std::vector<BmcSymbol::Ptr> _instances;
        std::vector<int> _indexhash;

        int _currentFrame;
        float _stockTime;
        float _oneFrameDelta;
        std::function<void (EventType, BmcMovieClip*)> _listener;
        std::map<std::string, std::function< void(BmcMovieClip*)>> _labelCallback;
        std::function<void(const std::string&, BmcMovieClip*)> _anyLabelCallback;
        bool _isLoop;
        int _playStartFrame;
        int _playFrameCount;
        float _speedScale;
        int _prevConstructFrame;
        bmc::BmcBlendMode _blendMode;
        BmcSoundPlayer::Ptr _soundPlayer;
        BmcClockGenerator::Ptr _clockGenerator = nullptr;
        bool _enableLog = false;
    };
}// cocone
