﻿// BmcMovieClipImpl.h
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
#include "BmcMovieClipAdapter.h"

//#define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
//#include <cocone/foundation/Log.h>

USING_NS_CC;

namespace cocone {
    class BmcMovieClipImpl : public ZeroRefCount<Node>, public BmcMovieClipAdapter
    {

    protected:
        void* query(IIDType iid) override;
        BmcMovieClipAdapter* callNewAndInit() const override;
        bool callNodeInit() override;
        void applyAdditionalColorTransformToChild(cocos2d::Node* child) const;

    public://////////////////////////////////////////////////////////////////////
        CCN_DECLARE_ASNODE_IMPL();

        BmcMovieClipImpl() = default;
        virtual ~BmcMovieClipImpl() = default;

        virtual void update(float dt) override;
        virtual void onExit() override;
        virtual void setOpacity(GLubyte opacity) override;
        virtual GLubyte getOpacity() const override;
        virtual const Mat4& getAdditionalTransform() const override;
        virtual AffineTransform getAdditionalAffineTransform() const override;
        virtual void setAdditionalColorTransform(const ColorTransform& additionalColorTransform) override;

        virtual void addChild(cocos2d::Node *child, int localZOrder, int tag) override;
        virtual void addChild(cocos2d::Node* child, int localZOrder, const std::string &name) override;
        virtual void addChild(cocos2d::Node *child, int zOrder) override;
        virtual void addChild(cocos2d::Node *child) override;
    };
} // cocone
