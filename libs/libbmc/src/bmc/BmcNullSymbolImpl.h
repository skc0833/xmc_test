// BmcSymbol.cpp.cpp
//
#pragma once

#include "BmcSymbolDefaultImpl.h"

namespace cocone
{

    /// アクセス不可を起こさないための NullObject
    class BmcNullSymbolImpl : public BmcSymbolDefaultImpl, public ZeroRefCount<cocos2d::Node>
    {
        cocone::bmc::BmcTimeline _timeline;
    public:
        // TODO impl assert
        BmcNullSymbolImpl() = default;
        virtual ~BmcNullSymbolImpl() = default;

        CCN_SHARABLEPTR_DEFINE(BmcNullSymbolImpl);
        CCN_DECLARE_ASNODE_IMPL();

        virtual bool operator == (const std::nullptr_t) const override { return true; }
        virtual bool operator != (const std::nullptr_t) const override { return false; }
        virtual explicit operator bool() const override { return false; }

        void* query(IIDType iid) override
        {
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            return nullptr;
        }

        virtual BmcSymbol::Ptr clone() const override
        {
            return new BmcNullSymbolImpl();
        }

        virtual const cocone::bmc::BmcTimeline& getTimeline()const override
        {
            return _timeline;
        }

        virtual BmcSymbol::Ptr getInstanceImpl(const std::string& instanceName) const override
        {
            return clone();
        }
        virtual BmcSymbol::Ptr getInstanceOne() const override
        {
            return clone();
        }
        virtual BmcSymbol::Ptr getInstanceBeginWith(const std::string& beginStr) override
        {
            return clone();
        }
        virtual BmcSymbol::Ptr destroyInstance(const std::string& instanceName) override
        {
            return clone();
        }

        virtual cocos2d::Size setupContentSize() override {
            return cocos2d::Size::ZERO;
        }

#ifdef skc_cocos_v3
        const cocos2d::Mat4& getAdditionalTransform() const
        {
            if (_additionalTransform) return *_additionalTransform;
            else return cocos2d::Mat4::IDENTITY;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform->m, &out);
            return out;
        }
#else
        virtual const cocos2d::Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }
#endif
    };


} // cocone

// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.
