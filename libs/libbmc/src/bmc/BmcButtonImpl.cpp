// BmcButtonDefaultImpl.cpp
//
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored.

#include <cocone/bmc/BmcButton.h>
#include <cocone/foundation/Log.h>

#include "BmcMovieClipAdapter.h"
#include "BmcFactoryImpl.h"

using namespace std;
USING_NS_CC;

#define DRAW_HITAREA (1)

#define LABEL_UP "up"
#define LABEL_UPNORMAL "upnormal"
#define LABEL_DOWN "down"
#define LABEL_DOWNNORMAL "downnormal"
#define LABEL_DISABLE "disable"

#define ELAPSED_PUSHING 0.6f

namespace cocone {

#if 1 //skc add
  static std::unordered_map<int, std::string> _label_map = {
    { 0, LABEL_UP },
    { 1, LABEL_UPNORMAL },
    { 2, LABEL_DOWN },
    { 3, LABEL_DOWNNORMAL },
    { 5, LABEL_DISABLE },
  };
  static const char* LableStr(int label) {
    return _label_map[label].c_str();
  }
#endif
  
  class BmcButtonImpl : public BmcButton, public BmcMovieClipAdapter, public ZeroRefCount<Node>
    {
        CCN_SHARABLEPTR_DEFINE(BmcButtonImpl);
        CCN_DECLARE_ASNODE_IMPL();

        enum LabelType
        {
            LabelType_Up,
            LabelType_UpNormal,
            LabelType_Down,
            LabelType_DownNormal,
            LabelType_Disable,
            LabelType_HitArea,
        };
        typedef BmcButtonImpl _myt;
        LabelType _currentPlayingLabel;
        std::function<bool(BmcButton*)> _handler;
        std::function<bool(BmcButton*)> _pushingHandler;
        std::function<bool(BmcButton*)> _touchBeganHandler;
        std::function<void(void)> _soundPlay;
        bool _enabled;
        bool _touchBegan;
        bool _checkBoxMode;
        bool _check;
      RefPtr<EventListenerTouchOneByOne> _touchListener;
        float _barrageBlockSec;
        double _barrageBlockCounter;
        float _pushingElapsed;
        cocos2d::Rect _hitArea;
        static int _buttonPressingCounter;
        bool _simultaneousPress;
        bool _onPressing;

    protected:///////////////////////////////////////////////////////////////////
        void* query(IIDType iid) override {
            if (iid == BmcMovieClip::iid()) {
                return static_cast<BmcMovieClip*>(this);
            }
            if (iid == BmcSymbol::iid()) {
                return static_cast<BmcSymbol*>(this);
            }
            if (iid == BmcButton::iid()) {
                return static_cast<BmcButton*>(this);
            }
            return nullptr;
        }
        BmcMovieClipAdapter* callNewAndInit() const override
        {
            auto t = new BmcButtonImpl();
            t->ZeroRefCount<Node>::init();
            return t;
        }
        bool callNodeInit() override {
            return ZeroRefCount<Node>::init();
        }
#ifdef skc_cocos_v3
        const cocos2d::Mat4& getAdditionalTransform() const
        {
            if (_additionalTransform) return *_additionalTransform;
            else return cocos2d::Mat4::IDENTITY;
        }
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const override
        {
            cocos2d::AffineTransform out;
            GLToCGAffine(this->_additionalTransform->m, &out);
            return out;
        }
#else
        virtual const Mat4& getAdditionalTransform() const override
        {
            return _additionalTransform;
        }
        virtual AffineTransform getAdditionalAffineTransform() const override
        {
            AffineTransform out;
            GLToCGAffine(this->_additionalTransform.m, &out);
            return out;
        }
#endif
    public://////////////////////////////////////////////////////////////////////
    public:
        BmcButtonImpl() :
            _enabled(false),
            _currentPlayingLabel(LabelType_Up),
            _touchBegan(false),
            _checkBoxMode(false),
            _check(false),
            _barrageBlockCounter(0.f),
            _barrageBlockSec(.2f),
            _pushingHandler(nullptr),
            _handler(nullptr),
            _touchBeganHandler(nullptr),
            _pushingElapsed(0),
            _hitArea(Rect::ZERO),
            _simultaneousPress(true),
            _onPressing(false)
        {

        }
        virtual ~BmcButtonImpl()
        {
            asNode()->getEventDispatcher()->removeEventListener(_touchListener);
        }

        virtual bool operator == (const std::nullptr_t) const override { return false; }
        virtual bool operator != (const std::nullptr_t) const override { return true; }
        virtual explicit operator bool() const override { return true; }

        virtual BmcSymbol::Ptr clone() const override
        {
            BmcSymbol::Ptr ret = BmcMovieClipAdapter::clone();
            BmcButtonImpl* me = static_cast<BmcButtonImpl*>(ret.get());
            me->setEventListener(std::bind(&_myt::mcEventListener, me, placeholders::_1, placeholders::_2));

            // タッチ設定
            me->_touchListener = EventListenerTouchOneByOne::create();
            me->_touchListener->setSwallowTouches(true);
            me->_touchListener->onTouchBegan = bind(&_myt::onTouchBegan, me, placeholders::_1, placeholders::_2);
            me->_touchListener->onTouchMoved = bind(&_myt::onTouchMoved, me, placeholders::_1, placeholders::_2);
            me->_touchListener->onTouchEnded = bind(&_myt::onTouchEnded, me, placeholders::_1, placeholders::_2);
            me->asNode()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(me->_touchListener.get(), me->asNode());
            me->_touchListener->setEnabled(this->_touchListener->isEnabled());

            me->_enabled = _enabled;
            me->_currentPlayingLabel = LabelType_UpNormal;
            me->_hitArea = _hitArea;
            me->gotoAndPlayToNextLabel(LABEL_UPNORMAL);
            me->_checkBoxMode = _checkBoxMode;
            me->_check = _check;
            me->_soundPlay = _soundPlay;
            return ret;
        }
        virtual void init(BmcLoader* source, const bmc::BmcTimeline& bmcTimeline) override
        {
            BmcMovieClipAdapter::init(source, bmcTimeline);
            this->setEventListener(std::bind(&_myt::mcEventListener, this, placeholders::_1, placeholders::_2));
            _touchListener = EventListenerTouchOneByOne::create();
            _touchListener->setSwallowTouches(true);
            _touchListener->onTouchBegan = bind(&_myt::onTouchBegan, this, placeholders::_1, placeholders::_2);
            _touchListener->onTouchMoved = bind(&_myt::onTouchMoved, this, placeholders::_1, placeholders::_2);
            _touchListener->onTouchEnded = bind(&_myt::onTouchEnded, this, placeholders::_1, placeholders::_2);
            asNode()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touchListener.get(), asNode());
            _touchListener->setEnabled(true);
            _enabled = true;

            _currentPlayingLabel = LabelType_UpNormal;

            BmcSymbol::Ptr hitArea = this->getInstance("$hitArea");
            if(hitArea!=nullptr){
                _hitArea = hitArea->getCurrentFrameBoundingBox();
//                hitArea->asNode()->setVisible(false);
                destroyInstance(hitArea);
#if defined(DRAW_HITAREA) && (DRAW_HITAREA)
                auto spr = Sprite::create();
                spr->setTextureRect({ .0f, .0f, _hitArea.size.width, _hitArea.size.height });
                spr->setPosition(_hitArea.origin);
                spr->setAnchorPoint(Vec2::ANCHOR_BOTTOM_LEFT);
                spr->setColor(Color3B::BLUE);
                spr->setOpacity(0xFF * .3f);
                asNode()->addChild(spr);
#endif
            }

            gotoAndPlayToNextLabel(LABEL_UPNORMAL);
            //play();
        }
        virtual void onEnter() override
        {
            Node::onEnter();
            play();
            childTouchDisable();
            scheduleUpdate();
        }
        virtual void scheduleUpdate()
        {
            Node::scheduleUpdate();
        }
        virtual void update(float delta) override
        {
            if (_barrageBlockCounter > .0f) {
              _barrageBlockCounter -= delta;
            }

            if (_pushingHandler!=nullptr && _checkBoxMode == false && _touchBegan){

                if (_pushingElapsed > ELAPSED_PUSHING){
                    _pushingHandler(this);
                }
                else{
                    _pushingElapsed+=delta;
                }
            }


            //nodeFuncUpdate(delta);
            Node::update(delta);
        }
        virtual void setPushSoundHandler(std::function<void(void)> soundPlay) override
        {
            _soundPlay = soundPlay;
        }
        virtual void setPushHandler(std::function<bool(BmcButton*)> handler) override
        {
            _handler = handler;
        }
        virtual void setPushingHandler(std::function<bool(BmcButton*)> handler) override
        {
            _pushingHandler = handler;
        }
        virtual void setTouchBeganHandler(std::function<bool(BmcButton*)> handler) override
        {
            _touchBeganHandler = handler;
        }
        virtual void setEnabled(bool enabled) override
        {
            _enabled = enabled;
            _touchListener->setEnabled(enabled);
            setLoop(enabled);
            if (_enabled == false){
                _currentPlayingLabel = LabelType_Disable;
                gotoAndPlayToNextLabel(LABEL_DISABLE);
            }
            else{
                _currentPlayingLabel = LabelType_UpNormal;
                gotoAndPlayToNextLabel(LABEL_UPNORMAL);
            }
        }
        virtual void setEventBlock( bool block ) override
        {
            if (!_enabled) return;
            _touchListener->setEnabled(!block);
        }
        virtual void setCheckBoxMode(bool checkbox) override
        {
            _checkBoxMode = checkbox;
        }
        virtual bool getCheckBoxMode()const override
        {
            return _checkBoxMode;
        }
        virtual void setCheck(bool check, bool playSound) override
        {
            setCheck(check, playSound, true);
        }
        virtual void setCheck(bool check, bool playSound, bool animated) override
        {
            if (_checkBoxMode == false || _touchListener->isEnabled() == false) {
                return;
            }

            setLoop(false);

            if (check) {
                if (animated) {
                    _currentPlayingLabel = LabelType_Down;
                    gotoAndPlayToNextLabel(LABEL_DOWN);
                }
                else {
                    _currentPlayingLabel = LabelType_DownNormal;
                    gotoAndPlayToNextLabel(LABEL_DOWNNORMAL);
                }
            }
            else {
                if (animated) {
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                }
                else {
                    _currentPlayingLabel = LabelType_UpNormal;
                    gotoAndPlayToNextLabel(LABEL_UPNORMAL);
                }
            }

            _touchBegan = false;
            if (playSound && _soundPlay != nullptr){
                _soundPlay();
            }
            _check = check;
        }
        virtual bool getCheck() const override
        {
            return _check;
        }
        virtual void setTouchEnabled(bool enable) override
        {
            _touchListener->setEnabled(enable);
            setLoop(enable);
        }
        virtual void setBarrageBlockTime(float blockSec=0.2f) override
        {
            _barrageBlockSec = blockSec;
        }
        virtual float getBarrageBlockTime() const override
        {
            return _barrageBlockSec;
        }
        virtual void setSwallowTouches( bool swallow ) override
        {
            _touchListener->setSwallowTouches( swallow );
        }
        virtual void touchDownAction() override
        {
            setLoop(false);
            _currentPlayingLabel = LabelType_Up;
            gotoAndPlayLabel(LABEL_DOWNNORMAL);
        }
    private:
        virtual void gotoAndPlayToNextLabel(const char* label)override{
            BmcMovieClipAdapter::gotoAndPlayToNextLabel(label);
            // ボタンタイプの子供がいれば変更する
            iterateInstancesRecursively([label](BmcSymbol* symbol){
                BmcButtonImpl* child = dynamic_cast<BmcButtonImpl*>(symbol);
                if (child != nullptr) {
                    child->gotoAndPlayToNextLabel(label);
                }
            });
        }
        void childTouchDisable()
        {
            // 子供たちがタッチに反応しないようにする
            iterateInstancesRecursively([](BmcSymbol* symbol){
                BmcButtonImpl* child = dynamic_cast<BmcButtonImpl*>(symbol);
                if (child != nullptr) {
                    child->setTouchEnabled(false);
                }
            });
        }
        void mcEventListener(BmcMovieClip::EventType eventType, BmcMovieClip* mc)
        {
            switch (eventType){
            case EventType_PlayEndFrame:
            {
                //ccn_dlog( "%p, _currentPlayingLabel=%d", this, _currentPlayingLabel);
                switch (_currentPlayingLabel){
                case LabelType_Up:
                    //ccn_dlog("LabelType_Up");
                    ccn_dlog("%p, current=%s -> %s", this, LableStr(LabelType_Up), LableStr(LabelType_UpNormal));
                    setLoop(true);
                    _currentPlayingLabel = LabelType_UpNormal;
                    gotoAndPlayToNextLabel(LABEL_UPNORMAL);
                    break;
                case LabelType_Down:
                    //ccn_dlog("LabelType_Down");
                    ccn_dlog("%p, current=%s -> %s", this, LableStr(LabelType_Down), LableStr(LabelType_DownNormal));
                    setLoop(true);
                    _currentPlayingLabel = LabelType_DownNormal;
                    gotoAndPlayToNextLabel(LABEL_DOWNNORMAL);
                    break;
                default:
                    break;
                }
            }
                break;
            default:
                break;
            }
        }
        bool pushFire()
        {
            _barrageBlockCounter = _barrageBlockSec;

            auto soundPlayFunc = _soundPlay;

            if (_handler != nullptr){
                if (_handler(this) == false){
                    return false;
                }
            }
            if (soundPlayFunc != nullptr){
                soundPlayFunc();
            }

            return true;
        }
        virtual void gotoTopAndPlayRecursively() override
        {
            // 何もしない
        }
    private: /// Layer -------------------------------------------------------------------------------------

        bool hasVisibleParents()
        {
            Node* pParent = this->getParent();
            for (Node *c = pParent; c != NULL; c = c->getParent())
            {
                if (!c->isVisible())
                {
                    return false;
                }
            }
            return true;
        }
        virtual bool onTouchBegan(Touch *pTouch, Event *pEvent)
        {
            //ccn_dlog("%p: %d/%d", this, _onPressing, _buttonPressingCounter);
            ccn_dlog("skc %p: %d/%d, %s", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
            if (isVisible() == false || hasVisibleParents() == false || (_buttonPressingCounter > 0 && _simultaneousPress == false)) {
              return false;
            }
            _pushingElapsed = 0.f;

            // 連打防止チェック
            if (_barrageBlockSec > 0.f) {
                if (_barrageBlockCounter > 0.f) {
                    return false;	// まだ連打ブロック時間内
                }
            }

            Point touchLocation = pTouch->getLocation();
            if (inHitArea(touchLocation) == false){
                if (_checkBoxMode == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                }
                return false;
            }

            bool ret = true;
            if (_checkBoxMode == false){
                if(_touchBeganHandler){
                   ret = _touchBeganHandler(this);
                }

                if (ret) {
                    _currentPlayingLabel = LabelType_Down;
                    gotoAndPlayToNextLabel(LABEL_DOWN);
                }
            }
            else{

                if(_touchBeganHandler){
                    ret = _touchBeganHandler(this);
                }

                //ccn_dlog( "DO Began");
                if (ret){
                    if (_check){
                        _currentPlayingLabel = LabelType_Up;
                        gotoAndPlayToNextLabel(LABEL_UP);
                    }
                    else{
                        _currentPlayingLabel = LabelType_Down;
                        gotoAndPlayToNextLabel(LABEL_DOWN);
                    }
                }

            }
            if (ret) {
              _buttonPressingCounter++;
              _onPressing = true;
              //ccn_dlog("%p: %d/%d", this, _onPressing, _buttonPressingCounter);
              ccn_dlog("skc %p: %d/%d, %s", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
            }

            return ret;
        }
        virtual void onTouchMoved(Touch *pTouch, Event *pEvent){
        ccn_dlog("skc %p: %d/%d, %s", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
            if (isVisible() == false || hasVisibleParents() == false){ return; }

            Point touchLocation = pTouch->getLocation();
            //ccn_dlog("%f, %f", touchLocation.x, touchLocation.y );
            bool hitArea = inHitArea(touchLocation);
            ccn_dlog("skc %f, %f -> hitArea=%d, _currentPlayingLabel=%d", touchLocation.x, touchLocation.y, hitArea, _currentPlayingLabel);
            if (_checkBoxMode == false){
                if (hitArea == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                    setLoop(false);
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                }
                else if (hitArea && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal) == false){
                    setLoop(false);
                    _currentPlayingLabel = LabelType_Down;
                    gotoAndPlayToNextLabel(LABEL_DOWN);
                }
            }
            else{
                //ccn_dlog( "DO Moved");
                if (_check){
                    if (hitArea == false && (_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Down;
                        gotoAndPlayToNextLabel(LABEL_DOWN);
                    }
                    else if (hitArea && (_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal) == false){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Up;
                        gotoAndPlayToNextLabel(LABEL_UP);
                    }
                }
                else{
                    if (hitArea == false && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Up;
                        gotoAndPlayToNextLabel(LABEL_UP);
                    }
                    else if (hitArea && (_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal) == false){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Down;
                        gotoAndPlayToNextLabel(LABEL_DOWN);
                    }
                }
            }
        }
        virtual void onTouchEnded(Touch *pTouch, Event *pEvent){
        ccn_dlog("skc %p: +++ %d/%d, %s", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
            if (isVisible() == false || hasVisibleParents() == false) {
              _buttonPressingCounter -= _onPressing;
              _onPressing = false;
              //ccn_dlog("%p: %d/%d", this, _onPressing, _buttonPressingCounter);
              return;
            }

            Point touchLocation = pTouch->getLocation();
            if (inHitArea(touchLocation)){
                if (_checkBoxMode == false){
                    setLoop(false);
                    _currentPlayingLabel = LabelType_Up;
                    gotoAndPlayToNextLabel(LABEL_UP);
                    pushFire();
                }
                else{
                    //ccn_dlog( "DO Ended");
                    _check = !_check;
                    if (!pushFire()){
                        _check = !_check;
                    }
                }
            }
            else if (_checkBoxMode){
                if (_check){
                    if ((_currentPlayingLabel == LabelType_Up || _currentPlayingLabel == LabelType_UpNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Down;
                        gotoAndPlayToNextLabel(LABEL_DOWN);
                    }
                }
                else{
                    if ((_currentPlayingLabel == LabelType_Down || _currentPlayingLabel == LabelType_DownNormal)){
                        setLoop(false);
                        _currentPlayingLabel = LabelType_Up;
                        gotoAndPlayToNextLabel(LABEL_UP);
                    }
                }
            }

            _buttonPressingCounter -= _onPressing;
            _onPressing = false;
            //ccn_dlog("%p: %d/%d", this, _onPressing, _buttonPressingCounter);
            ccn_dlog("skc %p: --- %d/%d, %s", this, _onPressing, _buttonPressingCounter, LableStr(_currentPlayingLabel));
        }
        virtual bool inHitArea(cocos2d::Point point) override
        {
            if (_hitArea.equals(Rect::ZERO)) {
                return BmcSymbolDefaultImpl::inHitArea(point);
            }

            auto node = asNode();
            if (node == nullptr) {
                return false;
            }

            point = asNode()->convertToNodeSpace(point);
            return _hitArea.containsPoint(point);
        }

        virtual void setSimultaneousPress(bool enabled) override
        {
          _simultaneousPress = enabled;
        }

        virtual bool getSimultaneousPress() const override
        {
          return _simultaneousPress;
        }

        virtual void onExit() override
        {
          _buttonPressingCounter -= _onPressing;
          _onPressing = false;
          //ccn_dlog("%p: %d/%d", this, _onPressing, _buttonPressingCounter);

          Node::onExit();
        }
    };

    int BmcButtonImpl::_buttonPressingCounter = 0;

    BMC_FACTORY_CREATE_FUNC_IMPL(BmcButton);

} // cocone
