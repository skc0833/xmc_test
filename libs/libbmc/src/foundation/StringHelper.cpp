﻿//
//  StringHelper.cpp
//
//


#include <cocone/foundation/StringHelper.h>

#include <cstdarg>
#include <iostream>
#include <sstream>

namespace cocone {
namespace stringhelper {

static std::string strvprintf(const char* format, std::va_list arg)
{
  std::va_list arg2;
  va_copy(arg2, arg);
  auto len = static_cast<std::size_t>(vsnprintf(nullptr, 0, format, arg2));

  std::string ret;
  ret.resize(len);

  vsnprintf(&ret[0], ret.size() + 1, format, arg);

  return ret;
}

std::string strsprintf(const char* format, ...)
{
  std::va_list arg;
  va_start(arg, format);
  auto ret = strvprintf(format, arg);
  va_end(arg);
  return ret;
}

std::string join(const std::vector<std::string>& arr, char delim /* = ',' */)
{
  std::ostringstream ret;
  for (auto it = std::begin(arr); it != std::end(arr);) {
    ret << *it;
    if ((++it) != end(arr)) {
      ret << delim;
    }
  }
  return ret.str();
}

std::vector<std::string> split(const std::string& str, char delim /* = ',' */)
{
  std::vector<std::string> v;
  std::stringstream ss(str);
  std::string buffer;
  while (std::getline(ss, buffer, delim)) {
    v.push_back(buffer);
  }
  return v;
}

std::string format_with_commas(long val)
{
  auto str = std::to_string(val);
  int pos = static_cast<int>(str.length()) - 3;
  while (pos > 0) {
    str.insert(pos, ",");
    pos -= 3;
  }

  return str;
}

}
}
