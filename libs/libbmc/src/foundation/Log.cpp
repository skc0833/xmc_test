// Log.cpp
//
#include <stdio.h>
#include <stdarg.h>
#include <assert.h>
#include <signal.h>

#include <cocos2d.h>
#include <cocone/foundation/Log.h>
#include <cocone/foundation/FileHelper.h>

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#pragma warning( disable : 4996 )
#endif

#define COCOS2D_DEBUG 1 //skc test
// define cocone_DbgBreak
#if COCOS2D_DEBUG>=1
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
  #define cocone_DbgBreak DebugBreak()
#else
  #define cocone_DbgBreak raise(SIGTRAP)
#endif
#else
#define cocone_DbgBreak ((void)0)
#endif

namespace fh = cocone::filehelper;

namespace cocone{ 
	const char* _____s_logPrefix[LogPrefix_CountOfLogPrefix]={
		"ERROR",
		"WARN",
		"INFO",
		"SAVE",
		"DEBUG"
	};
	inline std::string* vformatString( std::string* out, const char* format, va_list argptr )
	{
		int size = vsnprintf( nullptr, 0, format, argptr );
		out->resize( size );
		vsnprintf( &(*out)[0], size+1, format, argptr );
		return out;
	}
	std::string* formatString( std::string* out, const char* format, ... )
	{
		va_list argptr;
		va_start( argptr, format );
        
        int size = vsnprintf( nullptr, 0, format, argptr );
        out->resize( size );
        va_end( argptr );
        va_start( argptr, format );
        vsnprintf( &(*out)[0], size+1, format, argptr );
        
//		vformatString( out, format, argptr );
		va_end( argptr );
		return out;
	}
	std::string formatString( const char* format, ... )
	{
		std::string ret;
		va_list argptr;
		va_start( argptr, format );
        
        int size = vsnprintf( nullptr, 0, format, argptr );
        ret.resize( size );
        va_end( argptr );
        va_start( argptr, format );
        vsnprintf( &(ret)[0], size+1, format, argptr );
//		vformatString( &ret, format, argptr );
		va_end( argptr );
		return ret;
	}
    bool logOutputCond( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* condStr, const char* format, ... )
    {
		std::string s_headerBuffer;
		std::string s_buffer;
        auto fname = fh::basename(fileStr);
        formatString(&s_headerBuffer, "%s %s:%d :%s:trapped(%s) ", _____s_logPrefix[prefix], fname.c_str(), lineInt, funcStr, condStr);
		va_list argptr;
		va_start( argptr, format );
        
        int size = vsnprintf( nullptr, 0, format, argptr );
        s_buffer.resize( size );
        va_end( argptr );
        va_start( argptr, format );
        vsnprintf( &(s_buffer)[0], size+1, format, argptr );
        
//		vformatString( &s_buffer, format, argptr );
		va_end( argptr );
        
        s_headerBuffer += s_buffer;
        consoleOutput( prefix, s_headerBuffer.c_str() );
        if(isBreak){ cocone_DbgBreak; }
		return true;
    }
    bool logOutputCond( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* condStr)
    {
		std::string s_headerBuffer;
        auto fname = fh::basename(fileStr);
        formatString(&s_headerBuffer, "%s %s:%d %s:trapped(%s)\n", _____s_logPrefix[prefix], fname.c_str(), lineInt, funcStr, condStr);
        consoleOutput( prefix, s_headerBuffer.c_str() );
        if(isBreak){ cocone_DbgBreak; }
        return true;
    }
    bool logOutput( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* format, ... )
    {
		std::string s_headerBuffer;
		std::string s_buffer;
        auto fname = fh::basename(fileStr);
        formatString(&s_headerBuffer, "%s %s:%d %s: ", _____s_logPrefix[prefix], fname.c_str(), lineInt, funcStr);
		va_list argptr;
		va_start( argptr, format );
        int size = vsnprintf( nullptr, 0, format, argptr );
        s_buffer.resize( size );
        va_end( argptr );
        va_start( argptr, format );
        vsnprintf( &(s_buffer)[0], size+1, format, argptr );
        
//		vformatString( &s_buffer, format, argptr );
		va_end( argptr );
        
        s_headerBuffer += s_buffer;
        consoleOutput( prefix, s_headerBuffer.c_str() );
        if(isBreak){ cocone_DbgBreak; }
        return true;
    }
    bool logOutput( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt)
    {
		std::string s_headerBuffer;
        auto fname = fh::basename(fileStr);
        formatString(&s_headerBuffer, "%s %s:%d %s: ", _____s_logPrefix[prefix], fileStr, lineInt, funcStr);
        consoleOutput( prefix, s_headerBuffer.c_str() );
        if(isBreak){ cocone_DbgBreak; }
        return true;
    }
}



#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS || CC_TARGET_PLATFORM == CC_PLATFORM_MAC)
namespace cocone { 
    void consoleOutput( LogPrefix, const char* str )
    {
        cocos2d::log("%s", str);
//        fputs(str, stdout);
    }
}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#include <android/log.h>
namespace cocone{
    inline android_LogPriority getAndroidPriority( LogPrefix prefix )
    {
		android_LogPriority ap = ANDROID_LOG_VERBOSE;
		switch(prefix){
            case LogPrefix_Error: ap = ANDROID_LOG_ERROR; break;
            case LogPrefix_Warning: ap = ANDROID_LOG_WARN; break;
            case LogPrefix_Info: ap = ANDROID_LOG_INFO; break;
            case LogPrefix_Save: ap = ANDROID_LOG_INFO; break;
            case LogPrefix_Debug: ap = ANDROID_LOG_DEBUG; break;
            case LogPrefix_CountOfLogPrefix: break;
		}
        return ap;
    }
    void consoleOutput( LogPrefix prefix, const char* str )
    {
		android_LogPriority ap = getAndroidPriority(prefix);
	    __android_log_write(ap, "cocone", str);
    }
}
#elif (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
//#include <debugapi.h>
namespace cocone{
    void consoleOutput( LogPrefix, const char* str )
    {
		OutputDebugStringA(str);
		OutputDebugStringA("\n");
#ifdef COCONE_LOG_OUTPUT_FILE
		FILE* fp = ::fopen("log.txt", "at");
		if (fp != nullptr){
			::fprintf(fp, str);
			::fclose(fp);
		}
#endif

//		int32_t len = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, 0, 0);
//		std::vector<wchar_t> buf(len);
//		len = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, &buf[0], buf.size());
//		OutputDebugStringW(&buf[0]);
    }
}
#endif
