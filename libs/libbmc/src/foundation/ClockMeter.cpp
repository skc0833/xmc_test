﻿//
// ClockMeter.cpp

#include <cocone/foundation/ClockMeter.h>

#include <cocone/foundation/Log.h>

namespace cocone {

  ClockMeter::ClockMeter(bool run /* = false */)
  {
    if (run) {
      reset();
    }
  }

  void ClockMeter::reset()
  {
    start = std::chrono::high_resolution_clock::now();
  }

  double ClockMeter::elapsed() const
  {
    return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();
  }

  ScopedClockMeter::ScopedClockMeter(const std::string& file, int lineno)
    : ClockMeter(true), _file(file), _lineno(lineno)
  {
  }

  ScopedClockMeter::~ScopedClockMeter()
  {
    ccn_dlog("%s:%d : elapsed=%.0f", _file.c_str(), _lineno, elapsed());
  }
}
