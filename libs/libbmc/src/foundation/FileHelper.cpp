﻿//
//  FileHelper.cpp
//
//

#include <cocone/foundation/FileHelper.h>

#include <cocos2d.h>

USING_NS_CC;

namespace cocone {
namespace filehelper {

  std::string absolute_path(const std::string& relative_path)
  {
    return FileUtils::getInstance()->fullPathForFilename(relative_path);
  }

  std::string basename(const std::string& path, const std::string& suffix /* = "" */)
  {
    auto fname = path.substr(path.find_last_of('/') + 1);
    if (suffix.length() > 0) {
      auto pos = fname.rfind(suffix);
      if (pos == fname.length() - suffix.length()) {
        fname = fname.substr(0, pos);
      }
    }
    return fname;
  }

  std::string dirname(const std::string& path)
  {
    return path.substr(0, path.find_last_of('/'));
  }

  std::string extname(const std::string& path)
  {
    return path.substr(path.find_last_of('.') + 1);
  }

}
}
