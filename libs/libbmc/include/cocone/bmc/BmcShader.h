﻿// BmcShader.h
//
#pragma once

#include <cocone/foundation/Sharable.h>

#include "BmcGeom.h"

namespace cocone {
    class BmcTexturePart;

//    class BmcShader : public ZeroRefCount<cocos2d::Ref>
    class BmcShader : public AsRef
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcShader);

        virtual bool operator == (const std::nullptr_t) const = 0;
        virtual bool operator != (const std::nullptr_t) const = 0;
        virtual explicit operator bool() const = 0;

        virtual void init()=0;
        virtual void setTexture(cocos2d::Texture2D* tex)=0;
        virtual void setTexture(BmcTexturePart* tex)=0;
        virtual void setColorTransform( const ColorTransform& colorTransform )=0;

        virtual void useMostValuableProgram()=0;
    protected:
        virtual ~BmcShader(){}
    };

} // cocone
