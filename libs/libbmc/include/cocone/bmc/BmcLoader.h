// BmcLoader.h
//
#pragma once
#include <cocone/foundation/Sharable.h>

#include <cocone/bmc/BmcSymbol.h>
#include <cocone/bmc/BmcSoundPlayer.h>
#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/bmc/BmcClockGenerator.h>
#include <cocone/bmc/BmcShader.h> //skc for debug

namespace cocone {

    /// Bmcファイル単位におけるすべてのSymbolを格納したテーブル。
    /// 初期化時のみ使用し、その後は破棄すること
//    class BmcLoader : public ZeroRefCount<cocos2d::Ref>
    class BmcLoader : public AsRef
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcLoader);

        virtual bool operator == (const std::nullptr_t) const = 0;
        virtual bool operator != (const std::nullptr_t) const = 0;
        virtual explicit operator bool() const = 0;

        virtual void setSoundHandler( std::function<void (void)> playSound )=0;
        virtual bool init( const std::string& path )=0;

        virtual void initLazy(const std::string& path) = 0;
        virtual bool updateLazyInit() = 0;

        virtual bool hasSound() = 0;
        virtual void initSound(
            std::function<void(const std::string& soundFilePath)> preloadFunction
            , std::function<void(const std::string& soundFilePath)> playFunction
            )=0;
        virtual int getFrameRate()const=0;

        //	virtual BmcShader::Ptr getShader()=0;

        /// シンボルテーブルを取得する。
        virtual const std::vector<BmcSymbol::Ptr>& getSymbolTable()const=0;

        /// ステージ(トップとなるMovieClip)を取得する
        virtual BmcSymbol::Ptr getStage() = 0;
        virtual BmcShader::Ptr getShader() = 0; //skc for debug

        virtual BmcSoundPlayer::Ptr getSoundPlayer() = 0;

        /// デバイスが初期化され、テクスチャをリロードしないといけないシステムの場合、BmcTextureReloaderをManagerに登録してください。
        virtual BmcTextureReloader::Ptr getReloader() = 0;

        virtual BmcClockGenerator::Ptr getClockGenerator() = 0;
    protected:
        virtual ~BmcLoader(){}
    };

} // cocone
