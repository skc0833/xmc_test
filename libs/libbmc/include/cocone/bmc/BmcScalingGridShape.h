// BmcScalingGridShape.h
//
#pragma once

#include <cocone/foundation/Sharable.h>

#include <cocone/bmc/BmcShader.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/bmc/BmcTextureReloader.h>

namespace cocone {
    class BmcSymbol;

//    class BmcScalingGridShape : public cocos2d::Ref, public UnknownInterface
    class BmcScalingGridShape : /*virtual*/ public AsRef, public UnknownInterface
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcScalingGridShape);
        CCN_DECLARE_IID(BmcScalingGridShape);

        inline BmcSymbol* asSymbol(){ return query<BmcSymbol>(); }

        virtual void init(BmcShader::Ptr shader, BmcTexturePart::Ptr texture) = 0;
        virtual void initWithSpriteFrameName(BmcShader::Ptr shader, BmcTexturePart::Ptr texture, const std::string& spriteFrameName) = 0;
        virtual void registReloader(BmcTextureReloader::Ptr reloader) = 0;
        virtual void reload() = 0;
    protected:
        virtual ~BmcScalingGridShape(){}
    };

} // cocone
