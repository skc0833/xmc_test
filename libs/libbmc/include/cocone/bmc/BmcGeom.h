﻿// BmcGeom.h
//
#pragma once

#include <math/TransformUtils.h>

#include "protocol/BmcFile.pb.h"

namespace cocone {

    void matDebugLog(const cocos2d::Mat4& mat);
    inline cocos2d::AffineTransform toAffineTransform( const bmc::BmcMatrix& m )
    {
        return cocos2d::AffineTransformMake(m.a(), -m.b(), -m.c(), m.d(), m.tx() / CC_CONTENT_SCALE_FACTOR(), -m.ty() / CC_CONTENT_SCALE_FACTOR());
    }
    inline cocos2d::Mat4 toMat4(const bmc::BmcMatrix& m)
    {
        cocos2d::Mat4 k;
        k.m[0]= m.a();k.m[4]=-m.c();k.m[ 8]=    0;k.m[12]= m.tx();
        k.m[1]=-m.b();k.m[5]= m.d();k.m[ 9]=    0;k.m[13]=-m.ty();
        k.m[2]=     0;k.m[6]=     0;k.m[10]=    1;k.m[14]=     0;
        k.m[3]=     0;k.m[7]=     0;k.m[11]=    0;k.m[15]=     1;
        k.m[12] /= CC_CONTENT_SCALE_FACTOR();
        k.m[13] /= CC_CONTENT_SCALE_FACTOR();
        return k;
    }

    class ColorTransform
    {
        float m[4];
        float a[4];
    public:
        static const ColorTransform& Identity()
        {
            static ColorTransform identity( 256,256,256,256,0,0,0,0 );
            return identity;
        }
        ColorTransform()
        {
        }
        ColorTransform( const ColorTransform& s )
        {
            m[0]=s.m[0];
            m[1]=s.m[1];
            m[2]=s.m[2];
            m[3]=s.m[3];
            a[0]=s.a[0];
            a[1]=s.a[1];
            a[2]=s.a[2];
            a[3]=s.a[3];
        }
        ColorTransform( int rM, int gM, int bM, int aM, int rA, int gA, int bA, int aA )
        {
            m[0] = float(rM) / 256.f;
            m[1] = float(gM) / 256.f;
            m[2] = float(bM) / 256.f;
            m[3] = float(aM) / 256.f;
            a[0] = float(rA) / 256.f;
            a[1] = float(gA) / 256.f;
            a[2] = float(bA) / 256.f;
            a[3] = float(aA) / 256.f;
        }
        ColorTransform( const bmc::BmcColorTransform& c )
        {
            m[0] = float(c.rmul()) / 256.f;
            m[1] = float(c.gmul()) / 256.f;
            m[2] = float(c.bmul()) / 256.f;
            m[3] = float(c.amul()) / 256.f;
            a[0] = float(c.radd()) / 256.f;
            a[1] = float(c.gadd()) / 256.f;
            a[2] = float(c.badd()) / 256.f;
            a[3] = float(c.aadd()) / 256.f;
        }
        inline float* mutableMultiplier(){ return m; }
        inline float* mutableOffset(){ return a; }
        inline const float* multiplier()const{ return m; }
        inline const float* offset()const{ return a; }
        ColorTransform multiply( const ColorTransform& c ) const
        {
            ColorTransform o;
            o.m[0] = m[0]*c.m[0]; o.a[0] = a[0]*c.m[0] + c.a[0];
            o.m[1] = m[1]*c.m[1]; o.a[1] = a[1]*c.m[1] + c.a[1];
            o.m[2] = m[2]*c.m[2]; o.a[2] = a[2]*c.m[2] + c.a[2];
            o.m[3] = m[3]*c.m[3]; o.a[3] = a[3]*c.m[3] + c.a[3];
            return o;
        }

        inline bool operator == (const ColorTransform& s) const
        {
            return std::equal(std::begin(m), std::end(m), s.m)
              && std::equal(std::begin(a), std::end(a), s.a);
        }
        inline bool operator != (const ColorTransform& s) const
        {
            return !std::equal(std::begin(m), std::end(m), s.m)
              || !std::equal(std::begin(a), std::end(a), s.a);
        }
    };
}
