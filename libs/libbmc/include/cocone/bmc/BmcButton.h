// BmcButton.h
//
#pragma once
#include <cocone/foundation/Sharable.h>

namespace cocone {
    class BmcSymbol;

    class BmcButton : /*virtual*/ public AsRef, public UnknownInterface
//    class BmcButton : public cocos2d::Ref, public UnknownInterface
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcButton);
        CCN_DECLARE_IID(BmcButton);

        virtual bool operator == (const std::nullptr_t) const = 0;
        virtual bool operator != (const std::nullptr_t) const = 0;
        virtual explicit operator bool() const = 0;

        inline BmcSymbol* asSymbol(){ return query<BmcSymbol>(); }

        virtual void setPushSoundHandler(std::function<void(void)> soundPlay) = 0;
        virtual void setPushHandler( std::function<bool (BmcButton*)> handler )=0;
        virtual void setPushingHandler( std::function<bool (BmcButton*)> handler )=0;
        virtual void setTouchBeganHandler(std::function<bool(BmcButton*)> handler)=0;
        virtual void setEnabled( bool enabled )=0;
        virtual void setEventBlock( bool block )=0;
        virtual void setCheckBoxMode( bool checkbox )=0;
        virtual bool getCheckBoxMode()const=0;
        virtual void setCheck( bool check, bool playSound )=0;//default animated = true
        virtual void setCheck( bool check, bool playSound , bool animated)=0;
        virtual bool getCheck()const=0;
        virtual void setBarrageBlockTime(float blockSec=0.2f) = 0;	// 連打防止時間の設定
        virtual float getBarrageBlockTime() const = 0;
        virtual void setTouchEnabled(bool enable) = 0;
        virtual void setSwallowTouches( bool swallow ) = 0;
        virtual void touchDownAction() = 0;
        virtual void setSimultaneousPress(bool enabled) = 0; // default false
        virtual bool getSimultaneousPress() const = 0;

    protected:
        BmcButton(){}
        virtual ~BmcButton(){}
    };

} // cocone
