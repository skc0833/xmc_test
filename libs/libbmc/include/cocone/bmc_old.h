﻿// bmc.h
//
#pragma once

//#define USE_COCOS_SPRITE_LOGIC;

#include "bmc/protocol/BmcFile.pb.h"

#include "bmc/BmcFactory.h"
#include "bmc/BmcGeom.h"
#include "bmc/BmcSymbol.h"
#include "bmc/BmcLoader.h"
#include "bmc/BmcSoundPlayer.h"
#include "bmc/BmcMovieClip.h"
#include "bmc/BmcShape.h"
#include "bmc/BmcButton.h"
#include "bmc/BmcShader.h"
#include "bmc/BmcTexture.h"
#include "bmc/BmcTexturePart.h"
#include "bmc/BmcTextureReloader.h"
#include "bmc/BmcTextureReloadManager.h"
//
//#include "foundation/ApplicationContext.h"
//#include "foundation/BoolLock.h"
//#include "foundation/Component.h"
//#include "foundation/ComponentContainer.h"
//#include "foundation/Finally.h"
//#include "foundation/Interceptor.h"
//#include "foundation/Log.h"
//#include "foundation/MainThread.h"
//#include "foundation/SerialCommnadManager.h"
//#include "foundation/Sharable.h"
//#include "foundation/SystemPreference.h"
