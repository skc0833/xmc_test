﻿// BmcTextureReloader.h
//
#pragma once
#include <cocone/foundation/Sharable.h>

#include <cocone/bmc/BmcTextureReloadManager.h>

namespace cocone {

    class BmcTexture;
    class BmcScalingGridShape;
//    class BmcTextureReloader : public ZeroRefCount<cocos2d::Ref>
    class BmcTextureReloader : public AsRef
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcTextureReloader);

        virtual void init(const std::string& bmcFilePath, int imageSize ) = 0;
        virtual void registReloaderManager(BmcTextureReloadManager::Ptr reloaderManager) = 0;

        virtual void regist(int index, BmcTexture* weakPtr) = 0;
        virtual void unregist(int index) = 0;
        virtual void regist(BmcScalingGridShape* weakPtr) = 0;
        virtual void unregist(BmcScalingGridShape* weakPtr) = 0;

        virtual void reload() = 0;
    protected:
        virtual ~BmcTextureReloader(){}
    };

} // cocone
