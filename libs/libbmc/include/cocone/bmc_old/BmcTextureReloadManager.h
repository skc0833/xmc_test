﻿// BmcTextureReloadManager.h
//
#pragma once

namespace cocone {

    class BmcTextureReloader;
    class BmcTextureReloadManager : public AsRef
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcTextureReloadManager);

        static BmcTextureReloadManager& getInstance();
        

        virtual void regist(BmcTextureReloader* reloader) = 0;
        virtual void unregist(BmcTextureReloader* reloader) = 0;

        virtual void reload() = 0;
    protected:
        BmcTextureReloadManager(){}
        ~BmcTextureReloadManager(){}
    };

} // cocone
