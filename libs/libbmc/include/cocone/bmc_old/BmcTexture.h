﻿// BmcTexture.h
//
#pragma once
#include <cocone/foundation/Sharable.h>
#include <cocone/bmc/BmcTextureReloader.h>

namespace cocone {
    namespace bmc {
        class BmcImage;
    };


//    class BmcTexture : public ZeroRefCount<cocos2d::Ref>
    class BmcTexture : public AsRef
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcTexture);

        virtual void init(int index, const bmc::BmcImage* image) = 0;
        virtual void reInit(int index, const bmc::BmcImage* image) = 0;
        virtual void registReloader(BmcTextureReloader::Ptr reloader) = 0;
        virtual cocos2d::Texture2D* getCCTexture2D() = 0;

    protected:
        virtual ~BmcTexture(){}
    };

} // cocone
