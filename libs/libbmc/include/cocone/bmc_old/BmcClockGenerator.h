//
// BmcClockGenerator.h

#pragma once

#include <unordered_map>
#include <cocos2d.h>

#include <cocone/foundation/Sharable.h>
#include <cocone/bmc/BmcMovieClip.h>

namespace cocone {
  
class BmcClockGenerator
  : public ZeroRefCount<cocos2d::Ref>, public AsRef
{
public:
  CCN_SHARABLEPTR_DEFINE(BmcClockGenerator);
  static BmcClockGenerator::Ptr create();

  virtual cocos2d::Ref* asRef() override { return this; }
  virtual const cocos2d::Ref* asRef() const override { return this; }
  float getSpeedScale() const;
  void setSpeedScale(float speedScale); // NOTE: ClockGenerator は BMCファイル単位で保持するので、ファイル単位で加速します
  float getOneFrameDelta() const;
  void setOneFrameDelta(float oneFrameDelta);
  void update(float delta);
  void registerListener(const BmcMovieClip::Ptr& mc);
  void unregisterListener(const BmcMovieClip::Ptr& mc);
  void unregisterAllListeners();

private:

  explicit BmcClockGenerator();
  ~BmcClockGenerator(); //skc add
  void notify_listeners();

private:
  float _speed_scale = 1.f; // 使いみちが分からない スローモーションとか？
  float _one_frame_delta = 1.f / 30.f; // とりあえず30fps固定で

  bool _scheduled;
  float _stock_time;
  std::unordered_map<BmcMovieClip*, BmcMovieClip::Ptr> _listeners;
  std::unordered_map<BmcMovieClip*, BmcMovieClip::Ptr> _unregister_reservation;
};

}
