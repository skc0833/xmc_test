﻿// BmcButton.h
//
#pragma once
#include <cocone/foundation/Sharable.h>

namespace cocone {
    class BmcSymbol;

    class BmcButton : virtual public AsRef, public UnknownInterface
//    class BmcButton : public cocos2d::Ref, public UnknownInterface
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcButton);
        CCN_DECLARE_IID(BmcButton);

        inline BmcSymbol* asSymbol(){ return query<BmcSymbol>(); }

        virtual void setPushSoundHandler(std::function<void(void)> soundPlay) = 0;
        virtual void setPushHandler( std::function<bool (BmcButton*)> handler )=0;
        virtual void setEnabled( bool enabled )=0;
        virtual void setCheckBoxMode( bool checkbox )=0;
        virtual bool getCheckBoxMode()const=0;
        virtual void setCheck( bool check, bool playSound )=0;//default animated = true
        virtual void setCheck( bool check, bool playSound , bool animated)=0;
        virtual bool getCheck()const=0;

        virtual void setTouchDisable() = 0;
    protected:
        BmcButton(){}
        virtual ~BmcButton(){}
    };

} // cocone
