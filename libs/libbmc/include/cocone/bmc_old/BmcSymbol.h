﻿// BmcSymbol.h
//
#pragma once

#include <cocone/foundation/Sharable.h>
#include <cocone/foundation/Log.h>

#include "BmcGeom.h"

namespace cocone
{
    class BmcLibrary;
//    class BmcSymbol : public cocos2d::Ref, public UnknownInterface
    class BmcSymbol : virtual public AsRef, public UnknownInterface
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcSymbol);
        CCN_DECLARE_IID(BmcSymbol);

        virtual const cocos2d::Node* asNode() const = 0;
        virtual cocos2d::Node* asNode() = 0;

        const cocos2d::Ref* asRef() const override { return asNode(); }
        cocos2d::Ref* asRef() override { return asNode(); }
        inline operator const cocos2d::Node*() const { return asNode(); }
        inline operator cocos2d::Node*() { return asNode(); }


        virtual BmcSymbol::Ptr clone() const = 0;

        // タイムラインを所持しているか
        virtual bool hasTimeline()const=0;

        /// 変更予定
        virtual const bmc::BmcTimeline& getTimeline()const=0;

        /// 指定したインスタンス名で配置されているインスタンスを取得する。
        virtual BmcSymbol::Ptr getInstance( const std::string& instanceName ) const = 0;
        virtual BmcSymbol::Ptr getNullObject() const = 0;
        template<class T>
        CCNRefPtr<T> getInstance(const std::string& instanceName) const
        {
            auto instance = getInstance(instanceName);
            if (auto cast = dynamic_cast<T*>(instance.get())) {
                return CCNRefPtr<T>(cast);
            }
            return getNullObject();
        }
        /// 配置されたインスタンスが一つのみの場合にそれを取得する
        virtual BmcSymbol::Ptr getInstanceOne() const = 0;
        /// すべてのインスタンスを取得する
        virtual void getAllInstances( std::vector<BmcSymbol::Ptr>* out ) const = 0;

        /// 配置されているインスタンスを置き換える
        virtual void overwriteInstance( BmcSymbol::Ptr current, BmcSymbol::Ptr over )=0;
        /// 指定された名前を持つインスタンスすべてを置き換える
        virtual void overwriteInstances(const std::string& instanceName, BmcSymbol::Ptr over) = 0;

        /// インスタンスを破棄する。（破棄すると戻せない）破棄したインスタンスを返す。保持しなけれればメモリから削除される
        virtual BmcSymbol::Ptr destroyInstance( const std::string& instanceName )=0;
        /// インスタンスを破棄する。（破棄すると戻せない）破棄したインスタンスを返す。保持しなけれればメモリから削除される
        virtual void destroyInstance( BmcSymbol::Ptr instance )=0;

        /// インスタンスすべてに対して処理を行う
        virtual void iterateInstances(std::function<void(BmcSymbol*)> func) = 0;
        virtual void iterateInstancesForName(std::function<void(const std::string& instanceName, BmcSymbol*)> func) = 0;
        /// 指定した名前を持つインスタンスすべてに対して処理を行う
        virtual void iterateInstancesByName(const std::string& instanceName, std::function<void(BmcSymbol*)> func) = 0;
        /// 子シンボル含め、再帰的にインスタンスすべてに対して処理を行う
        virtual void iterateInstancesRecursively(std::function<void(BmcSymbol*)> func) = 0;
        /// 子シンボル含め、再帰的に指定した名前を持つインスタンスすべてに対して処理を行う
        virtual void iterateInstancesByNameRecursively(const std::string& instanceName, std::function<void(BmcSymbol*)> func) = 0;

        // protected にしたい
        virtual cocos2d::Size setupContentSize()=0;

        virtual void setBlendMode( bmc::BmcBlendMode blendMode )=0;

        virtual void setAdditionalTransform(const cocos2d::Mat4& mat) =0;
        virtual void setAdditionalTransform( const cocos2d::AffineTransform& t )=0;
        virtual const cocos2d::Mat4& getAdditionalTransform() const = 0;
        virtual cocos2d::AffineTransform getAdditionalAffineTransform() const = 0;
        virtual void clearAdditonalTransform()=0;

        // Flash上で指定されている色変更設定を上書きする
        virtual void setAdditonalColorTransform(const ColorTransform& additionalColorTransform) = 0;
        /// Flash上で指定されている色変更設定を取得する
        virtual const ColorTransform& getAdditionalColorTransform()const = 0;
        /// Node 側の色変更設定とFlash上の色変更設定を掛け合わせて最終的な色変更設定を取得する。
        virtual ColorTransform getColorTransform()const = 0;

        virtual cocos2d::Rect getCurrentFrameBoundingBox() const = 0;
        virtual bool inHitArea( cocos2d::Point point ) = 0;

        virtual void addLibrary(BmcLibrary& library) = 0;
        
    protected:
        BmcSymbol() = default;
        virtual ~BmcSymbol() = default;
    };

    inline BmcSymbol* areYouSymbol(cocos2d::Node* p){
        BmcSymbol* s = dynamic_cast<BmcSymbol*>(p);
        if (s == nullptr) {
//        if (ccn_econd(s == nullptr, "p(%p) is not BmcSymbol.", p)){
            return nullptr;
        }
        return s;
    }
    inline const BmcSymbol* areYouSymbol(const cocos2d::Node* p){
        const BmcSymbol* s = dynamic_cast<const BmcSymbol*>(p);
        if (s == nullptr) {
//        if (ccn_econd(s == nullptr, "p(%p) is not BmcSymbol.", p)){
            return nullptr;
        }
        return s;
    }
} // cocone
