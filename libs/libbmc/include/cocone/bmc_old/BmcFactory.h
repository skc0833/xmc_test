﻿// BmcButton.h
//
#pragma once

#include <cocone/bmc/BmcLoader.h>
#include <cocone/bmc/BmcMovieClip.h>
#include <cocone/bmc/BmcButton.h>
#include <cocone/bmc/BmcShape.h>
#include <cocone/bmc/BmcScalingGridShape.h>
#include <cocone/bmc/BmcShader.h>
#include <cocone/bmc/BmcTexture.h>
#include <cocone/bmc/BmcTexturePart.h>
#include <cocone/bmc/BmcTextureReloader.h>
#include <cocone/bmc/BmcTextureReloadManager.h>
#include <cocone/bmc/BmcLibrary.h>
#include <cocone/bmc/BmcSoundPlayer.h>

namespace cocone {

    class BmcFactory
    {
    public:
        static BmcLoader::Ptr createBmcLoader();
        static BmcMovieClip::Ptr createBmcMovieClip();
        static BmcButton::Ptr createBmcButton();
        static BmcShape::Ptr createBmcShape();
        static BmcScalingGridShape::Ptr createBmcScalingGridShape();
        static BmcShader::Ptr createBmcShader();
        static BmcTexture::Ptr createBmcTexture();
        static BmcTexturePart::Ptr createBmcTexturePart();
        static BmcTextureReloader::Ptr createBmcTextureReloader();
        static BmcTextureReloadManager::Ptr createBmcTextureReloadManager();
        //static BmcLibrary::Ptr createBmcLibrary();
        static BmcSoundPlayer::Ptr createBmcSoundPlayer();

        //        static RefPtr<class BmcShape> createBmcShape();

    protected:
        BmcFactory(){}
        virtual ~BmcFactory(){}
    };
} // cocone
