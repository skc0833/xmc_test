﻿// BmcMovieClip.h
//
#pragma once
#include "BmcSymbol.h"
#include "BmcGeom.h"

namespace cocone {

    class BmcMovieClip : virtual public AsRef, public UnknownInterface
//    class BmcMovieClip : public cocos2d::Ref, public UnknownInterface
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcMovieClip);
        CCN_DECLARE_IID(BmcMovieClip);

        enum EventType
        {
            EventType_PlayEndFrame, // ループ指示で再生ヘッドが移動する、または再生すべき最終フレームに移動したときに呼ばれます。
        };

        inline BmcSymbol* asSymbol(){ return query<BmcSymbol>(); }

        virtual void init( class BmcLoader* source, const bmc::BmcTimeline& bmcTimeline )=0;

        /// 再生ヘッドが置かれているフレームの番号を示します。
        virtual int getCurrentFrame()const=0;
        /// 再生ヘッドが置かれている現在のラベルです。ラベルがない場合、空のstd::stringを返します。
        virtual const std::string& getCurrentLabel()const=0;
        /// フレーム総数です。
        virtual int getTotalFrames()const=0;
        /// 指定したフレームラベルが設定されている開始フレームを取得します。
        virtual int getFrameByLabel( const char* label )const=0;

        /// 指定されたフレーム番号で再生を開始します。無効な番号が渡された場合無視されます。
        virtual void gotoAndPlay( int frameNumber )=0;
        /// 再生フレームを子シンボル含めて再帰的に先頭に移動させます。
        virtual void gotoTopAndPlayRecursively() = 0;
        /// 指定されたフレーム番号で再生を開始し、指定されたカウントで再生をストップ、またはループします。
        virtual void gotoAndPlay( int frameNumber, int count )=0;
        /// 指定されたフレームラベルを持つフレームから次のフレームラベルがある直前のフレーム(または最終フレーム）まで再生します。
        /// ループ設定がされている場合、指定したフレームラベル基準でループ再生します。
        virtual void gotoAndPlayToNextLabel( const char* label )=0;
        /// 指定されたフレームラベルから指定されたフレームラベル(または最終フレーム）まで再生します。
        virtual void gotoAndPlayLabel( const char* labelFrom, const char* labelTo = 0 )=0;
        /// 指定されたフレーム番号がある直前のフレーム(または最終フレーム）まで再生します。
        /// ループ設定がされている場合、指定したフレームラベル基準でループ再生します。
        virtual void gotoAndPlayToNextFrame( int frameNumber )=0;
        /// 指定されたフレーム番号に移動し、そこで停止させます。
        virtual void gotoAndStop( int frameNumber )=0;
        /// 現在のフレームから再生を開始します。
        virtual void play()=0;
        /// 現在のフレームで再生を停止します。
        virtual void stop()=0;
        /// 現在のフレームで全ての再生を停止します。
        virtual void stopAll() = 0;

        /// 最終フレームまで再生後、先頭フレームに再生ヘッドを移動するかどうかを指定します。デフォルトは移動する（ループする）です。
        /// このフラグはフレームラベル指定にも影響します。
        virtual void setLoop( bool isLoop )=0;

        /// デフォルトのスピードに掛け算する比率を設定します。
        virtual void setSpeedScale(float scale)=0;
        virtual float getSpeedScale()const=0;

        virtual void setEventListener( std::function<void (EventType, BmcMovieClip*)> listener )=0;
        virtual void clearEventListener()=0;
        // label callback
        virtual void setLabelCallback( const std::string& labelName, std::function<void(BmcMovieClip*)> listener )=0;
        virtual void clearLabelCallback()=0;
        
        virtual void nextFrame(bool child) = 0;
        virtual void prevFrame(bool child) = 0;
        virtual void updateFrameForView() = 0;
        virtual void processFrame() = 0;

    protected:
        BmcMovieClip(){}
        ~BmcMovieClip(){}
    };

} // cocone
