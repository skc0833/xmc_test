﻿// BmcShape.h
//
#pragma once

#include <cocone/foundation/Sharable.h>

#include <cocone/bmc/BmcShader.h>
#include <cocone/bmc/BmcTexturePart.h>

namespace cocone {
    class BmcSymbol;

    class BmcShape : virtual public AsRef, public UnknownInterface
//    class BmcShape : public cocos2d::Ref, public UnknownInterface
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcShape);
        CCN_DECLARE_IID(BmcShape);

        inline BmcSymbol* asSymbol(){ return query<BmcSymbol>(); }

        virtual void init( BmcShader::Ptr shader, BmcTexturePart::Ptr texture) = 0;
    protected:
        virtual ~BmcShape() = default;
    };

} // cocone
