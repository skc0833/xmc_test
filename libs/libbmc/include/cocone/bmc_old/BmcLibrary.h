﻿//
//  BmcLibrary.h
//  PocketColony
//
//  Created by 洪 起萬 on 2014/05/30.
//
//

#pragma once
#include <cocone/foundation/Sharable.h>

#include <cocone/bmc/BmcSymbol.h>

namespace cocone
{
    class BmcLibrary : public ZeroRefCount<cocos2d::Ref>
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcLibrary);
        
        virtual void loadLibraryByName(const std::string& name) = 0;
        virtual void loadLibraryByName(const std::vector<std::string>& names) = 0;
        virtual void loadLibrary(const std::string& fullPath) = 0;
        virtual void loadLibrary(const std::vector<std::string>& fullPath) = 0;
        
        virtual BmcSymbol::Ptr getSymbol(const std::string &referenced_key) = 0;
        virtual void iterateBySymbol(std::function<void(const std::string&, BmcSymbol::Ptr)> f) = 0;
    protected:
        BmcLibrary(){}
        virtual ~BmcLibrary(){}
    };
}
