﻿// BmcTexturePart.h
//
#pragma once

#include <cocone/foundation/Sharable.h>

#include "protocol/bmcFile.pb.h"


namespace cocone {

    class BmcTexture;

//    class BmcTexturePart : public ZeroRefCount<cocos2d::Ref>
    class BmcTexturePart : virtual public AsRef
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcTexturePart);

        virtual BmcTexture* getTexture()=0;
        virtual cocos2d::Size getTextureSize()const=0;
        virtual const bmc::BmcRectangle& getRectangle()const=0;
        virtual const cocos2d::Vec2& getOffset() const = 0;
        virtual int getTexLeft()const=0;
        virtual int getTexTop()const=0;
        virtual bool hasScale9Grid()const=0;
        virtual const bmc::BmcRectangle& getScale9GridRectangle()const=0;
        virtual void init( BmcTexture* tex, const bmc::BmcTextureRef& ref )=0;

    protected:
        virtual ~BmcTexturePart(){}
    };


} // cocone
