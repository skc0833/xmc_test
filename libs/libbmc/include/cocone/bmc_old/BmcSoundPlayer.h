﻿// BmcSoundPlayer.h
//
#pragma once
#include <cocone/foundation/Sharable.h>

#include "protocol/BmcFile.pb.h"

namespace cocone {

//    class BmcSoundPlayer : public ZeroRefCount<cocos2d::Ref>
    class BmcSoundPlayer : virtual public AsRef
    {
    public:
        CCN_SHARABLEPTR_DEFINE(BmcSoundPlayer);

        virtual void initSoundTable(const ::google_public::protobuf::RepeatedPtrField<std::string>& soundTable) = 0;
        virtual void initFunctions(
              std::function<void(const std::string& soundFilePath)> preloadFunction
            , std::function<void(const std::string& soundFilePath)> playFunction) = 0;
        virtual void playEffect(int index) = 0;

    protected:
        virtual ~BmcSoundPlayer(){}
    };

} // cocone
