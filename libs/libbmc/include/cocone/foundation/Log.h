// Log.h
// cocone log macros 
//
// include example.
// #define COCONE_DLOG_DISABLED // if this define is enabled, all ccn_dlog called in this file is ignored. 
// #include <cocone/foundation/Log.h>

#pragma once
#include <string>

// #if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
// #if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)

#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)
#define __func__ __FUNCTION__
#endif

namespace cocone{
	enum LogPrefix {
		LogPrefix_Error=0,
		LogPrefix_Warning,
		LogPrefix_Info,
		LogPrefix_Save,
		LogPrefix_Debug,
		LogPrefix_CountOfLogPrefix,
	};
    void consoleOutput( LogPrefix prefix, const char* str );
	std::string formatString( const char* format, ... );
	std::string* formatString( std::string* out, const char* format, ... );
    bool logOutputCond( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* condStr, const char* format, ... );
    bool logOutputCond( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* condStr);
    bool logOutput( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt, const char* format, ... );
    bool logOutput( bool isBreak, LogPrefix prefix, const char* funcStr, const char* fileStr, int lineInt );
}

// error 
#define ccn_econd(cond,...) ( (cond) && cocone::logOutputCond(true, cocone::LogPrefix_Error, __func__,__FILE__,__LINE__,#cond,##__VA_ARGS__))
#define ccn_elog(...) { cocone::logOutput(true, cocone::LogPrefix_Error, __func__,__FILE__,__LINE__,__VA_ARGS__); }
#ifdef NDEBUG
#define ccn_assert(cond,...) ((void)0)
#else
#define ccn_assert(cond,...) ( (cond) ? (void)0 : (void)cocone::logOutputCond(true, cocone::LogPrefix_Error, __func__,__FILE__,__LINE__,#cond,##__VA_ARGS__) )
#endif

// warning
#define ccn_wcond(cond,...) ( (cond) && cocone::logOutputCond(false,cocone::LogPrefix_Warning, __func__,__FILE__,__LINE__,#cond,##__VA_ARGS__) )
#define ccn_wlog(...) { cocone::logOutput(false, cocone::LogPrefix_Warning, __func__,__FILE__,__LINE__,__VA_ARGS__); }

// info
#define ccn_ilog(...) { cocone::logOutput(false, cocone::LogPrefix_Info, __func__,__FILE__,__LINE__,__VA_ARGS__); }

// save
#define ccn_slog(...) { cocone::logOutput(false, cocone::LogPrefix_Save, __func__,__FILE__,__LINE__,__VA_ARGS__); }

// debug
#if defined(NDEBUG) || defined(COCONE_DLOG_DISABLED)
#define ccn_dlog(...) ((void)0)
#else
#define ccn_dlog(...) { cocone::logOutput(false, cocone::LogPrefix_Debug,__func__,__FILE__,__LINE__,__VA_ARGS__); }
#endif

#if 0
template <typename T, typename... Args>
void printf(const char* s, const T& value, const Args&... args) {
	while (*s) {
		if (*s == '%' && *++s != '%') {
			std::cout << value;
			return printf(++s, args...);
		}

		std::cout << *s++;
	}
	throw std::runtime_error("extra arguments provided to printf");
}
#endif
