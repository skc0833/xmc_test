﻿//
// Singleton.h

#include <memory>
#include <iostream>

namespace cocone {

template <class Derived>
struct Singleton
{
public:
  // 対象とする派生クラスのインスタンスへの参照を得る
  template <class... Args>
  static Derived& singleton(Args... args)
  {
    if (s_singleton.get() == nullptr) {
      s_singleton = std::move(
        singleton_pointer_type(createInstance(args...))
      );
    }
    return getReference(s_singleton);
  }

private:
  typedef std::unique_ptr<Derived> singleton_pointer_type;

  // インスタンス本体を格納するポインタ
  static singleton_pointer_type s_singleton;

  template <class... Args>
  inline static Derived* createInstance(Args... args) {
    return new Derived(args...);
  }

  inline static Derived& getReference(singleton_pointer_type const& ptr) {
    return *ptr;
  }

protected:
  explicit Singleton() {}

private:
  explicit Singleton(Singleton const&) = delete;
  Singleton& operator =(Singleton const&) = delete;
  explicit Singleton(Singleton &&) = delete;
  Singleton& operator =(Singleton &&) = delete;
};

// 初期化されているかどうかを判定するため nullptr で初期化しておく
template <class Derived>
typename Singleton<Derived>::singleton_pointer_type
    Singleton<Derived>::s_singleton = std::unique_ptr<Derived>(nullptr);

}
