// Sharable.h
//
#pragma once
#include <cocos2d.h>


//            const_cast<cocos2d::Ref*>(static_cast<const cocos2d::Ref*>(ptr))->retain();
#define CC_REF_PTR_SAFE_RETAIN(ptr)\
    \
    do\
    {\
        if (ptr)\
        {\
            ptr->asRef()->retain();\
        }\
    \
    }   while (0);

//            const_cast<cocos2d::Ref*>(static_cast<const cocos2d::Ref*>(ptr))->release();
#define CC_REF_PTR_SAFE_RELEASE(ptr)\
    \
    do\
    {\
        if (ptr)\
        {\
            ptr->asRef()->release();\
        }\
    \
    }   while (0);

//            const_cast<cocos2d::Ref*>(static_cast<const cocos2d::Ref*>(ptr))->release();
#define CC_REF_PTR_SAFE_RELEASE_NULL(ptr)\
    \
    do\
    {\
        if (ptr)\
        {\
            ptr->asRef()->release();\
            ptr = nullptr;\
        }\
    \
    }   while (0);

namespace cocone {

    /// cocos2dの Node や、Ref から継承したいが、初期状態で_referenceCountが1になり、RefPtrを使用すると問題がある場合に使用する。
    /// (autoreleaseを使用したくないとき）
    template< typename _Cocos2dRefObject >
    class ZeroRefCount : public _Cocos2dRefObject
    {
    protected:
        ZeroRefCount()
        {
#if 1 //skc test
            this->autorelease();
#else
            this->_referenceCount = 0;
#endif
        }
        ~ZeroRefCount() {
            int r = this->_referenceCount;
        }
    };

    /// 本来は Ref であるが、Interfaceの段階ではそれを示せない。
    // だが、InterfaceとしてRefとして振舞いたいとき（RefPtrを使用したいとき）に、RefであることをInterface実装者に任せる場合に使用する。
    class AsRef
    {
    public:
        virtual const cocos2d::Ref* asRef() const = 0;
        virtual cocos2d::Ref* asRef() = 0;

        inline operator const cocos2d::Ref*() const { return asRef(); }
        inline operator cocos2d::Ref*() { return asRef(); }
    };

    typedef const char * IIDType;
    #define CCN_DECLARE_IID(_class) static IIDType iid(){ static const IIDType _iid = #_class"@cocone"; return _iid; }
    class UnknownInterface
    {
    public:
        virtual void* query(IIDType iid) = 0;
        template<class _I> inline _I* query() {
//            return reinterpret_cast<_I*>(query(_I::iid()));
            auto iid = _I::iid();
            auto ret = query(iid);
            return static_cast<_I*>(ret);
        }
    };

    template <typename T> class CCNRefPtr
    {
    public:
        
        inline CCNRefPtr()
        :
            _ptr(nullptr)
        {
            
        }
        
        inline CCNRefPtr(CCNRefPtr<T> && other)
        {
            _ptr = other._ptr;
            other._ptr = nullptr;
        }

        inline CCNRefPtr(T * ptr)
        {
            _ptr = const_cast<typename std::remove_const<T>::type*>(ptr);     // Const cast allows CCNRefPtr<T> to reference objects marked const too.
//            _ptr = static_cast<cocos2d::Ref*>(ptr);
            CC_REF_PTR_SAFE_RETAIN(_ptr);
        }
        
        inline CCNRefPtr(std::nullptr_t ptr)
        :
            _ptr(nullptr)
        {
            
        }
        
        inline CCNRefPtr(const CCNRefPtr<T> & other)
        :
            _ptr(other._ptr)
        {
            CC_REF_PTR_SAFE_RETAIN(_ptr);
        }
        
        inline ~CCNRefPtr()
        {
            //CC_REF_PTR_SAFE_RELEASE_NULL(_ptr); //skc test
            if (_ptr)
            {
                _ptr->asRef()->release();
                _ptr = nullptr;
            }
        }
        
        inline CCNRefPtr<T> & operator = (const CCNRefPtr<T> & other)
        {
            if (other._ptr != _ptr)
            {
                CC_REF_PTR_SAFE_RETAIN(other._ptr);
                CC_REF_PTR_SAFE_RELEASE(_ptr);
                _ptr = other._ptr;
            }
            
            return *this;
        }
        
        inline CCNRefPtr<T> & operator = (CCNRefPtr<T> && other)
        {
            if (&other != this)
            {
                CC_REF_PTR_SAFE_RELEASE(_ptr);
                _ptr = other._ptr;
                other._ptr = nullptr;
            }
            
            return *this;
        }
        
        inline CCNRefPtr<T> & operator = (T * other)
        {
            if (other != _ptr)
            {
                CC_REF_PTR_SAFE_RETAIN(other);
                CC_REF_PTR_SAFE_RELEASE(_ptr);
                _ptr = const_cast<typename std::remove_const<T>::type*>(other);     // Const cast allows CCNRefPtr<T> to reference objects marked const too.
            }
            
            return *this;
        }
        
        inline CCNRefPtr<T> & operator = (std::nullptr_t other)
        {
            CC_REF_PTR_SAFE_RELEASE_NULL(_ptr);
            return *this;
        }
        
        // Note: using reinterpret_cast<> instead of static_cast<> here because it doesn't require type info.
        // Since we verify the correct type cast at compile time on construction/assign we don't need to know the type info
        // here. Not needing the type info here enables us to use these operations in inline functions in header files when
        // the type pointed to by this class is only forward referenced.
        
        inline operator T * () const { return reinterpret_cast<T*>(_ptr); }
        
        inline T & operator * () const
        {
            CCASSERT(_ptr, "Attempt to dereference a null pointer!");
            return reinterpret_cast<T&>(*_ptr);
        }
        
        inline T * operator->() const
        {
            CCASSERT(_ptr, "Attempt to dereference a null pointer!");
            return reinterpret_cast<T*>(_ptr);
        }
        
        inline T * get() const { return reinterpret_cast<T*>(_ptr); }
        
        
        inline bool operator == (const CCNRefPtr<T> & other) const { return _ptr == other._ptr; }
        
        inline bool operator == (const T * other) const { return _ptr == other; }
        
        inline bool operator == (typename std::remove_const<T>::type * other) const { return _ptr == other; }
        
        inline bool operator == (const std::nullptr_t other) const { return _ptr == other; }
        
        
        inline bool operator != (const CCNRefPtr<T> & other) const { return _ptr != other._ptr; }
        
        inline bool operator != (const T * other) const { return _ptr != other; }
        
        inline bool operator != (typename std::remove_const<T>::type * other) const { return _ptr != other; }
        
        inline bool operator != (const std::nullptr_t other) const { return _ptr != other; }
        
        
        inline bool operator > (const CCNRefPtr<T> & other) const { return _ptr > other._ptr; }
        
        inline bool operator > (const T * other) const { return _ptr > other; }
        
        inline bool operator > (typename std::remove_const<T>::type * other) const { return _ptr > other; }
        
        inline bool operator > (const std::nullptr_t other) const { return _ptr > other; }
        
        
        inline bool operator < (const CCNRefPtr<T> & other) const { return _ptr < other._ptr; }
        
        inline bool operator < (const T * other) const { return _ptr < other; }
        
        inline bool operator < (typename std::remove_const<T>::type * other) const { return _ptr < other; }
        
        inline bool operator < (const std::nullptr_t other) const { return _ptr < other; }
        
            
        inline bool operator >= (const CCNRefPtr<T> & other) const { return _ptr >= other._ptr; }
        
        inline bool operator >= (const T * other) const { return _ptr >= other; }
        
        inline bool operator >= (typename std::remove_const<T>::type * other) const { return _ptr >= other; }
        
        inline bool operator >= (const std::nullptr_t other) const { return _ptr >= other; }
        
            
        inline bool operator <= (const CCNRefPtr<T> & other) const { return _ptr <= other._ptr; }
        
        inline bool operator <= (const T * other) const { return _ptr <= other; }
        
        inline bool operator <= (typename std::remove_const<T>::type * other) const { return _ptr <= other; }
        
        inline bool operator <= (const std::nullptr_t other) const { return _ptr <= other; }
        
            
        inline operator bool() const { return _ptr != nullptr; }
            
        inline void reset()
        {
            CC_REF_PTR_SAFE_RELEASE_NULL(_ptr);
        }
            
        inline void swap(CCNRefPtr<T> & other)
        {
            if (&other != this)
            {
                T* tmp = _ptr;
                _ptr = other._ptr;
                other._ptr = tmp;
            }
        }
        
        /**
         * This function assigns to this CCNRefPtr<T> but does not increase the reference count of the object pointed to.
         * Useful for assigning an object created through the 'new' operator to a CCNRefPtr<T>. Basically used in scenarios
         * where the CCNRefPtr<T> has the initial ownership of the object.
         *
         * E.G:
         *      CCNRefPtr<cocos2d::Image> image;
         *      image.weakAssign(new cocos2d::Image());
         *
         * Instead of:
         *      CCNRefPtr<cocos2d::Image> image;
         *      image = new cocos2d::Image();
         *      image->release();               // Required because new'd object already has a reference count of '1'.
         */
        inline void weakAssign(const CCNRefPtr<T> & other)
        {
            CC_REF_PTR_SAFE_RELEASE(_ptr);
            _ptr = other._ptr;
        }
        
    private:
        T * _ptr;
    };

    template<typename T1, typename T2> inline T1* interface_cast(T2* p)
    {
        return dynamic_cast<T1*>(p);
    }
    template<class T1, class T2> inline CCNRefPtr<T1> interface_cast(CCNRefPtr<T2> const& p)
    {
        return dynamic_cast<T1*>(p.get());
    }
}

#define CCN_SHARABLEPTR_DEFINE(_class) typedef cocone::CCNRefPtr<_class> Ptr
//#define CCN_SHARABLEPTR_DEFINE(_class) typedef cocos2d::RefPtr<_class> Ptr
#define CCN_DECLARE_ASNODE_IMPL() \
    cocos2d::Node* asNode() override { return this; } \
    const cocos2d::Node* asNode() const override { return this; } \
    cocos2d::Ref* asRef() override { return asNode(); } \
    const cocos2d::Ref* asRef() const override { return asNode(); }
#define CCN_DECLARE_ASREF_IMPL() \
    cocos2d::Ref* asRef() override { return this; } \
    const cocos2d::Ref* asRef() const override { return this; }

#undef CC_REF_PTR_SAFE_RETAIN
#undef CC_REF_PTR_SAFE_RELEASE
#undef CC_REF_PTR_SAFE_RELEASE_NULL
