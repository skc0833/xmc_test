﻿//
// ClockMeter.h

#include <string>
#include <chrono>

namespace cocone {
  
class ClockMeter
{
public:
  explicit ClockMeter(bool run = false);
  void reset();
  double elapsed() const; // milliseconds

protected:
  std::chrono::high_resolution_clock::time_point start;
};

class ScopedClockMeter
  : public ClockMeter
{
public:
  explicit ScopedClockMeter(const std::string& file, int lineno);
  ~ScopedClockMeter();

private:
  std::string _file;
  int _lineno;
};

#define SCOPED_CLOCK_METER  cocone::ScopedClockMeter __FILE__ ## __LINE__(__FILE__, __LINE__)

}
