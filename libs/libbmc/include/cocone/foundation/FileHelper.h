﻿//
//  FileHelper.h
//
//

#pragma once

#include <string>

namespace cocone {
namespace filehelper {
  
  // ruby の File っぽいやつら
  std::string absolute_path(const std::string& relative_path);
  std::string basename(const std::string& path, const std::string& suffix = "");
  std::string dirname(const std::string& path);
  std::string extname(const std::string& path);
  
}
}
