﻿//
//  StringHelper.h
//
//

#pragma once

#include <string>
#include <vector>

namespace cocone {
namespace stringhelper {
    
std::string strsprintf(const char* format, ...);
std::string join(const std::vector<std::string>& arr, char delim = ',');
std::vector<std::string> split(const std::string& str, char delim = ',');
std::string format_with_commas(long val);

}
}
