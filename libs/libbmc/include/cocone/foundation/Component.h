﻿// Component.h
//
#pragma once
#include "Sharable.h"
#include <typeindex>

USING_NS_CC;

namespace cocone
{
    class ComponentContainer;
    class Component : public ZeroRefCount<Ref>
    {
    public:
        typedef std::type_index CLSID;

        virtual CLSID getCLSID()const = 0;
        virtual void inject(){}
        virtual void injected(){}
        virtual void close();

        ComponentContainer* getParent()const{
            return _parent;
        }
        void setParent(ComponentContainer* p);

        template<typename CTy> inline CTy* queryComponent(){
            return static_cast<CTy*>(_queryComponent(typeid(CTy)));
        }
        virtual Component* _queryComponent(CLSID iid);

        virtual void isolated();
    protected:
        Component();
        virtual ~Component();

    private:
        ComponentContainer* _parent;
    };
}
